# React Material Components for the web

[![pipeline status](https://gitlab.com/lcruzc/material-toolbox/badges/master/pipeline.svg)](https://gitlab.com/lcruzc/material-toolbox/commits/master)
[![coverage report status](https://gitlab.com/lcruzc/material-toolbox/badges/master/coverage.svg)](https://lcruzc.gitlab.io/material-toolbox/coverage/lcov-report/)

This is the [React](https://facebook.github.io/react/) wrappers of [Material Components](https://github.com/material-components/material-components-web).

[Read the Documentation](https://lcruzc.gitlab.io/material-toolbox)


Note.- Most of examples, design and content are from:

* https://material.io/components/web/catalog/
* https://material-components-web.appspot.com
* https://react-mdc.github.io/
* https://jamesmfriedman.github.io/

If some credits is missed please send a PR or create an issue to add.
