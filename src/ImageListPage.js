// @flow strict
import React, { useEffect } from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import Text from './material-toolbox/typography/Text';

import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';

import ImageListNormal from './image-list-examples/Normal';
import ImageListMasonry from './image-list-examples/Masonry';

const resources = [
  {
    href: 'https://material.io/go/design-image-list',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href: 'https://material.io/components/web/catalog/image-lists/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const ImageListPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Image List</h1>

      <Text as="p" text="overline">
        Image Lists components are React wrappers of mdc-image-list components.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>ImageList</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>masonry</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>For masonry style</td>
          </tr>
          <tr>
            <td>textProtection</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Sets styles for text</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>ImageListItem</ComponentTitle>

      <ComponentTitle>ImageListImage</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>src</td>
            <td>string</td>
            <td />
            <td>✔</td>
            <td>Image path</td>
          </tr>
          <tr>
            <td>alt</td>
            <td>string</td>
            <td />
            <td>✔</td>
            <td>Alt prop for img</td>
          </tr>
          <tr>
            <td>asDiv</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Use div element instead of img</td>
          </tr>
          <tr>
            <td>withContainer</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Use aspect container when images do not have same size.</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>ImageListText</ComponentTitle>

      <SectionTitle>Demos</SectionTitle>

      <ImageListNormal />

      <ImageListMasonry />
    </LayoutCell>
  );
};

export default ImageListPage;
