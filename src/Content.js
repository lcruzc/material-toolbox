// @flow strict
import React from 'react';
import Loadable from 'react-loadable';  // eslint-disable-line
import { Route } from 'react-router-dom';  // eslint-disable-line
import LoadingC from 'react-loading-components';  // eslint-disable-line

import LayoutGrid from './material-toolbox/layout-grid/Grid';
import LayoutCell from './material-toolbox/layout-grid/Cell';
import LayoutInner from './material-toolbox/layout-grid/Inner';

import Hero from './Hero';
import HomePage from './HomePage';

const loading = (
  <LayoutCell align="middle" className="center" desktop={12}>
    <LoadingC type="puff" width={100} height={100} fill="#f44242" />
  </LayoutCell>
);

const AnimationPage = React.lazy(() => import('./AnimationPage'));
const ButtonPage = React.lazy(() => import('./ButtonPage'));
const CardPage = React.lazy(() => import('./CardPage'));
const CheckboxesPage = React.lazy(() => import('./CheckboxesPage'));
const ChipsPage = React.lazy(() => import('./ChipsPage'));
const DataTablesPage = React.lazy(() => import('./DataTablesPage'));
const DialogPage = React.lazy(() => import('./DialogPage'));
const DrawerPage = React.lazy(() => import('./DrawerPage'));
const ElevationsPage = React.lazy(() => import('./ElevationPage'));
const FABPage = React.lazy(() => import('./FABPage'));
const FormFieldsPage = React.lazy(() => import('./FormFieldsPage'));
const IconButtonsPage = React.lazy(() => import('./IconButtonsPage'));
const ImageListPage = React.lazy(() => import('./ImageListPage'));
const LayoutGridPage = React.lazy(() => import('./LayoutGridPage'));
const LinearProgressPage = React.lazy(() => import('./LinearProgressPage'));
const ListsPage = React.lazy(() => import('./ListsPage'));
const MenusPage = React.lazy(() => import('./MenuPage'));
const RTLPage = React.lazy(() => import('./RTLPage'));
const RadioButtonsPage = React.lazy(() => import('./RadioButtonPage'));
const RipplePage = React.lazy(() => import('./RipplesPage'));
const SelectsPage = React.lazy(() => import('./SelectPage'));
const SlidersPage = React.lazy(() => import('./SliderPage'));
const SnackbarsPage = React.lazy(() => import('./SnackbarPage'));
const SwitchesPage = React.lazy(() => import('./SwitchPage'));
const TabsPage = React.lazy(() => import('./TabsPage'));
const TextFieldsPage = React.lazy(() => import('./TextFieldPage'));
const ThemePage = React.lazy(() => import('./ThemePage'));
const TopAppBarPage = React.lazy(() => import('./TopAppBarPage'));
const TypographyPage = React.lazy(() => import('./TypographyPage'));

const Content = () => (
  <>
    <Route exact path="/" component={Hero} />
    <LayoutGrid fixed>
      <LayoutInner>
        <React.Suspense fallback={loading}>
          <Route exact path="/" component={HomePage} />
          <Route exact path="/animation" component={AnimationPage} />
          <Route exact path="/buttons" component={ButtonPage} />
          <Route exact path="/cards" component={CardPage} />
          <Route exact path="/checkboxes" component={CheckboxesPage} />
          <Route exact path="/chips" component={ChipsPage} />
          <Route exact path="/data-tables" component={DataTablesPage} />
          <Route exact path="/dialogs" component={DialogPage} />
          <Route exact path="/drawers" component={DrawerPage} />
          <Route exact path="/elevations" component={ElevationsPage} />
          <Route exact path="/fabs" component={FABPage} />
          <Route exact path="/form-fields" component={FormFieldsPage} />
          <Route exact path="/icon-buttons" component={IconButtonsPage} />
          <Route exact path="/image-lists" component={ImageListPage} />
          <Route exact path="/layout-grids" component={LayoutGridPage} />
          <Route
            exact
            path="/linear-progress-indicators"
            component={LinearProgressPage}
          />
          <Route exact path="/lists" component={ListsPage} />
          <Route exact path="/menus" component={MenusPage} />
          <Route exact path="/radio-buttons" component={RadioButtonsPage} />
          <Route exact path="/ripple" component={RipplePage} />
          <Route exact path="/rtl" component={RTLPage} />
          <Route exact path="/selects" component={SelectsPage} />
          <Route exact path="/sliders" component={SlidersPage} />
          <Route exact path="/snackbars" component={SnackbarsPage} />
          <Route exact path="/switches" component={SwitchesPage} />
          <Route exact path="/tabs" component={TabsPage} />
          <Route exact path="/text-fields" component={TextFieldsPage} />
          <Route exact path="/theme" component={ThemePage} />
          <Route exact path="/top-app-bars" component={TopAppBarPage} />
          <Route exact path="/typography" component={TypographyPage} />
        </React.Suspense>
      </LayoutInner>
    </LayoutGrid>
  </>
);

export default Content;
