// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import FluidContainer from '../FluidContainer';

afterEach(cleanup);

describe('example::FluidContainer', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<FluidContainer />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
