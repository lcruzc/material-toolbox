// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import FixedContainer from '../FixedContainer';

afterEach(cleanup);

describe('example::FixedContainer', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<FixedContainer />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
