// @flow strict
import React, { useState, useCallback } from 'react';

import Example from '../Example';

import LayoutCell from '../material-toolbox/layout-grid/Cell';
import LayoutGrid from '../material-toolbox/layout-grid/Grid';
import LayoutInner from '../material-toolbox/layout-grid/Inner';
import Text from '../material-toolbox/typography/Text';

const source = `import React, { useState, useCallback } from 'react';

import LayoutCell from 'material-toolbox/layout-grid/Cell';
import LayoutGrid from 'material-toolbox/layout-grid/Grid';
import LayoutInner from 'material-toolbox/layout-grid/Inner';
import Text from 'material-toolbox/typography/Text';

const FluidContainer = () => {
  const [state, setState] = useState({
    desktopMargin: '24px',
    desktopGutter: '24px',
    tabletMargin: '16px',
    tabletGutter: '16px',
    phoneMargin: '16px',
    phoneGutter: '16px',
  });

  const handleChange = useCallback((evt) => {
    const { name, value } = evt.currentTarget;
    setState(s => ({
      ...s,
      [name]: value,
    }));
  }, []);

  return (
    <section>
      <LayoutInner>
        <LayoutCell>
          <div className="cell-spacing-selector">
            Desktop Margin:{' '}
            <select
              name="desktopMargin"
              onChange={handleChange}
              value={state.desktopMargin}>
              <option value="8px">8px</option>
              <option value="16px">16px</option>
              <option value="24px">24px</option>
              <option value="40px">40px</option>
            </select>
            <div className="cell-spacing-selector">
              Desktop gutter:{' '}
              <select
                name="desktopGutter"
                onChange={handleChange}
                value={state.desktopGutter}>
                <option value="8px">8px</option>
                <option value="16px">16px</option>
                <option value="24px">24px</option>
                <option value="40px">40px</option>
              </select>
            </div>
          </div>
        </LayoutCell>
        <LayoutCell>
          <div className="cell-spacing-selector">
            Tablet Margin:{' '}
            <select
              name="tabletMargin"
              onChange={handleChange}
              value={state.tabletMargin}>
              <option value="8px">8px</option>
              <option value="16px">16px</option>
              <option value="24px">24px</option>
              <option value="40px">40px</option>
            </select>
            <div className="cell-spacing-selector">
              Tablet gutter:{' '}
              <select
                name="tabletGutter"
                onChange={handleChange}
                value={state.tabletGutter}>
                <option value="8px">8px</option>
                <option value="16px">16px</option>
                <option value="24px">24px</option>
                <option value="40px">40px</option>
              </select>
            </div>
          </div>
        </LayoutCell>
        <LayoutCell>
          <div className="cell-spacing-selector">
            Phone Margin:{' '}
            <select
              name="phoneMargin"
              onChange={handleChange}
              value={state.phoneMargin}>
              <option value="8px">8px</option>
              <option value="16px">16px</option>
              <option value="24px">24px</option>
              <option value="40px">40px</option>
            </select>
            <div className="cell-spacing-selector">
              Phone gutter:{' '}
              <select
                name="phoneGutter"
                onChange={handleChange}
                value={state.phoneGutter}>
                <option value="8px">8px</option>
                <option value="16px">16px</option>
                <option value="24px">24px</option>
                <option value="40px">40px</option>
              </select>
            </div>
          </div>
        </LayoutCell>
      </LayoutInner>

      <div
        style={{
          '--mdc-layout-grid-margin-desktop': state.desktopMargin,
          '--mdc-layout-grid-gutter-desktop': state.desktopGutter,
          '--mdc-layout-grid-margin-tablet': state.tabletMargin,
          '--mdc-layout-grid-gutter-tablet': state.tabletGutter,
          '--mdc-layout-grid-margin-phone': state.phoneMargin,
          '--mdc-layout-grid-gutter-phone': state.phoneGutter,
        }}>
        <Text as="h5" text="subtitle1">
          Grid of default wide (4 columns) items
        </Text>

        <LayoutGrid className="demo-grid">
          <LayoutInner>
            <LayoutCell className="demo-cell">4</LayoutCell>
            <LayoutCell className="demo-cell">4</LayoutCell>
            <LayoutCell className="demo-cell">4</LayoutCell>
          </LayoutInner>
        </LayoutGrid>

        <Text as="h5" text="subtitle1">
          Grid of 1 column wide items
        </Text>

        <LayoutGrid className="demo-grid">
          <LayoutInner>
            {[...Array.from(Array(12).keys())].map(x => (
              <LayoutCell className="demo-cell" span={1} key={x}>
                1
              </LayoutCell>
            ))}
          </LayoutInner>
        </LayoutGrid>

        <Text as="h5" text="subtitle1">
          Grid of differently sized items
        </Text>

        <LayoutGrid className="demo-grid">
          <LayoutInner>
            <LayoutCell className="demo-cell" span={6}>
              6
            </LayoutCell>
            <LayoutCell className="demo-cell" span={4}>
              4
            </LayoutCell>
            <LayoutCell className="demo-cell" span={2}>
              2
            </LayoutCell>
          </LayoutInner>
        </LayoutGrid>

        <Text as="h5" text="subtitle1">
          Grid of items with tweaks at different screen sizes
        </Text>

        <LayoutGrid className="demo-grid">
          <LayoutInner>
            <LayoutCell className="demo-cell" span={6} tablet={8}>
              6 (8 tablet)
            </LayoutCell>
            <LayoutCell className="demo-cell" span={4} tablet={6}>
              4 (6 tablet)
            </LayoutCell>
            <LayoutCell className="demo-cell" span={2} tablet={4}>
              2 (4 phone)
            </LayoutCell>
          </LayoutInner>
        </LayoutGrid>

        <Text as="h5" text="subtitle1">
          Grid nested within parent grid cell
        </Text>

        <LayoutGrid className="demo-grid">
          <LayoutInner>
            <LayoutCell className="demo-parent-cell" span={4}>
              <LayoutInner>
                <LayoutCell className="demo-cell demo-child-cell" span={4}>
                  <span>C4</span>
                </LayoutCell>
                <LayoutCell className="demo-cell demo-child-cell" span={4}>
                  <span>C4</span>
                </LayoutCell>
                <LayoutCell className="demo-cell demo-child-cell" span={4}>
                  <span>C4</span>
                </LayoutCell>
              </LayoutInner>
              <span>Parent 4</span>
            </LayoutCell>
            <LayoutCell className="demo-cell" span={4}>
              4
            </LayoutCell>
            <LayoutCell className="demo-cell" span={4}>
              4
            </LayoutCell>
          </LayoutInner>
        </LayoutGrid>

        <Text as="h4" text="headline6">
          Grid with max width
        </Text>

        <Text as="h5" text="subtitle1">
          Grid with max width (1280px) and center alignment by default
        </Text>

        <LayoutGrid className="demo-grid max-width">
          <LayoutInner>
            <LayoutCell className="demo-cell">4</LayoutCell>
            <LayoutCell className="demo-cell">4</LayoutCell>
            <LayoutCell className="demo-cell">4</LayoutCell>
          </LayoutInner>
        </LayoutGrid>

        <Text as="h5" text="subtitle1">
          Grid with max width (1280px) and left alignment
        </Text>

        <LayoutGrid className="demo-grid max-width" align="left">
          <LayoutInner>
            <LayoutCell className="demo-cell">4</LayoutCell>
            <LayoutCell className="demo-cell">4</LayoutCell>
            <LayoutCell className="demo-cell">4</LayoutCell>
          </LayoutInner>
        </LayoutGrid>
      </div>
    </section>
  );
};

export default FluidContainer;
`;

const FluidContainer = () => {
  const [state, setState] = useState({
    desktopMargin: '24px',
    desktopGutter: '24px',
    tabletMargin: '16px',
    tabletGutter: '16px',
    phoneMargin: '16px',
    phoneGutter: '16px',
  });

  const handleChange = useCallback((evt: SyntheticEvent<HTMLSelectElement>) => {
    const { name, value } = evt.currentTarget;
    setState(s => ({
      ...s,
      [name]: value,
    }));
  }, []);

  return (
    <section>
      <Example title="Layout grid (in fluid container)" source={source}>
        <LayoutInner>
          <LayoutCell>
            <div className="cell-spacing-selector">
              Desktop Margin:
              <select
                name="desktopMargin"
                onChange={handleChange}
                value={state.desktopMargin}
              >
                <option value="8px">8px</option>
                <option value="16px">16px</option>
                <option value="24px">24px</option>
                <option value="40px">40px</option>
              </select>
              <div className="cell-spacing-selector">
                Desktop gutter:
                <select
                  name="desktopGutter"
                  onChange={handleChange}
                  value={state.desktopGutter}
                >
                  <option value="8px">8px</option>
                  <option value="16px">16px</option>
                  <option value="24px">24px</option>
                  <option value="40px">40px</option>
                </select>
              </div>
            </div>
          </LayoutCell>
          <LayoutCell>
            <div className="cell-spacing-selector">
              Tablet Margin:
              <select
                name="tabletMargin"
                onChange={handleChange}
                value={state.tabletMargin}
              >
                <option value="8px">8px</option>
                <option value="16px">16px</option>
                <option value="24px">24px</option>
                <option value="40px">40px</option>
              </select>
              <div className="cell-spacing-selector">
                Tablet gutter:
                <select
                  name="tabletGutter"
                  onChange={handleChange}
                  value={state.tabletGutter}
                >
                  <option value="8px">8px</option>
                  <option value="16px">16px</option>
                  <option value="24px">24px</option>
                  <option value="40px">40px</option>
                </select>
              </div>
            </div>
          </LayoutCell>
          <LayoutCell>
            <div className="cell-spacing-selector">
              Phone Margin:
              <select
                name="phoneMargin"
                onChange={handleChange}
                value={state.phoneMargin}
              >
                <option value="8px">8px</option>
                <option value="16px">16px</option>
                <option value="24px">24px</option>
                <option value="40px">40px</option>
              </select>
              <div className="cell-spacing-selector">
                Phone gutter:
                <select
                  name="phoneGutter"
                  onChange={handleChange}
                  value={state.phoneGutter}
                >
                  <option value="8px">8px</option>
                  <option value="16px">16px</option>
                  <option value="24px">24px</option>
                  <option value="40px">40px</option>
                </select>
              </div>
            </div>
          </LayoutCell>
        </LayoutInner>

        <div
          style={{
            '--mdc-layout-grid-margin-desktop': state.desktopMargin,
            '--mdc-layout-grid-gutter-desktop': state.desktopGutter,
            '--mdc-layout-grid-margin-tablet': state.tabletMargin,
            '--mdc-layout-grid-gutter-tablet': state.tabletGutter,
            '--mdc-layout-grid-margin-phone': state.phoneMargin,
            '--mdc-layout-grid-gutter-phone': state.phoneGutter,
          }}
        >
          <Text as="h5" text="subtitle1">
            Grid of default wide (4 columns) items
          </Text>

          <LayoutGrid className="demo-grid">
            <LayoutInner>
              <LayoutCell className="demo-cell">4</LayoutCell>
              <LayoutCell className="demo-cell">4</LayoutCell>
              <LayoutCell className="demo-cell">4</LayoutCell>
            </LayoutInner>
          </LayoutGrid>

          <Text as="h5" text="subtitle1">
            Grid of 1 column wide items
          </Text>

          <LayoutGrid className="demo-grid">
            <LayoutInner>
              {[...Array.from(Array(12).keys())].map(x => (
                <LayoutCell className="demo-cell" span={1} key={x}>
                  1
                </LayoutCell>
              ))}
            </LayoutInner>
          </LayoutGrid>

          <Text as="h5" text="subtitle1">
            Grid of differently sized items
          </Text>

          <LayoutGrid className="demo-grid">
            <LayoutInner>
              <LayoutCell className="demo-cell" span={6}>
                6
              </LayoutCell>
              <LayoutCell className="demo-cell" span={4}>
                4
              </LayoutCell>
              <LayoutCell className="demo-cell" span={2}>
                2
              </LayoutCell>
            </LayoutInner>
          </LayoutGrid>

          <Text as="h5" text="subtitle1">
            Grid of items with tweaks at different screen sizes
          </Text>

          <LayoutGrid className="demo-grid">
            <LayoutInner>
              <LayoutCell className="demo-cell" span={6} tablet={8}>
                6 (8 tablet)
              </LayoutCell>
              <LayoutCell className="demo-cell" span={4} tablet={6}>
                4 (6 tablet)
              </LayoutCell>
              <LayoutCell className="demo-cell" span={2} tablet={4}>
                2 (4 phone)
              </LayoutCell>
            </LayoutInner>
          </LayoutGrid>

          <Text as="h5" text="subtitle1">
            Grid nested within parent grid cell
          </Text>

          <LayoutGrid className="demo-grid">
            <LayoutInner>
              <LayoutCell className="demo-parent-cell" span={4}>
                <LayoutInner>
                  <LayoutCell className="demo-cell demo-child-cell" span={4}>
                    <span>C4</span>
                  </LayoutCell>
                  <LayoutCell className="demo-cell demo-child-cell" span={4}>
                    <span>C4</span>
                  </LayoutCell>
                  <LayoutCell className="demo-cell demo-child-cell" span={4}>
                    <span>C4</span>
                  </LayoutCell>
                </LayoutInner>
                <span>Parent 4</span>
              </LayoutCell>
              <LayoutCell className="demo-cell" span={4}>
                4
              </LayoutCell>
              <LayoutCell className="demo-cell" span={4}>
                4
              </LayoutCell>
            </LayoutInner>
          </LayoutGrid>

          <Text as="h4" text="headline6">
            Grid with max width
          </Text>

          <Text as="h5" text="subtitle1">
            Grid with max width (1280px) and center alignment by default
          </Text>

          <LayoutGrid className="demo-grid max-width">
            <LayoutInner>
              <LayoutCell className="demo-cell">4</LayoutCell>
              <LayoutCell className="demo-cell">4</LayoutCell>
              <LayoutCell className="demo-cell">4</LayoutCell>
            </LayoutInner>
          </LayoutGrid>

          <Text as="h5" text="subtitle1">
            Grid with max width (1280px) and left alignment
          </Text>

          <LayoutGrid className="demo-grid max-width" align="left">
            <LayoutInner>
              <LayoutCell className="demo-cell">4</LayoutCell>
              <LayoutCell className="demo-cell">4</LayoutCell>
              <LayoutCell className="demo-cell">4</LayoutCell>
            </LayoutInner>
          </LayoutGrid>
        </div>
      </Example>
    </section>
  );
};

export default FluidContainer;
