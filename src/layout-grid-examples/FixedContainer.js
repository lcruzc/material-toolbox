// @flow strict
import React, { useCallback, useState } from 'react';

import Example from '../Example';

import LayoutCell from '../material-toolbox/layout-grid/Cell';
import LayoutGrid from '../material-toolbox/layout-grid/Grid';
import LayoutInner from '../material-toolbox/layout-grid/Inner';
import Text from '../material-toolbox/typography/Text';

const source = `
`;

const FixedContainer = () => {
  const [state, setState] = useState({
    desktopMargin: '24px',
    desktopGutter: '24px',
    desktopColumnWidth: '72px',
    tabletMargin: '16px',
    tabletGutter: '16px',
    tabletColumnWidth: '72px',
    phoneMargin: '16px',
    phoneGutter: '16px',
    phoneColumnWidth: '72px',
  });

  const handleChange = useCallback((evt: SyntheticEvent<HTMLSelectElement>) => {
    const { name, value } = evt.currentTarget;
    setState(s => ({
      ...s,
      [name]: value,
    }));
  }, []);

  return (
    <section>
      <Example title="Fixed column width layout grid" source={source}>
        <LayoutInner>
          <LayoutCell>
            <div className="cell-spacing-selector">
              Desktop Margin:
              <select
                name="desktopMargin"
                onChange={handleChange}
                value={state.desktopMargin}
              >
                <option value="8px">8px</option>
                <option value="16px">16px</option>
                <option value="24px">24px</option>
                <option value="40px">40px</option>
              </select>
              <div className="cell-spacing-selector">
                Desktop gutter:
                <select
                  name="desktopGutter"
                  onChange={handleChange}
                  value={state.desktopGutter}
                >
                  <option value="8px">8px</option>
                  <option value="16px">16px</option>
                  <option value="24px">24px</option>
                  <option value="40px">40px</option>
                </select>
              </div>
              <div className="cell-spacing-selector">
                Desktop Column Width:
                <select
                  name="desktopColumnWidth"
                  onChange={handleChange}
                  value={state.desktopColumnWidth}
                >
                  <option value="72px">72px</option>
                  <option value="84px">84px</option>
                </select>
              </div>
            </div>
          </LayoutCell>
          <LayoutCell>
            <div className="cell-spacing-selector">
              Tablet Margin:
              <select
                name="tabletMargin"
                onChange={handleChange}
                value={state.tabletMargin}
              >
                <option value="8px">8px</option>
                <option value="16px">16px</option>
                <option value="24px">24px</option>
                <option value="40px">40px</option>
              </select>
              <div className="cell-spacing-selector">
                Tablet gutter:
                <select
                  name="tabletGutter"
                  onChange={handleChange}
                  value={state.tabletGutter}
                >
                  <option value="8px">8px</option>
                  <option value="16px">16px</option>
                  <option value="24px">24px</option>
                  <option value="40px">40px</option>
                </select>
              </div>
              <div className="cell-spacing-selector">
                Tablet Column Width:
                <select
                  name="tabletColumnWidth"
                  onChange={handleChange}
                  value={state.tabletColumnWidth}
                >
                  <option value="72px">72px</option>
                  <option value="84px">84px</option>
                </select>
              </div>
            </div>
          </LayoutCell>
          <LayoutCell>
            <div className="cell-spacing-selector">
              Phone Margin:
              <select
                name="phoneMargin"
                onChange={handleChange}
                value={state.phoneMargin}
              >
                <option value="8px">8px</option>
                <option value="16px">16px</option>
                <option value="24px">24px</option>
                <option value="40px">40px</option>
              </select>
              <div className="cell-spacing-selector">
                Phone gutter:
                <select
                  name="phoneGutter"
                  onChange={handleChange}
                  value={state.phoneGutter}
                >
                  <option value="8px">8px</option>
                  <option value="16px">16px</option>
                  <option value="24px">24px</option>
                  <option value="40px">40px</option>
                </select>
              </div>
              <div className="cell-spacing-selector">
                Phone Column Width:
                <select
                  name="phoneColumnWidth"
                  onChange={handleChange}
                  value={state.phoneColumnWidth}
                >
                  <option value="72px">72px</option>
                  <option value="84px">84px</option>
                </select>
              </div>
            </div>
          </LayoutCell>
        </LayoutInner>

        <div
          style={{
            '--mdc-layout-grid-margin-desktop': state.desktopMargin,
            '--mdc-layout-grid-gutter-desktop': state.desktopGutter,
            '--mdc-layout-grid-column-width-desktop': state.desktopColumnWidth,
            '--mdc-layout-grid-margin-tablet': state.tabletMargin,
            '--mdc-layout-grid-gutter-tablet': state.tabletGutter,
            '--mdc-layout-grid-column-width-tablet': state.tabletColumnWidth,
            '--mdc-layout-grid-margin-phone': state.phoneMargin,
            '--mdc-layout-grid-gutter-phone': state.phoneGutter,
            '--mdc-layout-grid-column-width-phone': state.phoneColumnWidth,
          }}
        >
          <Text as="h5" text="subtitle1">
            Fixed column width layout grid and center alignment by default
          </Text>

          <LayoutGrid className="demo-grid" fixed>
            <LayoutInner>
              <LayoutCell className="demo-cell" span={1} />
              <LayoutCell className="demo-cell" span={1} />
              <LayoutCell className="demo-cell" span={1} />
            </LayoutInner>
          </LayoutGrid>

          <Text as="h5" text="subtitle1">
            Fixed column width layout grid and right alignment
          </Text>

          <LayoutGrid className="demo-grid" align="right" fixed>
            <LayoutInner>
              <LayoutCell className="demo-cell" span={1} />
              <LayoutCell className="demo-cell" span={1} />
              <LayoutCell className="demo-cell" span={1} />
            </LayoutInner>
          </LayoutGrid>
        </div>
      </Example>
    </section>
  );
};

export default FixedContainer;
