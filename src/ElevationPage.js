// @flow strict
import React, { useState, useCallback, useEffect } from 'react';

import Elevation from './material-toolbox/elevation';
import Text from './material-toolbox/typography/Text';
import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';
import LayoutCell from './material-toolbox/layout-grid/Cell';

import Highlighter from './Highlighter';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';
import Example from './Example';

const scssSource = `.elevations-demo-surfaces {
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  padding-top: 48px;

  .elevation-demo-surface {
    align-items: center;
    display: inline-flex;
    justify-content: space-around;
    margin: 15px;
    min-height: 100px;
    min-width: 187px;
  }
}

#elevation-demo-hover-el {
  align-items: center;
  border-radius: 4px;
  display: flex;
  justify-content: center;
  margin: 45px 15px 15px;
  padding: 2rem;
}
`;

const source = `import React, { useState, useCallback, useEffect } from 'react';

import Elevation from 'material-toolbox/elevation';

const elevations = Array(25)
  .fill(1)
  .map((_, index) => index);

const ElevationPage = () => {
  const [mouseOver, setMouseOver] = useState(false);
  const handleMouseEnter = useCallback(() => setMouseOver(true));
  const handleMouseLeave = useCallback(() => setMouseOver(false));

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <React.Fragment>
      <section className="elevations-demo-surfaces">
        {elevations.map(depth => (
          <Elevation depth={depth} key={depth}>
            <figure className="elevation-demo-surface">
              <figcaption>{depth} dp</figcaption>
            </figure>
          </Elevation>
        ))}
      </section>
      <section>
        <Elevation transition depth={mouseOver ? 8 : 2}>
          <div
            id="elevation-demo-hover-el"
            onMouseEnter={handleMouseEnter}
            onMouseLeave={handleMouseLeave}>
            <p>Hover over for a transition</p>
          </div>
        </Elevation>
      </section>
    </React.Fragment>
  );
};

export default ElevationPage;`;

const elevations = Array(25)
  .fill(1)
  .map((_, index) => index);

const resources = [
  {
    href: 'https://material.io/go/design-elevation',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href: 'https://material.io/components/web/catalog/elevation/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const ElevationPage = () => {
  const [mouseOver, setMouseOver] = useState<boolean>(false);
  const handleMouseEnter = useCallback(() => setMouseOver(true), []);
  const handleMouseLeave = useCallback(() => setMouseOver(false), []);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Elevation</h1>

      <Text as="p" text="overline">
        Elevation component, hook and HOC are React wrappers of mdc-elevation
        component.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>useElevation</ComponentTitle>

      <section>
        Hook used to get the correct className for elevation.
        <Highlighter language="jsx">
          {
            'useElevation(depth: number = 0, transition: boolean = false) => string'
          }
        </Highlighter>
      </section>

      <ComponentTitle>withElevation</ComponentTitle>

      <section>
        HOC to inject the class to the component.
        <Highlighter language="jsx">
          {`withElevation<Config: { className?: string }, Instance>(
  Component: AbstractComponent<Config, Instance>,
  { depth, transition }: { depth?: number, transition?: boolean } = {}
 ) => AbstractComponent<Config, Instance>`}
        </Highlighter>
      </section>

      <ComponentTitle>Elevation</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>depth</td>
            <td>number: 0-24</td>
            <td />
            <td>✔</td>
            <td>dp elevation</td>
          </tr>
          <tr>
            <td>transition</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>enable transition effect</td>
          </tr>
        </tbody>
      </table>

      <SectionTitle>Demos</SectionTitle>

      <Example source={source} title="Elevations" scss={scssSource}>
        <section className="elevations-demo-surfaces">
          {elevations.map(depth => (
            <Elevation depth={depth} key={depth}>
              <figure className="elevation-demo-surface">
                <figcaption>{depth} dp</figcaption>
              </figure>
            </Elevation>
          ))}
        </section>
        <section>
          <Elevation transition depth={mouseOver ? 8 : 2}>
            <div
              id="elevation-demo-hover-el"
              onMouseEnter={handleMouseEnter}
              onMouseLeave={handleMouseLeave}
            >
              <p>Hover over for a transition</p>
            </div>
          </Elevation>
        </section>
      </Example>
    </LayoutCell>
  );
};

export default ElevationPage;
