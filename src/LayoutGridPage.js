// @flow strict
import React, { useEffect } from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import Text from './material-toolbox/typography/Text';

import FluidContainer from './layout-grid-examples/FluidContainer';
import FixedContainer from './layout-grid-examples/FixedContainer';

import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';

const resources = [
  {
    href: 'https://material.io/components/web/catalog/layout-grid/',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
];

const LayoutGridPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Layout Grids</h1>

      <Text as="p" text="overline">
        Layout Grids components are React wrappers of mdc-layout-grid
        components.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>LayoutGrid</ComponentTitle>

      <p>
        This is a react wrapper for mdc-layout-grid grid. This component can
        contain any of component type but LayoutInner is the component to follow
        the guidelines.
      </p>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>align</td>
            <td>String values: left, right</td>
            <td />
            <td />
            <td>How elements will be aligned</td>
          </tr>
          <tr>
            <td>fixed</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Will be a fixed width</td>
          </tr>
        </tbody>
      </table>

      <Text as="h2" text="headline6">
        LayoutInner
      </Text>

      <p>
        This is a react wrapper for mdc-layout-grid inner. This component can
        contain any of component type but LayoutCell is the component to follow
        the guidelines.
      </p>

      <Text as="h2" text="headline6">
        LayoutCell
      </Text>

      <p>
        This is a react wrapper for mdc-layout-grid cell. It can contain any
        React component.
      </p>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>align</td>
            <td>String values: top, middle, bottom</td>
            <td />
            <td />
            <td>How cell will be vertically aligned</td>
          </tr>
          <tr>
            <td>span</td>
            <td>number values: 1-12</td>
            <td />
            <td />
            <td>how many columns will be filled in row up to parent width.</td>
          </tr>
          <tr>
            <td>order</td>
            <td>number values: 1-12</td>
            <td />
            <td />
            <td>how the elements will be ordered</td>
          </tr>
          <tr>
            <td>phone</td>
            <td>number values: 1-4</td>
            <td />
            <td />
            <td>how many columns will be filled in phone row</td>
          </tr>
          <tr>
            <td>tablet</td>
            <td>number values: 1-8</td>
            <td />
            <td />
            <td>how many columns will be filled in tablet row</td>
          </tr>
          <tr>
            <td>desktop</td>
            <td>number values: 1-12</td>
            <td />
            <td />
            <td>how many columns will be filled in desktop row</td>
          </tr>
        </tbody>
      </table>

      <SectionTitle>Demos</SectionTitle>

      <FluidContainer />
      <FixedContainer />
    </LayoutCell>
  );
};

export default LayoutGridPage;
