// @flow strict
import React from 'react';

import TabBar from '../material-toolbox/tab-bar';
import Tab from '../material-toolbox/tab';
import TabText from '../material-toolbox/tab/Text';
import TabIcon from '../material-toolbox/tab/Icon';
import TabScroller from '../material-toolbox/tab-scroller';

import Example from '../Example';

const source = `import React from 'react';

import TabBar from 'material-toolbox/tab-bar';
import Tab from 'material-toolbox/tab';
import TabText from 'material-toolbox/tab/Text';
import TabIcon from 'material-toolbox/tab/Icon';
import TabScroller from 'material-toolbox/tab-scroller';

const tabs = 'One Two Three Four Five Six Seven Eight Nine Ten Eleven Twelve'.split(' ');

const BasicTabBar = ({ second = false }) => (
  <TabBar initialActive={0}>
    <TabScroller>
      <Tab stacked={second} contentIndicator={second}>
        <TabIcon className="material-icons">access_time</TabIcon>
        <TabText>Recents</TabText>
      </Tab>
      <Tab stacked={second} contentIndicator={second}>
        <TabIcon className="material-icons">near_me</TabIcon>
        <TabText>Nearby</TabText>
      </Tab>
      <Tab stacked={second} contentIndicator={second}>
        <TabIcon className="material-icons">favorite</TabIcon>
        <TabText>Faborites</TabText>
      </Tab>
    </TabScroller>
  </TabBar>
);

const TabBars = () => (
  <div>
    <p>Tabs with icons next to labels</p>
    <BasicTabBar />

    <p>Tabs with icons above labels and indicators restricted to content</p>
    <BasicTabBar second />

    <p>Scrolling tabs</p>
    <TabBar initialActive={0} focusOnActivate>
      <TabScroller>
        {tabs.map(tab => (
          <Tab key={tab}>
            <TabText>Tab {tab}</TabText>
          </Tab>
        ))}
      </TabScroller>
    </TabBar>
  </div>
);

export default TabBars;`;

const tabs = 'One Two Three Four Five Six Seven Eight Nine Ten Eleven Twelve'.split(
  ' ',
);

const BasicTabBar = ({ second = false }: { second?: boolean }) => (
  <TabBar initialActive={0}>
    <TabScroller>
      <Tab stacked={second} contentIndicator={second}>
        <TabIcon className="material-icons">access_time</TabIcon>
        <TabText>Recents</TabText>
      </Tab>
      <Tab stacked={second} contentIndicator={second}>
        <TabIcon className="material-icons">near_me</TabIcon>
        <TabText>Nearby</TabText>
      </Tab>
      <Tab stacked={second} contentIndicator={second}>
        <TabIcon className="material-icons">favorite</TabIcon>
        <TabText>Faborites</TabText>
      </Tab>
    </TabScroller>
  </TabBar>
);

const TabBars = () => (
  <Example title="" source={source}>
    <p>Tabs with icons next to labels</p>
    <BasicTabBar />

    <p>Tabs with icons above labels and indicators restricted to content</p>
    <BasicTabBar second />

    <p>Scrolling tabs</p>
    <TabBar initialActive={0} focusOnActivate>
      <TabScroller>
        {tabs.map(tab => (
          <Tab key={tab}>
            <TabText>Tab {tab}</TabText>
          </Tab>
        ))}
      </TabScroller>
    </TabBar>
  </Example>
);

export default TabBars;
