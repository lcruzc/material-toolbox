// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TabBars from '../TabBars';

afterEach(cleanup);

describe('example::TabBars', () => {
  it('Should match snapshoot standard', () => {
    const { container } = render(<TabBars />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
