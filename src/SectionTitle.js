// @flow strict
import React from 'react';

import type { Node } from 'react';

import Text from './material-toolbox/typography/Text';

type Props = {
  children: Node,
};

const SectionTitle = ({ children }: Props) => (
  <Text className="section-title" as="h2" text="headline6">
    {children}
  </Text>
);

export default SectionTitle;
