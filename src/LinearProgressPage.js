// @flow strict
import React, { useEffect } from 'react';

import Example from './Example';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import Text from './material-toolbox/typography/Text';
import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';
import LinearProgress from './material-toolbox/linear-progress';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';

const source = `import React from 'react';

import LinearProgress from 'material-toolbox/linear-progress';

const Demo = () => {
  return (
    <div>
      <p>Buffered</p>
      <LinearProgress progress={0.5} buffer={0.75} />

      <p>Indeterminate</p>
      <LinearProgress indeterminate />

      <p>Reversed</p>
      <LinearProgress progress={0.5} reversed />

      <p>Reversed Buffered</p>
      <LinearProgress progress={0.5} buffer={0.75} reversed />
    </div>
  );
};

export default Demo;`;

const resources = [
  {
    href: 'https://material.io/go/design-progress-indicators',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href: 'https://material.io/components/web/catalog/linear-progress/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const LinearProgressPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Linear Progres Indicator</h1>

      <Text as="p" text="overline">
        Linear Progress components are React wrappers of mdc-linear-progress
        component.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>LinearProgress</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>progress</td>
            <td>number</td>
            <td />
            <td />
            <td>Number to fill from 0 to 1</td>
          </tr>
          <tr>
            <td>buffer</td>
            <td>number</td>
            <td />
            <td />
            <td>Number to fill from 0 to 1</td>
          </tr>
          <tr>
            <td>indeterminate</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>set a indeterminate progress</td>
          </tr>
          <tr>
            <td>reversed</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>set a reversed linear progress</td>
          </tr>
          <tr>
            <td>close</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>close the linear progress indicator</td>
          </tr>
        </tbody>
      </table>

      <SectionTitle>Demos</SectionTitle>

      <Example title="" source={source}>
        <p>Buffered</p>
        <LinearProgress progress={0.5} buffer={0.75} />

        <p>Indeterminate</p>
        <LinearProgress indeterminate />

        <p>Reversed</p>
        <LinearProgress progress={0.5} reversed />

        <p>Reversed Buffered</p>
        <LinearProgress progress={0.5} buffer={0.75} reversed />
      </Example>
    </LayoutCell>
  );
};

export default LinearProgressPage;
