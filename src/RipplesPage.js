// @flow strict
import React, { useEffect } from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import Text from './material-toolbox/typography/Text';

import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';

import BoundedRipple from './ripple-examples/Bounded';
import UnboundedRipple from './ripple-examples/Unbounded';
import ThemedRipple from './ripple-examples/Themed';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';
import Highlighter from './Highlighter';

const resources = [
  {
    href: 'https://material.io/go/design-states',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href: 'https://material.io/components/web/catalog/ripples/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const RipplesPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Ripple</h1>

      <Text as="p" text="overline">
        Ripple HOC and hook are React wrappers of mdc-ripple component.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>withRipple</ComponentTitle>

      <section>
        HOC that enable ripple in your component
        <Highlighter language="jsx">{`withRipple<+A: HTMLElement, +S: HTMLElement, Config, Instance>(
  Component: AbstractComponent<
    {
      ...{|
        rippleClassName: string,
        rippleStyle: Style,
        activator: {| current: ?A |},
        surface?: {| current: ?S |},
      |},
      ...Config,
    },
    Instance,
  >,
  {
    unbounded,
    primary,
    accent,
    disabled,
  }: {|
    unbounded?: boolean,
    primary?: boolean,
    accent?: boolean,
    disabled?: boolean,
  |} = {
    unbounded: false,
    primary: false,
    accent: false,
    disabled: false,
  },
) => AbstractComponent<Config, Instance>`}</Highlighter>
      </section>

      <ComponentTitle>useRipple</ComponentTitle>

      <section>
        Hook that enable your component with ripple
        <Highlighter language="jsx">{`useRipple<+A: HTMLElement, +S: HTMLElement>(
  {
    surface,
    activator,
    disabled = false,
    unbounded = false,
    primary = false,
    accent = false,
  }: {
    activator: {| current: ?A |},
    surface?: {| current: ?S |},
    disabled?: boolean,
    unbounded?: boolean,
    primary?: boolean,
    accent?: boolean,
  },
  partialAdapter: mixed, // should be memoized object or change a few to avoid recursion
) => {
  rippleClassName: Set<string>,
  rippleStyle: { [string]: string | number | boolean },
  ripple: MDCRippleFoundation,
}`}</Highlighter>
      </section>

      <SectionTitle>Demos</SectionTitle>

      <BoundedRipple />
      <UnboundedRipple />
      <ThemedRipple />
    </LayoutCell>
  );
};

export default RipplesPage;
