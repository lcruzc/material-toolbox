// @flow strict
import React, { useEffect } from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';
import Text from './material-toolbox/typography/Text';

import WithImage from './card-examples/WithImage';
import WithActions from './card-examples/WithActions';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';

const resources = [
  {
    href: 'https://material.io/go/design-cards',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href: 'https://material.io/components/web/catalog/cards/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const CardPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Card</h1>

      <Text as="p" text="overline">
        Card components are React wrappers of mdc-cards components.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>Card</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>as</td>
            <td>React.ElementType</td>
            <td>div</td>
            <td />
            <td>Component type that will be rendered.</td>
          </tr>
          <tr>
            <td>outlined</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set oulined style</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>CardActions</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>fullBleed</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>
              To have a single action button take up the entire width of the
              action row
            </td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>CardMedia</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>square</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>
              Automatically scales the media area’s height to equal its width
            </td>
          </tr>
          <tr>
            <td>ratio169</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>
              Automatically scales the media area’s height according to its
              width, maintaining a 16:9 aspect ratio
            </td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>CardAction</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>icon</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set actions for icons</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>CardActionIcons</ComponentTitle>

      <ComponentTitle>CardActionButtons</ComponentTitle>

      <ComponentTitle>CardMediaContent</ComponentTitle>

      <ComponentTitle>CardPrimaryAction</ComponentTitle>

      <SectionTitle>Demos</SectionTitle>

      <WithImage />
      <WithActions />
    </LayoutCell>
  );
};

export default CardPage;
