// @flow strict
import React, { useCallback, useState } from 'react';

import FormField from '../material-toolbox/form-field';
import Checkbox from '../material-toolbox/checkbox';
import Example from '../Example';

const source = `import React, { useCallback, useState } from 'react';

import FormField from 'material-toolbox/form-field';
import Checkbox from 'material-toolbox/checkbox';

const CheckboxExamples = () => {
  const [isChecked, setCheck] = useState(true);

  const handleChange = useCallback(() => setCheck(!isChecked), [isChecked]);

  return (
    <div style={{ display: 'inline-block' }}>
      <div>
        <FormField label="Unchecked" htmlFor="unchecked">
          <Checkbox id="unchecked" />
        </FormField>
      </div>

      <div>
        <FormField label="Indeterminate" htmlFor="indeterminate">
          <Checkbox id="indeterminate" indeterminate />
        </FormField>
      </div>

      <div>
        <FormField label="Checked" htmlFor="checked">
          <Checkbox
            id="checked"
            checked={isChecked}
            onChange={handleChange}
          />
        </FormField>
      </div>
    </div>
  );
};

export default CheckboxExamples;`;

const CheckboxExamples = () => {
  const [isChecked, setCheck] = useState<boolean>(true);

  const handleChange = useCallback(() => setCheck(!isChecked), [isChecked]);

  return (
    <Example title="" source={source}>
      <div style={{ display: 'inline-block' }}>
        <div>
          <FormField label="Unchecked" htmlFor="unchecked">
            <Checkbox id="unchecked" />
          </FormField>
        </div>

        <div>
          <FormField label="Indeterminate" htmlFor="indeterminate">
            <Checkbox id="indeterminate" indeterminate />
          </FormField>
        </div>

        <div>
          <FormField label="Checked" htmlFor="checked">
            <Checkbox
              id="checked"
              checked={isChecked}
              onChange={handleChange}
            />
          </FormField>
        </div>
      </div>
    </Example>
  );
};

export default CheckboxExamples;
