// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Examples from '../Examples';

afterEach(cleanup);

describe('examples::Examples', () => {
  it('Should match snapshoot standard', () => {
    const { container } = render(<Examples />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
