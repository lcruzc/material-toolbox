// @flow strict
import React from 'react';

import Theme from './material-toolbox/theme';
import LayoutGrid from './material-toolbox/layout-grid/Grid';
import LayoutInner from './material-toolbox/layout-grid/Inner';
import LayoutCell from './material-toolbox/layout-grid/Cell';
import Text from './material-toolbox/typography/Text';

const Hero = () => (
  <Theme background="primary" color="on-primary">
    <header>
      <LayoutGrid fixed>
        <LayoutInner>
          <LayoutCell align="bottom" span={12}>
            <Text text="headline3" as="h1">
              React Material Components for the Web
            </Text>
            <Text text="headline5" as="span">
              Create beautiful apps with modular and customizable UI components
              using React wrapper of Google&#39;s Material Components for the
              Web.
            </Text>
            <Text text="body2" as="p">
              Note.- This library is as simple as Material Web Components is,
              Tries to follow the guide lines as much as possible. The library
              do not do any assumptions on components or tags to be used unless
              the Material Components guide does.
            </Text>
          </LayoutCell>
        </LayoutInner>
      </LayoutGrid>
    </header>
  </Theme>
);

export default Hero;
