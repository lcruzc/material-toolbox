// @flow strict
import React, { useState, useCallback, useRef } from 'react';

import Example from '../Example';

import Dialog from '../material-toolbox/dialog';
import DialogContent from '../material-toolbox/dialog/Content';

import Button from '../material-toolbox/button';

import List from '../material-toolbox/list';
import ListItem from '../material-toolbox/list/Item';
import ListItemDetail from '../material-toolbox/list/ItemDetail';
import ListItemText from '../material-toolbox/list/ItemText';

const source = `import React, { useState, useCallback, useRef } from 'react';

import Dialog from 'material-toolbox/dialog';
import DialogContent from 'material-toolbox/dialog/Content';

import Button from 'material-toolbox/button';

import List from 'material-toolbox/list';
import ListItem from 'material-toolbox/list/Item';
import ListItemDetail from 'material-toolbox/list/ItemDetail';
import ListItemText from 'material-toolbox/list/ItemText';

const Simple = () => {
  const [isOpen, setOpen] = useState(false);
  const [selected, setSelected] = useState();
  const initialFocus = useRef();

  const handleClosed = useCallback(() => {
    setOpen(false);
  }, []);

  const handleClose = useCallback((evt) => {
    const { value } = evt.currentTarget.dataset;
    setSelected(value);
    setOpen(false);
  }, []);

  const handleOpen = useCallback(() => {
    setSelected(null);
    setOpen(true);
  }, []);

  const handleOpened = useCallback(() => {
    if (initialFocus.current) {
      initialFocus.current.focus();
    }
  }, []);

  return (
    <React.Fragment>
      <Butt on onClick={handleOpen}>Simple</Button>
      <p>{selected}</p>
      <Dialog
        title="Select an account"
        name="simple"
        open={isOpen}
        onClosed={handleClosed}
        onOpened={handleOpened}>
        <DialogContent>
          <List avatarList>
            <ListItem
              data-value="user1@example"
              onClick={handleClose}
              ref={initialFocus}>
              <ListItemDetail className="material-icons">person</ListItemDetail>
              <ListItemText>user1@example.com</ListItemText>
            </ListItem>
            <ListItem data-value="user2@example" onClick={handleClose}>
              <ListItemDetail className="material-icons">person</ListItemDetail>
              <ListItemText>user2@example.com</ListItemText>
            </ListItem>
            <ListItem data-value="add-account" onClick={handleClose}>
              <ListItemDetail className="material-icons">add</ListItemDetail>
              <ListItemText>Add account</ListItemText>
            </ListItem>
          </List>
        </DialogContent>
      </Dialog>
    </React.Fragment>
  );
};

export default Simple;`;

const Simple = () => {
  const [isOpen, setOpen] = useState<boolean>(false);
  const [selected, setSelected] = useState<?string>();
  const initialFocus = useRef<?HTMLLIElement>();

  const handleClosed = useCallback(() => {
    setOpen(false);
  }, []);

  const handleClose = useCallback((evt: SyntheticEvent<HTMLElement>) => {
    const { value } = evt.currentTarget.dataset;
    setSelected(value);
    setOpen(false);
  }, []);

  const handleOpen = useCallback(() => {
    setSelected(null);
    setOpen(true);
  }, []);

  const handleOpened = useCallback(() => {
    if (initialFocus.current) {
      initialFocus.current.focus();
    }
  }, []);

  return (
    <Example title="Simple" source={source}>
      <Button onClick={handleOpen}>Simple</Button>
      <p>{selected}</p>
      <Dialog
        title="Select an account"
        name="simple"
        open={isOpen}
        onClosed={handleClosed}
        onOpened={handleOpened}
      >
        <DialogContent>
          <List avatarList>
            <ListItem
              data-value="user1@example"
              onClick={handleClose}
              ref={initialFocus}
            >
              <ListItemDetail className="material-icons">person</ListItemDetail>
              <ListItemText>user1@example.com</ListItemText>
            </ListItem>
            <ListItem data-value="user2@example" onClick={handleClose}>
              <ListItemDetail className="material-icons">person</ListItemDetail>
              <ListItemText>user2@example.com</ListItemText>
            </ListItem>
            <ListItem data-value="add-account" onClick={handleClose}>
              <ListItemDetail className="material-icons">add</ListItemDetail>
              <ListItemText>Add account</ListItemText>
            </ListItem>
          </List>
        </DialogContent>
      </Dialog>
    </Example>
  );
};

export default Simple;
