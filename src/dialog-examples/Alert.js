// @flow strict
import React, { useState, useCallback } from 'react';

import MDCDialogFoundation from '@material/dialog/foundation';

import Example from '../Example';

import Dialog from '../material-toolbox/dialog';
import DialogContent from '../material-toolbox/dialog/Content';
import DialogActions from '../material-toolbox/dialog/Actions';
import DialogButton from '../material-toolbox/dialog/Button';

import Button from '../material-toolbox/button';
import ButtonLabel from '../material-toolbox/button/Label';

const source = `import React, { useState, useCallback } from 'react';

import MDCDialogFoundation from '@material/dialog/foundation';

import Dialog from 'material-toolbox/dialog';
import DialogContent from 'material-toolbox/dialog/Content';
import DialogActions from 'material-toolbox/dialog/Actions';
import DialogButton from 'material-toolbox/dialog/Button';

import Button from 'material-toolbox/button';
import ButtonLabel from 'material-toolbox/button/Label';

const SimpleDiscard = () => {
  const [isOpen, setOpen] = useState(false);

  const handleClosed = useCallback(() => {
    setOpen(false);
  }, []);

  const handleClose = useCallback(() => {
    setOpen(false);
  }, []);

  const handleOpen = useCallback(() => {
    setOpen(true);
  }, []);

  const initialFocusAttr = {
    [MDCDialogFoundation.strings.INITIAL_FOCUS_ATTRIBUTE]: true,
  };

  return (
    <React.Fragment>
      <Button onClick={handleOpen}>Alert</Button>
      <Dialog
        className="catalog-dialog-demo"
        name="alert-dialog"
        open={isOpen}
        onClosed={handleClosed}>
        <DialogContent>Discard draft?</DialogContent>
        <DialogActions>
          <DialogButton action="close">
            <ButtonLabel>Cancel</ButtonLabel>
          </DialogButton>
          <DialogButton {...initialFocusAttr} onClick={handleClose}>
            <ButtonLabel>Discard</ButtonLabel>
          </DialogButton>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
};

export default SimpleDiscard;`;

const SimpleDiscard = () => {
  const [isOpen, setOpen] = useState<boolean>(false);

  const handleClosed = useCallback(() => {
    setOpen(false);
  }, []);

  const handleClose = useCallback(() => {
    setOpen(false);
  }, []);

  const handleOpen = useCallback(() => {
    setOpen(true);
  }, []);

  const initialFocusAttr = {
    [MDCDialogFoundation.strings.INITIAL_FOCUS_ATTRIBUTE]: true,
  };

  return (
    <Example title="Alert" source={source}>
      <Button onClick={handleOpen}>Alert</Button>
      <Dialog name="alert-dialog" open={isOpen} onClosed={handleClosed}>
        <DialogContent>Discard draft?</DialogContent>
        <DialogActions>
          <DialogButton onClick={handleClose}>
            <ButtonLabel>Cancel</ButtonLabel>
          </DialogButton>
          <DialogButton {...initialFocusAttr} onClick={handleClose}>
            <ButtonLabel>Discard</ButtonLabel>
          </DialogButton>
        </DialogActions>
      </Dialog>
    </Example>
  );
};

export default SimpleDiscard;
