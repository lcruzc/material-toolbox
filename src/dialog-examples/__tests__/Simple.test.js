// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Simple from '../Simple';

afterEach(cleanup);

describe('example::Simple', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<Simple />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
