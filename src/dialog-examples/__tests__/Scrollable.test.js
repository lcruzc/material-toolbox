// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Scrollable from '../Scrollable';

afterEach(cleanup);

describe('example::Scrollable', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<Scrollable />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
