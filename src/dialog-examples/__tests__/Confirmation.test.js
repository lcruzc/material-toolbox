// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ConfirmationDialog from '../Confirmation';

afterEach(cleanup);

describe('example::Confirmation', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<ConfirmationDialog />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
