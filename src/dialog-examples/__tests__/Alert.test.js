// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Alert from '../Alert';

afterEach(cleanup);

describe('example::Alert', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<Alert />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
