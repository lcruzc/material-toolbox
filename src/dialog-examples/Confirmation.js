// @flow strict
import React, { useState, useCallback, useRef } from 'react';

import Example from '../Example';

import Dialog from '../material-toolbox/dialog';
import DialogContent from '../material-toolbox/dialog/Content';
import DialogActions from '../material-toolbox/dialog/Actions';
import DialogButton from '../material-toolbox/dialog/Button';

import Button from '../material-toolbox/button';
import ButtonLabel from '../material-toolbox/button/Label';

import Radio from '../material-toolbox/radio';

import List from '../material-toolbox/list';
import ListItem from '../material-toolbox/list/Item';
import ListItemDetail from '../material-toolbox/list/ItemDetail';
import ListItemText from '../material-toolbox/list/ItemText';

const source = `import React, { useState, useCallback, useRef } from 'react';

import Dialog from 'material-toolbox/dialog';
import DialogContent from 'material-toolbox/dialog/Content';
import DialogActions from 'material-toolbox/dialog/Actions';
import DialogButton from 'material-toolbox/dialog/Button';

import Button from 'material-toolbox/button';
import ButtonLabel from 'material-toolbox/button/Label';

import Radio from 'material-toolbox/radio';

import List from 'material-toolbox/list';
import ListItem from 'material-toolbox/list/Item';
import ListItemDetail from 'material-toolbox/list/ItemDetail';
import ListItemText from 'material-toolbox/list/ItemText';

const ConfirmationDialog = () => {
  const [isOpen, setOpen] = useState(false);
  const [selected, setSelected] = useState();
  const [value, setValue] = useState();
  const initialFocus = useRef();

  const handleClosed = useCallback(() => {
    setOpen(false);
  }, []);

  const handleDeny = useCallback(() => {
    setSelected('Declined... Maybe next time?');
    setOpen(false);
  }, []);

  const handleAccept = useCallback(() => {
    setSelected('Accepted, thanks!');
    setOpen(false);
  }, []);

  const handleOpen = useCallback(() => {
    setSelected(null);
    setOpen(true);
  }, []);

  const handleOpened = useCallback(() => {
    if (initialFocus.current) {
      initialFocus.current.focus();
    }
  }, []);

  const handleSelection = useCallback((evt) => {
    const { value: v } = evt.currentTarget.dataset;
    setValue(v);
  }, []);

  const handleChange = useCallback((evt) => {
    const { value: v } = evt.currentTarget;
    setValue(v);
  }, []);

  return (
    <React.Fragment>
      <Button onClick={handleOpen}>Confirmation</Button>
      <p>{selected}</p>
      <Dialog
        title="Phone ringtone"
        name="confirmation"
        open={isOpen}
        onClosed={handleClosed}
        onOpened={handleOpened}>
        <DialogContent>
          <List role="radiogroup">
            <ListItem
              data-value="option1"
              ref={initialFocus}
              onClick={handleSelection}>
              <ListItemDetail className="material-icons">
                <Radio
                  name="ringtone"
                  value="option1"
                  checked={value === 'option1'}
                  onChange={handleChange}
                />
              </ListItemDetail>
              <ListItemText>Never Gonna Give You Up</ListItemText>
            </ListItem>
            <ListItem data-value="option2" onClick={handleSelection}>
              <ListItemDetail className="material-icons">
                <Radio
                  name="ringtone"
                  value="option2"
                  checked={value === 'option2'}
                  onChange={handleChange}
                />
              </ListItemDetail>
              <ListItemText>Hot Cross Buns</ListItemText>
            </ListItem>
            <ListItem data-value="option3" onClick={handleSelection}>
              <ListItemDetail className="material-icons">
                <Radio
                  name="ringtone"
                  value="option3"
                  checked={value === 'option3'}
                  onChange={handleChange}
                />
              </ListItemDetail>
              <ListItemText>None</ListItemText>
            </ListItem>
          </List>
        </DialogContent>
        <DialogActions>
          <DialogButton onClick={handleDeny}>
            <ButtonLabel>Cancel</ButtonLabel>
          </DialogButton>
          <DialogButton onClick={handleAccept}>
            <ButtonLabel>OK</ButtonLabel>
          </DialogButton>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
};

export default ConfirmationDialog;`;

const ConfirmationDialog = () => {
  const [isOpen, setOpen] = useState<boolean>(false);
  const [selected, setSelected] = useState<?string>();
  const [value, setValue] = useState<?string>();
  const initialFocus = useRef<?HTMLLIElement>();

  const handleClosed = useCallback(() => {
    setOpen(false);
  }, []);

  const handleDeny = useCallback(() => {
    setSelected('Declined... Maybe next time?');
    setOpen(false);
  }, []);

  const handleAccept = useCallback(() => {
    setSelected('Accepted, thanks!');
    setOpen(false);
  }, []);

  const handleOpen = useCallback(() => {
    setSelected(null);
    setOpen(true);
  }, []);

  const handleOpened = useCallback(() => {
    if (initialFocus.current) {
      initialFocus.current.focus();
    }
  }, []);

  const handleSelection = useCallback(
    (evt: SyntheticEvent<HTMLInputElement>) => {
      const { value: v } = evt.currentTarget.dataset;
      setValue(v);
    },
    [],
  );

  const handleChange = useCallback((evt: SyntheticEvent<HTMLInputElement>) => {
    const { value: v } = evt.currentTarget;
    setValue(v);
  }, []);

  return (
    <Example title="Confirmation" source={source}>
      <Button onClick={handleOpen}>Confirmation</Button>
      <p>{selected}</p>
      <Dialog
        title="Phone ringtone"
        name="confirmation"
        open={isOpen}
        onClosed={handleClosed}
        onOpened={handleOpened}
      >
        <DialogContent>
          <List role="radiogroup">
            <ListItem
              data-value="option1"
              ref={initialFocus}
              onClick={handleSelection}
            >
              <ListItemDetail className="material-icons">
                <Radio
                  name="ringtone"
                  value="option1"
                  checked={value === 'option1'}
                  onChange={handleChange}
                />
              </ListItemDetail>
              <ListItemText>Never Gonna Give You Up</ListItemText>
            </ListItem>
            <ListItem data-value="option2" onClick={handleSelection}>
              <ListItemDetail className="material-icons">
                <Radio
                  name="ringtone"
                  value="option2"
                  checked={value === 'option2'}
                  onChange={handleChange}
                />
              </ListItemDetail>
              <ListItemText>Hot Cross Buns</ListItemText>
            </ListItem>
            <ListItem data-value="option3" onClick={handleSelection}>
              <ListItemDetail className="material-icons">
                <Radio
                  name="ringtone"
                  value="option3"
                  checked={value === 'option3'}
                  onChange={handleChange}
                />
              </ListItemDetail>
              <ListItemText>None</ListItemText>
            </ListItem>
          </List>
        </DialogContent>
        <DialogActions>
          <DialogButton onClick={handleDeny}>
            <ButtonLabel>Cancel</ButtonLabel>
          </DialogButton>
          <DialogButton onClick={handleAccept}>
            <ButtonLabel>OK</ButtonLabel>
          </DialogButton>
        </DialogActions>
      </Dialog>
    </Example>
  );
};

export default ConfirmationDialog;
