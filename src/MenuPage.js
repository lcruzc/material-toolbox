// @flow strict
import React, { useEffect } from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';
import Text from './material-toolbox/typography/Text';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';
import Highlighter from './Highlighter';

import SimpleMenu from './menu-examples/Simple';

const resources = [
  {
    href: 'https://material.io/go/design-menus',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href: 'https://material.io/components/web/catalog/menus/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const MenuPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Menu</h1>

      <Text as="p" text="overline">
        Menu component is a React wrapper of mdc-menu component.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>MenuSurfaceAnchor</ComponentTitle>

      <ComponentTitle>useMenuSurfaceAnchor</ComponentTitle>

      <section>
        <Highlighter language="jsx">
          {'useMenuSurfaceAnchor() => string // className string'}
        </Highlighter>
      </section>

      <ComponentTitle>withMenuSurfaceAnchor</ComponentTitle>

      <section>
        <Highlighter language="jsx">{`withMenuSurfaceAnchor<Config: { className?: string }, Instance>(
  Component: AbstractComponent<Config, Instance>,
) => AbstractComponent<Config, Instance>;`}</Highlighter>
      </section>

      <ComponentTitle>MenuSelectionGroup</ComponentTitle>

      <ComponentTitle>MenuSelectionGroupIcon</ComponentTitle>

      <p>Props same as ListItemDetail</p>

      <ComponentTitle>MenuItem</ComponentTitle>

      <p>Props same as ListItem</p>

      <ComponentTitle>Menu</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>fixed</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Fixed variant</td>
          </tr>
          <tr>
            <td>hoisted</td>
            <td>boolean</td>
            <td />
            <td />
            <td>If true mounted into body and needs coordinates</td>
          </tr>
          <tr>
            <td>open</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>If true open the menu, if false close it.</td>
          </tr>
          <tr>
            <td>quickOpen</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Quick open version of menu if true</td>
          </tr>
          <tr>
            <td>defaultFocusState</td>
            <td>number</td>
            <td />
            <td />
            <td>
              Any value from
              <b>
                {' "import { DefaultFocusState } from material-toolbox/menu"'}
              </b>
            </td>
          </tr>
          <tr>
            <td>anchorCorner</td>
            <td>number</td>
            <td />
            <td />
            <td>
              Any value from
              <b>{' "import { Corner } from material-toolbox/menu"'}</b>
            </td>
          </tr>
          <tr>
            <td>skipRestoreFocus</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Skip restore focus if true</td>
          </tr>
          <tr>
            <td>onOpened</td>
            <td>{'() => void'}</td>
            <td />
            <td />
            <td>Called when menu was openned</td>
          </tr>
          <tr>
            <td>onClosed</td>
            <td>{'() => void'}</td>
            <td />
            <td />
            <td>Called when menu was closed</td>
          </tr>
          <tr>
            <td>onSelected</td>
            <td>{'({ index: number, item: HTMLListItemElement }) => void'}</td>
            <td />
            <td />
            <td>Called when an item was selected</td>
          </tr>
          <tr>
            <td>coordinates</td>
            <td>{'{ x: number, y: number }'}</td>
            <td />
            <td />
            <td>Coordinates to use with hoisted prop</td>
          </tr>
          <tr>
            <td>anchorMargin</td>
            <td>
              {`{ top?: number, left?: number, bottom?: number, right?: number}`}
            </td>
            <td />
            <td />
            <td>Used to update marging</td>
          </tr>
          <tr>
            <td>selectedIndex</td>
            <td>number</td>
            <td />
            <td />
            <td>
              Select item but need to be child of MenuSelectionGroup with
              MenuSelectionGroupIcon inside MenuItem
            </td>
          </tr>
          <tr>
            <td>anchor</td>
            <td>{'{ current?: HTMLElement }'}</td>
            <td />
            <td />
            <td>
              When hoisted needs an anchor to display the correct position
            </td>
          </tr>
        </tbody>
      </table>
      <SectionTitle>Demos</SectionTitle>

      <SimpleMenu />
    </LayoutCell>
  );
};

export default MenuPage;
