// @flow strict
import React, { useEffect } from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';
import Text from './material-toolbox/typography/Text';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';
import Highlighter from './Highlighter';

import WithJavascript from './checkbox-examples/Examples';

const resources = [
  {
    href: 'https://material.io/go/design-checkboxes',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href:
      'https://material.io/components/web/catalog/input-controls/checkboxes/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const CheckboxesPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Checkbox</h1>

      <Text as="p" text="overline">
        Checkbox component is a React wrapper of mdc-checkbox component.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>Checkbox</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>indeterminate</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Sets to an indeterminate state.</td>
          </tr>
        </tbody>
      </table>

      <Highlighter language="jsx">{`ref type {
  root: ?HTMLDivElement,
  input: ?HTMLInputElement,
  checked: boolean, // set the checked state and update design, use only on uncontrolled component
  indeterminate: boolean, // set the checked state and update design, use only on uncontrolled component
}`}</Highlighter>

      <SectionTitle>Demos</SectionTitle>

      <WithJavascript />
    </LayoutCell>
  );
};

export default CheckboxesPage;
