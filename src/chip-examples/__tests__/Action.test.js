// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ActionChip from '../Action';

afterEach(cleanup);

describe('example::ActionChip', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<ActionChip />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
