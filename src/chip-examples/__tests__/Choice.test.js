// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ChoiceChip from '../Choice';

afterEach(cleanup);

describe('example::ChoiceChip', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<ChoiceChip />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
