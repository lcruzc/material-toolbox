// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ChipInput from '../Input';

afterEach(cleanup);

describe('example::ChipInput', () => {
  it('Should match snapshoot', () => {
    global.MutationObserver = class {
      constructor(callback) {} // eslint-disable-line

      disconnect() {} // eslint-disable-line

      observe(element, initObject) {} // eslint-disable-line
    };
    const { container } = render(<ChipInput />);
    expect(container.firstChild).toMatchSnapshot();
    delete global.MutationObserver;
  });
});
