// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import FilterChip from '../Filter';

afterEach(cleanup);

describe('example::FilterChip', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<FilterChip />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
