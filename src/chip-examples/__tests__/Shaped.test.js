// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ShapedChip from '../Shaped';

afterEach(cleanup);

describe('example::ShapedChip', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<ShapedChip />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
