// @flow strict
import React from 'react';

import Example from '../Example';

import Chip from '../material-toolbox/chip';
import ChipSet from '../material-toolbox/chip/Set';

const source = ``;

const Action = () => {
  return (
    <Example title="Shaped Chips" source={source}>
      <ChipSet className="chip-set-shaped">
        <Chip className="shaped" label="Bookcase" />
        <Chip className="shaped" label="TV Stand" />
        <Chip className="shaped" label="Sofas" />
        <Chip className="shaped" label="Office chairs" />
      </ChipSet>
    </Example>
  );
};

export default Action;
