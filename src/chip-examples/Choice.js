// @flow strict
import React from 'react';

import Example from '../Example';

import Chip from '../material-toolbox/chip';
import ChipSet from '../material-toolbox/chip/Set';

const source = `import React from 'react';

import Chip from 'material-toolbox/chip';
import ChipSet from 'material-toolbox/chip/Set';

const Choice = () => {
  return (
    <ChipSet choice>
      <Chip label="Extra small" />
      <Chip label="Small" />
      <Chip label="Medium" defaultSelected />
      <Chip label="Large" />
      <Chip label="Extra large" />
    </ChipSet>
  );
};

export default Choice;`;

const Choice = () => {
  return (
    <Example title="Choice Chips" source={source}>
      <ChipSet choice>
        <Chip label="Extra small" />
        <Chip label="Small" />
        <Chip label="Medium" defaultSelected />
        <Chip label="Large" />
        <Chip label="Extra large" />
      </ChipSet>
    </Example>
  );
};

export default Choice;
