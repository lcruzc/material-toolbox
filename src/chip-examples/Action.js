// @flow strict
import React from 'react';

import Example from '../Example';

import Chip from '../material-toolbox/chip';
import ChipSet from '../material-toolbox/chip/Set';
import ChipIcon from '../material-toolbox/chip/Icon';

const source = `import React from 'react';

import Chip from 'material-toolbox/chip';
import ChipSet from 'material-toolbox/chip/Set';
import ChipIcon from 'material-toolbox/chip/Icon';

const Action = () => {
  return (
    <ChipSet>
      <Chip
        leadingIcon={<ChipIcon className="material-icons">event</ChipIcon>}
        label="Add to calendar"
      />
      <Chip
        leadingIcon={<ChipIcon className="material-icons">bookmark</ChipIcon>}
        label="Bookmark"
      />
      <Chip
        leadingIcon={<ChipIcon className="material-icons">alarm</ChipIcon>}
        label="Set alarm"
      />
      <Chip
        leadingIcon={
          <ChipIcon className="material-icons">directions</ChipIcon>
        }
        label="Get direction"
      />
    </ChipSet>
  );
};

export default Action;`;

const Action = () => {
  return (
    <Example title="Action chips" source={source}>
      <ChipSet>
        <Chip
          leadingIcon={<ChipIcon className="material-icons">event</ChipIcon>}
          label="Add to calendar"
        />
        <Chip
          leadingIcon={<ChipIcon className="material-icons">bookmark</ChipIcon>}
          label="Bookmark"
        />
        <Chip
          leadingIcon={<ChipIcon className="material-icons">alarm</ChipIcon>}
          label="Set alarm"
        />
        <Chip
          leadingIcon={
            <ChipIcon className="material-icons">directions</ChipIcon>
          }
          label="Get direction"
        />
      </ChipSet>
    </Example>
  );
};

export default Action;
