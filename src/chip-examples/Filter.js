// @flow strict
import React from 'react';

import Example from '../Example';

import Chip from '../material-toolbox/chip';
import ChipIcon from '../material-toolbox/chip/Icon';
import ChipSet from '../material-toolbox/chip/Set';
import Text from '../material-toolbox/typography/Text';

const source = `import React from 'react';

import Chip from 'material-toolbox/chip';
import ChipIcon from 'material-toolbox/chip/Icon';
import ChipSet from 'material-toolbox/chip/Set';
import Text from 'material-toolbox/typography/Text';

const Filter = () => {
  return (
    <>
      <section>
        <Text text="subtitle1" as="h1" />
        No leading icon
        <ChipSet filter>
          <Chip filter label="Tops" defaultSelected />
          <Chip filter label="Bootoms" defaultSelected />
          <Chip filter label="Shoes" />
          <Chip filter label="Accessories" />
        </ChipSet>
      </section>

      <section>
        <Text text="subtitle1" as="h1" />
        With leading icon
        <ChipSet filter>
          <Chip
            filter
            leadingIcon={<ChipIcon className="material-icons">face</ChipIcon>}
            label="Tops"
            defaultSelected
          />
          <Chip
            filter
            leadingIcon={<ChipIcon className="material-icons">face</ChipIcon>}
            label="Bootoms"
            defaultSelected
          />
          <Chip
            filter
            leadingIcon={<ChipIcon className="material-icons">face</ChipIcon>}
            label="Shoes"
          />
          <Chip
            filter
            leadingIcon={<ChipIcon className="material-icons">face</ChipIcon>}
            label="Accessories"
          />
        </ChipSet>
      </section>
    </>
  );
};

export default Filter;`;

const Filter = () => {
  return (
    <Example title="Filter chips" source={source}>
      <section>
        <Text text="subtitle1" as="h1" />
        No leading icon
        <ChipSet filter>
          <Chip filter label="Tops" defaultSelected />
          <Chip filter label="Bootoms" defaultSelected />
          <Chip filter label="Shoes" />
          <Chip filter label="Accessories" />
        </ChipSet>
      </section>

      <section>
        <Text text="subtitle1" as="h1" />
        With leading icon
        <ChipSet filter>
          <Chip
            filter
            leadingIcon={<ChipIcon className="material-icons">face</ChipIcon>}
            label="Tops"
            defaultSelected
          />
          <Chip
            filter
            leadingIcon={<ChipIcon className="material-icons">face</ChipIcon>}
            label="Bootoms"
            defaultSelected
          />
          <Chip
            filter
            leadingIcon={<ChipIcon className="material-icons">face</ChipIcon>}
            label="Shoes"
          />
          <Chip
            filter
            leadingIcon={<ChipIcon className="material-icons">face</ChipIcon>}
            label="Accessories"
          />
        </ChipSet>
      </section>
    </Example>
  );
};

export default Filter;
