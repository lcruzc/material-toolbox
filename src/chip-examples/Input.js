// @flow strict
import React, { useState, useCallback } from 'react';

import Example from '../Example';

import Chip from '../material-toolbox/chip';
import ChipSet from '../material-toolbox/chip/Set';
import ChipIcon from '../material-toolbox/chip/Icon';
import TextField from '../material-toolbox/textfield';
import Button from '../material-toolbox/button';

const source = `import React, { useState, useCallback } from 'react';

import Chip from 'material-toolbox/chip';
import ChipSet from 'material-toolbox/chip/Set';
import ChipIcon from 'material-toolbox/chip/Icon';
import TextField from 'material-toolbox/textfield';
import Button from 'material-toolbox/button';

const Input = () => {
  const [chips, setChips] = useState(['tag-1', 'tag-2', 'tag-3']);
  const [text, setText] = useState('');

  const handleTextChange = useCallback(evt => {
    const { value } = evt.currentTarget;
    setText(value);
  }, []);

  const handleClick = useCallback(() => {
    setChips(chips_ => [...chips_, text]);
    setText('');
  }, [text]);

  const handleRemoval = useCallback(({ element }) => {
    const { data } = element.dataset;
    setChips(chips_ => chips_.filter(chip => chip !== data));
  }, []);

  return (
    <>
      <TextField
        value={text}
        onChange={handleTextChange}
        outlined
        label="Add new tag"
      />

      <Button onClick={handleClick}>Add</Button>

      <ChipSet input>
        {chips.map(chip => (
          <Chip
            label={chip}
            key={chip}
            data-data={chip}
            onRemoval={handleRemoval}
            trailingIcon={
              <ChipIcon trailing className="material-icons">
                close
              </ChipIcon>
            }
          />
        ))}
      </ChipSet>
    </>
  );
};

export default Input;`;

const Input = () => {
  const [chips, setChips] = useState(['tag-1', 'tag-2', 'tag-3']);
  const [text, setText] = useState('');

  const handleTextChange = useCallback(
    (evt: SyntheticEvent<HTMLInputElement>) => {
      const { value } = evt.currentTarget;
      setText(value);
    },
    [],
  );

  const handleClick = useCallback(() => {
    setChips(chips_ => [...chips_, text]);
    setText('');
  }, [text]);

  const handleRemoval = useCallback(({ element }) => {
    const { data } = element.dataset;
    setChips(chips_ => chips_.filter(chip => chip !== data));
  }, []);

  return (
    <Example title="Input Chips" source={source}>
      <TextField
        value={text}
        onChange={handleTextChange}
        outlined
        label="Add new tag"
      />

      <Button onClick={handleClick}>Add</Button>

      <ChipSet input>
        {chips.map(chip => (
          <Chip
            label={chip}
            key={chip}
            data-data={chip}
            onRemoval={handleRemoval}
            trailingIcon={
              <ChipIcon trailing className="material-icons">
                close
              </ChipIcon>
            }
          />
        ))}
      </ChipSet>
    </Example>
  );
};

export default Input;
