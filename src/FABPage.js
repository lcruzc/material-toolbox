// @flow strict
import React, { useEffect } from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';
import Text from './material-toolbox/typography/Text';

import FAB from './material-toolbox/fab';
import FABIcon from './material-toolbox/fab/Icon';
import FABLabel from './material-toolbox/fab/Label';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';

import Example from './Example';

const source = `import React from 'react';

import FAB from './material-toolbox/fab';
import FABIcon from './material-toolbox/fab/Icon';
import FABLabel from './material-toolbox/fab/Label';

const Demos = () => {
  return (
    <React.Fragment>
      <section>
        <ComponentTitle>Standard Floating Action Button</ComponentTitle>
        <FAB label="Favorite">
          <FABIcon className="material-icons">favorite_border</FABIcon>
        </FAB>
      </section>
      <section>
        <ComponentTitle>Mini Floating Action Button</ComponentTitle>
        <FAB label="Favorite" mini>
          <FABIcon className="material-icons">favorite_border</FABIcon>
        </FAB>
      </section>
      <section>
        <ComponentTitle>Extended FAB</ComponentTitle>
        <FAB label="Add" extended>
          <FABIcon className="material-icons">add</FABIcon>
          <FABLabel>Create</FABLabel>
        </FAB>
      </section>
      <section>
        <ComponentTitle>
          Extended FAB (Text label followed by icon)
        </ComponentTitle>
        <FAB label="Add" extended>
          <FABLabel>Create</FABLabel>
          <FABIcon className="material-icons">add</FABIcon>
        </FAB>
      </section>
      <section>
        <ComponentTitle>Extended FAB (without Icon)</ComponentTitle>
        <FAB label="Add" extended>
          <FABLabel>Create</FABLabel>
        </FAB>
      </section>
      <section>
        <ComponentTitle>FAB (Shaped)</ComponentTitle>
        <FAB className="demo-fab-shaped--one" label="Favorite">
          <FABIcon className="material-icons">favorite_border</FABIcon>
        </FAB>
        <FAB className="demo-fab-shaped--two" label="Favorite" mini>
          <FABIcon className="material-icons">favorite_border</FABIcon>
        </FAB>
        <FAB className="demo-fab-shaped--three" label="Favorite" extended>
          <FABIcon className="material-icons">add</FABIcon>
          <FABLabel>Create</FABLabel>
        </FAB>
      </section>
    </React.Fragment>
  );
};

export default Demos;`;

const scssSource = `.demo-fab-shaped--one:not(.mdc-fab--extended) {
  border-radius: 50% 0;
  margin-right: 20px;
}

.demo-fab-shaped--two:not(.mdc-fab--extended) {
  border-radius: 8px;
  margin-right: 20px;
}

.demo-fab-shaped--three {
  border-radius: 12px;
}`;

const resources = [
  {
    href: 'https://material.io/go/design-radio-buttons',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href:
      'https://material.io/components/web/catalog/input-controls/radio-buttons/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const FABPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Floating Action Button</h1>

      <Text as="p" text="overline">
        Floating Action Button component is a React wrappers of mdc-fab
        component.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>FAB</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>as</td>
            <td>Any component</td>
            <td>button</td>
            <td />
            <td>Element type that will generate the button</td>
          </tr>
          <tr>
            <td>exited</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Exit transition effect</td>
          </tr>
          <tr>
            <td>mini</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Mini variant</td>
          </tr>
          <tr>
            <td>extended</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Used with FABLabel</td>
          </tr>
          <tr>
            <td>label</td>
            <td>string</td>
            <td />
            <td>✔</td>
            <td>Used for accessiblility</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>FABIcon</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>as</td>
            <td>String: span, i, img, svg</td>
            <td>span</td>
            <td />
            <td>Element type that will generate the button</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>FABLabel</ComponentTitle>

      <SectionTitle>Demos</SectionTitle>

      <Example title="" source={source} scss={scssSource}>
        <section>
          <ComponentTitle>Standard Floating Action Button</ComponentTitle>
          <FAB label="Favorite">
            <FABIcon className="material-icons">favorite_border</FABIcon>
          </FAB>
        </section>
        <section>
          <ComponentTitle>Mini Floating Action Button</ComponentTitle>
          <FAB label="Favorite" mini>
            <FABIcon className="material-icons">favorite_border</FABIcon>
          </FAB>
        </section>
        <section>
          <ComponentTitle>Extended FAB</ComponentTitle>
          <FAB label="Add" extended>
            <FABIcon className="material-icons">add</FABIcon>
            <FABLabel>Create</FABLabel>
          </FAB>
        </section>
        <section>
          <ComponentTitle>
            Extended FAB (Text label followed by icon)
          </ComponentTitle>
          <FAB label="Add" extended>
            <FABLabel>Create</FABLabel>
            <FABIcon className="material-icons">add</FABIcon>
          </FAB>
        </section>
        <section>
          <ComponentTitle>Extended FAB (without Icon)</ComponentTitle>
          <FAB label="Add" extended>
            <FABLabel>Create</FABLabel>
          </FAB>
        </section>
        <section>
          <ComponentTitle>FAB (Shaped)</ComponentTitle>
          <FAB className="demo-fab-shaped--one" label="Favorite">
            <FABIcon className="material-icons">favorite_border</FABIcon>
          </FAB>
          <FAB className="demo-fab-shaped--two" label="Favorite" mini>
            <FABIcon className="material-icons">favorite_border</FABIcon>
          </FAB>
          <FAB className="demo-fab-shaped--three" label="Favorite" extended>
            <FABIcon className="material-icons">add</FABIcon>
            <FABLabel>Create</FABLabel>
          </FAB>
        </section>
      </Example>
    </LayoutCell>
  );
};

export default FABPage;
