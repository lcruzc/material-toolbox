// @flow strict
import React, { useEffect } from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';
import Text from './material-toolbox/typography/Text';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';

import DataTableStandard from './data-tables-examples/Standard';
import DataTableWithSelection from './data-tables-examples/WithSelection';

const resources = [
  {
    href: 'https://material.io/go/design-data-tables',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href: 'https://material.io/components/web/catalog/data-tables/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const DataTablesPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Data Table</h1>

      <Text as="p" text="overline">
        Data Table components are a React wrappers of mdc-data-table component.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>DataTableHead</ComponentTitle>

      <ComponentTitle>DataTableContent</ComponentTitle>

      <ComponentTitle>DataTableCell</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>numeric</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set align for numbers</td>
          </tr>
          <tr>
            <td>header</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set styles for header</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>DataTableRow</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>rowId</td>
            <td>string</td>
            <td />
            <td />
            <td>Use when onRowSelectionChanged is called.</td>
          </tr>
          <tr>
            <td>header</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set styles for header</td>
          </tr>
          <tr>
            <td>withCheckbox</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Add a column with a checkbox</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>DataTable</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>label</td>
            <td>string</td>
            <td />
            <td />
            <td>Label used for accessibility</td>
          </tr>
          <tr>
            <td>selected</td>
            <td>{'Array<string>'}</td>
            <td />
            <td />
            <td>Update selected rows must be rowId in DataTableRow</td>
          </tr>
          <tr>
            <td>defaultSelected</td>
            <td>{'Array<string>'}</td>
            <td />
            <td />
            <td>Set the default selected rows must be rowId in DataTableRow</td>
          </tr>
          <tr>
            <td>onSelectedAll</td>
            <td>{'() => void'}</td>
            <td />
            <td />
            <td>Called when all rows was selected</td>
          </tr>
          <tr>
            <td>onUnselectedAll</td>
            <td>{'() => void'}</td>
            <td />
            <td />
            <td>Called when all rows was unsellected</td>
          </tr>
          <tr>
            <td>onRowSelectionChanged</td>
            <td>{`({
    row: HTMLTableRowElement,
    rowId: string,
    rowIndex: number,
    selected: boolean,
  }) => void`}</td>
            <td />
            <td />
            <td>Called when a row selection was changed</td>
          </tr>
        </tbody>
      </table>

      <SectionTitle>Demos</SectionTitle>

      <DataTableStandard />

      <DataTableWithSelection />
    </LayoutCell>
  );
};

export default DataTablesPage;
