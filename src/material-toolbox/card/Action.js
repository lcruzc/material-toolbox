// @flow strict
import React from 'react';
import cx from 'classnames';

import type { Node } from 'react';

type Props = {
  children: Node,
  className?: string,
  icon: boolean,
};

const CardAction = ({ className, children, icon, ...rest }: Props) =>
  React.Children.map(children, child => (
    <child.type
      {...rest}
      {...child.props}
      className={cx(child.props.className, 'mdc-card__action', className, {
        'mdc-card__action--button': !icon,
        'mdc-card__action--icon': icon,
      })}
    />
  ));

CardAction.defaultProps = {
  className: undefined,
  icon: false,
};

export default CardAction;
