// @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  fullBleed: boolean,
  className?: string,
};

const CardActions = ({ className, fullBleed, ...rest }: Props) => (
  <div
    {...rest}
    className={cx('mdc-card__actions', className, {
      'mdc-card__actions--full-bleed': fullBleed,
    })}
  />
);

CardActions.defaultProps = {
  fullBleed: false,
  className: undefined,
};

export default CardActions;
