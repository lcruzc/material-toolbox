// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import CardPrimaryAction from '../PrimaryAction';

afterEach(cleanup);

describe('component::CardPrimaryAction', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<CardPrimaryAction />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
