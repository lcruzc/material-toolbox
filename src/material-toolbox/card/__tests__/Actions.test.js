// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import CardActions from '../Actions';

afterEach(cleanup);

describe('component::CardActions', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<CardActions />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot fullBleed', () => {
    const { container } = render(<CardActions fullBleed />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
