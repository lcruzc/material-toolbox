// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import CardActionIcons from '../ActionIcons';

afterEach(cleanup);

describe('component::CardActionIcons', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<CardActionIcons />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
