// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import CardMediaContent from '../MediaContent';

afterEach(cleanup);

describe('component::CardMediaContent', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<CardMediaContent />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
