// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import CardMedia from '../Media';

afterEach(cleanup);

describe('component::CardMedia', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<CardMedia />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot square', () => {
    const { container } = render(<CardMedia square />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot ratio169', () => {
    const { container } = render(<CardMedia ratio169 />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
