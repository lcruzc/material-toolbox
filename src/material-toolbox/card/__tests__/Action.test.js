// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import CardAction from '../Action';

afterEach(cleanup);

describe('component::CardAction', () => {
  it('Should match snapshoot icon', () => {
    const { container } = render(
      <CardAction icon>
        <div />
      </CardAction>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot', () => {
    const { container } = render(
      <CardAction>
        <div />
      </CardAction>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
