// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import CardActionButtons from '../ActionButtons';

afterEach(cleanup);

describe('component::CardActionButtons', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<CardActionButtons />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
