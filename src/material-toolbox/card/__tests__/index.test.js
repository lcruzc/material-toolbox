// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Card from '../index';

afterEach(cleanup);

describe('component::Card', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<Card />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot component section', () => {
    const { container } = render(<Card as="section" />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot outlined', () => {
    const { container } = render(<Card outlined />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
