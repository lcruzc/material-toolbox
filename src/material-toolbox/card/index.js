// @flow strict
import React from 'react';
import cx from 'classnames';

import type { ElementType } from 'react';

type Props = {
  className?: string,
  as?: ElementType,
  outlined?: boolean,
};

const Card = ({
  className,
  outlined = false,
  as: Component = 'div',
  ...rest
}: Props) => (
  <Component
    {...(rest: mixed)}
    className={cx(className, 'mdc-card', {
      'mdc-card--outlined': outlined,
    })}
  />
);

export default Card;
