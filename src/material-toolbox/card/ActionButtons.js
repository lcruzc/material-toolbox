// @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  className?: string,
};

const CardActionButtons = ({ className, ...rest }: Props) => (
  <div {...rest} className={cx('mdc-card__action-buttons', className)} />
);

CardActionButtons.defaultProps = {
  className: undefined,
};

export default CardActionButtons;
