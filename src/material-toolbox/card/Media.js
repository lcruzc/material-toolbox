// @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  className?: string,
  square: boolean,
  ratio169: boolean,
};

const CardMedia = ({ className, square, ratio169, ...rest }: Props) => (
  <section
    {...rest}
    className={cx(className, 'mdc-card__media', {
      'mdc-card__media--square': square,
      'mdc-card__media--16-9': ratio169,
    })}
  />
);

CardMedia.defaultProps = {
  className: undefined,
  square: false,
  ratio169: false,
};

export default CardMedia;
