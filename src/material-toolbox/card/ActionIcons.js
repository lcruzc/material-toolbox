// @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  className?: string,
};

const CardActionIcons = ({ className, ...rest }: Props) => (
  <div {...rest} className={cx('mdc-card__action-icons', className)} />
);

CardActionIcons.defaultProps = {
  className: undefined,
};

export default CardActionIcons;
