// @flow strict
import React, { useRef, useMemo } from 'react';
import cx from 'classnames';

import { useRipple } from '../ripple';

import type { Style } from '../mt-types';

type Props = {
  className?: string,
  style?: Style,
};

const CardPrimaryAction = ({ className, style, ...rest }: Props) => {
  const divRef = useRef();

  const { rippleStyle, rippleClassName } = useRipple({
    activator: divRef,
  });

  const memoizedStyle = useMemo(() => ({ ...style, ...rippleStyle }), [
    style,
    rippleStyle,
  ]);

  return (
    <div // eslint-disable-line jsx-a11y/no-static-element-interactions
      {...rest}
      tabIndex="0" // eslint-disable-line jsx-a11y/no-noninteractive-tabindex
      ref={divRef}
      style={memoizedStyle}
      className={cx(
        'mdc-card__primary-action',
        className,
        ...Array.from(rippleClassName),
      )}
    />
  );
};

export default CardPrimaryAction;
