// @flow strict
import React from 'react';

type Props = {};

const CardMediaContent = ({ ...rest }: Props) => (
  <div {...rest} className="mdc-card__media-content" />
);

export default CardMediaContent;
