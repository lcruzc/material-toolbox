// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import LayoutGrid from '../Grid';

afterEach(cleanup);

describe('component::LayoutGrid', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<LayoutGrid />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot fixed', () => {
    const { container } = render(<LayoutGrid fixed />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot align left', () => {
    const { container } = render(<LayoutGrid align="left" />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot align right', () => {
    const { container } = render(<LayoutGrid align="right" />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot with className', () => {
    const { container } = render(<LayoutGrid className="extra-class-name" />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
