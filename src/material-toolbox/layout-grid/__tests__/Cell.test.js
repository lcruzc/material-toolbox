// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import LayoutCell from '../Cell';

afterEach(cleanup);

describe('component::LayoutCell', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<LayoutCell />);
    expect(container.firstChild).toMatchSnapshot();
  });

  ['top', 'middle', 'bottom'].forEach(align => {
    it(`Should match snapshoot align ${align}`, () => {
      const { container } = render(<LayoutCell align={align} />);
      expect(container.firstChild).toMatchSnapshot();
    });
  });

  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].forEach(span => {
    it(`Should match snapshoot span ${span}`, () => {
      const { container } = render(<LayoutCell span={span} />);
      expect(container.firstChild).toMatchSnapshot();
    });
  });

  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].forEach(order => {
    it(`Should match snapshoot order ${order}`, () => {
      const { container } = render(<LayoutCell order={order} />);
      expect(container.firstChild).toMatchSnapshot();
    });
  });

  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].forEach(desktop => {
    it(`Should match snapshoot desktop ${desktop}`, () => {
      const { container } = render(<LayoutCell desktop={desktop} />);
      expect(container.firstChild).toMatchSnapshot();
    });
  });

  [1, 2, 3, 4, 5, 6, 7, 8].forEach(tablet => {
    it(`Should match snapshoot tablet ${tablet}`, () => {
      const { container } = render(<LayoutCell tablet={tablet} />);
      expect(container.firstChild).toMatchSnapshot();
    });
  });

  [1, 2, 3, 4].forEach(phone => {
    it(`Should match snapshoot phone ${phone}`, () => {
      const { container } = render(<LayoutCell phone={phone} />);
      expect(container.firstChild).toMatchSnapshot();
    });
  });
});
