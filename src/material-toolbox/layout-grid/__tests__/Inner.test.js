// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import LayoutCell from '../Cell';

afterEach(cleanup);

describe('component::LayoutCell', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<LayoutCell />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
