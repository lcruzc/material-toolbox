// @flow strict
import React from 'react';
import cx from 'classnames';

import type { Node } from 'react';

type Props = {
  children: Node,
  className?: string,
};

const Inner = ({ children, className }: Props) => (
  <div className={cx(className, 'mdc-layout-grid__inner')}>{children}</div>
);

export default Inner;
