// @flow strict
import React from 'react';
import cx from 'classnames';

import type { ElementType } from 'react';

type phoneCols = 1 | 2 | 3 | 4;
type tabletCols = phoneCols | 5 | 6 | 7 | 8;
type desktopCols = tabletCols | 9 | 10 | 11 | 12;

type Props = {
  align?: 'top' | 'middle' | 'bottom',
  className?: string,
  span?: desktopCols,
  order?: desktopCols,
  phone?: phoneCols,
  tablet?: tabletCols,
  desktop?: desktopCols,
  as?: ElementType,
};

const BASE = 'mdc-layout-grid__cell';

const Cell = ({
  align,
  className,
  span,
  order,
  phone,
  tablet,
  desktop,
  as: Component = 'div',
  ...rest
}: Props) => (
  <Component
    {...(rest: mixed)}
    className={cx(
      BASE,
      {
        [`${BASE}--span-${span || ''}`]: span,
        [`${BASE}--span-${tablet || ''}-tablet`]: tablet,
        [`${BASE}--span-${phone || ''}-phone`]: phone,
        [`${BASE}--span-${desktop || ''}-desktop`]: desktop,
        [`${BASE}--order-${order || ''}`]: order,
        [`${BASE}--align-${align || ''}`]: align,
      },
      className,
    )}
  />
);

export default Cell;
