// @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  className?: string,
  align?: 'left' | 'right',
  fixed?: boolean,
};

const Grid = ({ className, fixed = false, align, ...rest }: Props) => {
  const props = {
    ...rest,
    className: cx('mdc-layout-grid', className, {
      'mdc-layout-grid--fixed-column-width': fixed,
      [`mdc-layout-grid--align-${align || ''}`]: align,
    }),
  };

  return <div {...props} />;
};

export default Grid;
