// @flow strict
import React, { useReducer, useEffect, useRef } from 'react';
import cx from 'classnames';

import MDCTabScrollerFoundation from '@material/tab-scroller/foundation';
import * as util from '@material/tab-scroller/util';

import { matches } from '@material/dom/ponyfill';
import { applyPassive } from '@material/dom/events';

import type { Node } from 'react';

import { mtStylesReducer, mtClassNamesReducer } from '../reducers';
import { normalizePropToReactStyle } from '../utils';

import type { HTMLTabScrollerElement } from '../mt-types';

type Props = {
  children: Node,
  align?: 'start' | 'end' | 'center',
};

const TabScroller = ({ children, align, ...rest }: Props) => {
  const rootRef = useRef<?HTMLDivElement>();
  const contentRef = useRef<?HTMLDivElement>();
  const areaRef = useRef<?HTMLDivElement>();

  const [mtRootClassName, mtRootClassNameDispatch] = useReducer(
    mtClassNamesReducer,
    new Set<string>(),
  );

  const [mtAreaClassName, mtAreaClassNameDispatch] = useReducer(
    mtClassNamesReducer,
    new Set<string>(),
  );

  const [mtContentStyle, mtContentStyleDispatch] = useReducer(
    mtStylesReducer,
    {},
  );

  const [mtAreaStyle, mtAreaStyleDispatch] = useReducer(mtStylesReducer, {});

  useEffect(() => {
    const foundation = new MDCTabScrollerFoundation({
      eventTargetMatchesSelector: (evtTarget, selector) =>
        matches(evtTarget, selector),
      addClass: mClassName =>
        mtRootClassNameDispatch({ type: 'add', className: mClassName }),
      removeClass: mClassName =>
        mtRootClassNameDispatch({ type: 'remove', className: mClassName }),
      addScrollAreaClass: mClassName =>
        mtAreaClassNameDispatch({ type: 'add', className: mClassName }),
      setScrollAreaStyleProperty: (prop, value) =>
        mtAreaStyleDispatch({
          type: 'set',
          name: normalizePropToReactStyle(prop),
          value,
        }),
      setScrollContentStyleProperty: (prop, value) =>
        mtContentStyleDispatch({
          type: 'set',
          name: normalizePropToReactStyle(prop),
          value,
        }),
      getScrollContentStyleValue: propName =>
        window.getComputedStyle(contentRef.current).getPropertyValue(propName),
      setScrollAreaScrollLeft: scrollX => {
        if (areaRef.current) {
          areaRef.current.scrollLeft = scrollX;
        }
      },
      getScrollAreaScrollLeft: () =>
        areaRef.current && areaRef.current.scrollLeft,
      getScrollContentOffsetWidth: () =>
        contentRef.current && contentRef.current.offsetWidth,
      getScrollAreaOffsetWidth: () =>
        areaRef.current && areaRef.current.offsetWidth,
      computeScrollAreaClientRect: () =>
        areaRef.current && areaRef.current.getBoundingClientRect(),
      computeScrollContentClientRect: () =>
        contentRef.current && contentRef.current.getBoundingClientRect(),
      computeHorizontalScrollbarHeight: () =>
        util.computeHorizontalScrollbarHeight(document),
    });

    foundation.init();

    const content = contentRef.current;
    const area = areaRef.current;

    const handleInteraction = (...handleInteractionRest) =>
      foundation.handleInteraction(...handleInteractionRest);

    const handleTransitionEnd = (...handleTransitionEndRest) =>
      foundation.handleTransitionEnd(...handleTransitionEndRest);

    if (area) {
      area.addEventListener('wheel', handleInteraction, applyPassive());
      area.addEventListener('touchstart', handleInteraction, applyPassive());
      area.addEventListener('pointerdown', handleInteraction, applyPassive());
      area.addEventListener('mousedown', handleInteraction, applyPassive());
      area.addEventListener('keydown', handleInteraction, applyPassive());
    }

    if (content) {
      content.addEventListener('transitionend', handleTransitionEnd);
    }

    if (rootRef.current && contentRef.current) {
      // flowlint-next-line unclear-type:off
      const root: HTMLTabScrollerElement = (rootRef.current: any);

      root.foundation = foundation;
      root.content = contentRef.current;
    }

    return () => {
      if (area) {
        area.removeEventListener('wheel', handleInteraction, applyPassive());
        area.removeEventListener(
          'touchstart',
          handleInteraction,
          applyPassive(),
        );
        area.removeEventListener(
          'pointerdown',
          handleInteraction,
          applyPassive(),
        );
        area.removeEventListener(
          'mousedown',
          handleInteraction,
          applyPassive(),
        );
        area.removeEventListener('keydown', handleInteraction, applyPassive());
      }

      if (content) {
        content.removeEventListener('transitionend', handleTransitionEnd);
      }

      foundation.destroy();
    };
  }, []);

  return (
    <div
      ref={rootRef}
      {...(rest: mixed)}
      className={cx('mdc-tab-scroller', ...Array.from(mtRootClassName))}
    >
      <div
        ref={areaRef}
        style={mtAreaStyle}
        className={cx(
          'mdc-tab-scroller__scroll-area',
          ...Array.from(mtAreaClassName),
          {
            'mdc-tab-scroller--align-start': align === 'start',
            'mdc-tab-scroller--align-end': align === 'end',
            'mdc-tab-scroller--align-center': align === 'center',
          },
        )}
      >
        <div
          style={mtContentStyle}
          ref={contentRef}
          className={cx('mdc-tab-scroller__scroll-content')}
        >
          {children}
        </div>
      </div>
    </div>
  );
};

export default TabScroller;
