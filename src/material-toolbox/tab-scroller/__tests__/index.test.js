// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TabScroller from '../index';

afterEach(cleanup);

describe('component::TabScroller', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<TabScroller>Children</TabScroller>);
    expect(container.firstChild).toMatchSnapshot();
  });

  ['start', 'end', 'center'].forEach(align => {
    it(`Should match snapshoot ${align}`, () => {
      const { container } = render(
        <TabScroller align={align}>Children</TabScroller>,
      );
      expect(container.firstChild).toMatchSnapshot();
    });
  });
});
