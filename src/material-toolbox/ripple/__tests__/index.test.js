// @flow strict
import React from 'react';
import cx from 'classnames';

import { render, cleanup } from '@testing-library/react';

import type { Node } from 'react';

import withRipple from '../index';

import type { Style } from '../../mt-types';

type Props = {|
  className?: string,
  children?: Node,
|};

type DivProps = {
  rippleClassName: string,
  rippleStyle: Style,
  activator: {| current: ?HTMLDivElement |},
  surface?: {| current: ?HTMLElement |},
  ...Props,
};

const Div = ({
  className,
  activator,
  surface,
  rippleStyle,
  rippleClassName,
  ...rest
}: DivProps) => (
  <div
    {...rest}
    style={rippleStyle}
    ref={activator}
      tabIndex="0" // eslint-disable-line
    className={cx('ripple-demo-surface', rippleClassName, className)}
  />
);

const RippledDivBounded = withRipple<_, _, Props, _>(Div);
const RippleDivPrimary = withRipple<_, _, Props, _>(Div, { primary: true });
const RippleDivSecondary = withRipple<_, _, Props, _>(Div, { accent: true });
const RippleDivUnbounded = withRipple<_, _, Props, _>(Div, { unbounded: true });
const RippleDivDisabled = withRipple<_, _, Props, _>(Div, { disabled: true });

afterEach(cleanup);

describe('component::Button', () => {
  it('Should match snapshoot bounded', () => {
    const { container } = render(
      <RippledDivBounded>Bounded</RippledDivBounded>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot unbounded', () => {
    const { container } = render(
      <RippleDivUnbounded>Button</RippleDivUnbounded>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot disabled', () => {
    const { container } = render(<RippleDivDisabled>Button</RippleDivDisabled>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot primary', () => {
    const { container } = render(<RippleDivPrimary>Button</RippleDivPrimary>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot accent', () => {
    const { container } = render(
      <RippleDivSecondary>Button</RippleDivSecondary>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
