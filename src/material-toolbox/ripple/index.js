// @flow strict
import React, { useRef, useReducer, useEffect } from 'react';
import cx from 'classnames';

import * as util from '@material/ripple/util';

import MDCRippleFoundation from '@material/ripple/foundation';

import { applyPassive } from '@material/dom/events';
import { matches } from '@material/dom/ponyfill';

import type { AbstractComponent } from 'react';

import { mtStylesReducer, mtClassNamesReducer } from '../reducers';
import { getDisplayName } from '../utils';

import type { Style } from '../mt-types';

export const useRipple = <+A: HTMLElement, +S: HTMLElement>(
  {
    surface,
    activator,
    disabled = false,
    unbounded = false,
    primary = false,
    accent = false,
  }: {
    activator: {| current: ?A |},
    surface?: {| current: ?S |},
    disabled?: boolean,
    unbounded?: boolean,
    primary?: boolean,
    accent?: boolean,
  },
  partialAdapter: mixed,
) => {
  const mdcRipple = useRef();
  const [rippleStyle, distpachMTStyle] = useReducer(mtStylesReducer, {});
  const [mtClassName, dispatchClassName] = useReducer(
    mtClassNamesReducer,
    new Set<string>(),
  );

  useEffect(() => {
    let rippleActive = true;
    const domActivator = activator.current;
    const domSurface =
      surface && surface.current ? surface.current : domActivator;

    dispatchClassName({
      type: 'reset',
      className: cx({
        'mdc-ripple-surface--primary': primary,
        'mdc-ripple-surface--accent': accent,
      }),
    });

    mdcRipple.current = new MDCRippleFoundation({
      addClass: mClassName =>
        rippleActive &&
        dispatchClassName({ type: 'add', className: mClassName }),
      removeClass: mClassName =>
        rippleActive &&
        dispatchClassName({ type: 'remove', className: mClassName }),
      updateCssVariable: (varName, value) =>
        rippleActive && distpachMTStyle({ type: 'set', name: varName, value }),
      deregisterInteractionHandler: (evtType, handler) =>
        domActivator &&
        domActivator.removeEventListener(evtType, handler, applyPassive()),
      registerInteractionHandler: (evtType, handler) =>
        domActivator &&
        domActivator.addEventListener(evtType, handler, applyPassive()),
      browserSupportsCssVars: () => util.supportsCssVariables(window),
      isUnbounded: () => unbounded,
      isSurfaceActive: () => matches(domSurface, ':active'),
      isSurfaceDisabled: () => disabled,
      registerDocumentInteractionHandler: (evtType, handler) =>
        document.documentElement &&
        document.documentElement.addEventListener(
          evtType,
          handler,
          applyPassive(),
        ),
      deregisterDocumentInteractionHandler: (evtType, handler) =>
        document.documentElement &&
        document.documentElement.removeEventListener(
          evtType,
          handler,
          applyPassive(),
        ),
      registerResizeHandler: handler =>
        window.addEventListener('resize', handler),
      deregisterResizeHandler: handler =>
        window.removeEventListener('resize', handler),
      computeBoundingRect: () => {
        if (
          domSurface != null && // NOTE.- this compare null and undefined
          domSurface.getBoundingClientRect
        ) {
          return domSurface.getBoundingClientRect();
        }
        return {};
      },
      getWindowPageOffset: () => ({
        x: window.pageXOffset,
        y: window.pageYOffset,
      }),
      containsEventTarget: target => {
        // NOTE.- this compare null and undefined
        if (domActivator != null) {
          domActivator.contains(target);
        }
      },
      ...partialAdapter,
    });

    mdcRipple.current.init();
    return () => {
      rippleActive = false;
      if (mdcRipple.current) {
        mdcRipple.current.destroy();
      }
    };
  }, [
    accent,
    activator,
    disabled,
    partialAdapter,
    primary,
    surface,
    unbounded,
  ]);

  return {
    ripple: mdcRipple.current,
    rippleClassName: mtClassName,
    rippleStyle,
  };
};

const withRipple = <+A: HTMLElement, +S: HTMLElement, Config, Instance>(
  Component: AbstractComponent<
    {
      ...{|
        rippleClassName: string,
        rippleStyle: Style,
        activator: {| current: ?A |},
        surface?: {| current: ?S |},
      |},
      ...Config,
    },
    Instance,
  >,
  {
    unbounded,
    primary,
    accent,
    disabled,
  }: {|
    unbounded?: boolean,
    primary?: boolean,
    accent?: boolean,
    disabled?: boolean,
  |} = {
    unbounded: false,
    primary: false,
    accent: false,
    disabled: false,
  },
): AbstractComponent<Config, Instance> => {
  const Rippled = React.forwardRef<Config, Instance>((props: Config, ref) => {
    const surfaceRef = useRef<?HTMLElement>();
    const activatorRef = useRef<?HTMLElement>();

    const { rippleStyle, rippleClassName } = useRipple({
      surface: surfaceRef,
      activator: activatorRef,
      unbounded,
      primary,
      accent,
      disabled,
    });

    return (
      <Component
        ref={ref}
        {...props}
        rippleStyle={rippleStyle}
        rippleClassName={cx('mdc-ripple-surface', Array.from(rippleClassName))}
        activator={activatorRef}
        surface={surfaceRef}
      />
    );
  });

  Rippled.displayName = `WithRipple(${getDisplayName(Component)})`;

  return Rippled;
};

export default withRipple;
