// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import FloatingLabel from '../index';

afterEach(cleanup);

describe('component::FloatingLabel', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<FloatingLabel label="test" />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot with preFilled', () => {
    const { container } = render(<FloatingLabel label="test" preFilled />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
