// @flow strict
import React, {
  useImperativeHandle,
  useReducer,
  useRef,
  useEffect,
} from 'react';
import cx from 'classnames';

import MDCFloatingLabelFoundation from '@material/floating-label/foundation';

import { mtClassNamesReducer } from '../reducers';

import type { HTMLFloatinLabelElement } from '../mt-types';

type Props = {
  label?: string,
  preFilled?: boolean,
};

const FloatingLabel = React.forwardRef<Props, ?HTMLFloatinLabelElement>(
  ({ label, preFilled = false }: Props, ref) => {
    const rootRef = useRef<?HTMLSpanElement>();
    const [mtClassName, mtClassNameDispatch] = useReducer(
      mtClassNamesReducer,
      new Set<string>(preFilled ? ['mdc-floating-label--float-above'] : []),
    );

    useEffect(() => {
      const foundation = new MDCFloatingLabelFoundation({
        addClass: mClassName =>
          mtClassNameDispatch({ type: 'add', className: mClassName }),
        removeClass: mClassName =>
          mtClassNameDispatch({ type: 'remove', className: mClassName }),
        getWidth: () => rootRef.current && rootRef.current.offsetWidth,
        registerInteractionHandler: (evtType, handler) =>
          rootRef.current && rootRef.current.addEventListener(evtType, handler),
        deregisterInteractionHandler: (evtType, handler) =>
          rootRef.current &&
          rootRef.current.removeEventListener(evtType, handler),
      });

      foundation.init();

      // flowlint-next-line unclear-type:off
      const root: HTMLFloatinLabelElement = (rootRef.current: any);

      if (root) {
        root.foundation = foundation;
      }

      return () => {
        foundation.destroy();
      };
    }, []);

    // flowlint-next-line unclear-type:off
    useImperativeHandle(ref, () => (rootRef.current: any), []);

    return (
      <span
        className={cx('mdc-floating-label', ...Array.from(mtClassName))}
        ref={rootRef}
      >
        {label}
      </span>
    );
  },
);

FloatingLabel.displayName = 'FloatingLabel';

export default FloatingLabel;
