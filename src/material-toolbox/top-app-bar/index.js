// @flow strict
import React, { useEffect, useMemo, useRef, useReducer } from 'react';
import cx from 'classnames';

import { strings } from '@material/top-app-bar/constants';
import MDCTopAppBarFoundation from '@material/top-app-bar/standard/foundation';
import MDCShortTopAppBarFoundation from '@material/top-app-bar/short/foundation';
import MDCFixedTopAppBarFoundation from '@material/top-app-bar/fixed/foundation';

import { mtStylesReducer, mtClassNamesReducer } from '../reducers';

import type { Style } from '../mt-types';

type Props = {
  style?: Style,
  className?: string,
  fixed?: boolean,
  short?: boolean,
  collapsed?: boolean,
  prominent?: boolean,
  dense?: boolean,
  scrollTarget?: ?HTMLElement,
};

const TopAppBar = ({
  dense = false,
  short = false,
  collapsed,
  fixed = false,
  prominent = false,
  style,
  className,
  scrollTarget,
  ...rest
}: Props) => {
  const rootEl = useRef<?HTMLElement>();
  const foundationRef = useRef();
  const [mtStyle, distpachMTStyle] = useReducer(mtStylesReducer, {});
  const [mtClassName, dispatchClassName] = React.useReducer(
    mtClassNamesReducer,
    new Set<string>(),
  );

  useEffect(() => {
    const scrollTargetEl = scrollTarget || window;

    let mdcFoundation;

    const currentAdapter = {
      hasClass: mClassName =>
        rootEl.current && rootEl.current.classList.contains(mClassName),
      addClass: (mClassName: string): void =>
        dispatchClassName({ type: 'add', className: mClassName }),
      removeClass: (mClassName: string): void =>
        dispatchClassName({ type: 'remove', className: mClassName }),
      setStyle: (property, value) =>
        distpachMTStyle({ type: 'set', name: property, value }),
      getViewportScrollY: () => {
        return scrollTargetEl.pageYOffset !== undefined
          ? scrollTargetEl.pageYOffset
          : scrollTargetEl.scrollTop;
      },
      getTopAppBarHeight: () => rootEl.current && rootEl.current.clientHeight,
      getTotalActionItems: () =>
        rootEl.current &&
        rootEl.current.querySelectorAll(strings.ACTION_ITEM_SELECTOR).length,
    };

    if (short) {
      mdcFoundation = new MDCShortTopAppBarFoundation(currentAdapter);
    } else if (fixed) {
      mdcFoundation = new MDCFixedTopAppBarFoundation(currentAdapter);
    } else {
      mdcFoundation = new MDCTopAppBarFoundation(currentAdapter);
    }

    let handleWindowResize_;

    const handleTargetScroll_ = (...handlerRest) => {
      mdcFoundation.handleTargetScroll(...handlerRest);
    };

    scrollTargetEl.addEventListener('scroll', handleTargetScroll_);

    mdcFoundation.init();

    if (!short && !fixed) {
      handleWindowResize_ = (...handlerRest) => {
        mdcFoundation.handleWindowResize(...handlerRest);
      };

      window.addEventListener('resize', handleWindowResize_);
    }

    foundationRef.current = mdcFoundation;

    return () => {
      dispatchClassName({ type: 'reset', className: '' });

      scrollTargetEl.removeEventListener('scroll', handleTargetScroll_);

      if (handleWindowResize_) {
        window.removeEventListener('resize', handleWindowResize_);
      }

      mdcFoundation.destroy();
      foundationRef.current = null;
    };
  }, [fixed, short, prominent, dense, scrollTarget]);

  useEffect(() => {
    const foundation = foundationRef.current;

    if (foundation && collapsed != null && foundation.setAlwaysCollapsed) {
      foundation.setAlwaysCollapsed(collapsed);
    }
  }, [collapsed]);

  const memoizedStyle = useMemo(() => ({ ...style, ...mtStyle }), [
    mtStyle,
    style,
  ]);

  return (
    <header
      {...rest}
      ref={rootEl}
      style={memoizedStyle}
      className={cx(
        'mdc-top-app-bar',
        {
          'mdc-top-app-bar--dense': dense,
          'mdc-top-app-bar--short': short,
          'mdc-top-app-bar--fixed': fixed,
          'mdc-top-app-bar--prominent': prominent,
        },
        ...Array.from(mtClassName),
        className,
      )}
    />
  );
};

export default TopAppBar;
