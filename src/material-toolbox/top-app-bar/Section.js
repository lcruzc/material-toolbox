// @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  end?: boolean,
  className?: string,
};

const TopAppBarSection = ({ className, end = false, ...rest }: Props) => (
  <section
    {...rest}
    role={end ? 'toolbar' : undefined}
    className={cx(
      'mdc-top-app-bar__section',
      {
        'mdc-top-app-bar__section--align-start': !end,
        'mdc-top-app-bar__section--align-end': end,
      },
      className,
    )}
  />
);

export default TopAppBarSection;
