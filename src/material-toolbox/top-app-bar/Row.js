// @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  className?: string,
};

const TopAppBarRow = ({ className, ...rest }: Props) => (
  <div {...rest} className={cx('mdc-top-app-bar__row', className)} />
);

export default TopAppBarRow;
