// @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  className?: string,
};

const TopAppBarTitle = ({ className, ...rest }: Props) => (
  <span {...rest} className={cx('mdc-top-app-bar__title', className)} />
);

TopAppBarTitle.defaultProps = {
  className: undefined,
};

export default TopAppBarTitle;
