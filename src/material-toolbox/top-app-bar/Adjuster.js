// @flow strict
import React, { useMemo } from 'react';
import cx from 'classnames';

import type { Node } from 'react';

type HoopProps = {
  short?: boolean,
  dense?: boolean,
  prominent?: boolean,
};

type Props = {
  ...HoopProps,
  children: Node,
};

export const useTopAppBarAdjuster = ({
  dense = false,
  short = false,
  prominent = false,
}: HoopProps = {}) =>
  useMemo(
    () =>
      cx({
        'mdc-top-app-bar--fixed-adjust': !dense && !short && !prominent,
        'mdc-top-app-bar--dense-fixed-adjust': dense,
        'mdc-top-app-bar--short-fixed-adjust': short,
        'mdc-top-app-bar--prominent-fixed-adjust': prominent,
        'mdc-top-app-bar--dense-prominent-fixed-adjust': prominent && dense,
      }),
    [dense, prominent, short],
  );

const TopAppBarAdjuster = ({ children, short, dense, prominent }: Props) => {
  const className = useTopAppBarAdjuster({
    short,
    dense,
    prominent,
  });

  return React.Children.map(children, child => (
    <child.type
      {...child.props}
      className={cx(className, child.props.className)}
    />
  ));
};

export default TopAppBarAdjuster;
