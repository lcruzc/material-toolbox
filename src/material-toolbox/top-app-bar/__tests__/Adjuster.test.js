// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TopAppBarAdjuster from '../Adjuster';

afterEach(cleanup);

describe('component::TopAppBarAdjuster', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <TopAppBarAdjuster>
        <div />
      </TopAppBarAdjuster>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot short', () => {
    const { container } = render(
      <TopAppBarAdjuster short>
        <div />
      </TopAppBarAdjuster>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot dense', () => {
    const { container } = render(
      <TopAppBarAdjuster dense>
        <div />
      </TopAppBarAdjuster>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot prominent', () => {
    const { container } = render(
      <TopAppBarAdjuster prominent>
        <div />
      </TopAppBarAdjuster>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot prominent and dense', () => {
    const { container } = render(
      <TopAppBarAdjuster prominent dense>
        <div />
      </TopAppBarAdjuster>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
