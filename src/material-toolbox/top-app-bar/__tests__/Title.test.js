// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TopAppBarTitle from '../Title';

afterEach(cleanup);

describe('component::TopAppBarTitle', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<TopAppBarTitle>Title</TopAppBarTitle>);
    expect(container.firstChild).toMatchSnapshot();
  });
});
