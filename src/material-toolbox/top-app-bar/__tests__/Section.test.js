// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TopAppBarSection from '../Section';

afterEach(cleanup);

describe('component::TopAppBarSection', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<TopAppBarSection />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot end', () => {
    const { container } = render(<TopAppBarSection end />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
