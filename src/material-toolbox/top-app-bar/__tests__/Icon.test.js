// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TopAppBarIcon from '../Icon';

afterEach(cleanup);

describe('component::TopAppBarIcon', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<TopAppBarIcon />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
