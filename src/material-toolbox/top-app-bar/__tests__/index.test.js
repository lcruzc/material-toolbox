// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TopAppBar from '../index';

afterEach(cleanup);

describe('component::TopAppBar', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<TopAppBar />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match fixed', () => {
    const { container } = render(<TopAppBar fixed />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match dense', () => {
    const { container } = render(<TopAppBar dense />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match prominent', () => {
    const { container } = render(<TopAppBar prominent />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match short', () => {
    const { container } = render(<TopAppBar short />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match short collapsed', () => {
    const { container } = render(<TopAppBar short collapsed />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
