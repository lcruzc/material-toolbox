// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TopAppBarRow from '../Row';

afterEach(cleanup);

describe('component::TopAppBarRow', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<TopAppBarRow />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
