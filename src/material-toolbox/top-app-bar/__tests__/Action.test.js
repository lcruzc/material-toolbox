// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TopAppBarAction from '../Action';

afterEach(cleanup);

describe('component::TopAppBarAction', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<TopAppBarAction label="Action" />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
