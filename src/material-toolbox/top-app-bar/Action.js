// @flow strict
import React, { useRef, useMemo } from 'react';
import cx from 'classnames';

import { useRipple } from '../ripple';

import type { Style } from '../mt-types';

type Props = {
  label: string,
  className?: string,
  style?: Style,
};

const TopAppBarAction = ({ label, className, style, ...rest }: Props) => {
  const iconRef = useRef();

  const { rippleStyle, rippleClassName } = useRipple({
    activator: iconRef,
    unbounded: true,
  });

  const memoizedStyle = useMemo(() => ({ ...style, ...rippleStyle }), [
    style,
    rippleStyle,
  ]);

  return (
    <button
      type="button"
      ref={iconRef}
      {...(rest: mixed)}
      aria-label={label}
      style={memoizedStyle}
      className={cx(
        'mdc-top-app-bar__action-item mdc-icon-button',
        className,
        ...Array.from(rippleClassName),
      )}
    />
  );
};

export default TopAppBarAction;
