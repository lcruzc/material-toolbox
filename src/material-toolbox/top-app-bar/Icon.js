// @flow strict
import React, { useRef, useMemo } from 'react';
import cx from 'classnames';

import { useRipple } from '../ripple';

import type { Style } from '../mt-types';

type Props = {
  className?: string,
  style?: Style,
};

const TopAppBarIcon = ({ className, style, ...rest }: Props) => {
  const iconRef = useRef();

  const { rippleStyle, rippleClassName } = useRipple({
    activator: iconRef,
    unbounded: true,
  });

  const memoizedStyle = useMemo(() => ({ ...style, ...rippleStyle }), [
    style,
    rippleStyle,
  ]);

  return (
    <button
      type="button"
      ref={iconRef}
      {...(rest: mixed)}
      style={memoizedStyle}
      className={cx(
        'mdc-top-app-bar__navigation-icon mdc-icon-button',
        className,
        ...Array.from(rippleClassName),
      )}
    />
  );
};

export default TopAppBarIcon;
