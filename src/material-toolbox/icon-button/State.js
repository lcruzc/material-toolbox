// @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  as?: 'i' | 'svg' | 'img',
  isOn?: boolean,
  className?: string,
};

const IconButtonState = ({
  as: Component = 'i',
  isOn = false,
  className,
  ...rest
}: Props) => (
  <Component
    {...rest}
    className={cx('mdc-icon-button__icon', className, {
      'mdc-icon-button__icon--on': isOn,
    })}
  />
);

export default IconButtonState;
