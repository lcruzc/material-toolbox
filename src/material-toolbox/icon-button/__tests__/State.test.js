// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import IconButtonState from '../State';

afterEach(cleanup);

describe('component::IconButtonState', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <IconButtonState className="material-icons">favorite</IconButtonState>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot isOn', () => {
    const { container } = render(
      <IconButtonState className="material-icons" isOn>
        favorite
      </IconButtonState>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot as img', () => {
    const { container } = render(<IconButtonState as="img" />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot as svg', () => {
    const { container } = render(
      <IconButtonState as="svg">
        <path fill="none" d="M0 0h24v24H0z" />
      </IconButtonState>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
