// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import IconButton from '../index';

afterEach(cleanup);

describe('component::IconButton', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <IconButton className="material-icons">favorite</IconButton>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot as toggle', () => {
    const { container } = render(
      <IconButton toggle offLabel="off" onLabel="on" onChange={() => {}}>
        favorite
      </IconButton>,
    );
    expect(container.firstChild).toMatchSnapshot();
    // TODO: emulate click and see the next status
  });
});
