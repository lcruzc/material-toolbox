// @flow strict
import React, { useRef, useMemo, useEffect, useReducer } from 'react';
import cx from 'classnames';

import MDCIconButtonToggleFoundation from '@material/icon-button/foundation';

import type { ElementType } from 'react';

import { useRipple } from '../ripple';
import { mtAttrsReducer, mtClassNamesReducer } from '../reducers';

import type { Style } from '../mt-types';

type Props = {
  as?: ElementType,
  on?: boolean,
  offLabel?: string,
  onLabel?: string,
  toggle?: boolean,
  onChange?: ({ isOn: boolean }) => void,
  className?: string,
  style?: Style,
};

const IconButton = ({
  as: Component = 'button',
  on = false,
  toggle = false,
  offLabel,
  onLabel,
  onChange,
  className,
  style,
  ...rest
}: Props) => {
  const rootRef = useRef();
  const foundationRef = useRef<?MDCIconButtonToggleFoundation>();

  const { rippleStyle, rippleClassName } = useRipple({
    activator: rootRef,
    unbounded: true,
  });

  const [mtClassName, mtClassNameDispatch] = useReducer(
    mtClassNamesReducer,
    new Set<string>(),
  );

  const [mtAttrs, mtAttrsDispatch] = useReducer(mtAttrsReducer, {});

  useEffect(() => {
    if (!toggle) {
      return () => {};
    }

    const foundation = new MDCIconButtonToggleFoundation({
      hasClass: mClassName =>
        rootRef.current && rootRef.current.classList.contains(mClassName),
      addClass: mClassName =>
        mtClassNameDispatch({ type: 'add', className: mClassName }),
      removeClass: mClassName =>
        mtClassNameDispatch({ type: 'remove', className: mClassName }),
      setAttr: (attrName, attrValue) =>
        mtAttrsDispatch({ type: 'set', name: attrName, value: attrValue }),
    });

    foundationRef.current = foundation;
    foundation.init();

    const handleClick = (...handleClickRest) =>
      foundation.handleClick(...handleClickRest);

    const root = rootRef.current;

    if (root) {
      root.addEventListener('click', handleClick);
    }

    return () => {
      if (root) {
        root.removeEventListener('click', handleClick);
      }

      foundation.destroy();
      foundationRef.current = null;
    };
  }, [toggle]);

  useEffect(() => {
    if (foundationRef.current) {
      foundationRef.current.toggle(on);
    }
  }, [on]);

  useEffect(() => {
    if (foundationRef.current) {
      foundationRef.current.adapter_.notifyChange = (...notifyChangeRest) =>
        onChange && onChange(...notifyChangeRest);
    }
  }, [onChange]);

  const memoizedStyle = useMemo(() => ({ ...style, ...rippleStyle }), [
    style,
    rippleStyle,
  ]);

  return (
    <Component
      {...(rest: mixed)}
      ref={rootRef}
      style={memoizedStyle}
      aria-label={
        foundationRef.current && foundationRef.current.isOn()
          ? onLabel
          : offLabel
      }
      aria-hidden={
        foundationRef.current && foundationRef.current.isOn()
          ? 'true'
          : undefined
      }
      {...mtAttrs}
      className={cx(
        'mdc-icon-button',
        className,
        ...Array.from(rippleClassName),
        ...Array.from(mtClassName),
      )}
    />
  );
};

export default IconButton;
