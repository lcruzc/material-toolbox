// @flow strict
import React, { useEffect, useRef, useReducer } from 'react';
import cx from 'classnames';

import MDCLinearProgressFoundation from '@material/linear-progress/foundation';

import { mtClassNamesReducer } from '../reducers';

type Props = {
  close?: boolean,
  progress?: number,
  buffer?: number,
  indeterminate?: boolean,
  reversed?: boolean,
  className?: string,
};

const LinearProgress = ({
  close = false,
  indeterminate = false,
  reversed = false,
  progress,
  buffer,
  className,
  ...rest
}: Props) => {
  const foundationRef = useRef<?MDCLinearProgressFoundation>();
  const rootRef = useRef<?HTMLDivElement>();
  const bufferRef = useRef<?HTMLDivElement>();
  const primaryBarRef = useRef<?HTMLDivElement>();

  const [mtClassName, mtClassNameDispatch] = useReducer(
    mtClassNamesReducer,
    new Set<string>(),
  );

  useEffect(() => {
    const foundation = new MDCLinearProgressFoundation({
      addClass: mClassName =>
        mtClassNameDispatch({ type: 'add', className: mClassName }),
      getBuffer: () => bufferRef.current,
      getPrimaryBar: () => primaryBarRef.current,
      hasClass: mClassName =>
        rootRef.current && rootRef.current.classList.contains(mClassName),
      removeClass: mClassName =>
        mtClassNameDispatch({ type: 'remove', className: mClassName }),
      setStyle: (el: HTMLElement, styleProperty: string, value: string) =>
        el.style.setProperty(styleProperty, value),
    });

    foundation.init();
    foundationRef.current = foundation;

    return () => {
      foundation.destroy();
      foundationRef.current = null;
    };
  }, [reversed, indeterminate]);

  useEffect(() => {
    if (foundationRef.current) {
      foundationRef.current.setBuffer(buffer);
    }
  }, [buffer]);

  useEffect(() => {
    if (foundationRef.current) {
      foundationRef.current.setProgress(progress);
    }
  }, [progress]);

  useEffect(() => {
    if (foundationRef.current) {
      if (close) {
        foundationRef.current.close();
      } else {
        foundationRef.current.open();
      }
    }
  }, [close]);

  return (
    <div
      {...rest}
      ref={rootRef}
      role="progressbar"
      className={cx('mdc-linear-progress', ...Array.from(mtClassName), {
        'mdc-linear-progress--indeterminate': indeterminate,
        'mdc-linear-progress--reversed': reversed,
      })}
    >
      <div className="mdc-linear-progress__buffering-dots" />
      <div ref={bufferRef} className="mdc-linear-progress__buffer" />
      <div
        ref={primaryBarRef}
        className="mdc-linear-progress__bar mdc-linear-progress__primary-bar"
      >
        <span className="mdc-linear-progress__bar-inner" />
      </div>
      <div className="mdc-linear-progress__bar mdc-linear-progress__secondary-bar">
        <span className="mdc-linear-progress__bar-inner" />
      </div>
    </div>
  );
};

export default LinearProgress;
