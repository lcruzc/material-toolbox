// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import LinearProgress from '../index';

afterEach(cleanup);

describe('component::IconButton', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <LinearProgress progress={0.5} buffer={0.75} />,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot as indeterminate', () => {
    const { container } = render(<LinearProgress indeterminate />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot as reversed', () => {
    const { container } = render(<LinearProgress progress={0.5} reversed />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot as reversed buffered', () => {
    const { container } = render(
      <LinearProgress progress={0.5} buffer={0.75} reversed />,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
