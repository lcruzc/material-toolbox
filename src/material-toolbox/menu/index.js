// @flow strict
import React, {
  useEffect,
  useRef,
  useCallback,
  useImperativeHandle,
} from 'react';
import cx from 'classnames';

import MDCMenuFoundation from '@material/menu/foundation';
import { closest } from '@material/dom/ponyfill';

import type { Node } from 'react';

import MenuSurface from '../menu-surface';
import List from '../list';

import type {
  HTMLMenuSurfaceElement,
  HTMLListElement,
  HTMLListItemElement,
} from '../mt-types';

const noOp = () => {};

type Props = {
  className?: string,
  children?: Node,
  onOpened?: () => void,
  onSelected?: ({ index: number, item: HTMLListItemElement }) => void,
  selectedIndex?: number,
  defaultFocusState?: number,
  role?: 'listbox' | 'menu',
  wrapFocus?: boolean,
};

const Menu = React.forwardRef<Props, ?HTMLMenuSurfaceElement>(
  (
    {
      className,
      children,
      onOpened = noOp,
      onSelected = noOp,
      selectedIndex,
      defaultFocusState,
      role = 'menu',
      wrapFocus = true,
      ...rest
    }: Props,
    ref,
  ) => {
    const rootRef = useRef<?HTMLMenuSurfaceElement>();
    const listRef = useRef<?HTMLListElement>();
    const foundationRef = useRef<?MDCMenuFoundation>();

    useEffect(() => {
      let isMenuActive = true;

      const foundation = new MDCMenuFoundation({
        addClassToElementAtIndex: (index, mClassName) => {
          const list = listRef.current;

          if (list) {
            list.listElements[index].addMtClassName(mClassName);
          }
        },
        removeClassFromElementAtIndex: (index, mClassName) => {
          const list = listRef.current;

          if (list) {
            list.listElements[index].removeMtClassName(mClassName);
          }
        },
        addAttributeToElementAtIndex: (index, attr, value) => {
          const list = listRef.current;

          if (list) {
            list.listElements[index].setMtAttribute(attr, value);
          }
        },
        removeAttributeFromElementAtIndex: (index, attr) => {
          const list = listRef.current;

          if (list) {
            list.listElements[index].removeMtAttribute(attr);
          }
        },
        elementContainsClass: (element, mClassName) =>
          element.classList.contains(mClassName),
        closeSurface: (skipRestoreFocus: boolean) =>
          isMenuActive &&
          (rootRef.current &&
            rootRef.current.foundation.close(skipRestoreFocus)),
        getElementIndex: element =>
          listRef.current ? listRef.current.listElements.indexOf(element) : 0,
        getMenuItemCount: () =>
          listRef.current && listRef.current.listElements.length,
        focusItemAtIndex: index => {
          if (listRef.current) {
            listRef.current.listElements[index].focus();
          }
        },
        focusListRoot: () => {
          const element =
            rootRef.current &&
            rootRef.current.querySelector(
              MDCMenuFoundation.strings.LIST_SELECTOR,
            );

          if (element) {
            element.focus();
          }
        },
        isSelectableItemAtIndex: index =>
          !!closest(
            listRef.current && listRef.current.listElements[index],
            `.${MDCMenuFoundation.cssClasses.MENU_SELECTION_GROUP}`,
          ),
        getSelectedSiblingOfItemAtIndex: index => {
          const selectionGroupEl = closest(
            listRef.current && listRef.current.listElements[index],
            `.${MDCMenuFoundation.cssClasses.MENU_SELECTION_GROUP}`,
          );
          const selectedItemEl = selectionGroupEl.querySelector(
            `.${MDCMenuFoundation.cssClasses.MENU_SELECTED_LIST_ITEM}`,
          );
          return selectedItemEl && listRef.current
            ? // flowlint-next-line unclear-type:off
              (listRef.current: any).listElements.indexOf(selectedItemEl)
            : -1;
        },
        // Note. do nothing as MenuItem should handle this
        setEnabled: () => {},
      });

      foundation.init();

      foundationRef.current = foundation;

      const root = rootRef.current;

      const handleKeydown = (...args) => foundation.handleKeydown(...args);

      if (root) {
        root.items = listRef.current ? listRef.current.listElements : [];
        root.addEventListener('keydown', handleKeydown);
      }

      return () => {
        isMenuActive = false;

        if (root) {
          root.removeEventListener('keydown', handleKeydown);
          // $FlowFixMe
          delete root.items;
        }

        foundation.destroy();
        foundationRef.current = null;
      };
    }, [className]);

    useEffect(() => {
      if (foundationRef.current && listRef.current) {
        const { listElements } = listRef.current;
        foundationRef.current.adapter_.notifySelected = ({ index }) => {
          onSelected({ index, item: listElements[index] });
        };
      }
    }, [onSelected]);

    useEffect(() => {
      if (foundationRef.current && defaultFocusState != null) {
        foundationRef.current.setDefaultFocusState(defaultFocusState);
      }
    }, [defaultFocusState]);

    useEffect(() => {
      if (foundationRef.current && selectedIndex != null) {
        foundationRef.current.setSelectedIndex(selectedIndex);
      }
    }, [selectedIndex]);

    useImperativeHandle(ref, () => rootRef.current, []);

    const handleItemAction = useCallback(index => {
      if (foundationRef.current && listRef.current && !Array.isArray(index)) {
        const item = listRef.current.listElements[index];
        foundationRef.current.handleItemAction(item);
      }
    }, []);

    const handleMenuSurfaceOpened = useCallback(
      (...args) => {
        if (foundationRef.current) {
          foundationRef.current.handleMenuSurfaceOpened(...args);
          onOpened(...args);
        }
      },
      [onOpened],
    );

    return (
      <MenuSurface
        {...rest}
        ref={rootRef}
        onOpened={handleMenuSurfaceOpened}
        className={cx('mdc-menu', className)}
      >
        <List
          ref={listRef}
          role={role}
          aria-hidden="true"
          tabIndex="-1"
          wrapFocus={wrapFocus}
          onSelected={handleItemAction}
        >
          {children}
        </List>
      </MenuSurface>
    );
  },
);

Menu.displayName = 'Menu';

export { DefaultFocusState } from '@material/menu/constants';
export { Corner } from '@material/menu-surface/constants';

export default Menu;
