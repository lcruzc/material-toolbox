// @flow strict
import React from 'react';

import type { Node } from 'react';

type Props = {
  children?: Node,
};

const MenuSelectionGroup = ({ children }: Props) => {
  return (
    <li>
      <ul className="mdc-menu__selection-group">{children}</ul>
    </li>
  );
};

export default MenuSelectionGroup;
