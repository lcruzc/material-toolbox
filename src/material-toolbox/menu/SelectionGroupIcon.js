// @flow strict
import React from 'react';
import cx from 'classnames';

import ListItemDetail from '../list/ItemDetail';

type Props = {
  className?: string,
};

const MenuSelectionGroupIcon = ({ className, ...rest }: Props) => {
  return (
    <ListItemDetail
      {...rest}
      className={cx(className, 'mdc-menu__selection-group-icon')}
    />
  );
};

export default MenuSelectionGroupIcon;
