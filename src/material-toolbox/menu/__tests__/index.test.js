// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Menu, { DefaultFocusState } from '../index';
import MenuSelectionGroup from '../SelectionGroup';
import MenuItem from '../Item';

afterEach(cleanup);

describe('component::Menu', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<Menu />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot selectedIndex', () => {
    const { container } = render(
      <Menu selectedIndex={0}>
        <MenuSelectionGroup>
          <MenuItem>Item 1</MenuItem>
          <MenuItem>Item 2</MenuItem>
        </MenuSelectionGroup>
      </Menu>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot defaultFocusState', () => {
    const { container } = render(
      <Menu defaultFocusState={DefaultFocusState.FIRST_ITEM} />,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
