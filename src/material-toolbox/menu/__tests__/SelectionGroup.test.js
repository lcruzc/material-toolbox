// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import MenuSelectionGroup from '../SelectionGroup';

afterEach(cleanup);

describe('component::MenuSelectionGroup', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<MenuSelectionGroup />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
