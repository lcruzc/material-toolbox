// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import MenuSelectionGroupIcon from '../SelectionGroupIcon';

afterEach(cleanup);

describe('component::MenuSelectionGroupIcon', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<MenuSelectionGroupIcon />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
