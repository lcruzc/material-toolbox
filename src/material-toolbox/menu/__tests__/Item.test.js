// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import MenuItem from '../Item';

afterEach(cleanup);

describe('component::MenuItem', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<MenuItem>Button</MenuItem>);
    expect(container.firstChild).toMatchSnapshot();
  });
});
