// @flow strict
export {
  default,
  withMenuSurfaceAnchor,
  useMenuSurfaceAnchor,
} from '../menu-surface/Anchor';
