// @flow strict
import React from 'react';
import ListItem from '../list/Item';

type Props = {};

const MenuItem = ({ ...rest }: Props) => <ListItem {...rest} role="menuitem" />;

export default MenuItem;
