// @flow strict

export const mtStylesReducer = (
  state: { [string]: string },
  action: {| type: string, name: string, value: string |},
) => {
  switch (action.type) {
    case 'set':
      return {
        ...state,
        [action.name]: action.value,
      };
    default:
      throw new Error();
  }
};

export const mtAttrsReducer = (
  state: { [string]: ?string | ?number | ?boolean },
  action: {|
    type: string,
    name: string,
    value?: string | boolean | number,
  |},
) => {
  switch (action.type) {
    case 'set':
      return {
        ...state,
        [action.name]: action.value,
      };
    case 'unset':
      return {
        ...state,
        [action.name]: undefined,
      };
    default:
      throw new Error();
  }
};

export const mtClassNamesReducer = (
  state: Set<string>,
  action: {| type: string, className: string |},
) => {
  switch (action.type) {
    case 'add':
      state.add(action.className);
      return new Set<string>(state);
    case 'remove':
      state.delete(action.className);
      return new Set<string>(state);
    case 'reset':
      if (action.className) {
        return new Set<string>([action.className]);
      }
      return new Set<string>();
    default:
      throw new Error('No reducer type definded');
  }
};
