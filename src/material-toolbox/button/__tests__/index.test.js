// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Button from '../index';

afterEach(cleanup);

describe('component::Button', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<Button>Button</Button>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot oulined', () => {
    const { container } = render(<Button outlined>Button</Button>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot raised', () => {
    const { container } = render(<Button raised>Button</Button>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot dense', () => {
    const { container } = render(<Button dense>Button</Button>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot unelevated', () => {
    const { container } = render(<Button unelevated>Button</Button>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot as anchor', () => {
    const { container } = render(<Button as="a">Button</Button>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot disabled', () => {
    const { container } = render(<Button disabled>Button</Button>);
    expect(container.firstChild).toMatchSnapshot();
  });
});
