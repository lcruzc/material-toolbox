// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ButtonIcon from '../Icon';

afterEach(cleanup);

describe('component::ButtonIcon', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <ButtonIcon className="material-icons">favorite</ButtonIcon>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot as svg', () => {
    const { container } = render(
      <ButtonIcon as="svg">
        <path fill="none" d="M0 0h24v24H0z" />
      </ButtonIcon>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
