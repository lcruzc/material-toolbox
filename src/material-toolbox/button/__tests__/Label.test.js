// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ButtonLabel from '../Label';

afterEach(cleanup);

describe('component::ButtonLabel', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<ButtonLabel>Button</ButtonLabel>);
    expect(container.firstChild).toMatchSnapshot();
  });
});
