// @flow strict
import React, { useRef, useMemo } from 'react';
import cx from 'classnames';

import type { ElementType } from 'react';

import { useRipple } from '../ripple';

import type { Style } from '../mt-types';

type Props = {
  as?: ElementType,
  dense?: boolean,
  outlined?: boolean,
  raised?: boolean,
  unelevated?: boolean,
  disabled?: boolean,
  className?: string,
  style?: Style,
};

const Button = ({
  as: Component = 'button',
  dense = false,
  outlined = false,
  raised = false,
  unelevated = false,
  disabled = false,
  className,
  style,
  ...rest
}: Props) => {
  const buttonRef = useRef();

  const { rippleStyle, rippleClassName } = useRipple({
    activator: buttonRef,
  });

  const memoizedStyle = useMemo(() => ({ ...style, ...rippleStyle }), [
    style,
    rippleStyle,
  ]);

  return (
    <Component
      {...(rest: mixed)}
      ref={buttonRef}
      disabled={disabled}
      style={memoizedStyle}
      className={cx('mdc-button', className, ...Array.from(rippleClassName), {
        'mdc-button--dense': dense,
        'mdc-button--outlined': outlined,
        'mdc-button--raised': raised,
        'mdc-button--unelevated': unelevated,
      })}
    />
  );
};

export default Button;
