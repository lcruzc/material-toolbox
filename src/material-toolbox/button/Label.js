// @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  className?: string,
};

const ButtonLabel = ({ className, ...rest }: Props) => {
  return <span {...rest} className={cx('mdc-button__label', className)} />;
};

export default ButtonLabel;
