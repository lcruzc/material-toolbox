// @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  as?: 'i' | 'svg',
  className?: string,
};

const ButtonIcon = ({ as: Component = 'i', className, ...rest }: Props) => (
  <Component {...rest} className={cx('mdc-button__icon', className)} />
);

export default ButtonIcon;
