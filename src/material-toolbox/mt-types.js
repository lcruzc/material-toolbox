// @flow strict
import MDCTabScrollerFoundation from '@material/tab-scroller/foundation';
import MDCTabFoundation from '@material/tab/foundation';
import MDCTabIndicatorFoundation from '@material/tab-indicator/foundation';
import MDCRippleFoundation from '@material/ripple/foundation';
import MDCFloatingLabelFoundation from '@material/floating-label/foundation';
import MDCLineRippleFoundation from '@material/line-ripple/foundation';
import MDCTextFieldHelperTextFoundation from '@material/textfield/helper-text/foundation';
import MDCTextFieldIconFoundation from '@material/textfield/icon/foundation';
import MDCNotchedOutlineFoundation from '@material/notched-outline/foundation';
import MDCTextFieldCharacterCounterFoundation from '@material/textfield/character-counter/foundation';
import MDCMenuSurfaceFoundation from '@material/menu-surface/foundation';
import MDCSelectHelperTextFoundation from '@material/select/helper-text/foundation';
import MDCSelectIconFoundation from '@material/select/icon/foundation';
import MDCChipFoundation from '@material/chips/chip/foundation';

export type Style = { [name: string]: string | boolean | number };

export type HTMLTabScrollerElement = HTMLDivElement & {
  foundation: MDCTabScrollerFoundation,
  content: HTMLDivElement,
};

export type HTMLTabElement = HTMLElement & {
  foundation: MDCTabFoundation,
  indicator: MDCTabIndicatorFoundation,
};

export type HTMLTabIndicatorElement = HTMLElement & {
  foundation: MDCTabIndicatorFoundation,
};

export type HTMLInputElementWithRipple = HTMLInputElement & {
  ripple: MDCRippleFoundation,
};

export type HTMLListItemElement = HTMLElement & {
  setMtAttribute: (name: string, value: string | number | boolean) => void,
};

export type HTMLListElement = HTMLLIElement & {
  listElements: Array<HTMLListItemElement>,
};

export type HTMLFloatinLabelElement = HTMLElement & {
  foundation: MDCFloatingLabelFoundation,
};

export type HTMLLineRippleElement = HTMLElement & {
  foundation: MDCLineRippleFoundation,
};

export type HTMLTextFieldHelperTextElement = HTMLElement & {
  foundation: MDCTextFieldHelperTextFoundation,
};

export type HTMLTextFieldIconElement = HTMLElement & {
  foundation: MDCTextFieldIconFoundation,
};

export type HTMLNotchedOutlineElement = HTMLElement & {
  foundation: MDCNotchedOutlineFoundation,
};

export type HTMLTextFieldCharacterCounterElement = HTMLElement & {
  foundation: MDCTextFieldCharacterCounterFoundation,
};

export type HTMLMenuSurfaceElement = HTMLElement & {
  foundation: MDCMenuSurfaceFoundation,
  items: Array<HTMLListItemElement>,
};

export type HTMLSelectHelperTextElement = HTMLElement & {
  foundation: MDCSelectHelperTextFoundation,
};

export type HTMLSelectIconElement = HTMLElement & {
  foundation: MDCSelectIconFoundation,
};

export type HTMLChipElement = HTMLButtonElement & {
  foundation: MDCChipFoundation,
  defaultSelected: ?boolean,
};

export type HTMLChipIconElement = HTMLElement & {
  mtAddClassName: (mClassName: string) => void,
  mtRemoveClassName: (mClassName: string) => void,
};

// Refactored

export type HTMLDataTableRowElement = HTMLTableRowElement & {
  mtAddClassName: (mClassName: string) => void,
  mtRemoveClassName: (mClassName: string) => void,
  mtSetAttr: (attr: string, value: string | number | boolean) => void,
};

export type HTMLMTCheckboxElement = HTMLInputElement & {
  mtSetChecked: (isChecked: boolean) => void,
  mtSetIndterminate: (isIndeterminate: boolean) => void,
  ripple: MDCRippleFoundation,
};

export type HTMLCheckboxElement = {
  root: ?HTMLDivElement,
  input: ?HTMLInputElement,
  checked: boolean,
  indeterminate: boolean,
};
