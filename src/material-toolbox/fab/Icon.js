//  @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  as?: 'span' | 'i' | 'img' | 'svg',
  className?: string,
};

const FABIcon = ({ as: Component = 'span', className, ...rest }: Props) => (
  <Component {...rest} className={cx('mdc-fab__icon', className)} />
);

export default FABIcon;
