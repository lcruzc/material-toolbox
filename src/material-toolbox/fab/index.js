// @flow strict
import React, { useRef, useMemo } from 'react';
import cx from 'classnames';

import type { ElementType } from 'react';

import { useRipple } from '../ripple';

import type { Style } from '../mt-types';

type Props = {
  label: string,
  as?: ElementType,
  exited?: boolean,
  mini?: boolean,
  extended?: boolean,
  disabled?: boolean,
  className?: string,
  style?: Style,
};

const FAB = ({
  as: Component = 'button',
  disabled = false,
  exited = false,
  extended = false,
  mini = false,
  label,
  className,
  style,
  ...rest
}: Props) => {
  const buttonRef = useRef();

  const { rippleStyle, rippleClassName } = useRipple({
    activator: buttonRef,
  });

  const memoizedStyle = useMemo(() => ({ ...style, ...rippleStyle }), [
    style,
    rippleStyle,
  ]);

  return (
    <Component
      {...(rest: mixed)}
      ref={buttonRef}
      disabled={disabled}
      style={memoizedStyle}
      aria-label={label}
      className={cx('mdc-fab', className, ...Array.from(rippleClassName), {
        'mdc-fab--extended': extended,
        'mdc-fab--mini': mini,
        'mdc-fab--exited': exited,
      })}
    />
  );
};

export default FAB;
