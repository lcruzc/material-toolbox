//  @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  className?: string,
};

const FABLabel = ({ className, ...rest }: Props) => (
  <span {...rest} className={cx('mdc-fab__label', className)} />
);

export default FABLabel;
