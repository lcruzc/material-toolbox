// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import FABIcon from '../Icon';

afterEach(cleanup);

describe('component::FABIcon', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<FABIcon>Label</FABIcon>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot with as prop', () => {
    const { container } = render(<FABIcon as="i">Label</FABIcon>);
    expect(container.firstChild).toMatchSnapshot();
  });
});
