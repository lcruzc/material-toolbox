// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import FABLabel from '../Label';

afterEach(cleanup);

describe('component::FABLabel', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<FABLabel>Label</FABLabel>);
    expect(container.firstChild).toMatchSnapshot();
  });
});
