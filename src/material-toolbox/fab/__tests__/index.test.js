// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import FAB from '../index';

afterEach(cleanup);

describe('component::FAB', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<FAB label="test" />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot mini', () => {
    const { container } = render(<FAB label="test" mini />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot extended', () => {
    const { container } = render(<FAB label="test" extended />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot exited', () => {
    const { container } = render(<FAB label="test" exited />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot with as prop', () => {
    const { container } = render(<FAB label="test" as="a" />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
