// @flow strict
import React, {
  useRef,
  useEffect,
  useMemo,
  useCallback,
  useState,
  useReducer,
} from 'react';
import cx from 'classnames';

import MDCSelectFoundation from '@material/select/foundation';

import type { Node } from 'react';

import FloatingLabel from '../floating-label';
import Menu, { Corner } from '../menu';
import LineRipple from '../line-ripple';
import NotchedOutline from '../notched-outline';

import { mtClassNamesReducer } from '../reducers';
import { useRipple } from '../ripple';

import type {
  HTMLListItemElement,
  HTMLMenuSurfaceElement,
  HTMLFloatinLabelElement,
  HTMLLineRippleElement,
  HTMLNotchedOutlineElement,
  HTMLSelectHelperTextElement,
  HTMLSelectIconElement,
  Style,
} from '../mt-types';

const { cssClasses, strings } = MDCSelectFoundation;

const noOp = () => {};

type Props = {
  'aria-controls'?: string,
  'aria-labelledby'?: string,
  children?: Node,
  className?: string,
  defaultValue?: string,
  disabled?: boolean,
  enhanced?: boolean,
  helperText?: Node,
  label?: string,
  leadingIcon?: Node,
  onChange?: ({ value: string, index: number }) => void,
  outlined?: boolean,
  required?: boolean,
  selectedIndex?: number,
  style?: Style,
  invalid?: boolean,
  value?: string,
};

const Select = ({
  label,
  outlined = false,
  value,
  defaultValue,
  disabled,
  style,
  className,
  enhanced = false,
  children,
  onChange = noOp,
  selectedIndex,
  helperText,
  required,
  invalid,
  leadingIcon,
  'aria-labelledby': ariaLabelledby,
  'aria-controls': ariaControls,
  ...rest
}: Props) => {
  const rootRef = useRef<?HTMLDivElement>();
  const menuRef = useRef<?HTMLMenuSurfaceElement>();
  const helperTextRef = useRef<?HTMLSelectHelperTextElement>();
  const leadingIconRef = useRef<?HTMLSelectIconElement>();
  const hiddenInputRef = useRef<?HTMLInputElement>();
  const targetRef = useRef<?HTMLElement>();
  const labelRef = useRef<?HTMLFloatinLabelElement>();
  const lineRippleRef = useRef<?HTMLLineRippleElement>();
  const notchedOutlineRef = useRef<?HTMLNotchedOutlineElement>();

  const foundationRef = useRef<?MDCSelectFoundation>();
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [mtClassName, mtClassNameDispatch] = useReducer(
    mtClassNamesReducer,
    new Set<string>(),
  );

  const withLabel = label != null;
  const withLineRipple = !outlined;
  const withFilledLabel = label != null && !outlined;
  const withNotchedOutline = outlined;
  const withRipple = !outlined;

  const { rippleClassName, rippleStyle } = useRipple({
    activator: targetRef,
    disabled: outlined || disabled,
  });

  useEffect(() => {
    const targetEl = targetRef.current;

    const setEnhancedSelectedIndex = ({
      item,
    }: {
      item: HTMLListItemElement,
    }) => {
      // const selectedItem = this.menu_!.items[index]; // TODO: if later is removed item
      // flowlint-next-line unclear-type:off
      const selectedText: ?HTMLDivElement = (targetRef.current: any);
      const hiddenInput = hiddenInputRef.current;
      const foundation = foundationRef.current;

      if (hiddenInput && selectedText) {
        selectedText.textContent = item ? item.textContent.trim() : '';
        // TODO here need to use setValue when use React controlled uncontrolled components
        hiddenInput.value = item
          ? item.getAttribute(strings.ENHANCED_VALUE_ATTR) || ''
          : '';
      }

      if (foundation) {
        foundation.layout();
      }
    };

    const inputSelectAdapter = enhanced
      ? {
          getValue: () => {
            if (menuRef.current) {
              const listItem = menuRef.current.querySelector(
                strings.SELECTED_ITEM_SELECTOR,
              );
              if (
                listItem &&
                listItem.hasAttribute(strings.ENHANCED_VALUE_ATTR)
              ) {
                return listItem.getAttribute(strings.ENHANCED_VALUE_ATTR) || '';
              }
            }
            return '';
          },
          setValue: (mValue: string) => {
            if (menuRef.current) {
              const element = (menuRef.current.querySelector(
                `[${strings.ENHANCED_VALUE_ATTR}="${mValue}"]`,
              ): any); // flowlint-line unclear-type:off
              setEnhancedSelectedIndex({ item: element });
            }
          },
          setSelectedIndex: index => setEnhancedSelectedIndex(index),
          // NOTE.- this is handled by react
          // setDisabled: (isDisabled: boolean) => {
          //   this.selectedText_!.setAttribute('tabindex', isDisabled ? '-1' : '0');
          //   this.selectedText_!.setAttribute('aria-disabled', isDisabled.toString());
          //   if (this.hiddenInput_) {
          //     this.hiddenInput_.disabled = isDisabled;
          //   }
          // },
          checkValidity: () => {
            if (required === true && !(disabled === false)) {
              // See notes for required attribute under https://www.w3.org/TR/html52/sec-forms.html#the-select-element
              // TL;DR: Invalid if no index is selected, or if the first index is selected and has an empty value.
              return Boolean(value);
              // return (
              //   this.selectedIndex !== -1 &&
              //   (this.selectedIndex !== 0 || Boolean(this.value))
              // );
            }
            return true;
          },
          setValid: (isValid: boolean) => {
            let type = 'add';

            if (isValid) {
              type = 'remove';
            }

            if (targetRef.current) {
              targetRef.current.setAttribute(
                'aria-invalid',
                (!isValid).toString(),
              );
            }
            mtClassNameDispatch({ type, className: cssClasses.INVALID });
          },
        }
      : {
          // flowlint-next-line unclear-type:off
          getValue: () => targetEl && ((targetEl: any): HTMLInputElement).value,
          setValue: (mValue: string) => {
            if (targetEl instanceof HTMLSelectElement) {
              targetEl.value = mValue;
            }
          },
          openMenu: () => undefined,
          closeMenu: () => undefined,
          isMenuOpen: () => false,
          setSelectedIndex: (index: number) => {
            if (targetEl instanceof HTMLSelectElement) {
              targetEl.selectedIndex = index;
            }
          },
          //  Note. this handled by react
          //   setDisabled: (isDisabled: boolean) => {
          //     this.nativeControl_!.disabled = isDisabled;
          //   },
          setValid: (isValid: boolean) => {
            let type = 'add';

            if (isValid) {
              type = 'remove';
            }

            mtClassNameDispatch({ type, className: cssClasses.INVALID });
          },
          checkValidity: () => {
            if (targetEl instanceof HTMLSelectElement) {
              targetEl.checkValidity();
            }
          },
        };

    const foundation = new MDCSelectFoundation({
      ...inputSelectAdapter,
      addClass: mClassName =>
        mtClassNameDispatch({ type: 'add', className: mClassName }),
      removeClass: mClassName =>
        mtClassNameDispatch({ type: 'remove', className: mClassName }),
      hasClass: (mClassName: string) =>
        rootRef.current && rootRef.current.classList.contains(mClassName),
      setRippleCenter: (normalizedX: number) =>
        lineRippleRef.current &&
        lineRippleRef.current.foundation.setRippleCenter(normalizedX),
      activateBottomLine: () =>
        lineRippleRef.current && lineRippleRef.current.foundation.activate(),
      deactivateBottomLine: () =>
        lineRippleRef.current && lineRippleRef.current.foundation.deactivate(),
      hasOutline: () => Boolean(notchedOutlineRef.current),
      notchOutline: (labelWidth: number) =>
        notchedOutlineRef.current &&
        notchedOutlineRef.current.foundation.notch(labelWidth),
      closeOutline: () =>
        notchedOutlineRef.current &&
        notchedOutlineRef.current.foundation.closeNotch(),
      floatLabel: (shouldFloat: boolean) =>
        labelRef.current && labelRef.current.foundation.float(shouldFloat),
      getLabelWidth: () =>
        labelRef.current ? labelRef.current.foundation.getWidth() : 0,
    });

    foundation.init();

    const handleKeydown = (...args) => foundation.handleKeydown(...args);
    const handleChange = (...args) => foundation.handleChange(...args);
    const handleFocus = (...args) => foundation.handleFocus(...args);
    const handleBlur = (...args) => foundation.handleBlur(...args);
    const handleClick = (evt: MouseEvent | TouchEvent) => {
      if (enhanced && targetRef.current) {
        targetRef.current.focus();
      }
      // flowlint-next-line unclear-type: off
      const targetClientRect = ((evt.target: any): Element).getBoundingClientRect();
      const xCoordinate =
        // flowlint unclear-type: off
        ((evt: any): TouchEvent).touches != null
          ? ((evt: any): TouchEvent).touches[0].clientX
          : ((evt: any): MouseEvent).clientX;
      // flowlint unclear-type: error

      foundation.handleClick(xCoordinate - targetClientRect.left);
    };

    if (targetEl) {
      targetEl.addEventListener('change', handleChange);
      targetEl.addEventListener('focus', handleFocus);
      targetEl.addEventListener('blur', handleBlur);
      targetEl.addEventListener('click', handleClick);

      if (enhanced) {
        targetEl.addEventListener('keydown', handleKeydown);
      }
    }

    foundationRef.current = foundation;

    return () => {
      if (targetEl) {
        targetEl.removeEventListener('change', handleChange);
        targetEl.removeEventListener('focus', handleFocus);
        targetEl.removeEventListener('blur', handleBlur);
        targetEl.removeEventListener('click', handleClick);

        if (enhanced) {
          targetEl.removeEventListener('keydown', handleKeydown);
        }
      }

      foundation.destroy();
      foundationRef.current = null;
    };
  }, [disabled, enhanced, required, value]);

  useEffect(() => {
    const root = rootRef.current;

    if (helperText != null && root) {
      helperTextRef.current = (root.querySelector(
        'mdc-select-helper-text',
      ): any); // flowlint-line unclear-type:off

      if (leadingIcon != null) {
        leadingIconRef.current = (root.querySelector(
          MDCSelectFoundation.strings.ICON_SELECTOR,
        ): any); // flowlint-line unclear-type:off
      }
    }

    const foundation = foundationRef.current;

    if (foundation) {
      foundation.helperText_ =
        helperTextRef.current && helperTextRef.current.foundation;
      foundation.leadingIcon_ =
        leadingIconRef.current && leadingIconRef.current.foundation;
    }

    return () => {
      helperTextRef.current = null;
      leadingIconRef.current = null;
    };
  }, [helperText, leadingIcon]);

  useEffect(() => {
    if (foundationRef.current && enhanced) {
      foundationRef.current.adapter_.openMenu = () => {
        if (!isOpen) {
          setIsOpen(true);
          if (targetRef.current) {
            targetRef.current.setAttribute('aria-expanded', 'true');
          }
        }
      };
      foundationRef.current.adapter_.closeMenu = () => {
        if (isOpen) {
          setIsOpen(false);
        }
      };
      foundationRef.current.adapter_.isMenuOpen = () => isOpen;
    }
  }, [enhanced, isOpen]);

  useEffect(() => {
    if (foundationRef.current && enhanced) {
      foundationRef.current.adapter_.notifyChange = (mValue: string) => {
        onChange({ value: mValue, index: -1 }); // TODO: use selectedIndex
      };
    }
  }, [enhanced, onChange]);

  useEffect(() => {
    if (foundationRef.current && value != null) {
      foundationRef.current.setValue(value);
    }
  }, [value]);

  useEffect(() => {
    if (foundationRef.current && disabled != null) {
      foundationRef.current.setDisabled(disabled);
    }
  }, [disabled]);

  useEffect(() => {
    if (foundationRef.current && disabled != null) {
      foundationRef.current.setDisabled(disabled);
    }
  }, [disabled]);

  useEffect(() => {
    if (foundationRef.current && invalid != null) {
      foundationRef.current.setValid(!invalid);
    }
  }, [invalid]);

  useEffect(() => {
    if (foundationRef.current && selectedIndex != null) {
      foundationRef.current.setSelectedIndex(selectedIndex);
    }
  }, [selectedIndex]);

  const handleOpened = useCallback((...args) => {
    if (foundationRef.current) {
      foundationRef.current.handleMenuOpened(...args);

      if (menuRef.current) {
        const listItem = menuRef.current.querySelector(
          strings.SELECTED_ITEM_SELECTOR,
        );
        if (listItem) {
          listItem.focus();
        }
      }
    }
  }, []);

  const handleClosed = useCallback((...args) => {
    const foundation = foundationRef.current;

    if (foundation) {
      foundation.handleMenuClosed(...args);

      if (targetRef.current) {
        targetRef.current.removeAttribute('aria-expanded');
      }

      if (document.activeElement !== targetRef.current) {
        foundation.handleBlur();
      }
      setIsOpen(false);
    }
  }, []);

  const handleSelected = useCallback((...args) => {
    if (foundationRef.current) {
      foundationRef.current.setSelectedIndex(...args);
    }
  }, []);

  const memoizedStyle = useMemo(() => ({ ...style, ...rippleStyle }), [
    rippleStyle,
    style,
  ]);

  const rootProps = {
    style: memoizedStyle,
    ref: rootRef,
    className: cx(
      'mdc-select',
      {
        [cssClasses.REQUIRED]: required,
        [cssClasses.WITH_LEADING_ICON]: leadingIcon !== undefined,
        'mdc-select--outlined': outlined,
      },
      ...Array.from(mtClassName),
      ...Array.from(withRipple ? rippleClassName : []),
      className,
    ),
  };

  return (
    <>
      <div {...rootProps}>
        {leadingIcon}
        <i className="mdc-select__dropdown-icon" />
        {enhanced ? (
          <>
            <input
              {...rest}
              ref={hiddenInputRef}
              type="hidden"
              disabled={disabled}
            />
            <div // eslint-disable-line jsx-a11y/role-supports-aria-props
              ref={targetRef}
              aria-required={required}
              aria-disabled={disabled}
              className="mdc-select__selected-text"
              role="button"
              aria-haspopup="listbox"
              aria-controls={ariaControls}
              aria-labelledby={ariaLabelledby}
              tabIndex={disabled === true ? '-1' : '0'} // eslint-disable-line jsx-a11y/no-noninteractive-tabindex
            />
            <Menu
              anchorCorner={Corner.BOTTOM_START}
              anchor={rootRef}
              hoisted
              wrapFocus={false}
              ref={menuRef}
              open={isOpen}
              role="listbox"
              className="mdc-select__menu"
              disabled={disabled}
              onOpened={handleOpened}
              onClosed={handleClosed}
              onSelected={handleSelected}
            >
              {children}
            </Menu>
          </>
        ) : (
          <select
            {...rest}
            aria-controls={ariaControls}
            aria-labelledby={ariaLabelledby}
            required={required}
            ref={targetRef}
            defaultValue={defaultValue}
            className="mdc-select__native-control"
          >
            {children}
          </select>
        )}
        {withFilledLabel && (
          <FloatingLabel
            label={label}
            ref={labelRef}
            preFilled={
              (value != null && value !== '') ||
              (defaultValue != null && defaultValue !== '')
            }
          />
        )}
        {withLineRipple && <LineRipple ref={lineRippleRef} />}
        {withNotchedOutline && (
          <NotchedOutline ref={notchedOutlineRef}>
            {withLabel && (
              <FloatingLabel
                label={label}
                ref={labelRef}
                preFilled={value != null || defaultValue != null}
              />
            )}
          </NotchedOutline>
        )}
      </div>
      {helperText}
    </>
  );
};

export default Select;
