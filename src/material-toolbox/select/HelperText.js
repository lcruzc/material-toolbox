// @flow strict
import React, {
  useReducer,
  useImperativeHandle,
  useEffect,
  useRef,
} from 'react';
import cx from 'classnames';

import MDCSelectHelperTextFoundation from '@material/select/helper-text/foundation';

import { mtClassNamesReducer, mtAttrsReducer } from '../reducers';

import type { HTMLSelectHelperTextElement } from '../mt-types';

type Props = {
  text: string,
  persistent?: boolean,
  validation?: boolean,
};

const SelectHelperText = React.forwardRef<Props, HTMLSelectHelperTextElement>(
  ({ text, persistent = false, validation = false, ...rest }: Props, ref) => {
    const rootRef = useRef<?HTMLParagraphElement>();
    const [mtClassName, mtClassNameDispatch] = useReducer(
      mtClassNamesReducer,
      new Set<string>(),
    );

    const [mtAttrs, mtAttrsDispatch] = useReducer(mtAttrsReducer, {});

    useEffect(() => {
      const foundation = new MDCSelectHelperTextFoundation({
        addClass: mClassName =>
          mtClassNameDispatch({ type: 'add', className: mClassName }),
        removeClass: mClassName =>
          mtClassNameDispatch({ type: 'remove', className: mClassName }),
        hasClass: className =>
          rootRef.current && rootRef.current.classList.contains(className),
        setAttr: (attr, value) =>
          mtAttrsDispatch({ type: 'set', name: attr, value }),
        removeAttr: attr => mtAttrsDispatch({ type: 'unset', name: attr }),
        // NOTE.- next is not needed already
        // setContent: (content) => {
        //   this.root_.textContent = content;
        // },
      });

      foundation.init();

      // flowlint-next-line unclear-type:off
      const root: HTMLSelectHelperTextElement = (rootRef.current: any);

      if (root) {
        root.foundation = foundation;
      }

      return () => {
        foundation.destroy();
      };
    }, []);

    // flowlint-next-line unclear-type:off
    useImperativeHandle(ref, () => (rootRef.current: any), []);

    return (
      <p
        {...rest}
        {...mtAttrs}
        className={cx('mdc-select-helper-text', ...Array.from(mtClassName), {
          'mdc-select-helper-text--persistent': persistent,
          'mdc-select-helper-text--validation-msg': validation,
        })}
        ref={rootRef}
      >
        {text}
      </p>
    );
  },
);

SelectHelperText.displayName = 'SelectHelperText';

export default SelectHelperText;
