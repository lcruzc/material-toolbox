// @flow strict
import React from 'react';
import ListItem from '../list/Item';

type Props = {
  value?: string | number | boolean,
};

const SelectOption = ({ value, ...rest }: Props) => (
  <ListItem {...rest} role="option" data-value={value} />
);

export default SelectOption;
