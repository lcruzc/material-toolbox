// @flow strict
import React, { useReducer, useEffect, useRef } from 'react';
import cx from 'classnames';

import MDCSelectIconFoundation from '@material/select/icon/foundation';

import { mtAttrsReducer } from '../reducers';

import type { HTMLSelectIconElement } from '../mt-types';

const noOp = () => {};

type Props = {
  clickable?: boolean,
  className?: string,
  onAction?: () => void,
};

const SelectIcon = ({
  className,
  clickable = false,
  onAction = noOp,
  ...rest
}: Props) => {
  const rootRef = useRef<?HTMLElement>();
  const foundationRef = useRef<?MDCSelectIconFoundation>();
  const [mtAttrs, mtAttrDispatch] = useReducer(mtAttrsReducer, {
    tabIndex: clickable ? 0 : -1,
    role: clickable ? MDCSelectIconFoundation.strings.ICON_ROLE : undefined,
  });

  useEffect(() => {
    const foundation = new MDCSelectIconFoundation({
      getAttr: attr => rootRef.current && rootRef.current.getAttribute(attr),
      setAttr: (attr, value) => {
        const name = { tabindex: 'tabIndex' }[attr] || attr;
        mtAttrDispatch({ type: 'set', name, value });
      },
      removeAttr: attr => {
        const name = { tabindex: 'tabIndex' }[attr] || attr;
        mtAttrDispatch({ type: 'unset', name });
      },
      // NOTE.- react will handle this so no need to enable
      // setContent: (content) => {
      //   this.root_.textContent = content;
      // },
      registerInteractionHandler: (evtType, handler) =>
        rootRef.current && rootRef.current.addEventListener(evtType, handler),
      deregisterInteractionHandler: (evtType, handler) =>
        rootRef.current &&
        rootRef.current.removeEventListener(evtType, handler),
    });

    // flowlint-next-line unclear-type:off
    const root: HTMLSelectIconElement = (rootRef.current: any);

    if (root) {
      root.foundation = foundation;
    }

    foundation.init();

    foundationRef.current = foundation;

    return () => {
      foundation.destroy();
      foundationRef.current = null;
    };
  }, []);

  useEffect(() => {
    const foundation = foundationRef.current;

    if (foundation) {
      foundation.adapter_.notifyIconAction = onAction;
    }
  }, [onAction]);

  return (
    <i
      {...rest}
      {...mtAttrs}
      ref={rootRef}
      className={cx('mdc-select__icon', className)}
    />
  );
};

export default SelectIcon;
