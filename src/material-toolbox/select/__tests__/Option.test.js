// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Option from '../Option';

afterEach(cleanup);

describe('component::Option', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<Option>Button</Option>);
    expect(container.firstChild).toMatchSnapshot();
  });
});
