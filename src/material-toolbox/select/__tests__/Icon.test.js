// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import SelectIcon from '../Icon';

afterEach(cleanup);

describe('component::SelectIcon', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<SelectIcon />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
