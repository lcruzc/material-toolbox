// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Select from '../index';
import SelectIcon from '../Icon';
import SelectHelperText from '../HelperText';

afterEach(cleanup);

describe('component::Select', () => {
  it('Should match snapshoot', () => {
    const component = render(<Select label="test" />);
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot disabled', () => {
    const component = render(<Select label="test" disabled />);
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot outlined', () => {
    const component = render(<Select label="test" outlined />);
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot defaultValue', () => {
    const component = render(<Select label="test" defaultValue="test" />);
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot invalid', () => {
    const component = render(<Select label="test" invalid />);
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot aria-controls', () => {
    const component = render(
      <Select
        helperText={<SelectHelperText text="this is a helper text" id="test" />}
        label="test"
        aria-controls="test"
      />,
    );
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot persistentHelperText', () => {
    const component = render(
      <Select
        label="test"
        helperText={
          <SelectHelperText text="this is a helper text" persistent id="test" />
        }
        aria-controls="test"
      />,
    );
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot validation HelperText', () => {
    const component = render(
      <Select
        label="test"
        helperText={<SelectHelperText text="this is a helper text" id="test" />}
        invalid
        aria-controls="test"
      />,
    );
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot icon leading', () => {
    const component = render(
      <Select
        leadingIcon={<SelectIcon className="material-icons">delete</SelectIcon>}
        label="test"
      />,
    );
    expect(component).toMatchSnapshot();
  });
});
