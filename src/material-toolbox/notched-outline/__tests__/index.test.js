// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import NotchedOutline from '../index';

afterEach(cleanup);

describe('component::NotchedOutline', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<NotchedOutline />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
