// @flow strict
import React, {
  useRef,
  useImperativeHandle,
  useEffect,
  useReducer,
} from 'react';
import cx from 'classnames';

import MDCNotchedOutlineFoundation from '@material/notched-outline/foundation';

import type { Node } from 'react';

import { mtClassNamesReducer } from '../reducers';

import type { HTMLNotchedOutlineElement } from '../mt-types';

type Props = {
  children?: Node,
};

const NotchedOutline = React.forwardRef<Props, HTMLNotchedOutlineElement>(
  ({ children }: Props, ref) => {
    const rootRef = useRef<?HTMLDivElement>();
    const notchElementRef = useRef<?HTMLDivElement>();
    const [mtClassName, mtClassNameDispatch] = useReducer(
      mtClassNamesReducer,
      new Set<string>(),
    );

    useEffect(() => {
      const foundation = new MDCNotchedOutlineFoundation({
        addClass: mClassName =>
          mtClassNameDispatch({ type: 'add', className: mClassName }),
        removeClass: mClassName =>
          mtClassNameDispatch({ type: 'remove', className: mClassName }),
        setNotchWidthProperty: width =>
          notchElementRef.current &&
          notchElementRef.current.style.setProperty('width', `${width}px`),
        removeNotchWidthProperty: () =>
          notchElementRef.current &&
          notchElementRef.current.style.removeProperty('width'),
      });

      foundation.init();

      // flowlint-next-line unclear-type:off
      const root: HTMLNotchedOutlineElement = (rootRef.current: any);

      if (root) {
        root.foundation = foundation;
      }

      return () => {
        foundation.destroy();
      };
    }, []);

    // flowlint-next-line unclear-type:off
    useImperativeHandle(ref, () => (rootRef.current: any), []);

    return (
      <div
        ref={rootRef}
        className={cx(
          'mdc-notched-outline',
          'mdc-notched-outline--upgraded',
          {
            'mdc-notched-outline--no-label': !children,
          },
          ...Array.from(mtClassName),
        )}
      >
        <div className="mdc-notched-outline__leading" />
        <div ref={notchElementRef} className="mdc-notched-outline__notch">
          {children}
        </div>
        <div className="mdc-notched-outline__trailing" />
      </div>
    );
  },
);

NotchedOutline.displayName = 'NotchedOutline';

export default NotchedOutline;
