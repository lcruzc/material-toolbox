// @flow strict
import React, {
  useRef,
  useMemo,
  useEffect,
  useLayoutEffect,
  useReducer,
  useState,
  useImperativeHandle,
} from 'react';

import cx from 'classnames';
import MDCCheckboxFoundation from '@material/checkbox/foundation';
import { getCorrectEventName } from '@material/animation/util';

import { mtClassNamesReducer, mtAttrsReducer } from '../reducers';
import { useRipple } from '../ripple';

import type {
  Style,
  HTMLMTCheckboxElement,
  HTMLCheckboxElement,
} from '../mt-types';

type Props = {
  indeterminate?: boolean,
  disabled?: boolean,
  className?: string,
  style?: Style,
};

const Checkbox = React.forwardRef<Props, HTMLCheckboxElement>(
  (
    { indeterminate, disabled: propDisabled, className, style, ...rest }: Props,
    ref,
  ) => {
    const rootRef = useRef<?HTMLDivElement>();
    const foundationRef = useRef<?MDCCheckboxFoundation>();
    const checkboxRef = useRef<?HTMLInputElement>();
    const [disabled, setDisabled] = useState<?boolean>(propDisabled);

    const [checkboxAttrs, checkboxAttrsDispatch] = useReducer(
      mtAttrsReducer,
      {},
    );

    const [mtCheckboxClassName, mtCheckboxClassNameDispatch] = useReducer(
      mtClassNamesReducer,
      new Set<string>(),
    );

    const { rippleStyle, rippleClassName, ripple } = useRipple({
      surface: rootRef,
      activator: checkboxRef,
      unbounded: true,
    });

    useLayoutEffect(() => {
      const checkbox = checkboxRef.current;
      if (checkbox && indeterminate != null) {
        checkbox.indeterminate = indeterminate;
      }
    }, [indeterminate]);

    useEffect(() => {
      const root = rootRef.current;
      // flowlint-next-line unclear-type:off
      const checkbox: HTMLMTCheckboxElement = (checkboxRef.current: any);

      const foundation = new MDCCheckboxFoundation({
        addClass: mClassName =>
          mtCheckboxClassNameDispatch({ type: 'add', className: mClassName }),
        removeClass: mClassName =>
          mtCheckboxClassNameDispatch({
            type: 'remove',
            className: mClassName,
          }),
        forceLayout: () => root && root.offsetWidth,
        hasNativeControl: () => !!checkbox,
        isAttachedToDOM: () => root && root.parentNode,
        isChecked: () => checkbox && checkbox.checked,
        isIndeterminate: () => checkbox && checkbox.indeterminate,
        setNativeControlAttr: (attr, value) =>
          checkboxAttrsDispatch({ type: 'set', name: attr, value }),
        removeNativeControlAttr: attr =>
          checkboxAttrsDispatch({ type: 'unset', name: attr }),
        setNativeControlDisabled: setDisabled,
      });

      foundationRef.current = foundation;

      const handleChange = (...args) => foundation.handleChange(...args);

      const handleAnimationEnd = (...args) =>
        foundation.handleAnimationEnd(...args);

      if (root) {
        root.addEventListener(
          getCorrectEventName(window, 'animationend'),
          handleAnimationEnd,
        );
      }

      if (checkbox) {
        checkbox.addEventListener('change', handleChange);
        checkbox.mtSetChecked = isChecked => {
          checkbox.checked = isChecked;
          foundation.handleChange();
        };
        checkbox.mtSetIndterminate = mtIndterminate => {
          checkbox.indeterminate = mtIndterminate;
          foundation.handleChange();
        };
      }

      foundation.init();

      return () => {
        if (root) {
          root.removeEventListener(
            getCorrectEventName(window, 'animationend'),
            handleAnimationEnd,
          );
        }

        if (checkbox) {
          checkbox.removeEventListener('change', handleChange);
        }

        foundation.destroy();
        foundationRef.current = null;
      };
    }, []);

    useEffect(() => {
      const foundation = foundationRef.current;

      if (foundation && indeterminate != null) {
        foundation.handleChange();
      }
    }, [indeterminate]);

    useEffect(() => {
      const foundation = foundationRef.current;

      if (foundation && propDisabled != null) {
        foundation.setDisabled(propDisabled);
      }
    }, [propDisabled]);

    useEffect(() => {
      // flowlint-next-line unclear-type:off
      const checkbox: HTMLMTCheckboxElement = (checkboxRef.current: any);
      const foundation = foundationRef.current;

      if (foundation && checkbox) {
        checkbox.ripple = ripple;
      }
    }, [ripple]);

    useImperativeHandle(
      ref,
      () => ({
        root: rootRef.current,
        input: checkboxRef.current,
        // flowlint-next-line unsafe-getters-setters:off
        set checked(isChecked) {
          // flowlint-next-line unclear-type:off
          const checkbox: HTMLMTCheckboxElement = (checkboxRef.current: any);
          if (checkbox) {
            checkbox.mtSetChecked(isChecked);
          }
        },
        // flowlint-next-line unsafe-getters-setters:off
        set indeterminate(isIndeterminate) {
          // flowlint-next-line unclear-type:off
          const checkbox: HTMLMTCheckboxElement = (checkboxRef.current: any);

          if (checkbox) {
            checkbox.mtSetIndterminate(isIndeterminate);
          }
        },
      }),
      [],
    );

    const memoizedStyle = useMemo(() => ({ ...style, ...rippleStyle }), [
      style,
      rippleStyle,
    ]);

    return (
      <div
        style={memoizedStyle}
        ref={rootRef}
        className={cx(
          'mdc-checkbox',
          className,
          ...Array.from(rippleClassName),
          ...Array.from(mtCheckboxClassName),
        )}
      >
        <input
          {...rest}
          {...checkboxAttrs}
          disabled={disabled}
          type="checkbox"
          className="mdc-checkbox__native-control"
          ref={checkboxRef}
        />
        <div className="mdc-checkbox__background">
          <svg className="mdc-checkbox__checkmark" viewBox="0 0 24 24">
            <path
              className="mdc-checkbox__checkmark-path"
              fill="none"
              stroke="white"
              d="M1.73,12.91 8.1,19.28 22.79,4.59"
            />
          </svg>
          <div className="mdc-checkbox__mixedmark" />
        </div>
      </div>
    );
  },
);

Checkbox.displayName = 'Checkbox';

export default Checkbox;
