// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Checkbox from '../index';

afterEach(cleanup);

describe('component::Checkbox', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<Checkbox />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot indeterminate', () => {
    const { container } = render(<Checkbox indeterminate />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
