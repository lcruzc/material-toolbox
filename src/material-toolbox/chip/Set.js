// @flow strict
import React, { useEffect, useRef, useImperativeHandle } from 'react';
import cx from 'classnames';

import MDCChipSetFoundation from '@material/chips/chip-set/foundation';
import MDCChipFoundation from '@material/chips/chip/foundation';

import type { HTMLChipElement } from '../mt-types';

type Props = {
  className?: string,
  input?: boolean,
  choice?: boolean,
  filter?: boolean,
};

const { strings } = MDCChipSetFoundation;
const chipStrings = MDCChipFoundation.strings;

const ChipSet = React.forwardRef<Props, _>(
  (
    {
      className,
      input = false,
      choice = false,
      filter = false,
      ...rest
    }: Props,
    ref,
  ) => {
    const rootRef = useRef<?HTMLDivElement>();

    useEffect(() => {
      const root = rootRef.current;

      const foundation = new MDCChipSetFoundation({
        hasClass: mClassName => root && root.classList.contains(mClassName),
        // removeChip: chipId => {
        //   const index = this.findChipIndex_(chipId);
        //   if (index >= 0) {
        //     this.chips_[index].destroy();
        //     this.chips_.splice(index, 1);
        //   }
        // },
        setSelected: (chipId, selected) => {
          if (root) {
            const chipElements: HTMLChipElement[] = [].slice.call(
              root.querySelectorAll(strings.CHIP_SELECTOR),
            );

            const chip = chipElements.find(c => c.id === chipId);

            if (chip) {
              chip.foundation.setSelected(selected);
            }
          }
        },
      });

      if (root) {
        [].slice
          .call(root.querySelectorAll(strings.CHIP_SELECTOR))
          .forEach(chip => {
            if (chip.defaultSelected === true) {
              foundation.select(chip.id);
            }
          });
      }

      const handleChipInteraction = evt =>
        foundation.handleChipInteraction(evt.detail.chipId);

      const handleChipSelection = evt =>
        foundation.handleChipSelection(evt.detail.chipId, evt.detail.selected);

      const handleChipRemoval = evt =>
        foundation.handleChipRemoval(evt.detail.chipId);

      if (root) {
        // $FlowFixMe
        root.addEventListener(chipStrings.SELECTION_EVENT, handleChipSelection);
        // $FlowFixMe
        root.addEventListener(chipStrings.REMOVAL_EVENT, handleChipRemoval);
        // $FlowFixMe
        root.addEventListener(
          chipStrings.INTERACTION_EVENT,
          handleChipInteraction,
        );
      }

      foundation.init();

      return () => {
        if (root) {
          // $FlowFixMe
          root.removeEventListener(
            chipStrings.SELECTION_EVENT,
            handleChipSelection,
          );
          // $FlowFixMe
          root.removeEventListener(
            chipStrings.REMOVAL_EVENT,
            handleChipRemoval,
          );
          // $FlowFixMe
          root.removeEventListener(
            chipStrings.INTERACTION_EVENT,
            handleChipInteraction,
          );
        }

        foundation.destroy();
      };
    }, []);

    useImperativeHandle(
      ref,
      () => ({
        root: rootRef.current,
      }),
      [],
    );

    return (
      <div
        {...rest}
        ref={rootRef}
        className={cx(
          'mdc-chip-set',
          {
            'mdc-chip-set--input': input,
            'mdc-chip-set--choice': choice,
            'mdc-chip-set--filter': filter,
          },
          className,
        )}
      />
    );
  },
);

ChipSet.displayName = 'ChipSet';

export default ChipSet;
