// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ChipIcon from '../Icon';

afterEach(cleanup);

describe('component::ChipIcon', () => {
  it('Should match snapshoot', () => {
    const component = render(<ChipIcon />);
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot trailing', () => {
    const component = render(<ChipIcon trailing />);
    expect(component).toMatchSnapshot();
  });
});
