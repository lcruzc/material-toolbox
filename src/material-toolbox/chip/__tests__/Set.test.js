// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Set from '../Set';

afterEach(cleanup);

describe('component::Set', () => {
  it('Should match snapshoot', () => {
    const component = render(<Set />);
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot choice', () => {
    const component = render(<Set choice />);
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot filter', () => {
    const component = render(<Set filter />);
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot input', () => {
    const component = render(<Set input />);
    expect(component).toMatchSnapshot();
  });
});
