// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Chip from '../index';
import ChipIcon from '../Icon';

afterEach(cleanup);

describe('component::Chip', () => {
  it('Should match snapshoot', () => {
    const component = render(<Chip label="test" />);
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot beginExit', () => {
    const component = render(<Chip beginExit label="test" />);
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot filter', () => {
    const component = render(<Chip filter label="test" />);
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot defaultSelected', () => {
    const component = render(<Chip defaultSelected label="test" />);
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot shouldRemoveOnTrailingIconClick', () => {
    const component = render(
      <Chip shouldRemoveOnTrailingIconClick={false} label="test" />,
    );
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot leadingIcon', () => {
    const component = render(
      <Chip
        label="test"
        leadingIcon={<ChipIcon className="material-icons">event</ChipIcon>}
      />,
    );
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot trailingIconElement', () => {
    const component = render(
      <Chip
        label="test"
        trailingIcon={<ChipIcon className="material-icons">event</ChipIcon>}
      />,
    );
    expect(component).toMatchSnapshot();
  });
});
