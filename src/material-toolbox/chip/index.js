// @flow strict
import React, { useEffect, useRef, useMemo, useState, useReducer } from 'react';
import cx from 'classnames';

import MDCChipFoundation from '@material/chips/chip/foundation';

import type { Node } from 'react';

import { createEvent } from '../utils';
import { useRipple } from '../ripple';
import {
  mtClassNamesReducer,
  mtAttrsReducer,
  mtStylesReducer,
} from '../reducers';

import type { Style, HTMLChipElement, HTMLChipIconElement } from '../mt-types';

type Props = {
  className?: string,
  style?: Style,
  label: string,
  defaultSelected?: boolean,
  filter?: boolean,
  leadingIcon?: Node,
  trailingIcon?: Node,
  shouldRemoveOnTrailingIconClick?: boolean,
  beginExit?: boolean,
  onSelected?: ({
    chipId: string,
    selected: boolean,
    element: HTMLChipElement,
  }) => void,
  onRemoval?: ({ chipId: string, element: HTMLChipElement }) => void,
  onInteraction?: ({ chipId: string, element: HTMLChipElement }) => void,
};

let chipIdCounter = 0;

const noOp = () => {};

const { strings } = MDCChipFoundation;

const Chip = ({
  className,
  style,
  label,
  defaultSelected,
  filter = false,
  leadingIcon,
  trailingIcon,
  shouldRemoveOnTrailingIconClick,
  beginExit,
  onInteraction = noOp,
  onRemoval = noOp,
  onSelected = noOp,
  ...rest
}: Props) => {
  const [chipId] = useState(() => {
    chipIdCounter += 1;
    return `mdc-chip-${chipIdCounter}`;
  });

  const rootRef = useRef<?HTMLButtonElement>();
  const checkmarkRef = useRef<?HTMLSpanElement>();
  const foundationRef = useRef<?MDCChipFoundation>();
  const leadingIconRef = useRef<?HTMLChipIconElement>();
  const trailingIconRef = useRef<?HTMLChipIconElement>();

  const [mtAttrs, mtAttrsDispatch] = useReducer(mtAttrsReducer, {});
  const [mtStyle, mtStyleDispatch] = useReducer(mtStylesReducer, {});
  const [mtClassName, mtClassNameDispatch] = useReducer(
    mtClassNamesReducer,
    new Set<string>(),
  );

  const partialAdapter = useMemo(
    () => ({
      computeBoundingRect: () =>
        foundationRef.current && foundationRef.current.getDimensions(),
    }),
    [],
  );

  const { rippleStyle, rippleClassName } = useRipple(
    {
      activator: rootRef,
      unbounded: true,
    },
    partialAdapter,
  );

  useEffect(() => {
    // flowlint-next-line unclear-type:off
    const root: HTMLChipElement = (rootRef.current: any);

    if (root) {
      root.defaultSelected = defaultSelected;
    }
  }, [defaultSelected]);

  useEffect(() => {
    // flowlint-next-line unclear-type:off
    const root: HTMLChipElement = (rootRef.current: any);

    if (leadingIcon != null && root) {
      leadingIconRef.current = (root.querySelector(
        strings.LEADING_ICON_SELECTOR,
      ): any); // flowlint-line unclear-type:off
    }

    if (trailingIcon != null && root) {
      trailingIconRef.current = (root.querySelector(
        strings.TRAILING_ICON_SELECTOR,
      ): any); // flowlint-line unclear-type:off
    }

    return () => {
      trailingIconRef.current = null;
      leadingIconRef.current = null;
    };
  }, [leadingIcon, trailingIcon]);

  useEffect(() => {
    // flowlint-next-line unclear-type:off
    const root: HTMLChipElement = (rootRef.current: any);
    const checkmark = checkmarkRef.current;
    const leadingIconElement = leadingIconRef.current;
    const trailingIconElement = trailingIconRef.current;

    const foundation = new MDCChipFoundation({
      addClass: mClassName =>
        mtClassNameDispatch({ type: 'add', className: mClassName }),
      addClassToLeadingIcon: mClassName =>
        leadingIconElement && leadingIconElement.mtAddClassName(mClassName),
      eventTargetHasClass: (target, mClassName) =>
        target ? target.classList.contains(mClassName) : false,
      getCheckmarkBoundingClientRect: () =>
        checkmark && checkmark.getBoundingClientRect(),
      getComputedStyleValue: propertyName =>
        window.getComputedStyle(root).getPropertyValue(propertyName),
      getRootBoundingClientRect: () => root && root.getBoundingClientRect(),
      hasClass: mClassName => root && root.classList.contains(mClassName),
      hasLeadingIcon: () => !!leadingIconElement,
      notifyTrailingIconInteraction: () => {
        if (rootRef.current) {
          rootRef.current.dispatchEvent(
            createEvent(
              strings.TRAILING_ICON_INTERACTION_EVENT,
              {
                chipId,
                element: root,
              },
              true /* bubble */,
            ),
          );
        }
      },
      removeClass: mClassName =>
        mtClassNameDispatch({ type: 'remove', className: mClassName }),
      removeClassFromLeadingIcon: mClassName =>
        leadingIconElement && leadingIconElement.mtRemoveClassName(mClassName),
      setAttr: (attr, value) =>
        mtAttrsDispatch({ type: 'set', name: attr, value }),
      setStyleProperty: (propertyName, value) =>
        mtStyleDispatch({ type: 'set', name: propertyName, value }),
    });

    foundation.init();

    const handleInteraction = (...args) =>
      foundation.handleInteraction(...args);

    const handleTransitioinEnd = (...args) =>
      foundation.handleTransitionEnd(...args);

    const handleTrailingIconInteraction = (...args) =>
      foundation.handleTrailingIconInteraction(...args);

    if (root) {
      root.addEventListener('click', handleInteraction);
      root.addEventListener('keydown', handleInteraction);
      root.addEventListener('transitionend', handleTransitioinEnd);

      root.foundation = foundation;
    }

    if (trailingIconElement) {
      trailingIconElement.addEventListener(
        'keydown',
        handleTrailingIconInteraction,
      );
      trailingIconElement.addEventListener(
        'click',
        handleTrailingIconInteraction,
      );
    }

    foundationRef.current = foundation;

    return () => {
      if (trailingIconElement) {
        trailingIconElement.removeEventListener(
          'keydown',
          handleTrailingIconInteraction,
        );
        trailingIconElement.removeEventListener(
          'click',
          handleTrailingIconInteraction,
        );
      }

      if (root) {
        root.removeEventListener('click', handleInteraction);
        root.removeEventListener('keydown', handleInteraction);
        root.removeEventListener('transitionend', handleTransitioinEnd);

        delete root.foundation;
      }

      foundation.destroy();
      foundationRef.current = null;
    };
  }, [chipId]);

  useEffect(() => {
    // flowlint-next-line unclear-type:off
    const root: HTMLChipElement = (rootRef.current: any);
    const foundation = foundationRef.current;

    if (foundation && root) {
      foundation.adapter_.notifyInteraction = () => {
        root.dispatchEvent(
          createEvent(
            strings.INTERACTION_EVENT,
            {
              chipId,
              element: rootRef.current,
            },
            true /* bubble */,
          ),
        );

        onInteraction({ chipId, element: root });
      };
    }
  }, [chipId, onInteraction]);

  useEffect(() => {
    // flowlint-next-line unclear-type:off
    const root: HTMLChipElement = (rootRef.current: any);
    const foundation = foundationRef.current;

    if (foundation && root) {
      foundation.adapter_.notifyRemoval = () => {
        root.dispatchEvent(
          createEvent(
            strings.REMOVAL_EVENT,
            {
              chipId,
              element: root,
            },
            true /* bubble */,
          ),
        );

        onRemoval({ chipId, element: root });
      };
    }
  }, [chipId, onRemoval]);

  useEffect(() => {
    // flowlint-next-line unclear-type:off
    const root: HTMLChipElement = (rootRef.current: any);
    const foundation = foundationRef.current;
    if (foundation && root) {
      foundation.adapter_.notifySelection = mSelected => {
        root.dispatchEvent(
          createEvent(
            strings.SELECTION_EVENT,
            {
              chipId,
              selected: mSelected,
              element: root,
            },
            true /* bubble */,
          ),
        );

        onSelected({ chipId, selected: mSelected, element: root });
      };
    }
  }, [chipId, onSelected]);

  useEffect(() => {
    if (foundationRef.current && shouldRemoveOnTrailingIconClick != null) {
      foundationRef.current.setShouldRemoveOnTrailingIconClick(
        shouldRemoveOnTrailingIconClick,
      );
    }
  }, [shouldRemoveOnTrailingIconClick]);

  useEffect(() => {
    if (foundationRef.current && beginExit === true) {
      foundationRef.current.beginExit();
    }
  }, [beginExit]);

  const memoizedStyle = useMemo(
    () => ({ ...style, ...rippleStyle, ...mtStyle }),
    [style, rippleStyle, mtStyle],
  );

  return (
    <button
      {...rest}
      {...mtAttrs}
      id={chipId}
      ref={rootRef}
      style={memoizedStyle}
      type="button"
      className={cx(
        'mdc-chip',
        className,
        ...Array.from(rippleClassName),
        ...Array.from(mtClassName),
      )}
    >
      {leadingIcon}
      {filter && (
        <span ref={checkmarkRef} className="mdc-chip__checkmark">
          <svg className="mdc-chip__checkmark-svg" viewBox="-2 -3 30 30">
            <path
              className="mdc-chip__checkmark-path"
              fill="none"
              stroke="black"
              d="M1.73,12.91 8.1,19.28 22.79,4.59"
            />
          </svg>
        </span>
      )}
      <span className="mdc-chip__text">{label}</span>
      {trailingIcon}
    </button>
  );
};

export default Chip;
