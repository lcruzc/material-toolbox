// @flow strict
import React, { useEffect, useRef, useReducer } from 'react';
import cx from 'classnames';

import { mtClassNamesReducer } from '../reducers';

import type { HTMLChipIconElement } from '../mt-types';

type Props = {
  className?: string,
  trailing?: boolean,
};

const ChipIcon = ({ className, trailing = false, ...rest }: Props) => {
  const rootRef = useRef();
  const [mtClassName, mtClassNameDispatch] = useReducer(
    mtClassNamesReducer,
    new Set<string>(),
  );

  useEffect(() => {
    // flowlint-next-line unclear-type:off
    const root: HTMLChipIconElement = (rootRef.current: any);

    if (root) {
      root.mtAddClassName = mClassName =>
        mtClassNameDispatch({ type: 'add', className: mClassName });
      root.mtRemoveClassName = mClassName =>
        mtClassNameDispatch({ type: 'remove', className: mClassName });
    }
  }, []);

  return (
    <i
      {...rest}
      ref={rootRef}
      tabIndex={trailing ? 0 : undefined} // eslint-disable-line jsx-a11y/no-noninteractive-tabindex
      role={trailing ? 'button' : undefined}
      className={cx(
        'mdc-chip__icon',
        {
          'mdc-chip__icon--leading': !trailing,
          'mdc-chip__icon--trailing': trailing,
        },
        ...Array.from(mtClassName),
        className,
      )}
    />
  );
};

export default ChipIcon;
