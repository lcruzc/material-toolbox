// @flow strict
import React from 'react';
import cx from 'classnames';

import type { ElementType } from 'react';

type Props = {
  className?: string,
  as?: ElementType,
};

const DrawerTitle = ({ as: Component = 'h3', className, ...rest }: Props) => (
  <Component
    {...(rest: mixed)}
    className={cx(className, 'mdc-drawer__title')}
  />
);

export default DrawerTitle;
