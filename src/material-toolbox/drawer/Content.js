// @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  className?: string,
};

const DrawerContent = ({ className, ...rest }: Props) => (
  <div {...rest} className={cx(className, 'mdc-drawer__content')} />
);

export default DrawerContent;
