// @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  className?: string,
};

const DrawerHeader = ({ className, ...rest }: Props) => (
  <div {...rest} className={cx(className, 'mdc-drawer__header')} />
);

export default DrawerHeader;
