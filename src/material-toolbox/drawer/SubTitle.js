// @flow strict
import React from 'react';
import cx from 'classnames';

import type { ElementType } from 'react';

type Props = {
  className?: string,
  as?: ElementType,
};

const DrawerSubTitle = ({
  as: Component = 'h6',
  className,
  ...rest
}: Props) => (
  <Component
    {...(rest: mixed)}
    className={cx(className, 'mdc-drawer__subtitle')}
  />
);

export default DrawerSubTitle;
