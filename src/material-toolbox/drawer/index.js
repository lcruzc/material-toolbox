// @flow strict
import React, { useRef, useEffect, useReducer } from 'react';
import cx from 'classnames';

import * as util from '@material/drawer/util';
import MDCDismissibleDrawerFoundation from '@material/drawer/dismissible/foundation';
import MDCModalDrawerFoundation from '@material/drawer/modal/foundation';
import MDCListFoundation from '@material/list/foundation';

import createFocusTrap from 'focus-trap';

import { mtClassNamesReducer } from '../reducers';

const { SCRIM_SELECTOR } = MDCDismissibleDrawerFoundation.strings;

const noOp = () => {};

type Props = {
  className?: string,
  dismissible?: boolean,
  modal?: boolean,
  onClose?: () => void,
  onOpen?: () => void,
  open?: boolean,
};

const Drawer = ({
  className,
  dismissible = false,
  modal = false,
  onClose = noOp,
  onOpen = noOp,
  open = false,
  ...rest
}: Props) => {
  const foundationRef = useRef<
    ?MDCModalDrawerFoundation | ?MDCDismissibleDrawerFoundation,
  >();
  const rootRef = useRef<?HTMLElement>();
  const scrimRef = useRef<?HTMLElement>();
  const focusTrapRef = useRef();
  const previousFocusRef = useRef();
  const [mtClassName, mtClassNameDispatch] = useReducer(
    mtClassNamesReducer,
    new Set<string>(),
  );

  useEffect(() => {
    let foundation;
    let handleKeyDown;
    let handleTransitionEnd;

    const root = rootRef.current;

    if (modal || dismissible) {
      const Foundation = modal
        ? MDCModalDrawerFoundation
        : MDCDismissibleDrawerFoundation;

      foundation = new Foundation({
        addClass: mClassName =>
          mtClassNameDispatch({ type: 'add', className: mClassName }),
        removeClass: mClassName =>
          mtClassNameDispatch({ type: 'remove', className: mClassName }),
        hasClass: mClassName =>
          rootRef.current && rootRef.current.classList.contains(mClassName),
        elementHasClass: (element, mClassName) =>
          element.classList.contains(mClassName),
        saveFocus: () => {
          previousFocusRef.current = document.activeElement;
        },
        restoreFocus: () => {
          const previousFocus = previousFocusRef.current;
          if (
            previousFocus &&
            previousFocus.focus &&
            rootRef.current &&
            rootRef.current.contains(document.activeElement)
          ) {
            previousFocus.focus();
          }
        },
        focusActiveNavigationItem: () => {
          if (rootRef.current) {
            const activeNavItemEl = rootRef.current.querySelector(
              `.${MDCListFoundation.cssClasses.LIST_ITEM_ACTIVATED_CLASS}`,
            );
            if (activeNavItemEl) {
              activeNavItemEl.focus();
            }
          }
        },
        trapFocus: () =>
          focusTrapRef.current && focusTrapRef.current.activate(),
        releaseFocus: () =>
          focusTrapRef.current && focusTrapRef.current.deactivate(),
      });

      foundation.init();

      foundationRef.current = foundation;

      if (root) {
        handleKeyDown = (...args) => foundation.handleKeydown(...args);

        handleTransitionEnd = (...args) =>
          foundation.handleTransitionEnd(...args);

        root.addEventListener('keydown', handleKeyDown);
        root.addEventListener('transitionend', handleTransitionEnd);
      }
    }

    return () => {
      if (root && handleKeyDown && handleTransitionEnd) {
        root.removeEventListener('keydown', handleKeyDown);
        root.removeEventListener('transitionend', handleTransitionEnd);
      }

      if (foundation) {
        foundation.destroy();
        foundationRef.current = null;
      }
    };
  }, [dismissible, modal]);

  useEffect(() => {
    const root = rootRef.current;
    const foundation = foundationRef.current;

    let handleScrimClick;

    if (root && modal) {
      // flowlint-next-line unclear-type:off
      scrimRef.current = ((root.parentNode: any): HTMLElement).querySelector(
        SCRIM_SELECTOR,
      );
      focusTrapRef.current = util.createFocusTrapInstance(
        rootRef.current,
        createFocusTrap,
      );
    }

    const scrim = scrimRef.current;

    if (scrim && foundation) {
      handleScrimClick = (...args) => foundation.handleScrimClick(...args);
      scrim.addEventListener('click', handleScrimClick);
    }

    return () => {
      if (handleScrimClick && scrim) {
        scrim.removeEventListener('click', handleScrimClick);
      }
      scrimRef.current = null;
      focusTrapRef.current = null;
    };
  }, [modal]);

  useEffect(() => {
    if (foundationRef.current) {
      foundationRef.current.adapter_.notifyClose = onClose;
      foundationRef.current.adapter_.notifyOpen = onOpen;
    }
  }, [onClose, onOpen]);

  useEffect(() => {
    if (foundationRef.current) {
      if (open) {
        foundationRef.current.open();
      } else {
        foundationRef.current.close();
      }
    }
  }, [open]);

  return (
    <aside
      {...rest}
      className={cx(
        className,
        'mdc-drawer',
        {
          'mdc-drawer--dismissible': dismissible,
          'mdc-drawer--modal': modal,
        },
        ...Array.from(mtClassName),
      )}
      ref={rootRef}
    />
  );
};

export default Drawer;
