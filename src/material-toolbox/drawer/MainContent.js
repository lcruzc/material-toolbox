// @flow strict
import React from 'react';
import cx from 'classnames';

import type { Node } from 'react';

type Props = {
  children: Node,
  dismissible?: boolean,
  modal?: boolean,
  className?: string,
};

const DrawerMainContent = React.forwardRef<Props, HTMLDivElement>(
  ({ modal = false, dismissible = false, children, className }: Props, ref) => {
    if (dismissible) {
      return (
        <div ref={ref} className={cx('mdc-drawer-app-content', className)}>
          {children}
        </div>
      );
    }

    if (modal) {
      return (
        <>
          <div className={cx('mdc-drawer-scrim', className)} />
          {children}
        </>
      );
    }

    return children;
  },
);

DrawerMainContent.displayName = 'DrawerMainContent';

export default DrawerMainContent;
