// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import DrawerContent from '../Content';

afterEach(cleanup);

describe('component::DrawerContent', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<DrawerContent>Drawer Content</DrawerContent>);
    expect(container.firstChild).toMatchSnapshot();
  });
});
