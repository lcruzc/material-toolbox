// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import DrawerHeader from '../Header';

afterEach(cleanup);

describe('component::DrawerHeader', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<DrawerHeader />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
