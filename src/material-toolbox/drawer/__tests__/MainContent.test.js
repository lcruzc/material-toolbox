// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import DrawerMainContent from '../MainContent';

afterEach(cleanup);

describe('component::DrawerMainContent', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<DrawerMainContent>test</DrawerMainContent>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot dismissable', () => {
    const { container } = render(
      <DrawerMainContent dismissable>test</DrawerMainContent>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot modal', () => {
    const { container } = render(
      <DrawerMainContent modal>test</DrawerMainContent>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
