// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Drawer from '../index';
import DrawerMainContent from '../MainContent';

afterEach(cleanup);

describe('component::Drawer', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<Drawer>test</Drawer>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot dismissible', () => {
    const { container } = render(<Drawer dismissible>test</Drawer>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot modal', () => {
    const { container } = render(
      <div>
        <Drawer modal>test</Drawer>
        <DrawerMainContent modal>Main content</DrawerMainContent>
      </div>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
