// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import DrawerSubTitle from '../Title';

afterEach(cleanup);

describe('component::DrawerSubTitle', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<DrawerSubTitle>test</DrawerSubTitle>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot as', () => {
    const { container } = render(<DrawerSubTitle as="h5">test</DrawerSubTitle>);
    expect(container.firstChild).toMatchSnapshot();
  });
});
