// @flow strict
import React, { useMemo } from 'react';
import cx from 'classnames';

import type { Node, AbstractComponent } from 'react';

import { getDisplayName } from '../utils';

export const useElevation = (depth: number = 0, transition: boolean = false) =>
  useMemo(
    () =>
      cx({
        [`mdc-elevation--z${depth}`]: depth >= 0,
        'mdc-elevation-transition': transition,
      }),
    [depth, transition],
  );

export const withElevation = <Config: { className?: string }, Instance>(
  Component: AbstractComponent<Config, Instance>,
  { depth, transition }: { depth?: number, transition?: boolean } = {},
): AbstractComponent<Config, Instance> => {
  const Elevated = React.forwardRef<Config, Instance>(
    ({ className, ...rest }: Config, ref) => {
      const mtClassName = useElevation(depth, transition);

      return (
        <Component {...rest} ref={ref} className={cx(className, mtClassName)} />
      );
    },
  );

  Elevated.displayName = `WithElevation(${getDisplayName(Component)})`;

  return Elevated;
};

type Props = {
  children: Node,
  depth?: number,
  transition?: boolean,
  className?: string,
};

const Elevation = React.forwardRef<Props, _>(
  ({ children, className, transition, depth }: Props, ref) => {
    const child = React.Children.only(children);
    const mtClassName = useElevation(depth, transition);

    return (
      <child.type
        {...child.props}
        className={cx(mtClassName, className, child.props.className)}
        ref={ref}
      />
    );
  },
);

Elevation.displayName = 'Elevation';

export default Elevation;
