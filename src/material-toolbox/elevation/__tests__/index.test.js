// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Elevation, { withElevation } from '../index';

afterEach(cleanup);

const elevations = Array(25)
  .fill(1)
  .map((_, index) => index);

describe('component::Elevation', () => {
  elevations.forEach(depth => {
    it(`Should match snapshoot elevation-${depth}`, () => {
      const { container } = render(
        <Elevation depth={depth}>
          <div className="test" />
        </Elevation>,
      );
      expect(container.firstChild).toMatchSnapshot();
    });
  });

  elevations.forEach(depth => {
    it(`Should match snapshoot HOC elevation-${depth}`, () => {
      const Elevated = withElevation(props => <div {...props} />, { depth });
      const { container } = render(<Elevated />);
      expect(container.firstChild).toMatchSnapshot();
    });
  });

  it(`Should match snapshoot transition with elevation 1`, () => {
    const { container } = render(
      <Elevation depth={1} transition>
        <div className="test" />
      </Elevation>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
