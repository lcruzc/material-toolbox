// @flow strict
import React, { useEffect, useRef, useReducer } from 'react';
import cx from 'classnames';

import MDCSlidingTabIndicatorFoundation from '@material/tab-indicator/sliding-foundation';
import MDCFadingTabIndicatorFoundation from '@material/tab-indicator/fading-foundation';

import type { Node } from 'react';

import { mtClassNamesReducer, mtStylesReducer } from '../reducers';

import type { Style, HTMLTabIndicatorElement } from '../mt-types';

type Props = {
  fade?: boolean,
  icon?: boolean,
  children?: Node,
  className?: string,
  style?: Style,
};

const TabIndicator = ({
  className,
  children,
  fade = false,
  icon = false,
  style,
  ...rest
}: Props) => {
  const rootRef = useRef<?HTMLSpanElement>();
  const contentRef = useRef<?HTMLSpanElement>();
  const [mtStyle, mtStyleDispatch] = useReducer(mtStylesReducer, {});
  const [mtClassName, mtClassNameDispatch] = useReducer(
    mtClassNamesReducer,
    new Set<string>(),
  );

  useEffect(() => {
    let Foundation = MDCSlidingTabIndicatorFoundation;

    if (fade) {
      Foundation = MDCFadingTabIndicatorFoundation;
    }

    const foundation = new Foundation({
      addClass: mClassName =>
        mtClassNameDispatch({ type: 'add', className: mClassName }),
      removeClass: mClassName =>
        mtClassNameDispatch({ type: 'remove', className: mClassName }),
      computeContentClientRect: () =>
        contentRef.current && contentRef.current.getBoundingClientRect(),
      setContentStyleProperty: (prop, value) =>
        mtStyleDispatch({ type: 'set', name: prop, value }),
    });

    foundation.init();

    if (rootRef.current) {
      // flowlint-next-line unclear-type:off
      const root: HTMLTabIndicatorElement = (rootRef.current: any);
      root.foundation = foundation;
    }

    return () => {
      foundation.destroy();
    };
  }, [fade]);

  return (
    <span
      {...rest}
      ref={rootRef}
      className={cx('mdc-tab-indicator', ...Array.from(mtClassName), {
        'mdc-tab-indicator--fade': fade,
      })}
    >
      <span
        aria-hidden={icon ? 'true' : undefined}
        style={mtStyle}
        ref={contentRef}
        className={cx('mdc-tab-indicator__content', {
          'mdc-tab-indicator__content--underline': !icon,
          'mdc-tab-indicator__content--icon': icon,
        })}
      >
        {children}
      </span>
    </span>
  );
};

export default TabIndicator;
