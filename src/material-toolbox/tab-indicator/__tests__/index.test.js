// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TabIndicator from '../index';

afterEach(cleanup);

describe('component::TabIndicator', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<TabIndicator>favorite</TabIndicator>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot fade', () => {
    const { container } = render(<TabIndicator fade>favorite</TabIndicator>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot icon', () => {
    const { container } = render(<TabIndicator icon>favorite</TabIndicator>);
    expect(container.firstChild).toMatchSnapshot();
  });
});
