// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TabBar from '../index';

afterEach(cleanup);

describe('component::TabBar', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<TabBar>TabBar</TabBar>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot initialActive', () => {
    const { container } = render(<TabBar initialActive={0}>TabBar</TabBar>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot focusOnActivate', () => {
    const { container } = render(<TabBar focusOnActivate>TabBar</TabBar>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot useAutomaticActivation', () => {
    const { container } = render(
      <TabBar useAutomaticActivation>TabBar</TabBar>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot onChange', () => {
    const { container } = render(<TabBar onChange={() => {}}>TabBar</TabBar>);
    expect(container.firstChild).toMatchSnapshot();
  });
});
