// @flow strict
import React, { useEffect, useRef } from 'react';
import cx from 'classnames';

import MDCTabBarFoundation from '@material/tab-bar/foundation';
import MDCTabFoundation from '@material/tab/foundation';

import type { Node } from 'react';

import type { HTMLTabScrollerElement, HTMLTabElement } from '../mt-types';

const { strings } = MDCTabBarFoundation;

const noOp = () => {};

type Props = {
  initialActive?: number,
  focusOnActivate?: boolean,
  useAutomaticActivation?: boolean,
  children?: Node,
  className?: string,
  onChange?: (index: number) => void,
};

const TabBar = ({
  initialActive,
  focusOnActivate = true,
  useAutomaticActivation = false,
  className,
  children,
  onChange = noOp,
  ...rest
}: Props) => {
  const rootRef = useRef<?HTMLDivElement>();
  const scrollerRef = useRef<?HTMLTabScrollerElement>();
  const tabsRef = useRef<HTMLTabElement[]>([]);
  const foundationRef = useRef<?MDCTabBarFoundation>();

  useEffect(() => {
    const root = rootRef.current;

    if (root) {
      scrollerRef.current = (root.querySelector(
        strings.TAB_SCROLLER_SELECTOR,
      ): any); // flowlint-line unclear-type:off

      tabsRef.current = [].slice.call(
        root.querySelectorAll(strings.TAB_SELECTOR),
      );
    }
  }, [children]);

  useEffect(() => {
    const foundation = new MDCTabBarFoundation({
      scrollTo: scrollX =>
        scrollerRef.current && scrollerRef.current.foundation.scrollTo(scrollX),
      incrementScroll: scrollXIncrement =>
        scrollerRef.current &&
        scrollerRef.current.foundation.incrementScroll(scrollXIncrement),
      getScrollPosition: () =>
        scrollerRef.current &&
        scrollerRef.current.foundation.getScrollPosition(),
      getScrollContentWidth: () =>
        scrollerRef.current && scrollerRef.current.content.offsetWidth,
      getOffsetWidth: () => rootRef.current && rootRef.current.offsetWidth,
      isRTL: () =>
        window
          .getComputedStyle(rootRef.current)
          .getPropertyValue('direction') === 'rtl',
      setActiveTab: index => foundation.activateTab(index),
      activateTabAtIndex: (index, clientRect) =>
        tabsRef.current &&
        tabsRef.current[index].foundation.activate(clientRect),
      deactivateTabAtIndex: index =>
        tabsRef.current && tabsRef.current[index].foundation.deactivate(),
      focusTabAtIndex: index =>
        tabsRef.current && tabsRef.current[index].focus(),
      getTabIndicatorClientRectAtIndex: index =>
        tabsRef.current &&
        tabsRef.current[index].indicator.computeContentClientRect(),
      getTabDimensionsAtIndex: index =>
        tabsRef.current[index].foundation.computeDimensions(),
      getPreviousActiveTabIndex: () => {
        if (tabsRef.current) {
          return tabsRef.current.findIndex(elem => elem.foundation.isActive());
        }
        return -1;
      },
      getFocusedTabIndex: () => {
        const { activeElement } = document;
        return tabsRef.current.indexOf(activeElement);
      },
      getIndexOfTabById: id => {
        if (tabsRef.current) {
          return tabsRef.current.findIndex(elem => elem.dataset.tab === id);
        }
        return -1;
      },
      getTabListLength: () => (tabsRef.current ? tabsRef.current.length : 0),
    });

    foundationRef.current = foundation;

    foundation.init();

    const handleKeydown = (...handleKeydownRest) =>
      foundation.handleKeyDown(...handleKeydownRest);
    const handleTabInteraction = (...handleTabInteractionRest) =>
      foundation.handleTabInteraction(...handleTabInteractionRest);

    const root = rootRef.current;

    if (root) {
      root.addEventListener('keydown', handleKeydown);
      root.addEventListener(
        MDCTabFoundation.strings.INTERACTED_EVENT,
        handleTabInteraction,
      );
    }

    return () => {
      if (root) {
        root.removeEventListener('keydown', handleKeydown);
        root.removeEventListener(
          MDCTabFoundation.strings.INTERACTED_EVENT,
          handleTabInteraction,
        );
      }
      foundation.destroy();
      foundationRef.current = null;
    };
  }, []);

  useEffect(() => {
    if (tabsRef.current) {
      tabsRef.current.forEach(tab =>
        tab.foundation.setFocusOnActivate(focusOnActivate),
      );
    }
  }, [focusOnActivate]);

  useEffect(() => {
    if (foundationRef.current) {
      foundationRef.current.setUseAutomaticActivation(useAutomaticActivation);
    }
  }, [useAutomaticActivation]);

  useEffect(() => {
    if (foundationRef.current) {
      foundationRef.current.adapter_.notifyTabActivated = onChange;
    }
  }, [children, onChange]);

  useEffect(() => {
    if (
      initialActive !== undefined &&
      initialActive !== null &&
      foundationRef.current
    ) {
      foundationRef.current.activateTab(initialActive);
    }
  }, [initialActive]);

  return (
    <div
      {...rest}
      ref={rootRef}
      className={cx('mdc-tab-bar', className)}
      role="tablist"
    >
      {children}
    </div>
  );
};

export default TabBar;
