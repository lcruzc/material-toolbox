// @flow strict
import React, {
  useRef,
  useReducer,
  useImperativeHandle,
  useEffect,
} from 'react';
import cx from 'classnames';

import MDCLineRippleFoundation from '@material/line-ripple/foundation';

import { normalizePropToReactStyle } from '../utils';

import { mtClassNamesReducer, mtStylesReducer } from '../reducers';

import type { HTMLLineRippleElement } from '../mt-types';

type Prop = {};

const LineRipple = React.forwardRef<Prop, HTMLLineRippleElement>(
  (props, ref) => {
    const rootRef = useRef<?HTMLDivElement>();
    const [mtStyle, mtStyleDispatch] = useReducer(mtStylesReducer, {});
    const [mtClassName, mtClassNameDispatch] = useReducer(
      mtClassNamesReducer,
      new Set<string>(),
    );

    useEffect(() => {
      const foundation = new MDCLineRippleFoundation({
        addClass: mClassName =>
          mtClassNameDispatch({ type: 'add', className: mClassName }),
        removeClass: mClassName =>
          mtClassNameDispatch({ type: 'remove', className: mClassName }),
        hasClass: className =>
          rootRef.current && rootRef.current.classList.contains(className),
        setStyle: (propertyName, value) =>
          mtStyleDispatch({
            type: 'set',
            name: normalizePropToReactStyle(propertyName),
            value,
          }),
        registerEventHandler: (evtType, handler) =>
          rootRef.current && rootRef.current.addEventListener(evtType, handler),
        deregisterEventHandler: (evtType, handler) =>
          rootRef.current &&
          rootRef.current.removeEventListener(evtType, handler),
      });

      foundation.init();

      // flowlint-next-line unclear-type:off
      const root: HTMLLineRippleElement = (rootRef.current: any);

      if (root) {
        root.foundation = foundation;
      }

      return () => {
        foundation.destroy();
      };
    }, []);

    // flowlint-next-line unclear-type:off
    useImperativeHandle(ref, () => (rootRef.current: any), []);

    return (
      <div
        style={mtStyle}
        ref={rootRef}
        className={cx('mdc-line-ripple', ...Array.from(mtClassName))}
      />
    );
  },
);

LineRipple.displayName = 'LineRipple';

export default LineRipple;
