// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import LineRipple from '../index';

afterEach(cleanup);

describe('component::LineRipple', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<LineRipple />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
