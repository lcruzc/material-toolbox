// @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  className?: string,
  header?: boolean,
  numeric?: boolean,
};

const DataTableCell = ({
  className,
  header = false,
  numeric = false,
  ...rest
}: Props) => {
  const Component = header ? 'th' : 'td';

  return (
    <Component
      {...rest}
      role={header ? 'columnheader' : undefined}
      scope={header ? 'col' : undefined}
      className={cx(
        {
          'mdc-data-table__header-cell': header,
          'mdc-data-table__cell': !header,
          'mdc-data-table__cell--numeric': numeric,
        },
        className,
      )}
    />
  );
};

export default DataTableCell;
