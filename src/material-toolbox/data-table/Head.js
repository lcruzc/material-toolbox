// @flow strict
import React from 'react';

type Props = {};

const DataTableHead = ({ ...rest }: Props) => {
  return <thead {...rest} />;
};

export default DataTableHead;
