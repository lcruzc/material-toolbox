// @flow strict
import React, { useEffect, useRef, useState } from 'react';
import cx from 'classnames';

import { closest } from '@material/dom/ponyfill';
import { MDCDataTableFoundation } from '@material/data-table/foundation';
import { strings, cssClasses } from '@material/data-table/constants';

import type { Node } from 'react';

import type {
  HTMLMTCheckboxElement,
  HTMLDataTableRowElement,
} from '../mt-types';

type Props = {
  children?: Node,
  label?: string,
  className?: string,
  defaultSelected?: Array<string>,
  selected?: Array<string>,
  onSelectedAll?: () => void,
  onUnselectedAll?: () => void,
  onRowSelectionChanged?: ({
    row: HTMLTableRowElement,
    rowId: string,
    rowIndex: number,
    selected: boolean,
  }) => void,
};

const noOp = () => {};

const DataTable = ({
  children,
  label,
  className,
  defaultSelected: propDefaultSelected,
  selected: propSelected,
  onSelectedAll = noOp,
  onUnselectedAll = noOp,
  onRowSelectionChanged = noOp,
  ...rest
}: Props) => {
  const rootRef = useRef<?HTMLDivElement>();
  const foundationRef = useRef<?MDCDataTableFoundation>();
  const [selected, setSelected] = useState<?Array<string>>(propSelected);
  const [defaultSelected] = useState<?Array<string>>(
    propSelected || propDefaultSelected,
  );

  useEffect(() => {
    const root = rootRef.current;
    let headerRow;
    let content;

    if (root) {
      headerRow = root.querySelector(`.${cssClasses.HEADER_ROW}`);
      content = root.querySelector(`.${cssClasses.CONTENT}`);
    }

    const getHeaderCheckbox = (): ?HTMLMTCheckboxElement => {
      if (headerRow) {
        // flowlint-next-line unclear-type:off
        return (headerRow.querySelector('input[type=checkbox]'): any);
      }

      return undefined;
    };

    const getCheckbox = (index: number): ?HTMLMTCheckboxElement => {
      if (content) {
        // flowlint-next-line unclear-type:off
        return (content.querySelectorAll('input[type=checkbox]')[index]: any);
      }

      return undefined;
    };

    const foundation = new MDCDataTableFoundation({
      addClassAtRowIndex: (rowIndex: number, mClassName: string) => {
        const row: HTMLDataTableRowElement = foundation.getRows()[rowIndex];
        row.mtAddClassName(mClassName);
      },
      getRowCount: () => foundation.getRows().length,
      getRowElements: () =>
        root && [].slice.call(root.querySelectorAll(strings.ROW_SELECTOR)),
      getRowIdAtIndex: (rowIndex: number) =>
        foundation.getRows()[rowIndex].getAttribute(strings.DATA_ROW_ID_ATTR),
      getRowIndexByChildElement: (el: Element) => {
        return foundation.getRows().indexOf(closest(el, strings.ROW_SELECTOR));
      },
      getSelectedRowCount: () =>
        root && root.querySelectorAll(strings.ROW_SELECTED_SELECTOR).length,
      isCheckboxAtRowIndexChecked: (rowIndex: number) => {
        const checkbox = getCheckbox(rowIndex);
        return checkbox && checkbox.checked;
      },
      isHeaderRowCheckboxChecked: () => {
        const checkbox = getHeaderCheckbox();
        return checkbox && checkbox.checked;
      },
      isRowsSelectable: () =>
        !!(root && root.querySelector(strings.ROW_CHECKBOX_SELECTOR)),
      // notifyUnselectedAll: () => this.emit(events.UNSELECTED_ALL, {}, /** shouldBubble */ true),
      // Note. This only instanciate mdc-checkbox component, We don't need this.
      registerHeaderRowCheckbox: () => {},
      // Note. This only instanciate mdc-checkbox components, We don't need this.
      registerRowCheckboxes: () => {},
      removeClassAtRowIndex: (rowIndex: number, mClassName: string) => {
        const row: HTMLDataTableRowElement = foundation.getRows()[rowIndex];
        row.mtRemoveClassName(mClassName);
      },
      setAttributeAtRowIndex: (
        rowIndex: number,
        attr: string,
        value: string,
      ) => {
        const row: HTMLDataTableRowElement = foundation.getRows()[rowIndex];
        row.mtSetAttr(attr, value);
      },
      setHeaderRowCheckboxChecked: (checked: boolean) => {
        const checkbox = getHeaderCheckbox();
        if (checkbox) {
          checkbox.mtSetChecked(checked);
        }
      },
      setHeaderRowCheckboxIndeterminate: (indeterminate: boolean) => {
        const checkbox = getHeaderCheckbox();
        if (checkbox) {
          checkbox.mtSetIndterminate(indeterminate);
        }
      },
      setRowCheckboxCheckedAtIndex: (rowIndex: number, checked: boolean) => {
        const checkbox = getCheckbox(rowIndex);
        if (checkbox) {
          checkbox.mtSetChecked(checked);
        }
      },
    });

    const handleHeaderRowCheckboxChange = (...args) =>
      foundation.handleHeaderRowCheckboxChange(...args);

    const handleRowCheckboxChange = (...args) =>
      foundation.handleRowCheckboxChange(...args);

    foundation.init();
    foundation.layout();

    if (content) {
      content.addEventListener('change', handleRowCheckboxChange);
    }

    if (headerRow) {
      headerRow.addEventListener('change', handleHeaderRowCheckboxChange);
    }

    foundationRef.current = foundation;

    return () => {
      if (content) {
        content.removeEventListener('change', handleRowCheckboxChange);
      }

      if (headerRow) {
        headerRow.removeEventListener('change', handleHeaderRowCheckboxChange);
      }

      foundation.destroy();
      foundationRef.current = null;
    };
  }, []);

  useEffect(() => {
    const foundation = foundationRef.current;

    if (foundation) {
      foundation.adapter_.notifySelectedAll = onSelectedAll;
    }
  }, [onSelectedAll]);

  useEffect(() => {
    const foundation = foundationRef.current;

    if (foundation) {
      foundation.adapter_.notifyUnselectedAll = onUnselectedAll;
    }
  }, [onUnselectedAll]);

  useEffect(() => {
    const foundation = foundationRef.current;

    if (foundation) {
      foundation.adapter_.notifyRowSelectionChanged = data => {
        onRowSelectionChanged({
          row: foundation.getRows()[data.rowIndex],
          rowId: foundation
            .getRows()
            [data.rowIndex].getAttribute(strings.DATA_ROW_ID_ATTR),
          rowIndex: data.rowIndex,
          selected: data.selected,
        });
      };
    }
  }, [onRowSelectionChanged]);

  useEffect(() => {
    const foundation = foundationRef.current;

    if (foundation && selected != null) {
      foundation.setSelectedRowIds(selected);
    }
  }, [selected]);

  useEffect(() => {
    if (propSelected != null) {
      setSelected(propSelected);
    }
  }, [propSelected]);

  useEffect(() => {
    const foundation = foundationRef.current;

    if (foundation && defaultSelected != null) {
      foundation.setSelectedRowIds(defaultSelected);
    }
  }, [defaultSelected]);

  return (
    <div {...rest} ref={rootRef} className={cx('mdc-data-table', className)}>
      <table className="mdc-data-table__table" aria-label={label}>
        {children}
      </table>
    </div>
  );
};

export default DataTable;
