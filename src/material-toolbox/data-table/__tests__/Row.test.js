// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import DataTableRow from '../Row';

afterEach(cleanup);

describe('component::DataTableRow', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <table>
        <tbody>
          <DataTableRow />
        </tbody>
      </table>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot rowId', () => {
    const { container } = render(
      <table>
        <tbody>
          <DataTableRow rowId="row-id" />
        </tbody>
      </table>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot withCheckbox', () => {
    const { container } = render(
      <table>
        <tbody>
          <DataTableRow withCheckbox />
        </tbody>
      </table>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot header', () => {
    const { container } = render(
      <table>
        <thead>
          <DataTableRow header />
        </thead>
      </table>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot header and withCheckbox', () => {
    const { container } = render(
      <table>
        <thead>
          <DataTableRow header withCheckbox />
        </thead>
      </table>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
