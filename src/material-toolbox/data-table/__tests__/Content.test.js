// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import DataTableContent from '../Content';

afterEach(cleanup);

describe('component::DataTableContent', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <table>
        <DataTableContent>
          <tr>
            <td>Test</td>
          </tr>
        </DataTableContent>
      </table>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
