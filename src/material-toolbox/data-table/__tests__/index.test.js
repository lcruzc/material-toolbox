// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import DataTable from '../index';

afterEach(cleanup);

describe('component::DataTable', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<DataTable label="used for aria label" />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot defaultSelected', () => {
    const { container } = render(<DataTable defaultSelected={['test']} />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot selected', () => {
    const { container } = render(<DataTable selected={['test']} />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
