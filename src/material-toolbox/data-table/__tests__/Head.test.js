// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import DataTableHead from '../Head';

afterEach(cleanup);

describe('component::DataTableHead', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <table>
        <DataTableHead>
          <tr>
            <th>Test</th>
          </tr>
        </DataTableHead>
      </table>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
