// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import DataTableCell from '../Cell';

afterEach(cleanup);

describe('component::DataTableCell', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <table>
        <tbody>
          <tr>
            <DataTableCell>Test</DataTableCell>
          </tr>
        </tbody>
      </table>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot numeric', () => {
    const { container } = render(
      <table>
        <tbody>
          <tr>
            <DataTableCell numeric>4.0</DataTableCell>
          </tr>
        </tbody>
      </table>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot header', () => {
    const { container } = render(
      <table>
        <thead>
          <tr>
            <DataTableCell header>Test</DataTableCell>
          </tr>
        </thead>
      </table>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
