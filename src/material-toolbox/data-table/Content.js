// @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  className?: string,
};

const DataTableContent = ({ className, ...rest }: Props) => {
  return (
    <tbody {...rest} className={cx('mdc-data-table__content', className)} />
  );
};

export default DataTableContent;
