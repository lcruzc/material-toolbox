// @flow strict
import React, { useEffect, useRef, useReducer } from 'react';
import cx from 'classnames';

import type { Node } from 'react';

import DataTableCell from './Cell';
import Checkbox from '../checkbox';

import { mtAttrsReducer } from '../reducers';

import type { HTMLDataTableRowElement } from '../mt-types';

type Props = {
  className?: string,
  header?: boolean,
  withCheckbox?: boolean,
  rowId?: string,
  children?: Node,
};

const DataTableRow = ({
  className,
  children,
  header = false,
  withCheckbox = false,
  rowId,
  ...rest
}: Props) => {
  const rootRef = useRef<?HTMLTableRowElement>();

  const [mtAttrs, mtAtrrsDistpach] = useReducer(mtAttrsReducer, {});

  useEffect(() => {
    // flowlint-next-line unclear-type:off
    const root: HTMLDataTableRowElement = (rootRef.current: any);

    if (root) {
      root.mtAddClassName = mClassName => root.classList.add(mClassName);
      root.mtRemoveClassName = mClassName => root.classList.remove(mClassName);
    }

    root.mtSetAttr = (name, value) =>
      mtAtrrsDistpach({ type: 'set', name, value });
  }, []);

  return (
    <tr
      {...rest}
      {...mtAttrs}
      ref={rootRef}
      data-row-id={rowId}
      className={cx(
        {
          'mdc-data-table__header-row': header,
          'mdc-data-table__row': !header,
        },
        className,
      )}
    >
      {withCheckbox && (
        <DataTableCell
          className={cx({
            'mdc-data-table__header-cell--checkbox': withCheckbox && header,
            'mdc-data-table__cell--checkbox': withCheckbox && !header,
          })}
        >
          <Checkbox
            className={cx({
              'mdc-data-table__header-row-checkbox': withCheckbox && header,
              'mdc-data-table__row-checkbox': withCheckbox && !header,
            })}
          />
        </DataTableCell>
      )}
      {children}
    </tr>
  );
};

export default DataTableRow;
