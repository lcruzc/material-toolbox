// @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  className?: string,
};

const TabText = ({ className, ...rest }: Props) => (
  <span {...rest} className={cx('mdc-tab__text-label', className)} />
);

export default TabText;
