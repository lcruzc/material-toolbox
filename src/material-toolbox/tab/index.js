// @flow strict
import React, { useState, useRef, useEffect, useReducer } from 'react';
import cx from 'classnames';

import MDCTabFoundation from '@material/tab/foundation';

import type { ElementType, Node } from 'react';

import TabIndicator from '../tab-indicator';
import { useRipple } from '../ripple';
import { createEvent } from '../utils';
import { mtClassNamesReducer, mtAttrsReducer } from '../reducers';

import type { HTMLTabElement, HTMLTabIndicatorElement } from '../mt-types';

type Props = {
  as?: ElementType,
  id?: string,
  children?: Node,
  className?: string,
  contentIndicator?: boolean,
  fade?: boolean,
  minWidth?: boolean,
  stacked?: boolean,
};

let tabIdCounter = 0;

const Tab = ({
  as: Component = 'button',
  contentIndicator = false,
  fade = false,
  minWidth = false,
  stacked = false,
  id,
  className,
  children,
  ...rest
}: Props) => {
  const [tabId] = useState(() => {
    tabIdCounter += 1;
    return `mdc-tab-${tabIdCounter}`;
  });

  const indicatorRef = useRef<?HTMLTabIndicatorElement>();
  const rootRef = useRef<?HTMLElement>();
  const rippleSurface = useRef<?HTMLSpanElement>();
  const contentRef = useRef<?HTMLSpanElement>();

  const [mtClassName, mtClassNameDispatch] = useReducer(
    mtClassNamesReducer,
    new Set<string>(),
  );

  const [mtAttrs, mtAttrsDispatch] = useReducer(mtAttrsReducer, {
    'aria-selected': false,
    tabIndex: '-1',
  });

  const { rippleStyle, rippleClassName } = useRipple({
    activator: rootRef,
    surface: rippleSurface,
  });

  useEffect(() => {
    if (rootRef.current) {
      indicatorRef.current = (rootRef.current.querySelector(
        MDCTabFoundation.strings.TAB_INDICATOR_SELECTOR,
      ): any); // flowlint-line unclear-type:off
    }
  }, [children]);

  useEffect(() => {
    const foundation = new MDCTabFoundation({
      setAttr: (attr, value) =>
        mtAttrsDispatch({ type: 'set', name: attr, value }),
      addClass: mClassName =>
        mtClassNameDispatch({ type: 'add', className: mClassName }),
      removeClass: mClassName =>
        mtClassNameDispatch({ type: 'remove', className: mClassName }),
      hasClass: mClassName =>
        rootRef.current && rootRef.current.classList.contains(mClassName),
      activateIndicator: previousIndicatorClientRect =>
        indicatorRef.current &&
        indicatorRef.current.foundation.activate(previousIndicatorClientRect),
      deactivateIndicator: () =>
        indicatorRef.current && indicatorRef.current.foundation.deactivate(),
      notifyInteracted: () => {
        if (rootRef.current) {
          rootRef.current.dispatchEvent(
            createEvent(
              MDCTabFoundation.strings.INTERACTED_EVENT,
              {
                tabId: rootRef.current.dataset.tab,
              },
              true /* bubble */,
            ),
          );
        }
      },
      getOffsetLeft: () => rootRef.current && rootRef.current.offsetLeft,
      getOffsetWidth: () => rootRef.current && rootRef.current.offsetWidth,
      getContentOffsetLeft: () =>
        contentRef.current && contentRef.current.offsetLeft,
      getContentOffsetWidth: () =>
        contentRef.current && contentRef.current.offsetWidth,
      focus: () => rootRef.current && rootRef.current.focus(),
    });

    foundation.init();

    const handleClick = (...handleClickRef) => {
      foundation.handleClick(...handleClickRef);
    };

    // flowlint-next-line unclear-type:off
    const root: HTMLTabElement = (rootRef.current: any);

    if (root && indicatorRef.current) {
      root.foundation = foundation;
      root.indicator = indicatorRef.current.foundation;
      root.addEventListener('click', handleClick);
    }

    return () => {
      if (root) {
        root.removeEventListener('click', handleClick);
      }

      foundation.destroy();
    };
  }, []);

  return (
    <Component
      {...(rest: mixed)}
      {...mtAttrs}
      data-tab={tabId}
      ref={rootRef}
      className={cx('mdc-tab', className, ...Array.from(mtClassName), {
        'mdc-tab--min-width': minWidth,
        'mdc-tab--stacked': stacked,
      })}
      role="tab"
    >
      <span ref={contentRef} className="mdc-tab__content">
        {children}
        {contentIndicator && <TabIndicator fade={fade} />}
      </span>
      <span
        className={cx('mdc-tab__ripple', ...Array.from(rippleClassName))}
        ref={rippleSurface}
        style={rippleStyle}
      />
      {!contentIndicator && <TabIndicator fade={fade} />}
    </Component>
  );
};

export default Tab;
