// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TabText from '../Text';

afterEach(cleanup);

describe('component::TabText', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<TabText>Text</TabText>);
    expect(container.firstChild).toMatchSnapshot();
  });
});
