// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TabIcon from '../Icon';

afterEach(cleanup);

describe('component::TabIcon', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <TabIcon className="material-icons">favorite</TabIcon>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
