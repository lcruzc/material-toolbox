// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Tab from '../index';

afterEach(cleanup);

describe('component::Tab', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<Tab>Tab</Tab>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot as', () => {
    const { container } = render(<Tab as="a">Tab</Tab>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot contentIndicator', () => {
    const { container } = render(<Tab contentIndicator>Tab</Tab>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot fade', () => {
    const { container } = render(<Tab fade>Tab</Tab>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot minWidth', () => {
    const { container } = render(<Tab minWidth>Tab</Tab>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot as stacked', () => {
    const { container } = render(
      <Tab stacked>
        <div>icon</div>
        <div>text</div>
      </Tab>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
