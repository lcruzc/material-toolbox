// @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  className?: string,
};

const TabIcon = ({ className, ...rest }: Props) => (
  <span
    {...rest}
    className={cx('mdc-tab__icon', className)}
    aria-hidden="true"
  />
);

export default TabIcon;
