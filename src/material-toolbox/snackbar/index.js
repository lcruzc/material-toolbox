// @flow strict
import React, { useEffect, useRef, useReducer, useCallback } from 'react';
import cx from 'classnames';

import MDCSnackbarFoundation from '@material/snackbar/foundation';
import { announce } from '@material/snackbar/util';

import { mtClassNamesReducer } from '../reducers';

const noOp = () => {};

type Props = {
  buttonText?: string,
  className?: string,
  closeOnEscape?: boolean,
  dismissClassName?: string,
  dismissText?: string,
  label: string,
  leading?: boolean,
  onClosed?: (reason: string) => void,
  onClosing?: (reason: string) => void,
  onOpened?: () => void,
  onOpenning?: () => void,
  open?: boolean,
  stacked?: boolean,
  timeout?: number,
  withDismiss?: boolean,
};

const Snackbar = ({
  buttonText,
  className,
  closeOnEscape = true,
  dismissClassName,
  dismissText,
  label,
  leading = false,
  onClosed = noOp,
  onClosing = noOp,
  onOpened = noOp,
  onOpenning = noOp,
  open = false,
  stacked = false,
  timeout,
  withDismiss = false,
}: Props) => {
  const rootRef = useRef<?HTMLDivElement>();
  const labelRef = useRef<?HTMLDivElement>();
  const foundationRef = useRef<?MDCSnackbarFoundation>();
  const [mtClassName, mtClassNameDispatch] = useReducer(
    mtClassNamesReducer,
    new Set<string>(),
  );

  useEffect(() => {
    const foundation = new MDCSnackbarFoundation({
      addClass: mClassName =>
        mtClassNameDispatch({ type: 'add', className: mClassName }),
      removeClass: mClassName =>
        mtClassNameDispatch({ type: 'remove', className: mClassName }),
    });

    foundation.init();

    foundationRef.current = foundation;

    const root = rootRef.current;

    const handleKeyDown = (evt: Event) => {
      foundation.handleKeyDown(evt);
    };

    if (root) {
      root.addEventListener('keydown', handleKeyDown);
    }

    return () => {
      if (root) {
        root.removeEventListener('keydown', handleKeyDown);
      }

      foundation.destroy();
      foundationRef.current = null;
    };
  }, []);

  useEffect(() => {
    const foundation = foundationRef.current;

    if (foundation) {
      foundation.adapter_.announce = () => announce(labelRef.current);
      foundation.adapter_.notifyClosed = onClosed;
      foundation.adapter_.notifyClosing = onClosing;
      foundation.adapter_.notifyOpened = onOpened;
      foundation.adapter_.notifyOpening = onOpenning;
    }
  }, [label, onClosed, onClosing, onOpened, onOpenning]);

  useEffect(() => {
    if (foundationRef.current && timeout != null && timeout > 0) {
      foundationRef.current.setTimeoutMs(timeout);
    }
  }, [timeout]);

  useEffect(() => {
    if (foundationRef.current) {
      foundationRef.current.setCloseOnEscape(closeOnEscape);
    }
  }, [closeOnEscape]);

  useEffect(() => {
    if (foundationRef.current && open) {
      foundationRef.current.open();
    }
  }, [open]);

  const handleActionButtonClick = useCallback(
    (evt: SyntheticEvent<HTMLDivElement>) => {
      if (foundationRef.current) {
        foundationRef.current.handleActionButtonClick(evt);
      }
    },
    [],
  );

  const handleActionIconClick = useCallback(
    (evt: SyntheticEvent<HTMLDivElement>) => {
      if (foundationRef.current) {
        foundationRef.current.handleActionIconClick(evt);
      }
    },
    [],
  );

  return (
    <div
      ref={rootRef}
      className={cx('mdc-snackbar', className, ...Array.from(mtClassName), {
        'mdc-snackbar--stacked': stacked,
        'mdc-snackbar--leading': leading,
      })}
    >
      <div className="mdc-snackbar__surface">
        <div
          ref={labelRef}
          className="mdc-snackbar__label"
          role="status"
          aria-live="polite"
        >
          {label}
        </div>
        {(buttonText !== undefined || dismissText !== undefined) && (
          <div className="mdc-snackbar__actions">
            {buttonText !== undefined && (
              <button
                onClick={handleActionButtonClick}
                type="button"
                className="mdc-button mdc-snackbar__action"
              >
                {buttonText}
              </button>
            )}
            {withDismiss && (
              <button
                onClick={handleActionIconClick}
                type="button"
                className={cx(
                  'mdc-icon-button mdc-snackbar__dismiss',
                  dismissClassName,
                )}
                title="Dismiss"
              >
                {dismissText}
              </button>
            )}
          </div>
        )}
      </div>
    </div>
  );
};

export default Snackbar;
