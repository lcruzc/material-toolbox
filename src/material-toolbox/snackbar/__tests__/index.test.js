// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Snackbar from '../index';

afterEach(cleanup);

describe('component::Snackbar', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<Snackbar label="Snackbar test" />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot textButton', () => {
    const { container } = render(
      <Snackbar label="Snackbar test" textButton="Undo" />,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot leading', () => {
    const { container } = render(<Snackbar label="Snackbar test" leading />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot withDismiss', () => {
    const component = render(
      <Snackbar
        label="Snackbar test"
        withDismiss
        dismissClassName="material-icons"
        dismissContent="close"
      />,
    );
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot timeout', () => {
    const component = render(
      <Snackbar label="Snackbar test" timeout={10000} />,
    );
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot stacked', () => {
    const component = render(<Snackbar label="Snackbar test" stacked />);
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot open', () => {
    const component = render(<Snackbar label="Snackbar test" open />);
    expect(component).toMatchSnapshot();
  });
});
