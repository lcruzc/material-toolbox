// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Radio from '../index';

afterEach(cleanup);

describe('component::Radio', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<Radio />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
