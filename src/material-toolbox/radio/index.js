// @flow strict
import React, { useRef, useMemo, useEffect, useImperativeHandle } from 'react';
import cx from 'classnames';

import { useRipple } from '../ripple';

import type { Style, HTMLInputElementWithRipple } from '../mt-types';

type Props = {
  disabled?: boolean,
  className?: string,
  style?: Style,
};

const Radio = React.forwardRef<Props, ?HTMLInputElement>(
  ({ disabled = false, className, style, ...rest }: Props, ref) => {
    const rootRef = useRef<?HTMLDivElement>();
    const radioRef = useRef<?HTMLInputElement>();

    const { rippleStyle, rippleClassName, ripple } = useRipple({
      surface: rootRef,
      activator: radioRef,
      unbounded: true,
    });

    useEffect(() => {
      if (radioRef.current) {
        // flowlint-next-line unclear-type:off
        const radio: HTMLInputElementWithRipple = (radioRef.current: any);
        radio.ripple = ripple;
      }
    }, [ripple]);

    useImperativeHandle(ref, () => radioRef.current, []);

    const memoizedStyle = useMemo(() => ({ ...style, ...rippleStyle }), [
      rippleStyle,
      style,
    ]);

    return (
      <div
        style={memoizedStyle}
        ref={rootRef}
        className={cx('mdc-radio', className, ...Array.from(rippleClassName), {
          'mdc-radio--disabled': disabled,
        })}
      >
        <input
          ref={radioRef}
          {...(rest: mixed)}
          type="radio"
          className="mdc-radio__native-control"
          disabled={disabled}
        />
        <div className="mdc-radio__background">
          <div className="mdc-radio__outer-circle" />
          <div className="mdc-radio__inner-circle" />
        </div>
      </div>
    );
  },
);

Radio.displayName = 'Radio';

export default Radio;
