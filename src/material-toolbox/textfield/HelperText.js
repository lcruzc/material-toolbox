// @flow strict
import React, {
  useReducer,
  useImperativeHandle,
  useEffect,
  useRef,
} from 'react';
import cx from 'classnames';

import MDCTextFieldHelperTextFoundation from '@material/textfield/helper-text/foundation';

import { mtClassNamesReducer, mtAttrsReducer } from '../reducers';

import type { HTMLTextFieldHelperTextElement } from '../mt-types';

type Props = {
  text: string,
  persistent?: boolean,
  validation?: boolean,
};

const TextFieldHelperText = React.forwardRef<
  Props,
  HTMLTextFieldHelperTextElement,
>(({ text, persistent = false, validation = false, ...rest }: Props, ref) => {
  const rootRef = useRef<?HTMLParagraphElement>();
  const [mtClassName, mtClassNameDispatch] = useReducer(
    mtClassNamesReducer,
    new Set<string>(),
  );

  const [mtAttrs, mtAttrsDispatch] = useReducer(mtAttrsReducer, {});

  useEffect(() => {
    const foundation = new MDCTextFieldHelperTextFoundation({
      addClass: mClassName =>
        mtClassNameDispatch({ type: 'add', className: mClassName }),
      removeClass: mClassName =>
        mtClassNameDispatch({ type: 'remove', className: mClassName }),
      hasClass: className =>
        rootRef.current && rootRef.current.classList.contains(className),
      setAttr: (attr, value) =>
        mtAttrsDispatch({ type: 'set', name: attr, value }),
      removeAttr: attr => mtAttrsDispatch({ type: 'unset', name: attr }),
      // NOTE.- next is not needed already
      // setContent: (content) => {
      //   this.root_.textContent = content;
      // },
    });

    foundation.init();

    // flowlint-next-line unclear-type:off
    const root: HTMLTextFieldHelperTextElement = (rootRef.current: any);

    if (root) {
      root.foundation = foundation;
    }

    return () => {
      foundation.destroy();
    };
  }, []);

  // flowlint-next-line unclear-type:off
  useImperativeHandle(ref, () => (rootRef.current: any), []);

  return (
    <p
      {...rest}
      {...mtAttrs}
      className={cx('mdc-text-field-helper-text', ...Array.from(mtClassName), {
        'mdc-text-field-helper-text--persistent': persistent,
        'mdc-text-field-helper-text--validation-msg': validation,
      })}
      ref={rootRef}
    >
      {text}
    </p>
  );
});

TextFieldHelperText.displayName = 'TextFieldHelperText';

export default TextFieldHelperText;
