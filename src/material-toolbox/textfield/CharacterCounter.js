// @flow strict
import React, { useEffect, useRef, useImperativeHandle } from 'react';

import MDCTextFieldCharacterCounterFoundation from '@material/textfield/character-counter/foundation';

import type { HTMLTextFieldCharacterCounterElement } from '../mt-types';

type Props = {};

const TextFieldCharacterCounter = React.forwardRef<
  Props,
  HTMLTextFieldCharacterCounterElement,
>((props: Props, ref) => {
  const rootRef = useRef<?HTMLDivElement>();

  useEffect(() => {
    const foundation = new MDCTextFieldCharacterCounterFoundation({
      setContent: content => {
        if (rootRef.current) {
          rootRef.current.textContent = content;
        }
      },
    });

    foundation.init();

    // flowlint-next-line unclear-type:off
    const root: HTMLTextFieldCharacterCounterElement = (rootRef.current: any);

    if (root) {
      root.foundation = foundation;
    }

    return () => {
      foundation.destroy();
    };
  }, []);

  // flowlint-next-line unclear-type:off
  useImperativeHandle(ref, () => (rootRef.current: any), []);

  return <div ref={rootRef} className="mdc-text-field-character-counter" />;
});

TextFieldCharacterCounter.displayName = 'TextFieldCharacterCounter';

export default TextFieldCharacterCounter;
