// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TextField from '../index';
import TextFieldIcon from '../Icon';
import TextFieldHelperText from '../HelperText';

global.MutationObserver = class {
  constructor(callback) {} // eslint-disable-line

  disconnect() {} // eslint-disable-line

  observe(element, initObject) {} // eslint-disable-line
};

afterEach(cleanup);

describe('component::TextField', () => {
  it('Should match snapshoot', () => {
    const component = render(<TextField label="test" />);
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot fullWidth', () => {
    const component = render(<TextField label="test" fullWidth />);
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot multiline', () => {
    const component = render(<TextField label="test" multiline />);
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot disabled', () => {
    const component = render(<TextField label="test" disabled />);
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot outlined', () => {
    const component = render(<TextField label="test" outlined />);
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot defaultValue', () => {
    const component = render(<TextField label="test" defaultValue="test" />);
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot invalid', () => {
    const component = render(<TextField label="test" invalid />);
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot aria-controls', () => {
    const component = render(
      <TextField
        helperText={
          <TextFieldHelperText text="this is a helper text" id="test" />
        }
        label="test"
        aria-controls="test"
      />,
    );
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot persistentHelperText', () => {
    const component = render(
      <TextField
        label="test"
        helperText={
          <TextFieldHelperText
            text="this is a helper text"
            persistent
            id="test"
          />
        }
        aria-controls="test"
      />,
    );
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot validation HelperText', () => {
    const component = render(
      <TextField
        label="test"
        helperText={
          <TextFieldHelperText text="this is a helper text" id="tests" />
        }
        invalid
        aria-controls="test"
      />,
    );
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot icon leading', () => {
    const component = render(
      <TextField
        leadingIcon={
          <TextFieldIcon className="material-icons">delete</TextFieldIcon>
        }
        label="test"
      />,
    );
    expect(component).toMatchSnapshot();
  });

  it('Should match snapshoot icon trailing', () => {
    const component = render(
      <TextField
        trailingIcon={
          <TextFieldIcon className="material-icons">delete</TextFieldIcon>
        }
        label="test"
        outlined
      />,
    );
    expect(component).toMatchSnapshot();
  });
});
