// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TextFieldCharacterCounter from '../CharacterCounter';

afterEach(cleanup);

describe('component::TextFieldCharacterCounter', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<TextFieldCharacterCounter />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
