// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import HelperText from '../HelperText';

afterEach(cleanup);

describe('component::HelperText', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<HelperText text="test" />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
