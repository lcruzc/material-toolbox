// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TextFieldIcon from '../Icon';

afterEach(cleanup);

describe('component::TextFieldIcon', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<TextFieldIcon />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
