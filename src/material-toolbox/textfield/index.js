// @flow strict
import React, {
  useReducer,
  useEffect,
  useImperativeHandle,
  useRef,
  useMemo,
} from 'react';
import cx from 'classnames';

import { applyPassive } from '@material/dom/events';
import MDCTextFieldFoundation from '@material/textfield/foundation';
import MDCTextFieldHelperTextFoundation from '@material/textfield/helper-text/foundation';

import type { Node } from 'react';

import FloatingLabel from '../floating-label';
import LineRipple from '../line-ripple';
import NotchedOutline from '../notched-outline';
import TextFieldCharacterCounter from './CharacterCounter';

import { useRipple } from '../ripple';
import { mtClassNamesReducer } from '../reducers';

import type {
  HTMLTextFieldHelperTextElement,
  HTMLFloatinLabelElement,
  HTMLLineRippleElement,
  HTMLTextFieldIconElement,
  HTMLNotchedOutlineElement,
  HTMLTextFieldCharacterCounterElement,
  Style,
} from '../mt-types';

type Props = {
  label?: string,
  fullWidth?: boolean,
  multiline?: boolean,
  disabled?: boolean,
  outlined?: boolean,
  helperText?: Node,
  leadingIcon?: Node,
  trailingIcon?: Node,
  value?: string,
  defaultValue?: string,
  invalid?: boolean,
  withCharacterCounter?: boolean,
  className?: string,
  style?: Style,
};

const TextField = React.forwardRef<
  Props,
  ?HTMLInputElement | ?HTMLTextAreaElement,
>(
  (
    {
      style,
      className,
      label,
      fullWidth = false,
      multiline = false,
      outlined = false,
      disabled = false,
      withCharacterCounter = false,
      invalid = false,
      trailingIcon,
      leadingIcon,
      helperText,
      value,
      defaultValue,
      ...rest
    }: Props,
    ref,
  ) => {
    const rootRef = useRef<?HTMLLabelElement>();
    const foundationRef = useRef<?MDCTextFieldFoundation>();
    const inputRef = useRef<?HTMLInputElement | ?HTMLTextAreaElement>();
    const labelRef = useRef<?HTMLFloatinLabelElement>();
    const lineRippleRef = useRef<?HTMLLineRippleElement>();
    const notchedOutlineRef = useRef<?HTMLNotchedOutlineElement>();
    const helperInlineRef = useRef<?HTMLDivElement>();
    const helperTextRef = useRef<?HTMLTextFieldHelperTextElement>();
    const leadingIconRef = useRef<?HTMLTextFieldIconElement>();
    const trailingIconRef = useRef<?HTMLTextFieldIconElement>();
    const characterCounterRef = useRef<?HTMLTextFieldCharacterCounterElement>();

    const [mtClassName, mtClassNameDispatch] = useReducer(
      mtClassNamesReducer,
      new Set<string>(),
    );

    const { rippleClassName, rippleStyle } = useRipple({
      activator: inputRef,
      disabled: outlined || multiline || disabled || fullWidth,
    });

    useEffect(() => {
      const foundation = new MDCTextFieldFoundation({
        addClass: mClassName =>
          mtClassNameDispatch({ type: 'add', className: mClassName }),
        removeClass: mClassName =>
          mtClassNameDispatch({ type: 'remove', className: mClassName }),
        hasClass: mClassName =>
          rootRef.current && rootRef.current.classList.contains(mClassName),
        registerTextFieldInteractionHandler: (evtType, handler) =>
          rootRef.current && rootRef.current.addEventListener(evtType, handler),
        deregisterTextFieldInteractionHandler: (evtType, handler) =>
          rootRef.current &&
          rootRef.current.removeEventListener(evtType, handler),
        registerValidationAttributeChangeHandler: handler => {
          const input = inputRef.current;
          if (input) {
            const getAttributesList = (
              mutationsList: MutationRecord[],
            ): Array<?string> => {
              return mutationsList
                .map(mutation => mutation.attributeName)
                .filter(attributeName => attributeName);
            };

            const observer = new MutationObserver(mutationsList =>
              handler(getAttributesList(mutationsList)),
            );

            const config = { attributes: true };
            observer.observe(input, config);
            return observer;
          }

          return undefined;
        },
        deregisterValidationAttributeChangeHandler: observer =>
          observer.disconnect(),
        getNativeInput: () => inputRef.current,
        isFocused: () => document.activeElement === inputRef.current,
        registerInputInteractionHandler: (evtType, handler) =>
          inputRef.current &&
          inputRef.current.addEventListener(evtType, handler, applyPassive()),
        deregisterInputInteractionHandler: (evtType, handler) =>
          inputRef.current &&
          inputRef.current.removeEventListener(
            evtType,
            handler,
            applyPassive(),
          ),
        floatLabel: shouldFloat =>
          labelRef.current && labelRef.current.foundation.float(shouldFloat),
        getLabelWidth: () =>
          labelRef.current ? labelRef.current.foundation.getWidth() : 0,
        hasLabel: () => Boolean(labelRef.current),
        shakeLabel: shouldShake =>
          labelRef.current && labelRef.current.foundation.shake(shouldShake),
        activateLineRipple: () =>
          lineRippleRef.current && lineRippleRef.current.foundation.activate(),
        deactivateLineRipple: () =>
          lineRippleRef.current &&
          lineRippleRef.current.foundation.deactivate(),
        setLineRippleTransformOrigin: normalizedX =>
          lineRippleRef.current &&
          lineRippleRef.current.foundation.setRippleCenter(normalizedX),
        closeOutline: () =>
          notchedOutlineRef.current &&
          notchedOutlineRef.current.foundation.closeNotch(),
        hasOutline: () => Boolean(notchedOutlineRef.current), // Note. better use outlined prop?
        notchOutline: labelWidth =>
          notchedOutlineRef.current &&
          notchedOutlineRef.current.foundation.notch(labelWidth),
      });

      foundationRef.current = foundation;
    }, []);

    useEffect(() => {
      if (helperText != null && helperInlineRef.current) {
        helperTextRef.current = (helperInlineRef.current.querySelector(
          MDCTextFieldHelperTextFoundation.strings.ROOT_SELECTOR,
        ): any); // flowlint-line unclear-type:off
      }

      if (rootRef.current) {
        if (leadingIcon != null && trailingIcon != null) {
          const iconElements = rootRef.current.querySelectorAll(
            MDCTextFieldFoundation.strings.ICON_SELECTOR,
          );

          // flowlint unclear-type:off
          leadingIconRef.current = (iconElements[0]: any);
          trailingIconRef.current = (iconElements[0]: any);
        } else if (leadingIcon != null) {
          leadingIconRef.current = (rootRef.current.querySelector(
            MDCTextFieldFoundation.strings.ICON_SELECTOR,
          ): any);
        } else if (trailingIcon != null) {
          trailingIconRef.current = (rootRef.current.querySelector(
            MDCTextFieldFoundation.strings.ICON_SELECTOR,
          ): any);
          // flowlint unclear-type:error
        }
      }

      const foundation = foundationRef.current;

      if (foundation) {
        foundation.helperText_ =
          helperTextRef.current && helperTextRef.current.foundation;
        foundation.leadingIcon_ =
          leadingIconRef.current && leadingIconRef.current.foundation;
        foundation.trailingIcon_ =
          trailingIconRef.current && trailingIconRef.current.foundation;
        foundation.characterCounter_ =
          characterCounterRef.current && characterCounterRef.current.foundation;
      }

      return () => {
        helperTextRef.current = null;
        leadingIconRef.current = null;
        trailingIconRef.current = null;
      };
    }, [helperText, leadingIcon, trailingIcon]);

    useEffect(() => {
      if (foundationRef.current) {
        foundationRef.current.init();
      }

      return () => {
        if (foundationRef.current) {
          foundationRef.current.destroy();
          foundationRef.current = null;
        }
      };
    }, []);

    useEffect(() => {
      if (foundationRef.current) {
        foundationRef.current.setDisabled(disabled);
      }
    }, [disabled]);

    useEffect(() => {
      if (foundationRef.current) {
        foundationRef.current.setValid(!invalid);
      }
    }, [invalid]);

    useEffect(() => {
      if (foundationRef.current && (value != null || defaultValue != null)) {
        foundationRef.current.setValue(value != null ? value : defaultValue);
      }
    }, [defaultValue, value]);

    useImperativeHandle(ref, () => inputRef.current, []);

    const memoizedStyle = useMemo(() => ({ ...style, ...rippleStyle }), [
      rippleStyle,
      style,
    ]);

    const Input = multiline ? 'textarea' : 'input';
    const withLabel = label != null;
    const withFilledLabel =
      label != null && !outlined && !multiline && !fullWidth;
    const withLineRipple = !multiline && !outlined;
    const withNotchedOutline = outlined || multiline;
    const withRipple = !(outlined || multiline || fullWidth);

    const rootProps = {
      style: memoizedStyle,
      ref: rootRef,
      className: cx(
        'mdc-text-field',
        {
          'mdc-text-field--disabled': disabled,
          'mdc-text-field--fullwidth': fullWidth,
          'mdc-text-field--textarea': multiline,
          'mdc-text-field--no-label': !withLabel,
          'mdc-text-field--outlined': outlined,
          'mdc-text-field--with-leading-icon': leadingIcon !== undefined,
          'mdc-text-field--with-trailing-icon': trailingIcon !== undefined,
        },
        ...Array.from(mtClassName),
        ...Array.from(withRipple ? rippleClassName : []),
        className,
      ),
    };

    const inputProps = {
      ...rest,
      disabled,
      value,
      defaultValue,
      placeholder: fullWidth && !multiline && !outlined ? label : null,
      'aria-label': fullWidth && !multiline && !outlined ? label : null,
      className: 'mdc-text-field__input',
      ref: inputRef,
    };

    return (
      <>
        <label // eslint-disable-line jsx-a11y/label-has-for,jsx-a11y/label-has-associated-control
          {...rootProps}
        >
          {leadingIcon}
          {withCharacterCounter && multiline && (
            <TextFieldCharacterCounter ref={characterCounterRef} />
          )}
          <Input {...inputProps} />
          {withFilledLabel && (
            <FloatingLabel
              label={label}
              ref={labelRef}
              preFilled={value != null || defaultValue != null}
            />
          )}
          {trailingIcon}
          {withLineRipple && <LineRipple ref={lineRippleRef} />}
          {withNotchedOutline && (
            <NotchedOutline ref={notchedOutlineRef}>
              {withLabel && (
                <FloatingLabel
                  label={label}
                  ref={labelRef}
                  preFilled={value != null || defaultValue != null}
                />
              )}
            </NotchedOutline>
          )}
        </label>
        {(helperText !== undefined || withCharacterCounter) && (
          <div ref={helperInlineRef} className="mdc-text-field-helper-line">
            {helperText}
            {withCharacterCounter && !multiline && (
              <TextFieldCharacterCounter ref={characterCounterRef} />
            )}
          </div>
        )}
      </>
    );
  },
);

TextField.displayName = 'TextField';

export default TextField;
