// @flow strict
import React, {
  useRef,
  useImperativeHandle,
  useEffect,
  useReducer,
} from 'react';
import ReactDOM from 'react-dom';

import cx from 'classnames';

import MDCMenuSurfaceFoundation from '@material/menu-surface/foundation';
import * as util from '@material/menu-surface/util';

import { mtClassNamesReducer, mtStylesReducer } from '../reducers';

import type { HTMLMenuSurfaceElement } from '../mt-types';

const noOp = () => {};

type Props = {
  className?: string,
  fixed?: boolean,
  hoisted?: boolean,
  open?: boolean,
  quickOpen?: boolean,
  anchorCorner?: number,
  onOpened?: () => void,
  onClosed?: () => void,
  skipRestoreFocus?: boolean,
  anchorMargin?: {
    top?: number,
    left?: number,
    bottom?: number,
    right?: number,
  },
  coordinates?: {
    x: number,
    y: number,
  },
  anchor?: { current?: HTMLElement },
};

const MenuSurface = React.forwardRef<Props, ?HTMLMenuSurfaceElement>(
  (
    {
      className,
      quickOpen = false,
      fixed = false,
      hoisted,
      open,
      anchorCorner,
      anchorMargin,
      coordinates,
      onOpened = noOp,
      onClosed = noOp,
      skipRestoreFocus = false,
      anchor,
      ...rest
    }: Props,
    ref,
  ) => {
    const rootRef = useRef<?HTMLDivElement>();
    const prevFocusRef = useRef<?HTMLElement>();
    const foundationRef = useRef<?MDCMenuSurfaceFoundation>();
    const [mtStyle, mtStyleDispatch] = useReducer(mtStylesReducer, {});
    const [mtClassName, mtClassNameDispatch] = useReducer(
      mtClassNamesReducer,
      new Set<string>(),
    );

    useEffect(() => {
      // flowlint-next-line unclear-type:off
      const root: HTMLMenuSurfaceElement = (rootRef.current: any);
      let anchorElement;

      if (root) {
        const parentEl = root.parentElement;
        anchorElement =
          parentEl &&
          parentEl.classList.contains(
            MDCMenuSurfaceFoundation.cssClasses.ANCHOR,
          )
            ? parentEl
            : null;
      }

      const foundation = new MDCMenuSurfaceFoundation({
        addClass: mClassName =>
          mtClassNameDispatch({ type: 'add', className: mClassName }),
        removeClass: mClassName =>
          mtClassNameDispatch({ type: 'remove', className: mClassName }),
        hasClass: mClassName =>
          rootRef.current && rootRef.current.classList.contains(mClassName),
        hasAnchor: () => (anchor && anchor.current) || anchorElement,
        isElementInContainer: el =>
          rootRef.current && rootRef.current.contains(el),
        isRtl: () =>
          rootRef.current &&
          getComputedStyle(rootRef.current).getPropertyValue('direction') ===
            'rtl',
        setTransformOrigin: origin => {
          const propertyName = `${util.getTransformPropertyName(window)}Origin`;
          mtStyleDispatch({ type: 'set', name: propertyName, value: origin });
        },
        isFocused: () => document.activeElement === rootRef.current,
        saveFocus: () => {
          prevFocusRef.current = document.activeElement;
        },
        restoreFocus: () => {
          if (
            rootRef.current &&
            rootRef.current.contains(document.activeElement)
          ) {
            if (prevFocusRef.current && prevFocusRef.current.focus) {
              prevFocusRef.current.focus();
            }
          }
        },
        getInnerDimensions: () => {
          if (rootRef.current) {
            return {
              width: rootRef.current.offsetWidth,
              height: rootRef.current.offsetHeight,
            };
          }

          return { width: 0, height: 0 };
        },
        getAnchorDimensions: () => {
          const a = (anchor && anchor.current) || anchorElement;
          return a ? a.getBoundingClientRect() : null;
        },
        getWindowDimensions: () => {
          return { width: window.innerWidth, height: window.innerHeight };
        },
        getBodyDimensions: () => {
          if (document.body) {
            return {
              width: document.body.clientWidth,
              height: document.body.clientHeight,
            };
          }

          return { width: 0, height: 0 };
        },
        getWindowScroll: () => {
          return { x: window.pageXOffset, y: window.pageYOffset };
        },
        setPosition: position => {
          if ('left' in position) {
            mtStyleDispatch({
              type: 'set',
              name: 'left',
              value: position.left,
            });
          }

          if ('right' in position) {
            mtStyleDispatch({
              type: 'set',
              name: 'right',
              value: position.right,
            });
          }

          if ('top' in position) {
            mtStyleDispatch({
              type: 'set',
              name: 'top',
              value: position.top,
            });
          }

          if ('bottom' in position) {
            mtStyleDispatch({
              type: 'set',
              name: 'bottom',
              value: position.bottom,
            });
          }
        },
        setMaxHeight: height => {
          mtStyleDispatch({ type: 'set', name: 'maxHeight', value: height });
        },
      });

      foundation.init();

      foundationRef.current = foundation;

      root.foundation = foundation;

      const handleKeydown = (...args) => foundation.handleKeydown(...args);

      if (root) {
        root.addEventListener('keydown', handleKeydown);
      }

      return () => {
        if (root) {
          root.foundation = null;
          root.removeEventListener('keydown', handleKeydown);
        }

        foundation.destroy();
        foundationRef.current = null;
      };
    }, [anchor]);

    useEffect(() => {
      const foundation = foundationRef.current;

      if (foundation) {
        const handleClickBody = (...args) => {
          foundation.handleBodyClick(...args);
        };

        foundation.adapter_.notifyClose = (...args) => {
          if (document.body) {
            document.body.removeEventListener('click', handleClickBody);
          }
          onClosed(...args);
        };

        foundation.adapter_.notifyOpen = (...args) => {
          if (document.body) {
            document.body.addEventListener('click', handleClickBody);
          }
          onOpened(...args);
        };
      }
    }, [onClosed, onOpened]);

    useEffect(() => {
      if (foundationRef.current) {
        foundationRef.current.setFixedPosition(fixed);
      }
    }, [fixed]);

    useEffect(() => {
      if (foundationRef.current) {
        foundationRef.current.setIsHoisted(hoisted);
      }
    }, [hoisted]);

    useEffect(() => {
      if (foundationRef.current && anchorCorner != null) {
        foundationRef.current.setAnchorCorner(anchorCorner);
      }
    }, [anchorCorner]);

    useEffect(() => {
      if (foundationRef.current && hoisted === true && coordinates) {
        const { x, y } = coordinates;
        foundationRef.current.setAbsolutePosition(x, y);
      }
    }, [coordinates, hoisted]);

    useEffect(() => {
      if (foundationRef.current && anchorMargin) {
        foundationRef.current.setAnchorMargin(anchorMargin);
      }
    }, [anchorMargin]);

    useEffect(() => {
      if (foundationRef.current) {
        foundationRef.current.setQuickOpen(quickOpen);
      }
    }, [quickOpen]);

    useEffect(() => {
      if (foundationRef.current && open != null) {
        if (open) {
          foundationRef.current.open();
        } else {
          foundationRef.current.close(skipRestoreFocus);
        }
      }
    }, [open, skipRestoreFocus]);

    // flowlint-next-line unclear-type:off
    useImperativeHandle(ref, () => (rootRef.current: any), []);

    const node = (
      <div
        {...rest}
        ref={rootRef}
        style={mtStyle}
        className={cx(
          className,
          'mdc-menu-surface',
          ...Array.from(mtClassName),
          {
            [MDCMenuSurfaceFoundation.cssClasses.FIXED]: fixed,
          },
        )}
      />
    );

    if (document && document.body) {
      return hoisted === true
        ? ReactDOM.createPortal(node, document.body)
        : node;
    }

    return null;
  },
);

MenuSurface.displayName = 'MenuSurface';

export const { Corner } = MDCMenuSurfaceFoundation;

export default MenuSurface;
