// @flow strict
import React from 'react';
import cx from 'classnames';

import type { Node, AbstractComponent } from 'react';

import { getDisplayName } from '../utils';

export const useMenuSurfaceAnchor = () => {
  return 'mdc-menu-surface--anchor';
};

export const withMenuSurfaceAnchor = <Config: { className?: string }, Instance>(
  Component: AbstractComponent<Config, Instance>,
): AbstractComponent<Config, Instance> => {
  const Anchored = React.forwardRef<Config, Instance>(
    ({ className, ...rest }: Config, ref) => {
      const mtClassName = useMenuSurfaceAnchor();

      return (
        <Component {...rest} ref={ref} className={cx(className, mtClassName)} />
      );
    },
  );

  Anchored.displayName = `withMenuSurfaceAnchor(${getDisplayName(Component)})`;

  return Anchored;
};

type Props = {
  children: Node,
  className?: string,
};

const MenuSurfaceAnchor = React.forwardRef<Props, _>(
  ({ children, className }: Props, ref) => {
    const child = React.Children.only(children);
    const mtClassName = useMenuSurfaceAnchor();

    return (
      <child.type
        {...child.props}
        className={cx(mtClassName, className, child.props.className)}
        ref={ref}
      />
    );
  },
);

MenuSurfaceAnchor.displayName = 'MenuSurfaceAnchor';

export default MenuSurfaceAnchor;
