// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import MenuSurface, { Corner } from '../index';

afterEach(cleanup);

describe('component::MenuSurface', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<MenuSurface />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot hoisted', () => {
    const { container } = render(<MenuSurface hoisted />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot fixed', () => {
    const { container } = render(<MenuSurface fixed />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot open', () => {
    const { container } = render(<MenuSurface open />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot quickOpen', () => {
    const { container } = render(<MenuSurface quickOpen />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot skipRestoreFocus', () => {
    const { container } = render(<MenuSurface skipRestoreFocus />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot coordinates', () => {
    const { container } = render(
      <MenuSurface hoisted coordinates={{ x: 10, y: 10 }} />,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot anchorMargin', () => {
    const { container } = render(
      <MenuSurface
        anchorMargin={{ top: 10, left: 10, bottom: 10, right: 10 }}
      />,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot anchorCorner', () => {
    const { container } = render(
      <MenuSurface anchorCorner={Corner.TOP_LEFT} />,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
