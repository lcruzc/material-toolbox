// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import MenuSurfaceAnchor, { withMenuSurfaceAnchor } from '../Anchor';

afterEach(cleanup);

const WithAnchor = withMenuSurfaceAnchor(({ ...rest }) => {
  return <div {...rest}>Test</div>;
});

describe('component::MenuSurfaceAnchor', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <MenuSurfaceAnchor>
        <div>test</div>
      </MenuSurfaceAnchor>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot withMenuSurfaceAnchor', () => {
    const { container } = render(<WithAnchor />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
