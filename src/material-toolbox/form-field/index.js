// @flow strict
import React, { useRef, useEffect } from 'react';
import cx from 'classnames';
import MDCFormFieldFoundation from '@material/form-field/foundation';

import type { Node } from 'react';
import type { HTMLInputElementWithRipple } from '../mt-types';

type Props = {
  children: Node,
  label: string,
  alignEnd?: boolean,
};

const FormField = ({ children, label, alignEnd = false, ...rest }: Props) => {
  const rootRef = useRef();
  const labelRef = useRef<?HTMLLabelElement>();

  useEffect(() => {
    const root = rootRef.current;

    const getInput = (): ?HTMLInputElementWithRipple => {
      if (root) {
        // flowlint-next-line unclear-type:off
        return (root.querySelector('input'): any);
      }

      return undefined;
    };

    const foundation = new MDCFormFieldFoundation({
      registerInteractionHandler: (type, handler) =>
        labelRef.current && labelRef.current.addEventListener(type, handler),
      deregisterInteractionHandler: (type, handler) =>
        labelRef.current && labelRef.current.removeEventListener(type, handler),
      activateInputRipple: () => {
        const input = getInput();

        if (input && input.ripple) {
          input.ripple.activate();
        }
      },
      deactivateInputRipple: () => {
        const input = getInput();

        if (input && input.ripple) {
          input.ripple.deactivate();
        }
      },
    });

    foundation.init();

    return () => {
      foundation.destroy();
    };
  }, []);

  return (
    <div
      ref={rootRef}
      className={cx('mdc-form-field', {
        'mdc-form-field--align-end': alignEnd,
      })}
    >
      {children}
      <label // eslint-disable-line jsx-a11y/label-has-for, jsx-a11y/label-has-associated-control
        {...rest}
        ref={labelRef}
      >
        {label}
      </label>
    </div>
  );
};

export default FormField;
