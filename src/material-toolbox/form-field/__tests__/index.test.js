// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import FormField from '../index';

afterEach(cleanup);

describe('component::Ripple', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <FormField label="Should render label">
        <input type="button" />
      </FormField>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot alignEnd', () => {
    const { container } = render(
      <FormField label="Should render label" alignEnd>
        <input type="button" />
      </FormField>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
