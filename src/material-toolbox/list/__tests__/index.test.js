// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import List from '../index';

afterEach(cleanup);

describe('component::List', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <List as="nav" href="#">
        Item
      </List>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot dense', () => {
    const { container } = render(<List dense>List content</List>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot nonInterative', () => {
    const { container } = render(<List nonInterative>List content</List>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot avatarList', () => {
    const { container } = render(<List avatarList>List content</List>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot twoLines', () => {
    const { container } = render(<List twoLines>List content</List>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot wrapFocus', () => {
    const { container } = render(<List wrapFocus>List content</List>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot vertical', () => {
    const { container } = render(<List vertical>List content</List>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot listbox role', () => {
    const { container } = render(<List role="listbox">List content</List>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot group role', () => {
    const { container } = render(<List role="group">List content</List>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot radiogroup role', () => {
    const { container } = render(<List role="radiogroup">List content</List>);
    expect(container.firstChild).toMatchSnapshot();
  });
});
