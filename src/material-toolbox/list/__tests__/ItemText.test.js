// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ListItemText from '../ItemText';

afterEach(cleanup);

describe('component::ListItemText', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<ListItemText>Normal</ListItemText>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot primary', () => {
    const { container } = render(<ListItemText primary>Primary</ListItemText>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot secondary', () => {
    const { container } = render(
      <ListItemText secondary>Secondary</ListItemText>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
