// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ListSubheader from '../Subheader';

afterEach(cleanup);

describe('component::ListSubheader', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<ListSubheader>Title</ListSubheader>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot as h1', () => {
    const { container } = render(<ListSubheader as="h1">Title</ListSubheader>);
    expect(container.firstChild).toMatchSnapshot();
  });
});
