// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ListItem from '../Item';

afterEach(cleanup);

describe('component::ListItem', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <ListItem as="a" href="#">
        Item
      </ListItem>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot checkbox', () => {
    const { container } = render(<ListItem role="checkbox">Selected</ListItem>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot radio', () => {
    const { container } = render(<ListItem role="checkbox">Selected</ListItem>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot disabled', () => {
    const { container } = render(<ListItem disabled>Disabled</ListItem>);
    expect(container.firstChild).toMatchSnapshot();
  });
});
