// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ListItemDetail from '../ItemDetail';

afterEach(cleanup);

describe('component::ListItemDetail', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <ListItemDetail as="div">meta</ListItemDetail>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot as h1', () => {
    const { container } = render(<ListItemDetail end>end</ListItemDetail>);
    expect(container.firstChild).toMatchSnapshot();
  });
});
