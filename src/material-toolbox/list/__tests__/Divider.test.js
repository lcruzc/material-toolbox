// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ListDivider from '../Divider';

afterEach(cleanup);

describe('component::ListDivider', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<ListDivider as="hr" />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot inset', () => {
    const { container } = render(<ListDivider inset />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot padded', () => {
    const { container } = render(<ListDivider padded />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
