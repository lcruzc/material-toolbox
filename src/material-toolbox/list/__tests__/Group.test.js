// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ListGroup from '../Group';

afterEach(cleanup);

describe('component::ListGroup', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<ListGroup as="section" />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
