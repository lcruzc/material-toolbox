// @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  primary?: boolean,
  secondary?: boolean,
  className?: string,
};

const ListItemText = ({
  className,
  secondary = false,
  primary = false,
  ...rest
}: Props) => {
  const classname = cx(className, {
    'mdc-list-item__text': !secondary && !primary,
    'mdc-list-item__primary-text': primary,
    'mdc-list-item__secondary-text': secondary,
  });

  return <span {...rest} className={classname} />;
};

export default ListItemText;
