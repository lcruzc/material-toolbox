// @flow strict
import React from 'react';
import cx from 'classnames';

import type { ElementType } from 'react';

type Props = {
  as?: ElementType,
  className?: string,
};

const ListGroup = ({ as: Component = 'div', className, ...rest }: Props) => (
  <Component {...(rest: mixed)} className={cx('mdc-list-group', className)} />
);

export default ListGroup;
