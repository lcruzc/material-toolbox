// @flow strict
import React, { useEffect, useRef, useImperativeHandle } from 'react';
import cx from 'classnames';

import MDCListFoundation from '@material/list/foundation';
import { closest, matches } from '@material/dom/ponyfill';

import type { Node, ElementType } from 'react';

import type { HTMLListItemElement, HTMLListElement } from '../mt-types';

const noOp = () => {};

type Props = {
  as?: ElementType,
  asActived?: true,
  avatarList?: boolean,
  children?: Node,
  className?: string,
  dense?: boolean,
  nonInterative?: boolean,
  onSelected?: (selected: number[] | number) => void,
  role?: 'list' | 'listbox' | 'radiogroup' | 'group' | 'menu',
  selected?: number | number[],
  twoLines?: boolean,
  vertical?: boolean,
  wrapFocus?: boolean,
};

const { cssClasses, strings } = MDCListFoundation;

const List = React.forwardRef<Props, ?HTMLListElement>(
  (
    {
      as: Component = 'ul',
      asActived = false,
      avatarList = false,
      children,
      className,
      dense = false,
      nonInterative = false,
      onSelected = noOp,
      role = 'list',
      selected,
      twoLines = false,
      vertical = true,
      wrapFocus = false,
      ...rest
    }: Props,
    ref,
  ) => {
    const foundationRef = useRef<?MDCListFoundation>();
    const rootRef = useRef();
    const listElementsRef = useRef<HTMLListItemElement[]>([]);
    const childrenCount = React.Children.count(children);

    useEffect(() => {
      const root = rootRef.current;

      if (root) {
        listElementsRef.current = [].slice.call(
          root.querySelectorAll(`.${cssClasses.LIST_ITEM_CLASS}`),
        );

        [].slice
          .call(root.querySelectorAll(strings.FOCUSABLE_CHILD_ELEMENTS))
          .forEach((el: HTMLElement) => el.setAttribute('tabindex', '-1'));

        if (
          (selected == null || selected.length === 0) &&
          listElementsRef.current.length > 0 &&
          listElementsRef.current[0].setMtAttribute
        ) {
          listElementsRef.current[0].setMtAttribute('tabindex', 0);
        }

        root.listElements = listElementsRef.current || [];

        return () => {
          listElementsRef.current = [];
          delete root.listElements;
        };
      }

      return () => {};
    }, [childrenCount, selected]);

    useEffect(() => {
      const foundation = new MDCListFoundation({
        addClassForElementIndex: (index, mClassName) => {
          const element = listElementsRef.current[index];
          if (element) {
            element.addMtClassName(mClassName);
          }
        },
        focusItemAtIndex: index => {
          // flowlint-next-line unclear-type:off
          const element = ((listElementsRef.current[index]: any): HTMLElement);
          if (element) {
            element.focus();
          }
        },
        getAttributeForElementIndex: (index, attr) =>
          listElementsRef.current[index].getAttribute(attr),
        getFocusedElementIndex: () =>
          listElementsRef.current.indexOf(document.activeElement),
        getListItemCount: () =>
          listElementsRef.current && listElementsRef.current.length,
        hasCheckboxAtIndex: index => {
          const listItem = listElementsRef.current[index];
          return !!listItem.querySelector(strings.CHECKBOX_SELECTOR);
        },
        hasRadioAtIndex: index => {
          const listItem = listElementsRef.current[index];
          return !!listItem.querySelector(strings.RADIO_SELECTOR);
        },
        isCheckboxCheckedAtIndex: index => {
          const listItem = listElementsRef.current[index];
          const toggleEl = listItem.querySelector<HTMLInputElement>(
            strings.CHECKBOX_SELECTOR,
          );
          return toggleEl.checked;
        },
        isFocusInsideList: () => {
          return (
            rootRef.current != null &&
            rootRef.current.contains(document.activeElement)
          );
        },
        isRootFocused: () => document.activeElement === rootRef.current,
        removeClassForElementIndex: (index, mClassName) => {
          const element = listElementsRef.current[index];
          if (element) {
            element.removeMtClassName(mClassName);
          }
        },
        setAttributeForElementIndex: (index, attr, value) => {
          const element = listElementsRef.current[index];
          if (element) {
            element.setMtAttribute(attr, value);
          }
        },
        setCheckedCheckboxOrRadioAtIndex: (index, isChecked) => {
          const listItem = listElementsRef.current[index];
          const toggleEl = listItem.querySelector<HTMLInputElement>(
            MDCListFoundation.strings.CHECKBOX_RADIO_SELECTOR,
          );

          if (toggleEl) {
            toggleEl.checked = isChecked;

            const event = document.createEvent('Event');
            event.initEvent('change', true, true);
            toggleEl.dispatchEvent(event);
          }
        },
        setTabIndexForListItemChildren: (listItemIndex, tabIndexValue) => {
          const element = listElementsRef.current[listItemIndex];
          const listItemChildren: HTMLElement[] = element.querySelectorAll(
            strings.CHILD_ELEMENTS_TO_TOGGLE_TABINDEX,
          );
          listItemChildren.forEach(el =>
            el.setAttribute('tabindex', tabIndexValue),
          );
        },
        // Note. do nothing as ListItem should handle this
        setEnabled: () => {},
      });

      const getListItemIndex = (evt: SyntheticEvent<HTMLElement>) => {
        const eventTarget = evt.target; // as Element;
        const nearestParent = closest(
          eventTarget,
          `.${cssClasses.LIST_ITEM_CLASS}, .${cssClasses.ROOT}`,
        );

        // Get the index of the element if it is a list item.
        if (
          nearestParent &&
          matches(nearestParent, `.${cssClasses.LIST_ITEM_CLASS}`)
        ) {
          return listElementsRef.current.indexOf(nearestParent);
        }

        return -1;
      };

      const root = rootRef.current;

      const handleKeydownEvent = (evt: SyntheticKeyboardEvent<HTMLElement>) => {
        const index = getListItemIndex(evt);
        const { target } = evt;
        foundation.handleKeydown(
          evt,
          // flowlint-next-line unclear-type:off
          ((target: any): HTMLElement).classList.contains(
            cssClasses.LIST_ITEM_CLASS,
          ),
          index,
        );
      };

      const handleClick = (evt: SyntheticMouseEvent<HTMLElement>) => {
        const index = getListItemIndex(evt);
        const { target } = evt;

        // Toggle the checkbox only if it's not the target of the event,
        // or the checkbox will have 2 change events.
        const toggleCheckbox = !matches(
          target,
          strings.CHECKBOX_RADIO_SELECTOR,
        );
        foundation.handleClick(index, toggleCheckbox);
      };

      const handleFocusInEvent = (evt: SyntheticFocusEvent<HTMLElement>) => {
        const index = getListItemIndex(evt);
        foundation.handleFocusIn(evt, index);
      };

      const handleFocusOutEvent = (evt: SyntheticFocusEvent<HTMLElement>) => {
        const index = getListItemIndex(evt);
        foundation.handleFocusOut(evt, index);
      };

      if (root) {
        root.addEventListener('keydown', handleKeydownEvent);
        root.addEventListener('click', handleClick);
        root.addEventListener('focusin', handleFocusInEvent);
        root.addEventListener('focusout', handleFocusOutEvent);
      }

      foundation.init();

      // this.initializeListType();

      foundationRef.current = foundation;

      return () => {
        if (root) {
          root.removeEventListener('keydown', handleKeydownEvent);
          root.removeEventListener('click', handleClick);
          root.removeEventListener('focusin', handleFocusInEvent);
          root.removeEventListener('focusout', handleFocusOutEvent);
        }

        foundationRef.current = null;

        foundation.destroy();
      };
    }, [className]);

    useEffect(() => {
      const foundation = foundationRef.current;
      if (foundation) {
        foundation.setWrapFocus(wrapFocus);
      }
    }, [wrapFocus]);

    useEffect(() => {
      const foundation = foundationRef.current;
      if (foundation) {
        foundation.setSingleSelection(role === 'listbox');
      }
    }, [role]);

    useEffect(() => {
      const foundation = foundationRef.current;
      if (foundation) {
        if (role !== 'list') {
          foundation.setVerticalOrientation(vertical);
        }
        foundation.layout();
      }
    }, [role, vertical]);

    useEffect(() => {
      const foundation = foundationRef.current;

      if (foundation) {
        foundation.setUseActivatedClass(asActived);
      }
    }, [asActived]);

    useEffect(() => {
      const foundation = foundationRef.current;

      if (foundation) {
        foundation.adapter_.notifyAction = onSelected;
      }
    }, [onSelected]);

    useEffect(() => {
      const foundation = foundationRef.current;
      if (foundation && selected != null) {
        foundation.setSelectedIndex(selected);
      }
    }, [selected]);

    useImperativeHandle(ref, () => rootRef.current, []);

    const extraProps = {};

    if (role !== 'list') {
      extraProps['aria-orientation'] =
        vertical === true ? 'vertical' : 'horizontal';
    }

    return (
      <Component
        {...(rest: mixed)}
        {...extraProps}
        role={role}
        ref={rootRef}
        className={cx('mdc-list', className, {
          'mdc-list--two-line': twoLines,
          'mdc-list--avatar-list': avatarList,
          'mdc-list--dense': dense,
          'mdc-list--non-interactive': nonInterative,
        })}
      >
        {children}
      </Component>
    );
  },
);

List.displayName = 'List';

export default List;
