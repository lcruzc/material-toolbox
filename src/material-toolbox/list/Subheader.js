// @flow strict
import React from 'react';
import cx from 'classnames';

import type { ElementType } from 'react';

type Props = {
  as?: ElementType,
  className?: string,
};

const ListSubheader = ({ as: Component = 'h3', className, ...rest }: Props) => (
  <Component
    {...(rest: mixed)}
    className={cx('mdc-list-group__subheader', className)}
  />
);

export default ListSubheader;
