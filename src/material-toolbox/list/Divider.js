// @flow strict
import React from 'react';
import cx from 'classnames';

import type { ElementType } from 'react';

type Props = {
  as?: ElementType,
  inset?: boolean,
  padded?: boolean,
};

const ListDivider = ({
  as: Component = 'li',
  inset = false,
  padded = false,
  ...rest
}: Props) => (
  <Component
    {...(rest: mixed)}
    role="separator"
    className={cx('mdc-list-divider', {
      'mdc-list-divider--inset': inset,
      'mdc-list-divider--padded': padded,
    })}
  />
);

export default ListDivider;
