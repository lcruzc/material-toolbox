// @flow strict
import React, {
  useRef,
  useEffect,
  useMemo,
  useReducer,
  useImperativeHandle,
} from 'react';
import cx from 'classnames';

import type { ElementType } from 'react';

import { useRipple } from '../ripple';
import { mtAttrsReducer, mtClassNamesReducer } from '../reducers';

import type { Style } from '../mt-types';

type Props = {
  as?: ElementType,
  className?: string,
  style?: Style,
  role?: 'option' | 'checkbox' | 'radio' | 'menuitem',
  disabled?: boolean,
};

const ListItem = React.forwardRef<Props, _>(
  (
    {
      as: Component = 'li',
      className,
      style,
      role,
      disabled = false,
      ...rest
    }: Props,
    ref,
  ) => {
    const rootRef = useRef();
    const [mtAttrs, mtAttrsDistpach] = useReducer(mtAttrsReducer, {});
    const [mtClassName, mtClassNameDispatch] = useReducer(
      mtClassNamesReducer,
      new Set<string>(),
    );

    const { rippleStyle, rippleClassName } = useRipple({
      activator: rootRef,
    });

    useEffect(() => {
      const root = rootRef.current;
      mtAttrsDistpach({ type: 'set', name: 'tabIndex', value: -1 });

      if (root) {
        root.setMtAttribute = (attr, value) => {
          const name = { tabindex: 'tabIndex' }[attr] || attr;
          mtAttrsDistpach({ type: 'set', name, value });
        };
        root.removeMtAttribute = attr => {
          const name = { tabindex: 'tabIndex' }[attr] || attr;
          mtAttrsDistpach({ type: 'unset', name });
        };
        root.addMtClassName = mClassName => {
          mtClassNameDispatch({ type: 'add', className: mClassName });
        };
        root.removeMtClassName = mClassName => {
          mtClassNameDispatch({ type: 'remove', className: mClassName });
        };
      }
    }, [Component]);

    useImperativeHandle(ref, () => rootRef.current, []);

    const memoizedStyle = useMemo(() => ({ ...style, ...rippleStyle }), [
      style,
      rippleStyle,
    ]);

    const memoizedClassName = useMemo(
      () =>
        cx(
          'mdc-list-item',
          className,
          ...Array.from(rippleClassName),
          ...Array.from(mtClassName),
          {
            'mdc-list-item--disabled': disabled,
          },
        ),
      [className, disabled, mtClassName, rippleClassName],
    );

    return (
      <Component
        {...(rest: mixed)}
        {...mtAttrs}
        role={role}
        ref={rootRef}
        style={memoizedStyle}
        className={memoizedClassName}
      />
    );
  },
);

ListItem.displayName = 'ListItem';

export default ListItem;
