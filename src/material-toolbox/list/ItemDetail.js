// @flow strict
import React from 'react';
import cx from 'classnames';

import type { ElementType } from 'react';

type Props = {
  end?: boolean,
  className?: string,
  as?: ElementType,
};

const ListItemDetail = ({
  as: Component = 'span',
  className,
  end = false,
  ...rest
}: Props) => {
  const cn = cx(className, `mdc-list-item__${end ? 'meta' : 'graphic'}`);

  return <Component {...(rest: mixed)} className={cn} />;
};

export default ListItemDetail;
