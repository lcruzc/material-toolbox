// @flow strict
import React, { useMemo } from 'react';
import cx from 'classnames';

import type { Node } from 'react';

type useProps = {
  background?: 'primary' | 'secondary' | 'background' | 'surface',
  color?:
    | 'primary'
    | 'secondary'
    | 'on-primary'
    | 'on-secondary'
    | 'on-surface',
  on?: 'light' | 'dark',
  text?: 'primary' | 'secondary' | 'hint' | 'disabled' | 'icon',
};

const useTheme = ({
  background = '',
  color = '',
  on = '',
  text = '',
}: useProps) => {
  return useMemo(
    () =>
      cx({
        [`mdc-theme--${background}`]:
          background === 'background' || background === 'surface',
        [`mdc-theme--${background}-bg`]:
          background === 'primary' || background === 'secondary',
        [`mdc-theme--${color}`]: color !== '' && on === '' && text === '',
        [`mdc-theme--text-${text}-on-${on}`]:
          on !== '' && text !== '' && color === '',
      }),
    [background, color, on, text],
  );
};

type Props = {
  ...useProps,
  children?: Node,
  className?: string,
};

const Theme = ({
  background,
  children,
  className,
  color,
  on,
  text,
  ...rest
}: Props) => {
  const composedClassName = cx(
    className,
    useTheme({ background, color, on, text }),
  );

  return React.Children.map(children, child => {
    const newClassName = cx(composedClassName, child.props.className);
    return <child.type {...child.props} {...rest} className={newClassName} />;
  });
};

export default Theme;
