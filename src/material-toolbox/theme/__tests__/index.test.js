// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Theme from '../index';

afterEach(cleanup);

describe('component::Theme', () => {
  it('Should match snapshoot without properties', () => {
    const { container } = render(
      <Theme>
        <div />
      </Theme>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot with background', () => {
    const { container } = render(
      <Theme background="primary">
        <div />
      </Theme>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot with color', () => {
    const { container } = render(
      <Theme background="primary">
        <div />
      </Theme>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot with color and background', () => {
    const { container } = render(
      <Theme background="primary" color="on-primary">
        <div />
      </Theme>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot with text and on', () => {
    const { container } = render(
      <Theme text="primary" on="light">
        <div />
      </Theme>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot with text only or on only', () => {
    const { container } = render(
      <Theme text="primary">
        <div />
      </Theme>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
