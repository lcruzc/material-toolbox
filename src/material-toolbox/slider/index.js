// @flow strict
import React, { useRef, useEffect, useReducer } from 'react';
import cx from 'classnames';

import MDCSliderFoundation from '@material/slider/foundation';

import { applyPassive } from '@material/dom/events';

import { mtClassNamesReducer, mtAttrsReducer } from '../reducers';

const noOp = () => {};

type Props = {
  min?: number,
  max?: number,
  step?: number,
  value?: number,
  label?: string,
  discrete?: boolean,
  withMarkers?: boolean,
  disabled?: boolean,
  className?: string,
  tabIndex?: number | string,
  onInput?: (value: number) => void,
  onChange?: (value: number) => void,
};

const Slider = ({
  label,
  step = 0,
  min = 0,
  max = 100,
  value = 0,
  discrete = false,
  withMarkers = false,
  disabled = false,
  tabIndex = 0,
  onInput = noOp,
  onChange = noOp,
  className,
  ...rest
}: Props) => {
  // NOTE. Maybe useMemo?
  const initialClassName = cx({
    'mdc-slider--discrete': discrete,
    'mdc-slider--display-markers': withMarkers,
  });

  const foundationRef = useRef<?MDCSliderFoundation>();
  const rootRef = useRef<?HTMLDivElement>();
  const thumbContainerRef = useRef<?HTMLDivElement>();
  const trackRef = useRef<?HTMLDivElement>();
  const pinValueMarkerRef = useRef<?HTMLSpanElement>();
  const trackMarkerContainerRef = useRef<?HTMLDivElement>();
  const [mtClassName, mtClassNameDispatch] = useReducer(
    mtClassNamesReducer,
    new Set<string>([initialClassName]),
  );

  const [mtRootAttrs, mtRootAttrsDispatch] = useReducer(mtAttrsReducer, {});

  useEffect(() => {
    mtClassNameDispatch({
      type: 'reset',
      className: initialClassName,
    });

    const foundation = new MDCSliderFoundation({
      hasClass: mClassName =>
        rootRef.current && rootRef.current.classList.contains(mClassName),
      addClass: mClassName =>
        mtClassNameDispatch({ type: 'add', className: mClassName }),
      removeClass: mClassName =>
        mtClassNameDispatch({ type: 'remove', className: mClassName }),
      getAttribute: name =>
        rootRef.current && rootRef.current.getAttribute(name),
      setAttribute: (name, mValue) => {
        if (name !== 'tabindex') {
          mtRootAttrsDispatch({ type: 'set', name, value: mValue });
        }
      },
      removeAttribute: name => {
        if (name !== 'tabindex') {
          mtRootAttrsDispatch({ type: 'unset', name });
        }
      },
      computeBoundingRect: () =>
        rootRef.current && rootRef.current.getBoundingClientRect(),
      registerInteractionHandler: (evtType, handler) =>
        rootRef.current &&
        rootRef.current.addEventListener(evtType, handler, applyPassive()),
      deregisterInteractionHandler: (evtType, handler) =>
        rootRef.current &&
        rootRef.current.addEventListener(evtType, handler, applyPassive()),
      registerThumbContainerInteractionHandler: (evtType, handler) =>
        thumbContainerRef.current &&
        thumbContainerRef.current.addEventListener(
          evtType,
          handler,
          applyPassive(),
        ),
      deregisterThumbContainerInteractionHandler: (evtType, handler) =>
        thumbContainerRef.current &&
        thumbContainerRef.current.removeEventListener(
          evtType,
          handler,
          applyPassive(),
        ),
      registerBodyInteractionHandler: (evtType, handler) =>
        document.body && document.body.addEventListener(evtType, handler),
      deregisterBodyInteractionHandler: (evtType, handler) =>
        document.body && document.body.removeEventListener(evtType, handler),
      registerResizeHandler: handler =>
        window.addEventListener('resize', handler),
      deregisterResizeHandler: handler =>
        window.removeEventListener('resize', handler),
      setThumbContainerStyleProperty: (propertyName, mValue) => {
        if (thumbContainerRef.current) {
          thumbContainerRef.current.style.setProperty(propertyName, mValue);
        }
      },
      setTrackStyleProperty: (propertyName, mValue) => {
        if (trackRef.current) {
          trackRef.current.style.setProperty(propertyName, mValue);
        }
      },
      setMarkerValue: mValue => {
        const pinValueMarker = pinValueMarkerRef.current;

        if (pinValueMarker) {
          pinValueMarker.innerText = mValue.toLocaleString();
        }
      },
      appendTrackMarkers: numMarkers => {
        // NOTE.- Maybe use a counter to add elements in react markup
        const trackMarkerContainer = trackMarkerContainerRef.current;

        if (trackMarkerContainer) {
          const frag = document.createDocumentFragment();
          for (let i = 0; i < numMarkers; i += 1) {
            const marker = document.createElement('div');
            marker.classList.add('mdc-slider__track-marker');
            frag.appendChild(marker);
          }
          trackMarkerContainer.appendChild(frag);
        }
      },
      removeTrackMarkers: () => {
        const trackMarkerContainer = trackMarkerContainerRef.current;

        while (trackMarkerContainer && trackMarkerContainer.firstChild) {
          trackMarkerContainer.removeChild(trackMarkerContainer.firstChild);
        }
      },
      setLastTrackMarkersStyleProperty: (propertyName, mValue) => {
        if (rootRef.current) {
          // We remove and append new nodes, thus, the last track marker must be dynamically found.
          const lastTrackMarker = rootRef.current.querySelector(
            MDCSliderFoundation.strings.LAST_TRACK_MARKER_SELECTOR,
          );

          if (lastTrackMarker) {
            lastTrackMarker.style.setProperty(propertyName, mValue);
          }
        }
      },
      isRTL: () =>
        rootRef.current &&
        getComputedStyle(rootRef.current).direction === 'rtl',
    });

    foundation.init();

    if (withMarkers) {
      foundation.setupTrackMarker();
    }

    foundationRef.current = foundation;

    return () => {
      foundation.destroy();
      foundationRef.current = null;
    };
  }, [discrete, initialClassName, withMarkers]);

  useEffect(() => {
    if (foundationRef.current) {
      foundationRef.current.adapter_.notifyInput = () => {
        if (foundationRef.current) {
          onInput(foundationRef.current.getValue());
        }
      };
      foundationRef.current.adapter_.notifyChange = () => {
        if (foundationRef.current) {
          onChange(foundationRef.current.getValue());
        }
      };
      foundationRef.current.getTabIndex = () => tabIndex;
    }
  }, [onChange, onInput, tabIndex]);

  useEffect(() => {
    if (foundationRef.current) {
      foundationRef.current.setMin(min);
    }
  }, [min]);

  useEffect(() => {
    if (foundationRef.current) {
      foundationRef.current.setMax(max);
    }
  }, [max]);

  useEffect(() => {
    if (foundationRef.current) {
      foundationRef.current.setStep(step);
    }
  }, [step]);

  useEffect(() => {
    if (foundationRef.current) {
      foundationRef.current.setValue(value);
    }
  }, [value]);

  useEffect(() => {
    if (foundationRef.current) {
      foundationRef.current.setDisabled(disabled);
    }
  }, [disabled]);

  return (
    <div
      {...rest}
      {...mtRootAttrs}
      ref={rootRef}
      className={cx('mdc-slider', ...Array.from(mtClassName), className)}
      tabIndex={disabled ? undefined : tabIndex}
      role="slider" // eslint-disable-line jsx-a11y/role-has-required-aria-props
      aria-label={label}
    >
      <div className="mdc-slider__track-container">
        <div ref={trackRef} className="mdc-slider__track" />
        {withMarkers && (
          <div
            ref={trackMarkerContainerRef}
            className="mdc-slider__track-marker-container"
          />
        )}
      </div>
      <div ref={thumbContainerRef} className="mdc-slider__thumb-container">
        {discrete && (
          <div className="mdc-slider__pin">
            <span
              ref={pinValueMarkerRef}
              className="mdc-slider__pin-value-marker"
            />
          </div>
        )}
        <svg className="mdc-slider__thumb" width="21" height="21">
          <circle cx="10.5" cy="10.5" r="7.875" />
        </svg>
        <div className="mdc-slider__focus-ring" />
      </div>
    </div>
  );
};

export default Slider;
