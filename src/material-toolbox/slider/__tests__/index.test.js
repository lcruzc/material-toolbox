// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Slider from '../index';

afterEach(cleanup);

describe('component::Slider', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<Slider />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot min', () => {
    const { container } = render(<Slider min={20} step={2} />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot max', () => {
    const { container } = render(<Slider max={20} step={2} />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot discrete', () => {
    const { container } = render(<Slider discrete />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot withMarkers', () => {
    const { container } = render(<Slider discrete withMarkers />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot disabled', () => {
    const { container } = render(<Slider disabled />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
