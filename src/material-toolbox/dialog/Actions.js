// @flow strict
import React, { useMemo } from 'react';
import cx from 'classnames';

import type { Node } from 'react';

type Props = {
  children: Node,
  className?: string,
  name?: string,
  reversed?: boolean,
};

const DialogActions = ({
  name,
  reversed = false,
  className,
  children,
  ...rest
}: Props) => {
  const childrenA = useMemo(() => {
    if (reversed) {
      return React.Children.toArray(children).reverse();
    }

    return children;
  }, [children, reversed]);

  return (
    <footer {...rest} className={cx('mdc-dialog__actions', className)}>
      {childrenA}
    </footer>
  );
};

export default DialogActions;
