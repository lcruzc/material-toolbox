// @flow strict
import React from 'react';
import classnames from 'classnames';

type Props = {
  className?: string,
  name?: string,
  reversed?: boolean,
};

const DialogContent = ({ name, className, reversed, ...rest }: Props) => (
  <section
    {...rest}
    id={`${name || ''}-content`}
    className={classnames(className, 'mdc-dialog__content')}
  />
);

export default DialogContent;
