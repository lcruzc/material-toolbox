// @flow strict
import React from 'react';
import cx from 'classnames';

import Button from '../button';

type Props = {
  className?: string,
  default?: boolean,
};

const DialogButton = ({ className, ...rest }: Props) => {
  return (
    <Button
      {...rest}
      className={cx('mdc-button mdc-dialog__button', className)}
    />
  );
};

export default DialogButton;
