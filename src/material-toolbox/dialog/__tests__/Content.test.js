// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import DialogContent from '../Content';

afterEach(cleanup);

describe('component::Content', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<DialogContent name="test" />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
