// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import DialogActions from '../Actions';

afterEach(cleanup);

describe('component::Actions', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <DialogActions name="test">
        <button type="button">button 1</button>
        <button type="button">button 2</button>
      </DialogActions>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot reversed', () => {
    const { container } = render(
      <DialogActions name="test" reversed>
        <button type="button">button 1</button>
        <button type="button">button 2</button>
      </DialogActions>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
