// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import DialogButton from '../Button';

afterEach(cleanup);

describe('component::Button', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<DialogButton>Button</DialogButton>);
    expect(container.firstChild).toMatchSnapshot();
  });
});
