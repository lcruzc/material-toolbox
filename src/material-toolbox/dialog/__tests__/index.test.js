// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';
import { act } from 'react-dom/test-utils';

import Dialog from '../index';

afterEach(cleanup);

describe('component::Dialog', () => {
  it('Should match snapshoot', () => {
    act(() => {
      const { container } = render(<Dialog name="test" />);
      expect(container.firstChild).toMatchSnapshot();
    });
    act(() => {
      const { container } = render(<Dialog name="test" open />);
      expect(container.firstChild).toMatchSnapshot();
    });
  });

  it('Should match snapshoot with title', () => {
    act(() => {
      const { container } = render(<Dialog name="test" title="title dialog" />);
      expect(container.firstChild).toMatchSnapshot();
    });
    act(() => {
      const { container } = render(
        <Dialog name="test" title="title dialog" open />,
      );
      expect(container.firstChild).toMatchSnapshot();
    });
  });

  it('Should work with autoStackButtons', () => {
    const { container } = render(<Dialog name="test" autoStackButtons />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should work with events', () => {
    const handleOpening = () => {};
    const handleOpened = () => {};
    const handleClosing = () => {};
    const handleClosed = () => {};

    const { container } = render(
      <Dialog
        name="test"
        onOpening={handleOpening}
        onOpened={handleOpened}
        onClosing={handleClosing}
        onClosed={handleClosed}
      />,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
