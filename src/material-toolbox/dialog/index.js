// @flow strict
import React, { useState, useRef, useEffect, useReducer } from 'react';

import MDCDialogFoundation from '@material/dialog/foundation';
import * as util from '@material/dialog/util';
import cx from 'classnames';

import { FocusTrap } from 'focus-trap';
import { closest, matches } from '@material/dom/ponyfill';

import type { Node } from 'react';

import { mtClassNamesReducer } from '../reducers';

const noOp = () => {};

type Props = {
  children?: Node,
  name: string,
  title?: string,
  open?: boolean,
  autoStackButtons?: boolean,
  onClosing?: () => void,
  onClosed?: () => void,
  onOpening?: () => void,
  onOpened?: () => void,
  className?: string,
};

const Dialog = ({
  children,
  className,
  name,
  title,
  open = false,
  autoStackButtons,
  onOpening = noOp,
  onOpened = noOp,
  onClosing = noOp,
  onClosed = noOp,
  ...rest
}: Props) => {
  const foundationRef = useRef<?MDCDialogFoundation>();
  const rootRef = useRef<?HTMLDivElement>();
  const buttons = useRef<Array<HTMLElement>>([]);
  const defaultButton = useRef<?HTMLElement>();
  const content = useRef<?HTMLElement>();
  const [reversed, setReversed] = useState<boolean>(false);
  const [mtClassName, mtClassNameDistpatch] = useReducer(
    mtClassNamesReducer,
    new Set<string>(),
  );

  useEffect(() => {
    const root = rootRef.current;
    if (root) {
      buttons.current = [].slice.call(
        root.querySelectorAll(MDCDialogFoundation.strings.BUTTON_SELECTOR),
      );
      defaultButton.current = root.querySelector(
        `[${MDCDialogFoundation.strings.BUTTON_DEFAULT_ATTRIBUTE}]`,
      );
      content.current = root.querySelector(
        MDCDialogFoundation.strings.CONTENT_SELECTOR,
      );
    }
  }, []);

  useEffect(() => {
    const root = rootRef.current;
    const initialFocus = document.querySelector(
      `[${MDCDialogFoundation.strings.INITIAL_FOCUS_ATTRIBUTE}]`,
    );

    let container;

    if (root) {
      container = root.querySelector(
        MDCDialogFoundation.strings.CONTAINER_SELECTOR,
      );
    }

    const focusTrap = util.createFocusTrapInstance(
      container,
      FocusTrap,
      initialFocus,
    );

    const foundation = new MDCDialogFoundation({
      addBodyClass: mClassName =>
        document.body && document.body.classList.add(mClassName),
      addClass: mClassName =>
        mtClassNameDistpatch({ type: 'add', className: mClassName }),
      areButtonsStacked: () => util.areTopsMisaligned(buttons.current),
      clickDefaultButton: () =>
        defaultButton.current && defaultButton.current.click(),
      eventTargetMatches: (target, selector) =>
        target ? matches(target, selector) : false,
      getActionFromEvent: (evt: Event) => {
        if (!evt.target) {
          return '';
        }
        const element = closest(
          evt.target,
          `[${MDCDialogFoundation.strings.ACTION_ATTRIBUTE}]`,
        );
        return (
          element &&
          element.getAttribute(MDCDialogFoundation.strings.ACTION_ATTRIBUTE)
        );
      },
      getInitialFocusEl: () => initialFocus,
      hasClass: mClassName => root && root.classList.contains(mClassName),
      isContentScrollable: () => util.isScrollable(content.current),
      releaseFocus: () => focusTrap.deactivate(),
      removeBodyClass: mClassName =>
        document.body && document.body.classList.remove(mClassName),
      removeClass: mClassName =>
        mtClassNameDistpatch({ type: 'remove', className: mClassName }),
      trapFocus: () => focusTrap.activate(),
    });

    foundationRef.current = foundation;
    foundation.init();

    const handleClick = (...handleOpeningRest) =>
      foundation.handleClick(...handleOpeningRest);

    const handleKeydown = (...handleOpeningRest) =>
      foundation.handleKeydown(...handleOpeningRest);

    if (root) {
      root.addEventListener('click', handleClick);
      root.addEventListener('keydown', handleKeydown);
    }

    return () => {
      if (root) {
        root.removeEventListener('click', handleClick);
        root.removeEventListener('keydown', handleKeydown);
      }

      foundation.destroy();
      foundationRef.current = null;
    };
  }, []);

  useEffect(() => {
    const foundation = foundationRef.current;
    if (foundation) {
      if (open) {
        foundation.open();
      } else {
        foundation.close();
      }
    }
  }, [open]);

  useEffect(() => {
    const foundation = foundationRef.current;

    if (foundation && autoStackButtons !== undefined) {
      foundation.setAutoStackButtons(autoStackButtons);
    }
  }, [autoStackButtons]);

  useEffect(() => {
    const foundation = foundationRef.current;
    if (foundation) {
      foundation.adapter_.reverseButtons = () => setReversed(!reversed);
    }
  }, [reversed]);

  useEffect(() => {
    const foundation = foundationRef.current;
    if (foundation) {
      foundation.adapter_.notifyClosed = onClosed;
      foundation.adapter_.notifyOpened = onOpened;

      const LAYOUT_EVENTS = ['resize', 'orientationchange'];

      const handleDocumentKeydown = (...handleDocumentKeydownRest) =>
        foundation.handleDocumentKeydown(...handleDocumentKeydownRest);

      const handleLayout = () => foundation.layout();

      foundation.adapter_.notifyOpening = (...onOpeningRest) => {
        onOpening(...onOpeningRest);
        LAYOUT_EVENTS.forEach(evtType =>
          window.addEventListener(evtType, handleLayout),
        );
        document.addEventListener('keydown', handleDocumentKeydown);
      };

      foundation.adapter_.notifyClosing = (...onClosingRest) => {
        onClosing(...onClosingRest);
        LAYOUT_EVENTS.forEach(evtType =>
          window.removeEventListener(evtType, handleLayout),
        );
        document.removeEventListener('keydown', handleDocumentKeydown);
      };
    }
  }, [onClosed, onClosing, onOpened, onOpening]);

  return (
    <div
      aria-modal="true"
      role="alertdialog"
      {...(rest: mixed)}
      className={cx('mdc-dialog', className, ...Array.from(mtClassName))}
      ref={rootRef}
      aria-labelledby={`${name}-title`}
      aria-describedby={`${name}-content`}
      id={name}
    >
      <div className="mdc-dialog__container">
        <div className="mdc-dialog__surface">
          {title != null && (
            <h2 className="mdc-dialog__title" id={`${name}-title`}>
              {title}
            </h2>
          )}
          {React.Children.map(children, child => {
            return (
              <child.type {...child.props} name={name} reversed={reversed} />
            );
          })}
        </div>
      </div>
      <div className="mdc-dialog__scrim" />
    </div>
  );
};

export default Dialog;
