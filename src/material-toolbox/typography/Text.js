// @flow strict
import React from 'react';
import cx from 'classnames';

import type { ElementType } from 'react';

type Props = {
  as?: ElementType,
  className?: string,
  text:
    | 'headline1'
    | 'headline2'
    | 'headline3'
    | 'headline4'
    | 'headline5'
    | 'headline6'
    | 'subtitle1'
    | 'subtitle2'
    | 'body1'
    | 'body2'
    | 'caption'
    | 'button'
    | 'overline',
};

const Text = ({ as: Component = 'span', text, className, ...rest }: Props) => {
  const rootClassName = cx(`mdc-typography--${text}`, className);
  return <Component {...(rest: mixed)} className={rootClassName} />;
};

export default Text;
