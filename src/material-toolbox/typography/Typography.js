// @flow strict
import React from 'react';
import cx from 'classnames';

import type { Node } from 'react';

type Props = {
  children: Node,
  className?: string,
};

const Typography = ({ children, className }: Props) => {
  const composedClassName = cx('mdc-typography', className);

  return React.Children.map(children, child => {
    const newClassName = cx(composedClassName, child.props.className);
    return <child.type {...child.props} className={newClassName} />;
  });
};

export default Typography;
