import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Typography from '../Typography';

afterEach(cleanup);

describe('component::Typography', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <Typography>
        <div />
      </Typography>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot with className', () => {
    const { container } = render(
      <Typography className="extra-className">
        <div className="child-className" />
      </Typography>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
