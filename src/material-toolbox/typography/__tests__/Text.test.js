// @flow strict
import React from 'react';

import { render, cleanup } from '@testing-library/react';

import Text from '../Text';

afterEach(cleanup);

describe('component::Text', () => {
  [
    'headline1',
    'headline2',
    'headline3',
    'headline4',
    'headline5',
    'headline6',
    'subtitle1',
    'subtitle2',
    'body2',
    'body1',
    'caption',
    'button',
  ].forEach(item => {
    it(`Should match snapshoot ${item}`, () => {
      const { container } = render(<Text text={item} />);
      expect(container.firstChild).toMatchSnapshot();
    });
  });

  it('Should match snapshoot with other component', () => {
    const { container } = render(<Text text="body1" as="p" />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
