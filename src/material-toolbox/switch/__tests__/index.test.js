// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Switch from '../index';

afterEach(cleanup);

describe('component::Switch', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<Switch />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot checked', () => {
    const handleChange = jest.fn();
    const { container } = render(<Switch checked onChange={handleChange} />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot disabled', () => {
    const { container } = render(<Switch disabled />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
