// @flow strict
import React, {
  useEffect,
  useRef,
  useReducer,
  useImperativeHandle,
} from 'react';
import cx from 'classnames';

import MDCSwitchFoundation from '@material/switch/foundation';

import { mtClassNamesReducer } from '../reducers';
import { useRipple } from '../ripple';

import type { HTMLInputElementWithRipple } from '../mt-types';

type Props = {
  checked?: boolean,
  disabled?: boolean,
  className?: string,
};

const Switch = React.forwardRef<Props, ?HTMLInputElement>(
  ({ checked, disabled, className, ...rest }: Props, ref) => {
    const rootRef = useRef<?HTMLDivElement>();
    const foundationRef = useRef<?MDCSwitchFoundation>();
    const checkboxRef = useRef<?HTMLInputElement>();
    const rippleRef = useRef<?HTMLDivElement>();
    const [mtClassName, mtClassNameDispatch] = useReducer(
      mtClassNamesReducer,
      new Set<string>(),
    );

    const { rippleClassName, rippleStyle, ripple } = useRipple({
      surface: rippleRef,
      activator: checkboxRef,
      unbounded: true,
    });

    useEffect(() => {
      const foundation = new MDCSwitchFoundation({
        addClass: mClassName =>
          mtClassNameDispatch({ type: 'add', className: mClassName }),
        removeClass: mClassName =>
          mtClassNameDispatch({ type: 'remove', className: mClassName }),
      });

      foundation.init();

      foundationRef.current = foundation;

      const root = rootRef.current;

      const handleChange = (...args) => foundation.handleChange(...args);

      if (root) {
        root.addEventListener('change', handleChange);
      }

      return () => {
        if (root) {
          root.removeEventListener('change', handleChange);
        }
        foundation.destroy();
        foundationRef.current = null;
      };
    }, []);

    useEffect(() => {
      if (foundationRef.current && checkboxRef.current) {
        // flowlint-next-line unclear-type:off
        const checkbox: HTMLInputElementWithRipple = (checkboxRef.current: any);
        checkbox.ripple = ripple;
      }
    }, [ripple]);

    useEffect(() => {
      if (foundationRef.current) {
        foundationRef.current.setDisabled(disabled);
      }
    }, [disabled]);

    useEffect(() => {
      if (foundationRef.current) {
        foundationRef.current.setChecked(checked);
      }
    }, [checked]);

    useImperativeHandle(ref, () => checkboxRef.current, []);

    return (
      <div
        ref={rootRef}
        className={cx('mdc-switch', className, ...Array.from(mtClassName))}
      >
        <div className="mdc-switch__track" />
        <div
          ref={rippleRef}
          style={rippleStyle}
          className={cx(
            'mdc-switch__thumb-underlay',
            ...Array.from(rippleClassName),
          )}
        >
          <div className="mdc-switch__thumb">
            <input
              {...rest}
              checked={checked}
              disabled={disabled}
              ref={checkboxRef}
              type="checkbox"
              className="mdc-switch__native-control"
              role="switch"
            />
          </div>
        </div>
      </div>
    );
  },
);

export default Switch;
