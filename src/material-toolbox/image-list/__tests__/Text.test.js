// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ImageListText from '../index';

afterEach(cleanup);

describe('component::ImageListText', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<ImageListText>Text</ImageListText>);
    expect(container.firstChild).toMatchSnapshot();
  });
});
