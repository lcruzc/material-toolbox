// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ImageListImage from '../Image';

afterEach(cleanup);

describe('component::ImageListImage', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <ImageListImage src="/path/to/img" alt="Alt Text" />,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot asDiv and withContainer', () => {
    const { container } = render(
      <ImageListImage asDiv withContainer src="/path/to/img" alt="Alt Text" />,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
