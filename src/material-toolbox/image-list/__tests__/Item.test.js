// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ImageListItem from '../Item';

afterEach(cleanup);

describe('component::ImageListItem', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<ImageListItem>Text</ImageListItem>);
    expect(container.firstChild).toMatchSnapshot();
  });
});
