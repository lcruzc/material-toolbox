// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ImageList from '../index';

afterEach(cleanup);

describe('component::ImageList', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<ImageList />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot textProtection', () => {
    const { container } = render(<ImageList textProtection />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot masonry', () => {
    const { container } = render(<ImageList masonry />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
