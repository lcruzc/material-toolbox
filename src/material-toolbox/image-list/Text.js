// @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  className?: string,
};

const ImageListText = ({ className, ...rest }: Props) => (
  <div className="mdc-image-list__supporting">
    <span {...rest} className={cx('mdc-image-list__label', className)} />
  </div>
);

export default ImageListText;
