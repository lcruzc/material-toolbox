// @flow strict
import React, { useMemo } from 'react';
import cx from 'classnames';

type Props = {
  src: string,
  alt: string,
  asDiv?: boolean,
  withContainer?: boolean,
  className?: string,
};

const ImageListImage = ({
  className,
  src,
  alt,
  asDiv = false,
  withContainer = false,
  ...rest
}: Props) => {
  const imageClassName = cx('mdc-image-list__image', className);
  const extraProps = useMemo(() => {
    const n = {};

    if (asDiv) {
      n.style = { backgroundImage: `url(${src})` };
    } else {
      n.alt = alt;
      n.src = src;
    }

    return n;
  }, [alt, asDiv, src]);

  if (withContainer) {
    let Component = 'img';
    if (asDiv) {
      Component = 'div';
    }

    return (
      <div className="mdc-image-list__image-aspect-container">
        <Component {...rest} {...extraProps} className={imageClassName} />
      </div>
    );
  }

  return <img {...rest} src={src} alt={alt} className={imageClassName} />;
};

export default ImageListImage;
