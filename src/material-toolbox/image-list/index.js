// @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  masonry?: boolean,
  textProtection?: boolean,
  className?: string,
};

const ImageList = ({ className, masonry, textProtection, ...rest }: Props) => {
  return (
    <ul
      {...rest}
      className={cx('mdc-image-list', className, {
        'mdc-image-list--masonry': masonry,
        'mdc-image-list--with-text-protection': textProtection,
      })}
    />
  );
};

export default ImageList;
