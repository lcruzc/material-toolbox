// @flow strict
import React from 'react';
import cx from 'classnames';

type Props = {
  className?: string,
};

const ImageListItem = ({ className, ...rest }: Props) => {
  return <li {...rest} className={cx('mdc-image-list__item', className)} />;
};

export default ImageListItem;
