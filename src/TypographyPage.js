// @flow strict
import React, { useEffect } from 'react';
import Highlighter from './Highlighter';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import Text from './material-toolbox/typography/Text';
import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import Example from './Example';
import ThemedLink from './ThemedLink';

const fontCode = `<head>
  <link
    href="https://fonts.googleapis.com/css?family='Roboto:300,400,500"
    rel="stylesheet"
    >
</head>
`;

const source = `import Text from 'material-toolbox/typography/Text';
import Typography from 'material-toolbox/typography/Typography';

const Styles = () => (
  <Typography>
    <div>
      <Text text="headline1" as="h1">
        Headline 1
      </Text>
      <Text text="headline2" as="h2">
        Headline 2
      </Text>
      <Text text="headline3" as="h3">
        Headline 3
      </Text>
      <Text text="headline4" as="h4">
        Headline 4
      </Text>
      <Text text="headline5" as="h1">
        Headline 5
      </Text>
      <Text text="headline6" as="h2">
        Headline 6
      </Text>
      <Text text="subtitle1" as="h2">
        Subtitle 1
      </Text>
      <Text text="subtitle2" as="h3">
        Subtitle 2
      </Text>
      <Text text="body1" as="p">
        Body 1 paragraph. Lorem ipsum dolor sit amet, consectetur
        adipiscing elit, sed do eiusmod tempor incididunt ut labore et
        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
        exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate
        velit esse cillum dolore eu fugiat nulla pariatur.
      </Text>
      <Text text="body2" as="p">
        Body 2 paragraph. Lorem ipsum dolor sit amet, consectetur
        adipiscing elit, sed do eiusmod tempor incididunt ut labore et
        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
        exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate
        velit esse cillum dolore eu fugiat nulla pariatur.
      </Text>
      <Text text="button" as="p">
        button text
      </Text>
      <Text text="caption" as="p">
        Caption text
      </Text>
      <Text text="overline" as="p">
        overline text
      </Text>
    </div>
  </Typography>
);
`;

const sassExample = `@import '@material/typography/mdc-typography';

html, body {
  @include mdc-typography-base;
}

p {
  @include mdc-typography(body1);
}
`;

const resources = [
  {
    href: 'https://material.io/go/design-typography',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href: 'https://material.io/components/web/catalog/typography/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const TypographyPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  });

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Typography</h1>

      <Text as="p" text="overline">
        Typography component is a React wrapper of mdc-typography component.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Load Roboto</SectionTitle>

      <Highlighter language="html">{fontCode}</Highlighter>

      <SectionTitle>Mixin Usage (Optional)</SectionTitle>

      <Highlighter language="css">{sassExample}</Highlighter>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>Typography</ComponentTitle>

      <p>
        Container of typography components. Every typography texts should be
        included into this component.
      </p>

      <ComponentTitle>Text</ComponentTitle>

      <p>Container that sets the correct style</p>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>text</td>
            <td>
              String value: headline1, headline2, headline3, headline4,
              headline5, headline6, subtitle1, subtitle2, body2, body1, caption,
              button, overline
            </td>
            <td />
            <td>✔</td>
            <td>Style of text component.</td>
          </tr>
          <tr>
            <td>as</td>
            <td>React.ElementType</td>
            <td>span</td>
            <td />
            <td>Component that will be rendered</td>
          </tr>
        </tbody>
      </table>

      <SectionTitle>Demos</SectionTitle>

      <Example title="" source={source}>
        <div>
          <Text text="headline1" as="h1">
            Headline 1
          </Text>
          <Text text="headline2" as="h2">
            Headline 2
          </Text>
          <Text text="headline3" as="h3">
            Headline 3
          </Text>
          <Text text="headline4" as="h4">
            Headline 4
          </Text>
          <Text text="headline5" as="h1">
            Headline 5
          </Text>
          <Text text="headline6" as="h2">
            Headline 6
          </Text>
          <Text text="subtitle1" as="h2">
            Subtitle 1
          </Text>
          <Text text="subtitle2" as="h3">
            Subtitle 2
          </Text>
          <Text text="body1" as="p">
            Body 1 paragraph. Lorem ipsum dolor sit amet, consectetur adipiscing
            elit, sed do eiusmod tempor incididunt ut labore et dolore magna
            aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
            dolor in reprehenderit in voluptate velit esse cillum dolore eu
            fugiat nulla pariatur.
          </Text>
          <Text text="body2" as="p">
            Body 2 paragraph. Lorem ipsum dolor sit amet, consectetur adipiscing
            elit, sed do eiusmod tempor incididunt ut labore et dolore magna
            aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
            dolor in reprehenderit in voluptate velit esse cillum dolore eu
            fugiat nulla pariatur.
          </Text>
          <Text text="button" as="p">
            button text
          </Text>
          <Text text="caption" as="p">
            Caption text
          </Text>
          <Text text="overline" as="p">
            overline text
          </Text>
        </div>
      </Example>
    </LayoutCell>
  );
};

export default TypographyPage;
