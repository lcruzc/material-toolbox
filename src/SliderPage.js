// @flow strict
import React, { useEffect } from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';
import Text from './material-toolbox/typography/Text';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';

import Example from './Example';

import Slider from './material-toolbox/slider';

const source = `import React from 'react';

import Slider from 'material-toolbox/slider';

const Demo = () => {
  return (
    <React.Fragment>
      <p>Continuous</p>
      <Slider max={50} />
      <p>Discrete</p>
      <Slider max={50} discrete />
      <p>Discrete with tick markers</p>
      <Slider max={50} discrete withMarkers />
    </React.Fragment>
  );
};

export default Demo;`;

const resources = [
  {
    href: 'https://material.io/go/design-sliders',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href: 'https://material.io/components/web/catalog/input-controls/sliders/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const SliderPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Slider</h1>

      <Text as="p" text="overline">
        Button components are React wrappers of mdc-button component.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>Slider</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>value</td>
            <td>number</td>
            <td />
            <td />
            <td>Value that will be set on slider</td>
          </tr>
          <tr>
            <td>min</td>
            <td>number</td>
            <td>0</td>
            <td />
            <td>Min value on slider</td>
          </tr>
          <tr>
            <td>max</td>
            <td>number</td>
            <td>100</td>
            <td />
            <td>Max value on slider</td>
          </tr>
          <tr>
            <td>step</td>
            <td>number</td>
            <td>1</td>
            <td />
            <td>Step to use on discrete slider</td>
          </tr>
          <tr>
            <td>label</td>
            <td>string</td>
            <td />
            <td />
            <td>Text used on asistable devices.</td>
          </tr>
          <tr>
            <td>withMarkers</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Show markers on discrete slider</td>
          </tr>
          <tr>
            <td>discrete</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Create a discrete slider</td>
          </tr>
          <tr>
            <td>onInput</td>
            <td>{'(value: number) => void'}</td>
            <td />
            <td />
            <td>callback called when changing the slider.</td>
          </tr>
          <tr>
            <td>onChange</td>
            <td>{'(value: number) => void'}</td>
            <td />
            <td />
            <td>callback called when slider changed the value.</td>
          </tr>
        </tbody>
      </table>

      <SectionTitle>Demos</SectionTitle>

      <Example title="" source={source}>
        <p>Continuous</p>
        <Slider max={50} />
        <p>Discrete</p>
        <Slider max={50} discrete />
        <p>Discrete with tick markers</p>
        <Slider max={50} discrete withMarkers />
      </Example>
    </LayoutCell>
  );
};

export default SliderPage;
