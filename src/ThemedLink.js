// @flow strict
import React from 'react';

import Link from './Link';
import Theme from './material-toolbox/theme';

type Props = {
  to?: string,
};

const ThemedLink = React.forwardRef<Props, _>(({ to, ...rest }: Props, ref) => {
  const Component = to != null ? Link : 'a';
  const props = {};

  if (to != null) {
    props.to = to;
  }

  return (
    <Theme color="primary">
      <Component {...(rest: mixed)} {...props} ref={ref} />
    </Theme>
  );
});

ThemedLink.displayName = 'ThemedLink';

export default ThemedLink;
