// @flow strict
import React, { useEffect } from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import Text from './material-toolbox/typography/Text';
import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';

import SingleList from './list-examples/Single';
import TwoLinesList from './list-examples/TwoLines';
import LeadingIconList from './list-examples/LeadingIcon';
import WithActivedItemList from './list-examples/WithActivedItem';
import TrailingIconList from './list-examples/TrailingIcon';
import TwoLinesComplete from './list-examples/TwoLinesComplete';
import WithComponents from './list-examples/WithComponents';

const resources = [
  {
    href: 'https://material.io/go/design-lists',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href: 'https://material.io/components/web/catalog/lists/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const ListPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  });

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>List</h1>
      <Text as="p" text="overline">
        List components are React wrappers of mdc-list components.
      </Text>
      <SectionTitle>Resources</SectionTitle>
      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>ListDivider</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>as</td>
            <td>React.ElementType</td>
            <td>li</td>
            <td />
            <td>Component that will be rendered.</td>
          </tr>
          <tr>
            <td>inset</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Inset variant of devider</td>
          </tr>
          <tr>
            <td>padded</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>padded variant of devider</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>ListItemDetail</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>as</td>
            <td>React.ElementType</td>
            <td>li</td>
            <td />
            <td>Component that will be rendered.</td>
          </tr>
          <tr>
            <td>end</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set the end element in ListItem</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>ListItemText</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>primary</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>For two lines defines the primary text.</td>
          </tr>
          <tr>
            <td>secondary</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>For two lines defines the secondary text.</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>ListSubheader</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>as</td>
            <td>React.ElementType</td>
            <td>h3</td>
            <td />
            <td>Component that will be rendered.</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>ListItem</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>as</td>
            <td>React.ElementType</td>
            <td>li</td>
            <td />
            <td>Component that will be rendered.</td>
          </tr>
          <tr>
            <td>disabled</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Disabled the item</td>
          </tr>
          <tr>
            <td>role</td>
            <td>string: option, radio, checkbox</td>
            <td />
            <td />
            <td>Set the type of item.</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>List</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>as</td>
            <td>React.ElementType</td>
            <td>ul</td>
            <td />
            <td>Component that will be rendered.</td>
          </tr>
          <tr>
            <td>asActived</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Use actived state instead of selected</td>
          </tr>
          <tr>
            <td>avatarList</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>avatar list design enabled</td>
          </tr>
          <tr>
            <td>dense</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>dense layout</td>
          </tr>
          <tr>
            <td>nonInterative</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>non interactive list</td>
          </tr>
          <tr>
            <td>onSelected</td>
            <td>{'(selected: number[] | number) => void'}</td>
            <td />
            <td />
            <td>Allow to continue key navigation of items</td>
          </tr>
          <tr>
            <td>role</td>
            <td>string: list, listbox, radiogroup, group</td>
            <td>list</td>
            <td />
            <td>
              List role with item role: list - undefined, listbox - option,
              radiogroup - radio, group - checkbox
            </td>
          </tr>
          <tr>
            <td>selected</td>
            <td>number | number[]</td>
            <td />
            <td />
            <td>
              selected value of list, number[] for group role and number for
              radio and listbox roles
            </td>
          </tr>
          <tr>
            <td>twoLines</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>two lines design enabled</td>
          </tr>
          <tr>
            <td>vertical</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>vertical design enabled</td>
          </tr>
          <tr>
            <td>wrapFocus</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Allow to continue key navigation of items</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>ListGroup</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>as</td>
            <td>React.ElementType</td>
            <td>div</td>
            <td />
            <td>Component that will be rendered.</td>
          </tr>
        </tbody>
      </table>

      <SectionTitle>Demos</SectionTitle>

      <SingleList />

      <TwoLinesList />

      <LeadingIconList />

      <WithActivedItemList />

      <WithActivedItemList shaped />

      <TrailingIconList />

      <TwoLinesComplete />

      <WithComponents title="List with Trailing Checkbox" type="checkbox" />

      <WithComponents title="List with Trailing Radio Buttons" type="radio" />
    </LayoutCell>
  );
};

export default ListPage;
