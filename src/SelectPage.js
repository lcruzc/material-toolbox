// @flow strict
import * as React from 'react';
// import Highlighter from './Highlighter';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import Text from './material-toolbox/typography/Text';
// import Theme from './material-toolbox/theme';
// import Button from './material-toolbox/button';
import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';

import Selects from './select-examples/Selects';

const resources = [
  {
    href: 'https://material.io/go/design-text-fields',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href:
      'https://material.io/components/web/catalog/input-controls/select-menus/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const SelectPage = () => {
  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Select</h1>
      <Text as="p" text="overline">
        Select component is React wrappers of mdc-select component.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>SelectIcon</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>clickable</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Enable click feature into icon</td>
          </tr>
          <tr>
            <td>onActived</td>
            <td>{'() => void'}</td>
            <td />
            <td />
            <td>
              Callback called when component icon is clicked or Enter key.
            </td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>SelectHelperText</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>text</td>
            <td>string</td>
            <td />
            <td>✔</td>
            <td>Text to display</td>
          </tr>
          <tr>
            <td>persistent</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set persistent state</td>
          </tr>
          <tr>
            <td>validation</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set validation state</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>SelectOption</ComponentTitle>

      <ComponentTitle>Select</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>label</td>
            <td>string</td>
            <td />
            <td>✔</td>
            <td>Label text for element</td>
          </tr>
          <tr>
            <td>disabled</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set disabled state</td>
          </tr>
          <tr>
            <td>outlined</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set the outlined UI.</td>
          </tr>
          <tr>
            <td>helperText</td>
            <td>React.Node</td>
            <td />
            <td />
            <td>TextFieldHelperText element used to render the helper text</td>
          </tr>
          <tr>
            <td>leadingIcon</td>
            <td>React.Node</td>
            <td />
            <td />
            <td>TextFieldIcon element used to render the leading icon</td>
          </tr>
          <tr>
            <td>enhanced</td>
            <td>boolean</td>
            <td />
            <td />
            <td>
              Enhanced select needs <b>material-toolbox/select/Option</b>
              component instead of <b>option</b> into children
            </td>
          </tr>
          <tr>
            <td>selectedIndex</td>
            <td>number</td>
            <td />
            <td />
            <td>Select the item</td>
          </tr>
          <tr>
            <td>invalid</td>
            <td>boolean</td>
            <td />
            <td />
            <td>If true set invalid/error UI to component.</td>
          </tr>
        </tbody>
      </table>

      <SectionTitle>Demos</SectionTitle>

      <Selects />
    </LayoutCell>
  );
};

export default SelectPage;
