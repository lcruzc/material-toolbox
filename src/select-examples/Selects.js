// @flow strict
import React from 'react';

import Example from '../Example';

import Text from '../material-toolbox/typography/Text';
import Select from '../material-toolbox/select';
import SelectOption from '../material-toolbox/select/Option';

const source = `import React from 'react';

import Text from 'material-toolbox/typography/Text';
import Select from 'material-toolbox/select';
import SelectOption from 'material-toolbox/select/Option';

const Selects = ({ outlined, title, className }) => {
  return (
    <div className="select-row">
      <div>
        <Text as="p" text="subtitle1">
          {title}
        </Text>
        <Select label="Fruit" outlined={outlined} className={className}>
          <option />
          <option value="apple">Apple</option>
          <option value="orange">Orange</option>
          <option value="banana">Banana</option>
        </Select>
      </div>

      <div>
        <Text as="p" text="subtitle1">
          {title} Enhanced
        </Text>
        <Select
          label="Fruit"
          enhanced
          outlined={outlined}
          className={className}>
          <SelectOption />
          <SelectOption value="Apple">Apple</SelectOption>
          <SelectOption value="orange">Orange</SelectOption>
          <SelectOption value="banana">Banana</SelectOption>
        </Select>
      </div>
    </div>
  );
};

export default Selects;`;

type Props = {
  outlined?: boolean,
  title: string,
  className?: string,
};

const Selects = ({ outlined, title, className }: Props) => {
  return (
    <div className="select-row">
      <div>
        <Text as="p" text="subtitle1">
          {title}
        </Text>
        <Select label="Fruit" outlined={outlined} className={className}>
          <option // eslint-disable-line jsx-a11y/control-has-associated-label
          />
          <option value="apple">Apple</option>
          <option value="orange">Orange</option>
          <option value="banana">Banana</option>
        </Select>
      </div>

      <div>
        <Text as="p" text="subtitle1">
          {title} Enhanced
        </Text>
        <Select
          label="Fruit"
          enhanced
          outlined={outlined}
          className={className}
        >
          <SelectOption />
          <SelectOption value="Apple">Apple</SelectOption>
          <SelectOption value="orange">Orange</SelectOption>
          <SelectOption value="banana">Banana</SelectOption>
        </Select>
      </div>
    </div>
  );
};

const AllSelects = () => {
  return (
    <Example title="" source={source}>
      <Selects title="Filled" />
      <Selects outlined title="Outlined" />
      <Selects className="select-filled-shaped" title="Shaped Filled" />
      <Selects
        outlined
        className="select-outlined-shaped"
        title="Outlined Filled"
      />
    </Example>
  );
};

export default AllSelects;
