// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import AllSelects from '../Selects';

afterEach(cleanup);

describe('example::AllSelects', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<AllSelects />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
