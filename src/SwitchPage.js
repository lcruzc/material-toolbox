// @flow strict
import React, { useEffect } from 'react';

import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';
import Text from './material-toolbox/typography/Text';
import LayoutCell from './material-toolbox/layout-grid/Cell';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';

import Example from './Example';

import FormField from './material-toolbox/form-field';
import Switch from './material-toolbox/switch';

const source = `import React from 'react';

import FormField from 'material-toolbox/form-field';
import Switch from 'material-toolbox/switch';

const Demos = () => {
  return (
    <FormField label="off/on" for="off-on">
      <Switch id="off-on" />
    </FormField>
  );
};

export default Demos;`;

const resources = [
  {
    href: 'https://material.io/go/design-switches',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href: 'https://material.io/components/web/catalog/input-controls/switches/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const SwitchPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Switch</h1>

      <Text as="p" text="overline">
        Switch component is React wrapper of mdc-switch component.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>Switch</ComponentTitle>

      <p>Accept all checkbox input type props</p>

      <SectionTitle>Demos</SectionTitle>

      <Example title="" source={source}>
        <FormField label="off/on" htmlFor="off-on">
          <Switch id="off-on" />
        </FormField>
      </Example>
    </LayoutCell>
  );
};

export default SwitchPage;
