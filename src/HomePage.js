// @flow strict
import React from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import Text from './material-toolbox/typography/Text';

import Highlighter from './Highlighter';
import ThemedLink from './ThemedLink';

const importCode = '@import "@material/drawer/mdc-drawer";';

const HomePage = () => (
  <LayoutCell align="bottom" span={12}>
    <Text as="div" text="body2">
      <Text as="p" text="overline">
        NOTE.- This library is under deep changes, searching the best way to
        follow the React Style. Please use it with caution.
      </Text>
      <p>Second note.- Most of examples, design and content are from:</p>
      <p>
        <ThemedLink
          to="https://material.io/components/web/catalog/"
          target="_blank"
        >
          https://material.io/components/web/catalog/
        </ThemedLink>
      </p>
      <p>
        <ThemedLink
          to="https://material-components-web.appspot.com"
          target="_blank"
        >
          https://material-components-web.appspot.com
        </ThemedLink>
      </p>
      <p>
        <ThemedLink to="https://react-mdc.github.io/" target="_blank">
          https://react-mdc.github.io/
        </ThemedLink>
      </p>
      <p>
        <ThemedLink to="https://jamesmfriedman.github.io/" target="_blank">
          https://jamesmfriedman.github.io/
        </ThemedLink>
      </p>
    </Text>

    <Text as="h2" text="headline5">
      Getting started
    </Text>

    <Text as="p" text="body1">
      Get up and running with React Material Components web
    </Text>

    <Highlighter language="bash">
      $ npm install --save material-toolbox
    </Highlighter>

    <Text as="h2" text="headline5">
      Notes
    </Text>

    <p>
      1. As React is a Javascript library css only versions is not available.
    </p>

    <p>
      2. As this is a React wrapper will skip some MRC events and props this in
      favor of React declarative style.
    </p>

    <p>
      3. You have to import the css/scss files from material components, into
      your scss or the css as needed.
    </p>

    <Highlighter language="css">{importCode}</Highlighter>

    <Text as="h2" text="headline5">
      Development Notes
    </Text>

    <p>
      1. Adapters.- classNames will use javascript native add, remove, contain
      code. This because exists some transitions where react does not handle
      well because state is asynchronous and does bulk update and we need to
      update classes in the same time.
    </p>
  </LayoutCell>
);

export default HomePage;
