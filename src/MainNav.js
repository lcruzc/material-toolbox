// @flow strict
import React, { useState, useCallback } from 'react';

import { withRouter } from 'react-router-dom'; // eslint-disable-line

import type { Location } from 'react-router-dom';

import Link from './Link';
import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';

type Props = {
  className?: string,
  location: Location,
};

const navs = [
  { to: '/', label: 'Home' },
  { to: '/animation', label: 'Animation' },
  { to: '/buttons', label: 'Buttons' },
  { to: '/cards', label: 'Cards' },
  { to: '/checkboxes', label: 'Checkboxes' },
  { to: '/chips', label: 'Chips' },
  { to: '/data-tables', label: 'Data Tables' },
  { to: '/dialogs', label: 'Dialogs' },
  { to: '/drawers', label: 'Drawers' },
  { to: '/elevations', label: 'Elevations' },
  { to: '/fabs', label: 'FABs' },
  { to: '/form-fields', label: 'Form Field' },
  { to: '/icon-buttons', label: 'Icon Buttons' },
  { to: '/image-lists', label: 'Image Lists' },
  { to: '/layout-grids', label: 'Layout Grid' },
  { to: '/linear-progress-indicators', label: 'Liner Progress Indicators' },
  { to: '/lists', label: 'Lists' },
  { to: '/menus', label: 'Menus' },
  { to: '/radio-buttons', label: 'Radio Buttons' },
  { to: '/ripple', label: 'Ripple' },
  { to: '/rtl', label: 'RTL' },
  { to: '/selects', label: 'Selects' },
  { to: '/sliders', label: 'Sliders' },
  { to: '/snackbars', label: 'Snackbars' },
  { to: '/switches', label: 'Switches' },
  { to: '/tabs', label: 'Tab Bars' },
  { to: '/text-fields', label: 'Text Fields' },
  { to: '/theme', label: 'Theme' },
  { to: '/top-app-bars', label: 'Top App Bars' },
  { to: '/typography', label: 'Typography' },
];

const MainNav = withRouter(({ className, location }: Props) => {
  const [selected, setSelected] = useState(() => {
    return navs.findIndex(nav => nav.to === location.pathname);
  });

  const handleSelected = useCallback(index => {
    setSelected(index);
  }, []);

  return (
    <List
      wrapFocus
      asActived
      onSelect={handleSelected}
      selected={selected}
      role="listbox"
      className={className}
      as="nav"
    >
      {navs.map(nav => (
        <Link key={nav.to} component={ListItem} to={nav.to} as="a">
          {nav.label}
        </Link>
      ))}
    </List>
  );
});

export default MainNav;
