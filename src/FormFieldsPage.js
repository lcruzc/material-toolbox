// @flow strict
import React, { useEffect } from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import Text from './material-toolbox/typography/Text';
import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';

const resources = [
  {
    href:
      'https://material.io/components/web/catalog/input-controls/form-fields/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const FormFieldsPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Form Fields</h1>

      <Text as="p" text="overline">
        Form Field component is a React wrapper of mdc-form-field component.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>Form Field</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>label</td>
            <td>string</td>
            <td />
            <td>✔</td>
            <td>Label that will be set into label input</td>
          </tr>
          <tr>
            <td>alignEnd</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Change label before component</td>
          </tr>
        </tbody>
      </table>

      <SectionTitle>Demos</SectionTitle>

      <p>
        Many examples can be found into
        <ThemedLink to="./checkboxes">Checkboxes</ThemedLink>,
        <ThemedLink to="./radio-buttons">Radio Buttons</ThemedLink> and
        <ThemedLink to="./switches">Switches</ThemedLink>
      </p>
    </LayoutCell>
  );
};

export default FormFieldsPage;
