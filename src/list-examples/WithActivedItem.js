// @flow strict
import React, { useCallback, useState } from 'react';
import cx from 'classnames';

import List from '../material-toolbox/list';
import ListItem from '../material-toolbox/list/Item';
import ListItemDetail from '../material-toolbox/list/ItemDetail';

import Example from '../Example';

const source = `import React, { useCallback, useState } from 'react';
import cx from 'classnames';

import List from 'material-toolbox/list';
import ListItem from 'material-toolbox/list/Item';
import ListItemDetail from 'material-toolbox/list/ItemDetail';

const menuItems = ['inbox', 'star', 'send', 'drafts'];

const WithActivedItem = ({ shaped = false }) => {
  const [selected, setSelected] = useState();

  const handleSelected = useCallback(index => {
    setSelected(index);
  }, []);

  const title = shaped
    ? 'List with shaped activated item'
    : 'List with activated item';

  const className = cx('demo-list', {
    'demo-list-item-shaped': shaped,
  });

  return (
    <List
      selected={selected}
      onSelected={handleSelected}
      className={className}
      role="listbox">
      {menuItems.map(item => (
        <ListItem role="option" data-item={item} key={item}>
          <ListItemDetail className="material-icons">{item}</ListItemDetail>
          {item}
        </ListItem>
      ))}
    </List>
  );
};

export default WithActivedItem;`;

const scssSource = `.demo-list-item-shaped .mdc-list-item {
  border-radius: 0 32px 32px 0;
}

[dir="rtl"] .demo-list-item-shaped .mdc-list-item,
.demo-list-item-shaped .mdc-list-item[dir="rtl"] {
  border-radius: 32px 0 0 32px;
}
`;

type Props = {
  shaped?: boolean,
};

const menuItems = ['inbox', 'star', 'send', 'drafts'];

const WithActivedItem = ({ shaped = false }: Props) => {
  const [selected, setSelected] = useState();

  const handleSelected = useCallback(index => {
    setSelected(index);
  }, []);

  const title = shaped
    ? 'List with shaped activated item'
    : 'List with activated item';

  const className = cx('demo-list', {
    'demo-list-item-shaped': shaped,
  });

  return (
    <Example title={title} source={source} scss={scssSource}>
      <List
        selected={selected}
        onSelected={handleSelected}
        className={className}
        role="listbox"
      >
        {menuItems.map(item => (
          <ListItem role="option" data-item={item} key={item}>
            <ListItemDetail className="material-icons">{item}</ListItemDetail>
            {item}
          </ListItem>
        ))}
      </List>
    </Example>
  );
};

export default WithActivedItem;
