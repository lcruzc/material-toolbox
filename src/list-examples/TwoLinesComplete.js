// @flow strict
import React from 'react';

import List from '../material-toolbox/list';
import ListDivider from '../material-toolbox/list/Divider';
import ListItem from '../material-toolbox/list/Item';
import ListItemDetail from '../material-toolbox/list/ItemDetail';
import ListItemText from '../material-toolbox/list/ItemText';

import Example from '../Example';

const source = `import React from 'react';

import List from 'material-toolbox/list';
import ListDivider from 'material-toolbox/list/Divider';
import ListItem from 'material-toolbox/list/Item';
import ListItemDetail from 'material-toolbox/list/ItemDetail';
import ListItemText from 'material-toolbox/list/ItemText';

const TwoLinesList = () => (
  <List className="demo-list" twoLines avatarList>
    <ListItem>
      <ListItemDetail className="material-icons">folder</ListItemDetail>
      <ListItemText>
        <ListItemText primary>Dog photos</ListItemText>
        <ListItemText secondary>9 Jan 2018</ListItemText>
      </ListItemText>
      <ListItemDetail className="material-icons" end>
        info
      </ListItemDetail>
    </ListItem>
    <ListItem>
      <ListItemDetail className="material-icons">folder</ListItemDetail>
      <ListItemText>
        <ListItemText primary>Cat photos</ListItemText>
        <ListItemText secondary>22 Dec 2017</ListItemText>
      </ListItemText>
      <ListItemDetail className="material-icons" end>
        info
      </ListItemDetail>
    </ListItem>
    <ListDivider />
    <ListItem>
      <ListItemDetail className="material-icons">folder</ListItemDetail>
      <ListItemText>
        <ListItemText primary>Potatos</ListItemText>
        <ListItemText secondary>30 Noc 2017</ListItemText>
      </ListItemText>
      <ListItemDetail className="material-icons" end>
        info
      </ListItemDetail>
    </ListItem>
    <ListItem>
      <ListItemDetail className="material-icons">folder</ListItemDetail>
      <ListItemText>
        <ListItemText primary>Carrots</ListItemText>
        <ListItemText secondary>17 Oct 2017</ListItemText>
      </ListItemText>
      <ListItemDetail className="material-icons" end>
        info
      </ListItemDetail>
    </ListItem>
  </List>
);
`;

const TwoLinesList = () => (
  <Example
    title="Two-Line with Leading and Trailing Icon and Devider"
    source={source}
  >
    <List className="demo-list" twoLines avatarList>
      <ListItem>
        <ListItemDetail className="material-icons">folder</ListItemDetail>
        <ListItemText>
          <ListItemText primary>Dog photos</ListItemText>
          <ListItemText secondary>9 Jan 2018</ListItemText>
        </ListItemText>
        <ListItemDetail className="material-icons" end>
          info
        </ListItemDetail>
      </ListItem>
      <ListItem>
        <ListItemDetail className="material-icons">folder</ListItemDetail>
        <ListItemText>
          <ListItemText primary>Cat photos</ListItemText>
          <ListItemText secondary>22 Dec 2017</ListItemText>
        </ListItemText>
        <ListItemDetail className="material-icons" end>
          info
        </ListItemDetail>
      </ListItem>
      <ListDivider />
      <ListItem>
        <ListItemDetail className="material-icons">folder</ListItemDetail>
        <ListItemText>
          <ListItemText primary>Potatos</ListItemText>
          <ListItemText secondary>30 Noc 2017</ListItemText>
        </ListItemText>
        <ListItemDetail className="material-icons" end>
          info
        </ListItemDetail>
      </ListItem>
      <ListItem>
        <ListItemDetail className="material-icons">folder</ListItemDetail>
        <ListItemText>
          <ListItemText primary>Carrots</ListItemText>
          <ListItemText secondary>17 Oct 2017</ListItemText>
        </ListItemText>
        <ListItemDetail className="material-icons" end>
          info
        </ListItemDetail>
      </ListItem>
    </List>
  </Example>
);

export default TwoLinesList;
