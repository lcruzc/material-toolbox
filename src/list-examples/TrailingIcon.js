// @flow strict
import React from 'react';

import List from '../material-toolbox/list';
import ListItem from '../material-toolbox/list/Item';
import ListItemDetail from '../material-toolbox/list/ItemDetail';

import Example from '../Example';

const source = `import React from 'react';

import List from 'material-toolbox/list';
import ListItem from 'material-toolbox/list/Item';
import ListItemDetail from 'material-toolbox/list/ItemDetail';

const TrailingIconList = () => (
  <List className="demo-list">
    <ListItem>
      Line Item
      <ListItemDetail className="material-icons" end>
        info
      </ListItemDetail>
    </ListItem>

    <ListItem>
      Line Item
      <ListItemDetail className="material-icons" end>
        info
      </ListItemDetail>
    </ListItem>

    <ListItem>
      Line Item
      <ListItemDetail className="material-icons" end>
        info
      </ListItemDetail>
    </ListItem>
  </List>
);

export default TrailingIconList;`;

const TrailingIconList = () => (
  <Example title="Trailing Icon" source={source}>
    <List className="demo-list">
      <ListItem>
        Line Item
        <ListItemDetail className="material-icons" end>
          info
        </ListItemDetail>
      </ListItem>

      <ListItem>
        Line Item
        <ListItemDetail className="material-icons" end>
          info
        </ListItemDetail>
      </ListItem>

      <ListItem>
        Line Item
        <ListItemDetail className="material-icons" end>
          info
        </ListItemDetail>
      </ListItem>
    </List>
  </Example>
);

export default TrailingIconList;
