// @flow strict
import React from 'react';

import List from '../material-toolbox/list';
import ListItem from '../material-toolbox/list/Item';
import ListItemText from '../material-toolbox/list/ItemText';

import Example from '../Example';

const source = `import React from 'react';

import List from 'material-toolbox/list';
import ListItem from 'material-toolbox/list/Item';
import ListItemText from 'material-toolbox/list/ItemText';

const TwoLinesList = () => (
  <List className="demo-list" twoLines>
    <ListItem>
      <ListItemText>
        <ListItemText primary>Line Item</ListItemText>
        <ListItemText secondary>Secondary Text</ListItemText>
      </ListItemText>
    </ListItem>
    <ListItem>
      <ListItemText>
        <ListItemText primary>Line Item</ListItemText>
        <ListItemText secondary>Secondary Text</ListItemText>
      </ListItemText>
    </ListItem>
    <ListItem>
      <ListItemText>
        <ListItemText primary>Line Item</ListItemText>
        <ListItemText secondary>Secondary Text</ListItemText>
      </ListItemText>
    </ListItem>
  </List>
);

export default TwoLinesList`;

const TwoLinesList = () => (
  <Example title="Two lines" source={source}>
    <List className="demo-list" twoLines>
      <ListItem>
        <ListItemText>
          <ListItemText primary>Line Item</ListItemText>
          <ListItemText secondary>Secondary Text</ListItemText>
        </ListItemText>
      </ListItem>
      <ListItem>
        <ListItemText>
          <ListItemText primary>Line Item</ListItemText>
          <ListItemText secondary>Secondary Text</ListItemText>
        </ListItemText>
      </ListItem>
      <ListItem>
        <ListItemText>
          <ListItemText primary>Line Item</ListItemText>
          <ListItemText secondary>Secondary Text</ListItemText>
        </ListItemText>
      </ListItem>
    </List>
  </Example>
);

export default TwoLinesList;
