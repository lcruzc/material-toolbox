// @flow strict
import React from 'react';

import List from '../material-toolbox/list';
import ListItem from '../material-toolbox/list/Item';
import ListItemDetail from '../material-toolbox/list/ItemDetail';
import ListItemText from '../material-toolbox/list/ItemText';
import ListDivider from '../material-toolbox/list/Divider';
import Checkbox from '../material-toolbox/checkbox';
import Radio from '../material-toolbox/radio';

import Example from '../Example';

type Props = {
  title: string,
  type: 'checkbox' | 'radio',
};

const source = `import React from 'react';

import List from 'material-toolbox/list';
import ListItem from 'material-toolbox/list/Item';
import ListItemDetail from 'material-toolbox/list/ItemDetail';
import ListItemText from 'material-toolbox/list/ItemText';
import ListDivider from 'material-toolbox/list/Divider';
import Checkbox from 'material-toolbox/checkbox';
import Radio from 'material-toolbox/radio';

const WithComponents = ({ title, type }: Props) => {
  const Component = type === 'checkbox' ? Checkbox : Radio;
  const role = type === 'checkbox' ? 'group' : 'radiogroup';
  const label = type === 'checkbox' ? 'List with checkbox items' : undefined;

  return (
    <List className="demo-list" role={role}>
      <ListItem role={type}>
        <ListItemText>Dog Photos</ListItemText>
        <ListItemDetail end>
          <Component name={\`\${type}-list-component\`} />
        </ListItemDetail>
      </ListItem>
      <ListItem role={type}>
        <ListItemText>Cat Photos</ListItemText>
        <ListItemDetail end>
          <Component name={\`\${type}-list-component\`} />
        </ListItemDetail>
      </ListItem>
      <ListDivider />
      <ListItem role={type}>
        <ListItemText>Potatos</ListItemText>
        <ListItemDetail end>
          <Component name={\`\${type}-list-component\`} />
        </ListItemDetail>
      </ListItem>
      <ListItem role={type}>
        <ListItemText>Carrots</ListItemText>
        <ListItemDetail end>
          <Component name={\`\${type}-list-component\`} />
        </ListItemDetail>
      </ListItem>
    </List>
  );
};

export default WithComponents;`;

const WithComponents = ({ title, type }: Props) => {
  const Component = type === 'checkbox' ? Checkbox : Radio;
  const role = type === 'checkbox' ? 'group' : 'radiogroup';
  const label = type === 'checkbox' ? 'List with checkbox items' : undefined;

  return (
    <Example title={title} source={source} aria-label={label}>
      <List className="demo-list" role={role}>
        <ListItem role={type}>
          <ListItemText>Dog Photos</ListItemText>
          <ListItemDetail end>
            <Component name={`${type}-list-component`} />
          </ListItemDetail>
        </ListItem>
        <ListItem role={type}>
          <ListItemText>Cat Photos</ListItemText>
          <ListItemDetail end>
            <Component name={`${type}-list-component`} />
          </ListItemDetail>
        </ListItem>
        <ListDivider />
        <ListItem role={type}>
          <ListItemText>Potatos</ListItemText>
          <ListItemDetail end>
            <Component name={`${type}-list-component`} />
          </ListItemDetail>
        </ListItem>
        <ListItem role={type}>
          <ListItemText>Carrots</ListItemText>
          <ListItemDetail end>
            <Component name={`${type}-list-component`} />
          </ListItemDetail>
        </ListItem>
      </List>
    </Example>
  );
};

export default WithComponents;
