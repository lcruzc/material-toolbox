// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Single from '../Single';

afterEach(cleanup);

describe('example::Single', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<Single />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
