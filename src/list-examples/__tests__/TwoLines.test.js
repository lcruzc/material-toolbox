// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TwoLines from '../TwoLines';

afterEach(cleanup);

describe('example::TwoLines', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<TwoLines />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
