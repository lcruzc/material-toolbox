// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TwoLinesComplete from '../TwoLinesComplete';

afterEach(cleanup);

describe('example::TwoLinesComplete', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<TwoLinesComplete />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
