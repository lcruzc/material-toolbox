// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import WithComponents from '../WithComponents';

afterEach(cleanup);

describe('example::WithComponents', () => {
  it('Should match snapshoot with radio type', () => {
    const { container } = render(<WithComponents title="test" type="radio" />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot with checkbox type', () => {
    const { container } = render(
      <WithComponents title="test" type="checkbox" />,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
