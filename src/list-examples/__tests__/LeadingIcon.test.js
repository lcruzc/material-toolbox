// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import LeadingIcon from '../LeadingIcon';

afterEach(cleanup);

describe('example::LeadingIcon', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<LeadingIcon />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
