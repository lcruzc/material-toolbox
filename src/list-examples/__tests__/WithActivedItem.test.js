// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import WithActivedItem from '../WithActivedItem';

afterEach(cleanup);

describe('example::WithActivedItem', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<WithActivedItem />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot shaped', () => {
    const { container } = render(<WithActivedItem shaped />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
