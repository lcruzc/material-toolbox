// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TrailingIcon from '../TrailingIcon';

afterEach(cleanup);

describe('example::TrailingIcon', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<TrailingIcon />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
