// @flow strict
import React from 'react';

import List from '../material-toolbox/list';
import ListItem from '../material-toolbox/list/Item';
import ListItemDetail from '../material-toolbox/list/ItemDetail';

import Example from '../Example';

const source = `import React from 'react';

import List from 'material-toolbox/list';
import ListItem from 'material-toolbox/list/Item';
import ListItemDetail from 'material-toolbox/list/ItemDetail';

const LeadingIconList = () => (
  <List className="demo-list">
    <ListItem>
      <ListItemDetail className="material-icons">wifi</ListItemDetail>
      Line Item
    </ListItem>
    <ListItem>
      <ListItemDetail className="material-icons">bluetooth</ListItemDetail>
      Line Item
    </ListItem>
    <ListItem>
      <ListItemDetail className="material-icons">data_usage</ListItemDetail>
      Line Item
    </ListItem>
  </List>
);
`;

const LeadingIconList = () => (
  <Example title="Leading Icon" source={source}>
    <List className="demo-list">
      <ListItem>
        <ListItemDetail className="material-icons">wifi</ListItemDetail>
        Line Item
      </ListItem>
      <ListItem>
        <ListItemDetail className="material-icons">bluetooth</ListItemDetail>
        Line Item
      </ListItem>
      <ListItem>
        <ListItemDetail className="material-icons">data_usage</ListItemDetail>
        Line Item
      </ListItem>
    </List>
  </Example>
);

export default LeadingIconList;
