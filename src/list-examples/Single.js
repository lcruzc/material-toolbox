// @flow strict
import React from 'react';

import List from '../material-toolbox/list';
import ListItem from '../material-toolbox/list/Item';
import ListItemText from '../material-toolbox/list/ItemText';

import Example from '../Example';

const source = `import React from 'react';

import List from 'material-toolbox/list';
import ListItem from 'material-toolbox/list/Item';
import ListItemText from 'material-toolbox/list/ItemText';

const SingleList = () => (
  <List className="demo-list">
    <ListItem>
      <ListItemText>List Item</ListItemText>
    </ListItem>
    <ListItem>
      <ListItemText>List Item</ListItemText>
    </ListItem>
    <ListItem>
      <ListItemText>List Item</ListItemText>
    </ListItem>
  </List>
);

export default SingleList`;

const SingleList = () => (
  <Example title="Single Line" source={source}>
    <List className="demo-list">
      <ListItem>
        <ListItemText>List Item</ListItemText>
      </ListItem>
      <ListItem>
        <ListItemText>List Item</ListItemText>
      </ListItem>
      <ListItem>
        <ListItemText>List Item</ListItemText>
      </ListItem>
    </List>
  </Example>
);

export default SingleList;
