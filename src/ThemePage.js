// @flow strict
import React, { useEffect } from 'react';

import Text from './material-toolbox/typography/Text';
import LayoutCell from './material-toolbox/layout-grid/Cell';
import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';
import Highlighter from './Highlighter';

const resources = [
  {
    href: 'https://material.io/go/design-color-theming',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href: 'https://material.io/components/web/catalog/theme/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const ThemePage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  });

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Theme</h1>

      <Text as="p" text="overline">
        Theme component is a React wrapper of mdc-theme component.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>useTheme</ComponentTitle>

      <Highlighter language="jsx">{`const useTheme = ({
  background = '',
  color = '',
  on = '',
  text = '',
}: {
  background?: 'primary' | 'secondary' | 'background' | 'surface',
  color?:
    | 'primary'
    | 'secondary'
    | 'on-primary'
    | 'on-secondary'
    | 'on-surface',
  on?: 'light' | 'dark',
  text?: 'primary' | 'secondary' | 'hint' | 'disabled' | 'icon',
}) => string`}</Highlighter>

      <ComponentTitle>Theme</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>background</td>
            <td>Values: primary, secondary, background, surface</td>
            <td />
            <td />
            <td>Background colors</td>
          </tr>
          <tr>
            <td>color</td>
            <td>
              Colors: primary, secondary, on-primary, on-secondary, on-surface
            </td>
            <td />
            <td />
            <td>
              Text color: <strong>Do not set on or text properties</strong>
            </td>
          </tr>
          <tr>
            <td>on</td>
            <td>Values: light, dark</td>
            <td />
            <td />
            <td>
              Set text color, <strong>requires</strong> set
              <strong>text</strong> property
            </td>
          </tr>
          <tr>
            <td>text</td>
            <td>Values: primary, secondary, hint, disabled, icon</td>
            <td />
            <td />
            <td>
              Set text color, <strong>requires</strong> set <strong>on</strong>
              property
            </td>
          </tr>
        </tbody>
      </table>
    </LayoutCell>
  );
};

export default ThemePage;
