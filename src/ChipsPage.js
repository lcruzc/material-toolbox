// @flow strict
import React, { useEffect } from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';
import Text from './material-toolbox/typography/Text';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';

import ChoiceChip from './chip-examples/Choice';
import FilterChip from './chip-examples/Filter';
import ActionChip from './chip-examples/Action';
import ShapedChip from './chip-examples/Shaped';
import InputChip from './chip-examples/Input';

const resources = [
  {
    href: 'https://material.io/go/design-chips',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href: 'https://material.io/components/web/catalog/chips/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const ChipsPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Chip</h1>

      <Text as="p" text="overline">
        Chips components are React wrappers of mdc-chip component.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>ChipSet</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>choice</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Enable choice behaviour of chip set</td>
          </tr>
          <tr>
            <td>filter</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Enable filter behaviour of chip set</td>
          </tr>
          <tr>
            <td>input</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Enable input behaviour of chip set</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>Chip</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>label</td>
            <td>string</td>
            <td />
            <td>✔</td>
            <td>Set text label</td>
          </tr>
          <tr>
            <td>filter</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>
              Enable filter behaviour of chip requires if chip set is also
              filter
            </td>
          </tr>
          <tr>
            <td>beginExit</td>
            <td>boolean</td>
            <td />
            <td />
            <td>If set to true will start remove behaviour</td>
          </tr>
          <tr>
            <td>shouldRemoveOnTrailingIconClick</td>
            <td>boolean</td>
            <td>true</td>
            <td />
            <td>If set to false won&#39;t remove the chip</td>
          </tr>
          <tr>
            <td>defaultSelected</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Sets the initial selected state.</td>
          </tr>
          <tr>
            <td>leadingIcon</td>
            <td>React.Node</td>
            <td />
            <td />
            <td>ChipIcon element used to render the leading icon</td>
          </tr>

          <tr>
            <td>trailingIcon</td>
            <td>React.Node</td>
            <td />
            <td />
            <td>
              ChipIcon element to render the trailing icon, usually used to
              remove chip.
            </td>
          </tr>
          <tr>
            <td>onInteraction</td>
            <td>{'({ chipId: string, root: HTMLChipElement }) => void'}</td>
            <td />
            <td />
            <td>Callback when user interact with chip.</td>
          </tr>
          <tr>
            <td>onRemoval</td>
            <td>{'({ chipId: string, root: HTMLChipElement }) => void'}</td>
            <td />
            <td />
            <td>Callback when user remove the chip.</td>
          </tr>
          <tr>
            <td>onSelected</td>
            <td>
              {
                '({ chipId: string, root: HTMLChipElement, selected: boolean }) => void'
              }
            </td>
            <td />
            <td />
            <td>Callback when user selecte/deselect the chip.</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>ChipIcon</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>trailing</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set trailing style.</td>
          </tr>
        </tbody>
      </table>

      <SectionTitle>Demos</SectionTitle>

      <ChoiceChip />

      <FilterChip />

      <ActionChip />

      <ShapedChip />

      <InputChip />
    </LayoutCell>
  );
};

export default ChipsPage;
