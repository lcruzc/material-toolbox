/* istanbul ignore file */
import React from 'react';
import ReactDOM from 'react-dom';
// eslint-disable-next-line import/no-extraneous-dependencies
import { AppContainer } from 'react-hot-loader';
// eslint-disable-next-line import/no-extraneous-dependencies
import PermanentDrawerApp from './App';

import './styles.scss';

ReactDOM.render(
  <AppContainer>
    <PermanentDrawerApp />
  </AppContainer>,
  document.getElementById('root-permanent'),
);

if (module.hot) {
  module.hot.accept();
}
