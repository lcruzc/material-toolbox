// @flow strict
import React, { useState, useCallback } from 'react';

import Drawer from '../../material-toolbox/drawer';
import DrawerHeader from '../../material-toolbox/drawer/Header';
import DrawerTitle from '../../material-toolbox/drawer/Title';
import DrawerSubTitle from '../../material-toolbox/drawer/SubTitle';
import DrawerContent from '../../material-toolbox/drawer/Content';
import DrawerMainContent from '../../material-toolbox/drawer/MainContent';

import TopAppBar from '../../material-toolbox/top-app-bar';
import TopAppBarAdjuster from '../../material-toolbox/top-app-bar/Adjuster';
import TopAppBarRow from '../../material-toolbox/top-app-bar/Row';
import TopAppBarIcon from '../../material-toolbox/top-app-bar/Icon';
import TopAppBarSection from '../../material-toolbox/top-app-bar/Section';
import TopAppBarTitle from '../../material-toolbox/top-app-bar/Title';

import List from '../../material-toolbox/list';
import ListDetail from '../../material-toolbox/list/ItemDetail';
import ListDivider from '../../material-toolbox/list/Divider';
import ListItem from '../../material-toolbox/list/Item';
import ListSubHeader from '../../material-toolbox/list/Subheader';

const DismissibleDrawerApp = () => {
  const [scrollTarget, setScrollTarget] = useState<?HTMLDivElement>();
  const [isDrawerOpen, setDrawerOpen] = useState<boolean>(false);

  const onRefUpdate = useCallback((element: ?HTMLDivElement) => {
    setScrollTarget(element);
  }, []);

  const handleClick = useCallback(() => {
    setDrawerOpen(!isDrawerOpen);
  }, [setDrawerOpen, isDrawerOpen]);

  return (
    <div className="root-dismissible">
      <Drawer dismissible open={isDrawerOpen}>
        <DrawerHeader>
          <DrawerTitle as="h3">Mail</DrawerTitle>
          <DrawerSubTitle as="h6">email@material.io</DrawerSubTitle>
        </DrawerHeader>
        <DrawerContent>
          <List as="nav" asActived selected={0} role="listbox">
            <ListItem role="option" as="a" href="#">
              <ListDetail className="material-icons" as="i">
                inbox
              </ListDetail>
              Inbox
            </ListItem>
            <ListItem role="option" as="a" href="#">
              <ListDetail className="material-icons" as="i">
                star
              </ListDetail>
              Star
            </ListItem>
            <ListItem role="option" as="a" href="#">
              <ListDetail className="material-icons" as="i">
                send
              </ListDetail>
              Sent Mail
            </ListItem>
            <ListItem role="option" as="a" href="#">
              <ListDetail className="material-icons" as="i">
                drafts
              </ListDetail>
              Drafts
            </ListItem>
            <ListDivider as="hr" />
            <ListSubHeader>Labels</ListSubHeader>
            <ListItem role="option" as="a" href="#">
              <ListDetail className="material-icons" as="i">
                bookmark
              </ListDetail>
              Family
            </ListItem>
            <ListItem role="option" as="a" href="#">
              <ListDetail className="material-icons" as="i">
                bookmark
              </ListDetail>
              Friends
            </ListItem>
            <ListItem role="option" as="a" href="#">
              <ListDetail className="material-icons" as="i">
                bookmark
              </ListDetail>
              Work
            </ListItem>
            <ListDivider as="hr" />
            <ListItem role="option" as="a" href="#">
              <ListDetail className="material-icons" as="i">
                settings
              </ListDetail>
              Settings
            </ListItem>
            <ListItem role="option" as="a" href="#">
              <ListDetail className="material-icons" as="i">
                announcement
              </ListDetail>
              Help & feedback
            </ListItem>
          </List>
        </DrawerContent>
      </Drawer>
      <DrawerMainContent dismissible>
        <div className="demo-app-content-dismissible" ref={onRefUpdate}>
          <TopAppBar scrollTarget={scrollTarget}>
            <TopAppBarRow>
              <TopAppBarSection align="start">
                <TopAppBarIcon className="material-icons" onClick={handleClick}>
                  menu
                </TopAppBarIcon>
                <TopAppBarTitle>Dismissible Drawer</TopAppBarTitle>
              </TopAppBarSection>
            </TopAppBarRow>
          </TopAppBar>
          <TopAppBarAdjuster>
            <main className="demo-main-dismissible">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
            </main>
          </TopAppBarAdjuster>
        </div>
      </DrawerMainContent>
    </div>
  );
};

export default DismissibleDrawerApp;
