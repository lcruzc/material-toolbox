/* istanbul ignore file */
import React from 'react';
import ReactDOM from 'react-dom';
// eslint-disable-next-line import/no-extraneous-dependencies
import { AppContainer } from 'react-hot-loader';
import PersistentDrawerApp from './App';

import './styles.scss';

ReactDOM.render(
  <AppContainer>
    <PersistentDrawerApp />
  </AppContainer>,
  document.getElementById('root-dismissible'),
);

if (module.hot) {
  module.hot.accept();
}
