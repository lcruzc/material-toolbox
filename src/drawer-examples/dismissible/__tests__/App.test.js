// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import App from '../App';

afterEach(cleanup);

describe('example::DismissibleApp', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<App />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
