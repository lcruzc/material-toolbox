// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Modal from '../Modal';

afterEach(cleanup);

describe('example::Modal', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<Modal />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
