// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Permanent from '../Permanent';

afterEach(cleanup);

describe('example::Permanent', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<Permanent />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
