// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Dismissible from '../Dismissible';

afterEach(cleanup);

describe('example::Dismissible', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<Dismissible />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
