// @flow strict
import React, { useEffect } from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import Text from './material-toolbox/typography/Text';

import ThemedLink from './ThemedLink';

const RTLPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>RTL</h1>

      <Text as="p" text="overline">
        UIs for languages that are read from right-to-left (RTL).
      </Text>

      <p>
        Such as Arabic and Hebrew, should be mirrored to ensure content is easy
        to understand.
      </p>

      <p>
        So this is not implemented as React component this should be installed
        and used as you need in your components.
        <ThemedLink
          target="_blank"
          href="https://material.io/components/web/catalog/rtl/"
        >
          Read This.
        </ThemedLink>
      </p>
    </LayoutCell>
  );
};

export default RTLPage;
