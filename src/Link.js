// @flow strict
import React, {
  useRef,
  useEffect,
  useContext,
  useImperativeHandle,
} from 'react';

import type { ElementType } from 'react';

// $FlowFixMe
import { __RouterContext as RouterContext } from 'react-router'; // eslint-disable-line
// $FlowFixMe
import { createLocation } from 'history'; // eslint-disable-line

export const resolveToLocation = (
  to: string | ((currentLocation: string) => string),
  currentLocation: string,
) => (typeof to === 'function' ? to(currentLocation) : to);

export const normalizeToLocation = (
  to: string | ((currentLocation: string) => string),
  currentLocation: string,
) => {
  return typeof to === 'string'
    ? createLocation(to, null, null, currentLocation)
    : to;
};

function isModifiedEvent(event) {
  // $FlowFixMe
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
}

type LinkAnchorProps = {
  navigate: () => void,
  onClick: (evt: SyntheticEvent<HTMLAnchorElement>) => void,
};

const LinkAnchor = React.forwardRef(({ ...rest }: LinkAnchorProps, ref) => {
  return (
    // eslint-disable-next-line jsx-a11y/no-static-element-interactions,jsx-a11y/click-events-have-key-events,jsx-a11y/anchor-has-content
    <a {...(rest: mixed)} ref={ref} />
  );
});

LinkAnchor.displayName = 'LinkAnchor';

type LinkProps = {
  component?: ElementType,
  replace?: boolean,
  to?: string,
};

/**
 * The public API for rendering a history-aware <a>.
 */
const Link = React.forwardRef<LinkProps, _>(
  (
    {
      component: Component = LinkAnchor,
      replace,
      to = undefined,
      ...rest
    }: LinkProps,
    ref,
  ) => {
    const rootRef = useRef();
    const { history, location } = useContext(RouterContext);

    useEffect(() => {
      const root = rootRef.current;
      const navigate = (event: SyntheticEvent<HTMLAnchorElement>) => {
        // $FlowFixMe
        const { target } = rest;

        if (
          !event.defaultPrevented && // onClick prevented default
          // $FlowFixMe
          event.button === 0 && // ignore everything but left clicks
          (!target || target === '_self') && // let browser handle "target=_blank" etc.
          !isModifiedEvent(event) // ignore clicks with modifier keys
        ) {
          event.preventDefault();
          const toLocation = resolveToLocation(to, location);
          const method = replace === true ? history.replace : history.push;

          method(toLocation);
        }
      };

      if (root) {
        root.addEventListener('click', navigate);
      }

      return () => {
        if (root) {
          root.removeEventListener('click', navigate);
        }
      };
    }, [history.push, history.replace, location, replace, rest, to]);

    useImperativeHandle(ref, () => rootRef.current, []);

    const normalizedLocation = normalizeToLocation(
      resolveToLocation(to, location),
      location,
    );

    const href = normalizedLocation
      ? history.createHref(normalizedLocation)
      : '';

    return <Component {...(rest: mixed)} href={href} ref={rootRef} />;
  },
);

Link.displayName = 'Link';

export default Link;
