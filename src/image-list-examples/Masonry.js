// @flow strict
import React from 'react';

import Example from '../Example';

import ImageList from '../material-toolbox/image-list';
import ImageListItem from '../material-toolbox/image-list/Item';
import ImageListImage from '../material-toolbox/image-list/Image';
import ImageListText from '../material-toolbox/image-list/Text';

const source = `import React from 'react';

import ImageList from 'material-toolbox/image-list';
import ImageListItem from 'material-toolbox/image-list/Item';
import ImageListImage from 'material-toolbox/image-list/Image';
import ImageListText from 'material-toolbox/image-list/Text';

const total = [
  { size: '3x2', index: 16 },
  { size: '2x3', index: 1 },
  { size: '3x2', index: 1 },
  { size: '2x3', index: 2 },
  { size: '2x3', index: 3 },
  { size: '3x2', index: 2 },
  { size: '2x3', index: 4 },
  { size: '3x2', index: 3 },
  { size: '2x3', index: 5 },
  { size: '3x2', index: 4 },
  { size: '2x3', index: 6 },
  { size: '3x2', index: 5 },
  { size: '2x3', index: 7 },
  { size: '3x2', index: 6 },
  { size: '3x2', index: 7 },
];

const ImageListMasonry = () => {
  return (
    <ImageList className="masonry-image-list" masonry>
      {total.map((ele, index) => (
        <ImageListItem key={index}>
          <ImageListImage
            alt="Text Label"
            src={\`https://material-components.github.io/material-components-web-catalog/static/media/photos/\${ele.size}/\${ele.index}.jpg\`}
          />
          <ImageListText>Text Label</ImageListText>
        </ImageListItem>
      ))}
    </ImageList>
  );
};

export default ImageListMasonry;`;

const scssSource = `.masonry-image-list {
  @include mdc-image-list-masonry-columns(5);
}`;

const total = [
  { size: '3x2', index: 16 },
  { size: '2x3', index: 1 },
  { size: '3x2', index: 1 },
  { size: '2x3', index: 2 },
  { size: '2x3', index: 3 },
  { size: '3x2', index: 2 },
  { size: '2x3', index: 4 },
  { size: '3x2', index: 3 },
  { size: '2x3', index: 5 },
  { size: '3x2', index: 4 },
  { size: '2x3', index: 6 },
  { size: '3x2', index: 5 },
  { size: '2x3', index: 7 },
  { size: '3x2', index: 6 },
  { size: '3x2', index: 7 },
];

const ImageListMasonry = () => {
  return (
    <Example title="Masonry Image List" source={source} scss={scssSource}>
      <ImageList className="masonry-image-list" masonry>
        {total.map((ele, index) => (
          <ImageListItem
            key={index} // eslint-disable-line react/no-array-index-key
          >
            <ImageListImage
              alt="Text Label"
              src={`https://material-components.github.io/material-components-web-catalog/static/media/photos/${ele.size}/${ele.index}.jpg`}
            />
            <ImageListText>Text Label</ImageListText>
          </ImageListItem>
        ))}
      </ImageList>
    </Example>
  );
};

export default ImageListMasonry;
