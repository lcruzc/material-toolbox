// @flow strict
import React from 'react';

import Example from '../Example';

import ImageList from '../material-toolbox/image-list';
import ImageListItem from '../material-toolbox/image-list/Item';
import ImageListImage from '../material-toolbox/image-list/Image';
import ImageListText from '../material-toolbox/image-list/Text';

const source = `import React from 'react';

import ImageList from 'material-toolbox/image-list';
import ImageListItem from 'material-toolbox/image-list/Item';
import ImageListImage from 'material-toolbox/image-list/Image';
import ImageListText from 'material-toolbox/image-list/Text';

const total = [...new Array(15).keys()];

const ImageListMasonry = () => {
  return (
    <ImageList className="normal-image-list" textProtection>
      {total.map((ele, index) => (
        <ImageListItem key={index}>
          <ImageListImage
            withContainer
            alt="Text Label"
            src={\`https://material-components.github.io/material-components-web-catalog/static/media/photos/3x2/\${index + 1}.jpg\`}
          />
          <ImageListText>Text Label</ImageListText>
        </ImageListItem>
      ))}
    </ImageList>
  );
};

export default ImageListMasonry;`;

const scssSource = `.normal-image-list {
  @include mdc-image-list-standard-columns(5);
}`;

const total = [...Array.from(new Array(15).keys())];

const ImageListMasonry = () => {
  return (
    <Example
      title="Standard Image List with Text Protection"
      source={source}
      scss={scssSource}
    >
      <ImageList className="normal-image-list" textProtection>
        {total.map((ele, index) => (
          <ImageListItem
            key={index} // eslint-disable-line react/no-array-index-key
          >
            <ImageListImage
              withContainer
              alt="Text Label"
              src={`https://material-components.github.io/material-components-web-catalog/static/media/photos/3x2/${index +
                1}.jpg`}
            />
            <ImageListText>Text Label</ImageListText>
          </ImageListItem>
        ))}
      </ImageList>
    </Example>
  );
};

export default ImageListMasonry;
