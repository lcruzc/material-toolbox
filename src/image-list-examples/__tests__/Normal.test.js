// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ImageListNormal from '../Normal';

afterEach(cleanup);

describe('example::ImageListNormal', () => {
  it('Should match snapshoot standard', () => {
    const { container } = render(<ImageListNormal />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
