// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ImageListMasonry from '../Masonry';

afterEach(cleanup);

describe('example::ImageListMasonry', () => {
  it('Should match snapshoot standard', () => {
    const { container } = render(<ImageListMasonry />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
