// @flow strict
import React from 'react';

import FormField from '../material-toolbox/form-field';
import Radio from '../material-toolbox/radio';

import Example from '../Example';

const source = `import React from 'react';

import FormField from 'material-toolbox/form-field';
import Radio from 'material-toolbox/radio';

const Demo = () => (
  <div>
    <FormField htmlFor="ex0-default-radio1-label" label="Radio 1">
      <Radio id="ex0-default-radio1-label" name="ex0-default" />
    </FormField>

    <FormField htmlFor="ex0-default-radio2-label" label="Radio 2">
      <Radio id="ex0-default-radio2-label" name="ex0-default" />
    </FormField>
  </div>
);

export default Demo;`;

const Demo = () => (
  <Example title="" source={source}>
    <FormField htmlFor="ex0-default-radio1-label" label="Radio 1">
      <Radio id="ex0-default-radio1-label" name="ex0-default" />
    </FormField>

    <FormField htmlFor="ex0-default-radio2-label" label="Radio 2">
      <Radio id="ex0-default-radio2-label" name="ex0-default" />
    </FormField>
  </Example>
);

export default Demo;
