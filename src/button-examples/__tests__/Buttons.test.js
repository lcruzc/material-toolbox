// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Buttons from '../Buttons';

afterEach(cleanup);

describe('example::Buttons', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<Buttons title="default" />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot disabled', () => {
    const { container } = render(<Buttons title="disabled" disabled />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot raised', () => {
    const { container } = render(<Buttons title="raised" raised />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot unelevated', () => {
    const { container } = render(<Buttons title="unelevated" unelevated />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot outlined', () => {
    const { container } = render(<Buttons title="outlined" outlined />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
