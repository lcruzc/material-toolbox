// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import BasicExample from '../BasicExample';

afterEach(cleanup);

describe('example::BasicExample', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<BasicExample />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
