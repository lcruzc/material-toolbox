// @flow strict
import React, { useCallback, useState } from 'react';

import Example from '../Example';
import Button from '../material-toolbox/button';
import Snackbar from '../material-toolbox/snackbar';

const source = `import React, { useCallback, useState } from 'react';

import Button from 'material-toolbox/button';
import Snackbar from 'material-toolbox/snackbar';

const states = [
  { label: "Can't send photo. Retry in 5 seconds.", buttonText: 'Retry' },
  {
    label: 'Your photo has been archived.',
    buttonText: 'Undo',
    leading: true,
  },
  {
    label: 'This item already has the label "travel". You can add a new label.',
    buttonText: 'Add anew label',
    stacked: true,
  },
];

const BasicExample = () => {
  const [isOpen, setIsOpen] = useState();
  const [selectedState, setSelectedState] = useState(0);

  const handleBaseLineClick = useCallback(evt => {
    const { state } = evt.currentTarget.dataset;

    setSelectedState(+state);
    setIsOpen(true);
  }, []);

  const handleClosed = useCallback(() => {
    setIsOpen(false);
  }, []);

  return (
    <section className="snackbar-example">
      <Button data-state="0" raised onClick={handleBaseLineClick}>
        Baseline
      </Button>

      <Button data-state="1" raised onClick={handleBaseLineClick}>
        Leading
      </Button>

      <Button data-state="2" raised onClick={handleBaseLineClick}>
        Stacked
      </Button>

      <Snackbar
        {...states[selectedState]}
        onClosed={handleClosed}
        open={isOpen}
        dismissClassName="material-icons"
        dismissText="close"
        withDismiss
      />
    </section>
  );
};

export default BasicExample;`;

const states = [
  { label: "Can't send photo. Retry in 5 seconds.", buttonText: 'Retry' },
  {
    label: 'Your photo has been archived.',
    buttonText: 'Undo',
    leading: true,
  },
  {
    label: 'This item already has the label "travel". You can add a new label.',
    buttonText: 'Add anew label',
    stacked: true,
  },
];

const BasicExample = () => {
  const [isOpen, setIsOpen] = useState();
  const [selectedState, setSelectedState] = useState(0);

  const handleBaseLineClick = useCallback(
    (evt: SyntheticEvent<HTMLButtonElement>) => {
      const { state } = evt.currentTarget.dataset;

      setSelectedState(+state);
      setIsOpen(true);
    },
    [],
  );

  const handleClosed = useCallback(() => {
    setIsOpen(false);
  }, []);

  return (
    <Example title="Basic Example" source={source}>
      <section className="snackbar-example">
        <Button data-state="0" raised onClick={handleBaseLineClick}>
          Baseline
        </Button>

        <Button data-state="1" raised onClick={handleBaseLineClick}>
          Leading
        </Button>

        <Button data-state="2" raised onClick={handleBaseLineClick}>
          Stacked
        </Button>

        <Snackbar
          {...states[selectedState]}
          onClosed={handleClosed}
          open={isOpen}
          dismissClassName="material-icons"
          dismissText="close"
          withDismiss
        />
      </section>
    </Example>
  );
};

export default BasicExample;
