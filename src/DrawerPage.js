// @flow strict
import React, { useEffect } from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import Text from './material-toolbox/typography/Text';

import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';

import Permanent from './drawer-examples/Permanent';
import Dismissible from './drawer-examples/Dismissible';
import Modal from './drawer-examples/Modal';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';

const resources = [
  {
    href: 'https://material.io/go/design-navigation-drawer',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href: 'https://material.io/components/web/catalog/drawers/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const DrawerPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Drawer</h1>

      <Text as="p" text="overline">
        Drawer components are React wrappers of mdc-drawer component.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>DrawerContent</ComponentTitle>

      <ComponentTitle>DrawerHeader</ComponentTitle>

      <ComponentTitle>DrawerTitle</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>as</td>
            <td>React.ElementType</td>
            <td>h3</td>
            <td />
            <td>Add the component required when using a modal drawer</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>DrawerSubTitle</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>as</td>
            <td>React.ElementType</td>
            <td>h6</td>
            <td />
            <td>Add the component required when using a modal drawer</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>DrawerMainContent</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>modal</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Add the component required when using a modal drawer</td>
          </tr>
          <tr>
            <td>dismissible</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Add the component required when using a dimissible</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>Drawer</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>open</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set if drawer is open or close</td>
          </tr>
          <tr>
            <td>modal</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Modal drawer</td>
          </tr>
          <tr>
            <td>dismissible</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Dismissible drawer</td>
          </tr>
          <tr>
            <td>onOpen</td>
            <td>() =&gt; void</td>
            <td />
            <td />
            <td>called when drawer is opened</td>
          </tr>
          <tr>
            <td>onClose</td>
            <td>() =&gt; void</td>
            <td />
            <td />
            <td>called when drawer is closed</td>
          </tr>
        </tbody>
      </table>

      <SectionTitle>Demos</SectionTitle>

      <Permanent />

      <Dismissible />

      <Modal />
    </LayoutCell>
  );
};

export default DrawerPage;
