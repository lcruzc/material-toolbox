// @flow strict
import React from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import Text from './material-toolbox/typography/Text';

import ThemedLink from './ThemedLink';

const AnimationPage = () => (
  <LayoutCell align="bottom" span={12}>
    <h1>Animation</h1>
    <Text as="p" text="body1">
      Material in motion is responsive and natural. Use these easing curves and
      duration patterns to create smooth and consistent motion.
    </Text>
    <Text as="p" text="body2">
      So this is not implemented as React component this should be installed and
      used as you need in your components.
      <ThemedLink
        target="_blank"
        to="https://material.io/components/web/catalog/animation/"
      >
        Read This.
      </ThemedLink>
    </Text>
  </LayoutCell>
);

export default AnimationPage;
