// @flow strict
import React from 'react';

import type { Node } from 'react';

import Text from './material-toolbox/typography/Text';

type Props = {
  children: Node,
};

const ComponentTitle = ({ children }: Props) => (
  <Text as="h3" text="subtitle1">
    {children}
  </Text>
);

export default ComponentTitle;
