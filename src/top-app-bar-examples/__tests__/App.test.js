// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import App from '../App';

afterEach(cleanup);

describe('app::App', () => {
  it('Should match snapshoot standard', () => {
    const { container } = render(<App type="standard" />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot fixed', () => {
    const { container } = render(<App type="fixed" />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot prominent', () => {
    const { container } = render(<App type="prominent" />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot dense', () => {
    const { container } = render(<App type="dense" />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot short', () => {
    const { container } = render(<App type="short" />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot alwaysShort', () => {
    const { container } = render(<App type="alwaysShort" />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
