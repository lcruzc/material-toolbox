// @flow strict
import React from 'react';

import TopAppBar from '../material-toolbox/top-app-bar';
import TopAppBarRow from '../material-toolbox/top-app-bar/Row';
import TopAppBarSection from '../material-toolbox/top-app-bar/Section';
import TopAppBarTitle from '../material-toolbox/top-app-bar/Title';
import TopAppBarIcon from '../material-toolbox/top-app-bar/Icon';
import TopAppBarAction from '../material-toolbox/top-app-bar/Action';
import TopAppBarAdjuster from '../material-toolbox/top-app-bar/Adjuster';

type Props = {
  type: 'fixed' | 'standard' | 'prominent' | 'dense' | 'short' | 'alwaysShort',
};

const TopAppBarExample = ({ type }: Props) => {
  let titleApp;
  const topAppBarProps = {
    short: false,
    fixed: false,
    dense: false,
    prominent: false,
    collapsed: false,
  };

  // eslint-disable-next-line default-case
  switch (type) {
    case 'standard':
      titleApp = 'Standard';
      break;
    case 'fixed':
      titleApp = 'Fixed';
      topAppBarProps.fixed = true;
      break;
    case 'dense':
      titleApp = 'Dense';
      topAppBarProps.dense = true;
      break;
    case 'prominent':
      titleApp = 'Prominent';
      topAppBarProps.prominent = true;
      break;
    case 'short':
      titleApp = 'Short';
      topAppBarProps.short = true;
      break;
    case 'alwaysShort':
      topAppBarProps.short = true;
      topAppBarProps.collapsed = true;
      break;
  }

  return (
    <>
      <TopAppBar {...topAppBarProps}>
        <TopAppBarRow>
          <TopAppBarSection>
            <TopAppBarIcon href="#" className="material-icons">
              menu
            </TopAppBarIcon>
            <TopAppBarTitle>{titleApp}</TopAppBarTitle>
          </TopAppBarSection>

          <TopAppBarSection end>
            <TopAppBarAction
              label="Download"
              href="#"
              className="material-icons"
            >
              file_download
            </TopAppBarAction>
            {!topAppBarProps.short && (
              <>
                <TopAppBarAction
                  label="Print"
                  href="#"
                  className="material-icons"
                >
                  print
                </TopAppBarAction>
                <TopAppBarAction
                  label="Bookmark this page"
                  href="#"
                  className="material-icons"
                >
                  bookmark
                </TopAppBarAction>
              </>
            )}
          </TopAppBarSection>
        </TopAppBarRow>
      </TopAppBar>
      <TopAppBarAdjuster {...topAppBarProps}>
        <main className="demo-main">
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec odio
            lorem, efficitur ac faucibus eget, tempor ac tellus. Sed a neque
            leo. Donec hendrerit ac purus in rutrum. Aliquam in velit eu massa
            porttitor dapibus. Sed posuere volutpat orci, non egestas lectus
            rhoncus id. Cras a odio molestie, semper nibh id, pellentesque
            neque. Aliquam imperdiet risus non nisl ultricies, sit amet rhoncus
            purus congue. Donec consectetur leo quis leo dapibus consequat.
            Aliquam pretium nulla posuere tortor aliquam suscipit. Duis ornare
            orci vel nisl lobortis lacinia.
          </p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec odio
            lorem, efficitur ac faucibus eget, tempor ac tellus. Sed a neque
            leo. Donec hendrerit ac purus in rutrum. Aliquam in velit eu massa
            porttitor dapibus. Sed posuere volutpat orci, non egestas lectus
            rhoncus id. Cras a odio molestie, semper nibh id, pellentesque
            neque. Aliquam imperdiet risus non nisl ultricies, sit amet rhoncus
            purus congue. Donec consectetur leo quis leo dapibus consequat.
            Aliquam pretium nulla posuere tortor aliquam suscipit. Duis ornare
            orci vel nisl lobortis lacinia.
          </p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec odio
            lorem, efficitur ac faucibus eget, tempor ac tellus. Sed a neque
            leo. Donec hendrerit ac purus in rutrum. Aliquam in velit eu massa
            porttitor dapibus. Sed posuere volutpat orci, non egestas lectus
            rhoncus id. Cras a odio molestie, semper nibh id, pellentesque
            neque. Aliquam imperdiet risus non nisl ultricies, sit amet rhoncus
            purus congue. Donec consectetur leo quis leo dapibus consequat.
            Aliquam pretium nulla posuere tortor aliquam suscipit. Duis ornare
            orci vel nisl lobortis lacinia.
          </p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec odio
            lorem, efficitur ac faucibus eget, tempor ac tellus. Sed a neque
            leo. Donec hendrerit ac purus in rutrum. Aliquam in velit eu massa
            porttitor dapibus. Sed posuere volutpat orci, non egestas lectus
            rhoncus id. Cras a odio molestie, semper nibh id, pellentesque
            neque. Aliquam imperdiet risus non nisl ultricies, sit amet rhoncus
            purus congue. Donec consectetur leo quis leo dapibus consequat.
            Aliquam pretium nulla posuere tortor aliquam suscipit. Duis ornare
            orci vel nisl lobortis lacinia.
          </p>
        </main>
      </TopAppBarAdjuster>
    </>
  );
};

export default TopAppBarExample;
