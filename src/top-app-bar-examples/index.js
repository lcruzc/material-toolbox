/* istanbul ignore file */
import React from 'react';
import ReactDOM from 'react-dom';
// eslint-disable-next-line import/no-extraneous-dependencies
import { AppContainer } from 'react-hot-loader';
// eslint-disable-next-line import/no-extraneous-dependencies
import App from './App';

import './styles.scss';

ReactDOM.render(
  <AppContainer>
    <App type={window.TOP_APP_VAR_TYPE} />
  </AppContainer>,
  document.getElementById('root'),
);

if (module.hot) {
  module.hot.accept();
}
