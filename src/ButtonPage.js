// @flow strict
import React, { useEffect, useState, useCallback } from 'react';

import Buttons from './button-examples/Buttons';

import Example from './Example';

import Checkbox from './material-toolbox/checkbox';
import FormField from './material-toolbox/form-field';
import LayoutCell from './material-toolbox/layout-grid/Cell';
import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';
import Text from './material-toolbox/typography/Text';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';

const source = `import React from 'react';

import Text from 'material-toolbox/typography/Text';
import Button from 'material-toolbox/button';
import ButtonIcon from 'material-toolbox/button/Icon';

const Buttons = ({
  title,
  raised,
  unelevated,
  outlined,
  disabled,
  className,
}) => (
  <section>
    <Text text="subtitle1" as="h3">
      {title}
    </Text>
    <Button
      className={className}
      unelevated={unelevated}
      raised={raised}
      outlined={outlined}
      disabled={disabled}>
      Baseline
    </Button>
    <Button
      className={className}
      unelevated={unelevated}
      raised={raised}
      outlined={outlined}
      disabled={disabled}
      dense>
      Dense
    </Button>
    <Button
      className={className}
      unelevated={unelevated}
      raised={raised}
      outlined={outlined}
      disabled={disabled}>
      <ButtonIcon className="material-icons">favorite</ButtonIcon>
      Icon
    </Button>
    <Button
      className={className}
      unelevated={unelevated}
      raised={raised}
      outlined={outlined}
      disabled={disabled}>
      <ButtonIcon
        as="svg"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 24 24"
        fill="#000000">
        <path fill="none" d="M0 0h24v24H0z" />
        <path d="M23 12c0-6.07-4.93-11-11-11S1 5.93 1 12s4.93 11 11 11 11-4.93 11-11zM5 17.64C3.75 16.1 3 14.14 3 12c0-2.13.76-4.08 2-5.63v11.27zM17.64 5H6.36C7.9 3.75 9.86 3 12 3s4.1.75 5.64 2zM12 14.53L8.24 7h7.53L12 14.53zM17 9v8h-4l4-8zm-6 8H7V9l4 8zm6.64 2c-1.55 1.25-3.51 2-5.64 2s-4.1-.75-5.64-2h11.28zM21 12c0 2.14-.75 4.1-2 5.64V6.37c1.24 1.55 2 3.5 2 5.63z" />
      </ButtonIcon>
      SVG Icon
    </Button>
    <Button
      className={className}
      outlined={outlined}
      unelevated={unelevated}
      raised={raised}
      as="a"
      href="javascript:void(0)" // eslint-disable-line
    >
      Link
    </Button>
  </section>
);

Buttons.defaultProps = {
  raised: false,
  unelevated: false,
  outlined: false,
  disabled: false,
};

export default Buttons;`;

const scssSource = `@import "@material/button/mdc-button";

.button-examples {
  .mdc-button {
    margin: 8px 16px;
  }

  .demo-ink-color {
    @include mdc-button-icon-color(white);
    @include mdc-button-ink-color(orange);
    @include mdc-states(orange);
  }

  .demo-icon-color {
    @include mdc-button-icon-color(orange);
  }

  .big-round-corner-button {
    @include mdc-button-shape-radius(18px);
  }
}`;

const resources = [
  {
    href: 'https://material.io/go/design-buttons',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href: 'https://material.io/components/web/catalog/buttons/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const ButtonPage = () => {
  const [disabled, setDisabled] = useState<boolean>(false);

  const handleClick = useCallback(() => {
    setDisabled(!disabled);
  }, [disabled]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Button</h1>

      <Text as="p" text="overline">
        Button components are React wrappers of mdc-button component.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>ButtonLabel</ComponentTitle>

      <ComponentTitle>Button</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>as</td>
            <td>Any component</td>
            <td>button</td>
            <td />
            <td>Element type that will generate the button</td>
          </tr>
          <tr>
            <td>outlined</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Sets the outlined style</td>
          </tr>
          <tr>
            <td>raised</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Sets the raised style</td>
          </tr>
          <tr>
            <td>unelevated</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Sets unelevated style</td>
          </tr>
          <tr>
            <td>dense</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Sets dense style</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>ButtonIcon</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>as</td>
            <td>String: i, svg</td>
            <td>i</td>
            <td />
            <td>Element type that will generate the icon</td>
          </tr>
        </tbody>
      </table>

      <SectionTitle>Demos</SectionTitle>

      <div>
        <FormField label="Disable buttons (excluding links)" htmlFor="disabled">
          <Checkbox id="disabled" checked={disabled} onChange={handleClick} />
        </FormField>
      </div>

      <section className="button-examples">
        <Example title="" source={source} scss={scssSource}>
          <Buttons title="Text Button" disabled={disabled} />
          <Buttons title="Raised Button" disabled={disabled} raised />
          <Buttons title="Unelevated Button" disabled={disabled} unelevated />
          <Buttons title="Outlined Button" disabled={disabled} outlined />
          <Buttons
            title="Shaped Button"
            unelevated
            disabled={disabled}
            className="big-round-corner-button"
          />
          <Buttons
            title="Different Color Icons/Ink Button"
            unelevated
            disabled={disabled}
            className="demo-ink-color demo-icon-color"
          />
        </Example>
      </section>
    </LayoutCell>
  );
};

export default ButtonPage;
