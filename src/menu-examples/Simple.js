// @flow strict
import React, { useCallback, useState } from 'react';

import Menu from '../material-toolbox/menu';
import MenuItem from '../material-toolbox/menu/Item';
import MenuAnchor from '../material-toolbox/menu/Anchor';

import ListItemText from '../material-toolbox/list/ItemText';
import ListDivider from '../material-toolbox/list/Divider';

import Button from '../material-toolbox/button';

import Example from '../Example';

const source = `import React, { useCallback, useState } from 'react';

import Menu from 'material-toolbox/menu';
import MenuItem from 'material-toolbox/menu/Item';
import MenuAnchor from 'material-toolbox/menu/Anchor';

import ListItemText from 'material-toolbox/list/ItemText';
import ListDivider from 'material-toolbox/list/Divider';

import Button from 'material-toolbox/button';

const SimpleMenu = () => {
  const [isOpen, setIsOpen] = useState(false);

  const handleClick = useCallback(() => {
    setIsOpen(true);
  }, []);

  const handleClosed = useCallback(() => {
    setIsOpen(false);
  }, []);

  return (
    <MenuAnchor>
      <div>
        <Button onClick={handleClick}>Open Menu</Button>
        <Menu onSelected={handleClosed} open={isOpen} onClosed={handleClosed}>
          <MenuItem>
            <ListItemText>Passionfruit</ListItemText>
          </MenuItem>
          <MenuItem>
            <ListItemText>Orange</ListItemText>
          </MenuItem>
          <MenuItem>
            <ListItemText>Guava</ListItemText>
          </MenuItem>
          <MenuItem>
            <ListItemText>Pitaya</ListItemText>
          </MenuItem>
          <ListDivider />
          <MenuItem>
            <ListItemText>Pineapple</ListItemText>
          </MenuItem>
          <MenuItem>
            <ListItemText>Mango</ListItemText>
          </MenuItem>
          <MenuItem>
            <ListItemText>Papaya</ListItemText>
          </MenuItem>
          <MenuItem>
            <ListItemText>Lychee</ListItemText>
          </MenuItem>
        </Menu>
      </div>
    </MenuAnchor>
  );
};

export default SimpleMenu;`;

const SimpleMenu = () => {
  const [isOpen, setIsOpen] = useState(false);

  const handleClick = useCallback(() => {
    setIsOpen(true);
  }, []);

  const handleClosed = useCallback(() => {
    setIsOpen(false);
  }, []);

  return (
    <Example source={source} title="">
      <MenuAnchor>
        <div>
          <Button onClick={handleClick}>Open Menu</Button>
          <Menu onSelected={handleClosed} open={isOpen} onClosed={handleClosed}>
            <MenuItem>
              <ListItemText>Passionfruit</ListItemText>
            </MenuItem>
            <MenuItem>
              <ListItemText>Orange</ListItemText>
            </MenuItem>
            <MenuItem>
              <ListItemText>Guava</ListItemText>
            </MenuItem>
            <MenuItem>
              <ListItemText>Pitaya</ListItemText>
            </MenuItem>
            <ListDivider />
            <MenuItem>
              <ListItemText>Pineapple</ListItemText>
            </MenuItem>
            <MenuItem>
              <ListItemText>Mango</ListItemText>
            </MenuItem>
            <MenuItem>
              <ListItemText>Papaya</ListItemText>
            </MenuItem>
            <MenuItem>
              <ListItemText>Lychee</ListItemText>
            </MenuItem>
          </Menu>
        </div>
      </MenuAnchor>
    </Example>
  );
};

export default SimpleMenu;
