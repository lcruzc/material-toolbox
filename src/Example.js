// @flow strict
import React, { useCallback, useState } from 'react';

import type { Node } from 'react';

import TabBar from './material-toolbox/tab-bar';
import Tab from './material-toolbox/tab';
import TabText from './material-toolbox/tab/Text';
import TabScroller from './material-toolbox/tab-scroller';
import Text from './material-toolbox/typography/Text';

import Highlighter from './Highlighter';

type Props = {
  title: string,
  children: Node,
  source: string,
  scss?: string,
};

const Example = ({ title, children, source, scss }: Props) => {
  const [tabSelected, setTabSelected] = useState<number>(0);

  const handleChange = useCallback((index: number) => {
    setTabSelected(index);
  }, []);

  return (
    <div className="example">
      <Text text="subtitle1" as="h2">
        {title}
      </Text>
      <TabBar
        className="mt-tab"
        focusOnActivate={false}
        initialActive={0}
        onChange={handleChange}
      >
        <TabScroller align="center">
          <Tab fade minWidth>
            <TabText>Example</TabText>
          </Tab>
          <Tab fade minWidth>
            <TabText>Source</TabText>
          </Tab>
        </TabScroller>
      </TabBar>
      <section className={tabSelected === 0 ? '' : 'hide'}>{children}</section>
      <section className={tabSelected === 1 ? '' : 'hide'}>
        <Highlighter language="jsx">{source}</Highlighter>
        {scss != null && <Highlighter language="scss">{scss}</Highlighter>}
      </section>
    </div>
  );
};

export default Example;
