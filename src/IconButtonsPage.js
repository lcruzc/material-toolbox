// @flow strict
import React, { useEffect } from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';
import Text from './material-toolbox/typography/Text';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';

import Demos from './icon-button-examples/Demo';

const resources = [
  {
    href: 'https://material.io/go/design-buttons',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href: 'https://material.io/components/web/catalog/buttons/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const IconButtonsPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Icon Button</h1>

      <Text as="p" text="overline">
        Button components are React wrappers of mdc-button component.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>IconButton</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>as</td>
            <td>Any component</td>
            <td>button</td>
            <td />
            <td>Element type that will generate the button</td>
          </tr>
          <tr>
            <td>on</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>When toggle this enable this makes a controlled component</td>
          </tr>
          <tr>
            <td>offLabel</td>
            <td>string</td>
            <td />
            <td />
            <td>When toggle is enable this used for Accessibility</td>
          </tr>
          <tr>
            <td>onLabel</td>
            <td>string</td>
            <td />
            <td />
            <td>When toggle is enable this used for Accessibility</td>
          </tr>
          <tr>
            <td>toggle</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Enable toggle behaviour</td>
          </tr>
          <tr>
            <td>onChange</td>
            <td>{'({ isOn: boolean }) => void'}</td>
            <td />
            <td />
            <td>When toogle is enabled this notify changes on toggle state</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>IconButtonState</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>as</td>
            <td>Any component</td>
            <td>i</td>
            <td />
            <td>Element type that will generate the icon button</td>
          </tr>
          <tr>
            <td>isOn</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Defines which type of Stage is.</td>
          </tr>
        </tbody>
      </table>

      <SectionTitle>Demos</SectionTitle>

      <Demos />
    </LayoutCell>
  );
};

export default IconButtonsPage;
