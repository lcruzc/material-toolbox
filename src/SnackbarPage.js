// @flow strict
import React, { useEffect } from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';
import Text from './material-toolbox/typography/Text';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';
import BasicExample from './snackbar-examples/BasicExample';

const cbSignature = '() => void';

const resources = [
  {
    href: 'https://material.io/go/design-snackbar',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href: 'https://material.io/components/web/catalog/snackbars/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const SnackbarPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Snackbar</h1>

      <Text as="p" text="overline">
        Snackbar component is a React wrapper of mdc-snackbar component.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>Snackbar</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>buttonText</td>
            <td>string</td>
            <td />
            <td />
            <td>Text used for action button</td>
          </tr>
          <tr>
            <td>closeOnEscape</td>
            <td>boolean</td>
            <td>true</td>
            <td />
            <td>Close snackbar when ESC key was pressed.</td>
          </tr>
          <tr>
            <td>dismissClassName</td>
            <td>string</td>
            <td />
            <td />
            <td>className for dismiss icon action</td>
          </tr>
          <tr>
            <td>dismissText</td>
            <td>string</td>
            <td />
            <td />
            <td>Text used in dismiss icon if needed.</td>
          </tr>
          <tr>
            <td>label</td>
            <td>string</td>
            <td />
            <td>✔</td>
            <td>Text used to inform user.</td>
          </tr>
          <tr>
            <td>leading</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Leading design for snackbar</td>
          </tr>
          <tr>
            <td>onClosed</td>
            <td>{cbSignature}</td>
            <td />
            <td />
            <td>Notify that snackbar when closed</td>
          </tr>
          <tr>
            <td>onClosing</td>
            <td>{cbSignature}</td>
            <td />
            <td />
            <td>Notify that snackbar when closing</td>
          </tr>
          <tr>
            <td>onOpened</td>
            <td>{cbSignature}</td>
            <td />
            <td />
            <td>Notify that snackbar when openned</td>
          </tr>
          <tr>
            <td>onOpenning</td>
            <td>{cbSignature}</td>
            <td />
            <td />
            <td>Notify that snackbar when openning</td>
          </tr>
          <tr>
            <td>open</td>
            <td>boolean</td>
            <td />
            <td />
            <td>Set to true and the snackbar is open for a while</td>
          </tr>
          <tr>
            <td>timeout</td>
            <td>number</td>
            <td />
            <td />
            <td>The time that snackbar is visible in miliseconds</td>
          </tr>
          <tr>
            <td>stacked</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Stacked design for snackbar</td>
          </tr>
          <tr>
            <td>withDismiss</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Enable dismiss icon button</td>
          </tr>
        </tbody>
      </table>

      <BasicExample />
    </LayoutCell>
  );
};

export default SnackbarPage;
