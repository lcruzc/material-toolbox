// @flow strict
import React from 'react';
import cx from 'classnames';

import Example from '../Example';

import LayoutCell from '../material-toolbox/layout-grid/Cell';
import LayoutInner from '../material-toolbox/layout-grid/Inner';

import withRipple from '../material-toolbox/ripple';

import type { Style } from '../material-toolbox/mt-types';

type Props = {|
  className?: string,
|};

type DivProps = {
  rippleClassName: string,
  rippleStyle: Style,
  activator: {| current: ?HTMLDivElement |},
  surface?: {| current: ?HTMLElement |},
  ...Props,
};

const RippledDiv = withRipple<_, _, Props, _>(
  ({
    className,
    activator,
    surface,
    rippleStyle,
    rippleClassName,
    ...rest
  }: DivProps) => (
    <div
      {...rest}
      style={rippleStyle}
      ref={activator}
      tabIndex="0" // eslint-disable-line
      className={cx(
        className,
        rippleClassName,
        'material-icons ripple-demo-icon',
      )}
    >
      favorite
    </div>
  ),
  { unbounded: true },
);

const unboundedSource = `import withRipple from 'material-toolbox/ripple';

const RippledDiv = withRipple(
  ({
    className,
    activator,
    surface,
    rippleStyle,
    rippleClassName,
    ...rest
  }) => (
    <div
      {...rest}
      style={rippleStyle}
      ref={activator}
      tabIndex="0" // eslint-disable-line
      className={cx(
        className,
        rippleClassName,
        'material-icons ripple-demo-icon',
      )}>
      favorite
    </div>
  ),
  { unbounded: true },
);

export default RippledDiv`;

const scssSource = `.ripple-demo-icon {
  cursor: pointer;
  font-size: 24px;
  height: 24px;
  overflow: visible;
  padding: 12px;
  width: 24px;

  &::before,
  &::after {
    height: 100%;
    left: calc(50% - 50%);
    top: calc(50% - 50%);
    width: 100%;
  }

  &.mdc-ripple-upgraded::before,
  &.mdc-ripple-upgraded::after {
    height: var(--mdc-ripple-fg-size, 100%);
    left: var(--mdc-ripple-left, calc(50% - 50%));
    top: var(--mdc-ripple-top, calc(50% - 50%));
    width: var(--mdc-ripple-fg-size, 100%);
  }

  &.mdc-ripple-upgraded::after {
    height: var(--mdc-ripple-fg-size, 100%);
    width: var(--mdc-ripple-fg-size, 100%);
  }
}`;

const UnboundedRipple = () => (
  <LayoutInner>
    <LayoutCell desktop={12}>
      <Example title="Unbounded" source={unboundedSource} scss={scssSource}>
        <RippledDiv />
      </Example>
    </LayoutCell>
  </LayoutInner>
);

export default UnboundedRipple;
