// @flow strict
import React from 'react';
import cx from 'classnames';

import type { Node } from 'react';

import Example from '../Example';

import LayoutCell from '../material-toolbox/layout-grid/Cell';
import LayoutInner from '../material-toolbox/layout-grid/Inner';
import Elevation from '../material-toolbox/elevation';
import Theme from '../material-toolbox/theme';

import withRipple from '../material-toolbox/ripple';

import type { Style } from '../material-toolbox/mt-types';

type Props = {|
  className?: string,
  children?: Node,
|};

type DivProps = {
  rippleClassName: string,
  rippleStyle: Style,
  activator: {| current: ?HTMLDivElement |},
  surface?: {| current: ?HTMLElement |},
  ...Props,
};

const Div = ({
  className,
  activator,
  surface,
  rippleStyle,
  rippleClassName,
  ...rest
}: DivProps) => (
  <div
    {...rest}
    style={rippleStyle}
    ref={activator}
      tabIndex="0" // eslint-disable-line
    className={cx('ripple-demo-surface', rippleClassName, className)}
  />
);

const RippledDivPrimary = withRipple<_, _, Props, _>(Div, { primary: true });

const RippledDivSecondary = withRipple<_, _, Props, _>(Div, { accent: true });

const themedSource = `import withRipple from 'material-toolbox/ripple';

const Div = ({
  className,
  activator,
  surface,
  rippleStyle,
  rippleClassName,
  ...rest
}) => (
  <div
    {...rest}
    style={rippleStyle}
    ref={activator}
      tabIndex="0" // eslint-disable-line
    className={cx('ripple-demo-surface', rippleClassName, className)}
  />
);

export const RippledDivPrimary = withRipple(Div, { primary: true });

export const RippledDivSecondary = withRipple(Div, { accent: true });`;

const scssSource = `.ripple-demo-surface {
  align-items: center;
  cursor: pointer;
  display: flex;
  height: 100px;
  justify-content: center;
  padding: 1rem;
  user-select: none;
  width: 200px;
}`;

const ThemedRipple = () => (
  <LayoutInner>
    <LayoutCell desktop={12}>
      <Example title="Theme Styles" source={themedSource} scss={scssSource}>
        <Theme color="primary">
          <Elevation depth={2}>
            <RippledDivPrimary>Primary</RippledDivPrimary>
          </Elevation>
        </Theme>

        <Theme color="secondary">
          <Elevation depth={2}>
            <RippledDivSecondary>Secondary</RippledDivSecondary>
          </Elevation>
        </Theme>
      </Example>
    </LayoutCell>
  </LayoutInner>
);

export default ThemedRipple;
