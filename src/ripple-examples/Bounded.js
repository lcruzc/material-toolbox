// @flow strict
import React from 'react';
import cx from 'classnames';

import Example from '../Example';

import LayoutCell from '../material-toolbox/layout-grid/Cell';
import LayoutInner from '../material-toolbox/layout-grid/Inner';
import Elevation from '../material-toolbox/elevation';

import withRipple from '../material-toolbox/ripple';

import type { Style } from '../material-toolbox/mt-types';

type Props = {|
  className?: string,
|};

type DivProps = {
  rippleClassName: string,
  rippleStyle: Style,
  activator: {| current: ?HTMLDivElement |},
  surface?: {| current: ?HTMLElement |},
  ...Props,
};

const RippledDiv = withRipple<_, _, Props, _>(
  ({
    className,
    activator,
    surface,
    rippleStyle,
    rippleClassName,
    ...rest
  }: DivProps) => (
    <div
      {...rest}
      style={rippleStyle}
      ref={activator}
      tabIndex="0" // eslint-disable-line
      className={cx('ripple-demo-surface', rippleClassName, className)}
    >
      Interact with me!
    </div>
  ),
);

const boundedSource = `import withRipple from 'material-toolbox/ripple';

const RippledDiv = withRipple(
  ({
    className,
    activator,
    surface,
    rippleStyle,
    rippleClassName,
    ...rest
  }) => (
    <div
      {...rest}
      style={rippleStyle}
      ref={activator}
      tabIndex="0" // eslint-disable-line
      className={cx('ripple-demo-surface', rippleClassName, className)}>
      Interact with me!
    </div>
  ),
);

export default RippledDiv;`;

const scssSource = `.ripple-demo-surface {
  align-items: center;
  cursor: pointer;
  display: flex;
  height: 100px;
  justify-content: center;
  padding: 1rem;
  user-select: none;
  width: 200px;
}`;

const BoundedRipple = () => (
  <LayoutInner>
    <LayoutCell desktop={12}>
      <Example title="Bounded" source={boundedSource} scss={scssSource}>
        <Elevation depth={2}>
          <RippledDiv />
        </Elevation>
      </Example>
    </LayoutCell>
  </LayoutInner>
);

export default BoundedRipple;
