// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import BoundedRipple from '../Bounded';

afterEach(cleanup);

describe('example::BoundedRipple', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<BoundedRipple />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
