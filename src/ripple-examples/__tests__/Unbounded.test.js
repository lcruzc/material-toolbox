// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import UnboundedRipple from '../Unbounded';

afterEach(cleanup);

describe('example::UnboundedRipple', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<UnboundedRipple />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
