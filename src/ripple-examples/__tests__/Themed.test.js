// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ThemedRipple from '../Themed';

afterEach(cleanup);

describe('example::ThemedRipple', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<ThemedRipple />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
