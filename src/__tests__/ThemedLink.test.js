// @flow strict
import React from 'react';
import { MemoryRouter } from 'react-router'; // eslint-disable-line
import { render, cleanup } from '@testing-library/react';

import ThemedLink from '../ThemedLink';

afterEach(cleanup);

describe('component::ThemedLink', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <MemoryRouter>
        <ThemedLink to="test" />
      </MemoryRouter>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
