// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TypographyPage from '../TypographyPage';

afterEach(cleanup);

describe('page::TypographyPage', () => {
  it('Should match snapshoot', () => {
    const scrollTo = jest.fn();
    window.scrollTo = scrollTo;
    const { container } = render(<TypographyPage />);
    expect(container.firstChild).toMatchSnapshot();
    expect(scrollTo.mock.calls).toHaveLength(1);
    delete window.scrollTo;
  });
});
