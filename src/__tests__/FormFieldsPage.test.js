// @flow strict
import React from 'react';
// $FlowFixMe
import { MemoryRouter } from 'react-router'; // eslint-disable-line
import { render, cleanup } from '@testing-library/react';

import FormFieldsPage from '../FormFieldsPage';

afterEach(cleanup);

describe('page::FormFieldsPage', () => {
  it('Should match snapshoot', () => {
    const scrollTo = jest.fn();
    window.scrollTo = scrollTo;
    const { container } = render(
      <MemoryRouter>
        <FormFieldsPage />
      </MemoryRouter>,
    );
    expect(container.firstChild).toMatchSnapshot();
    expect(scrollTo.mock.calls).toHaveLength(1);
    delete window.scrollTo;
  });
});
