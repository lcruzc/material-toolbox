// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Highlighter from '../Highlighter';

afterEach(cleanup);

describe('component::Highlighter', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <Highlighter>const variable = 3;</Highlighter>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
