// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import DataTablesPage from '../DataTablesPage';

afterEach(cleanup);

describe('page::DataTablesPage', () => {
  it('Should match snapshoot', () => {
    const scrollTo = jest.fn();
    window.scrollTo = scrollTo;
    const { container } = render(<DataTablesPage />);
    expect(container.firstChild).toMatchSnapshot();
    expect(scrollTo.mock.calls).toHaveLength(1);
    delete window.scrollTo;
  });
});
