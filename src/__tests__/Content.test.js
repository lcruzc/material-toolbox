// @flow strict
import React from 'react';
// $FlowFixMe
import { MemoryRouter } from 'react-router'; // eslint-disable-line
import { render, cleanup } from '@testing-library/react';

import Content from '../Content';

afterEach(cleanup);

describe('component::Content', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <MemoryRouter>
        <Content />
      </MemoryRouter>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
