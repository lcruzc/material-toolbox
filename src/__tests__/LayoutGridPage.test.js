// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import LayoutGridPage from '../LayoutGridPage';

afterEach(cleanup);

describe('page::LayoutGridPage', () => {
  it('Should match snapshoot', () => {
    const scrollTo = jest.fn();
    window.scrollTo = scrollTo;
    const { container } = render(<LayoutGridPage />);
    expect(container.firstChild).toMatchSnapshot();
    expect(scrollTo.mock.calls).toHaveLength(1);
    delete window.scrollTo;
  });
});
