// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import CardPage from '../CardPage';

afterEach(cleanup);

describe('page::CardPage', () => {
  it('Should match snapshoot', () => {
    const scrollTo = jest.fn();
    window.scrollTo = scrollTo;
    const { container } = render(<CardPage />);
    expect(container.firstChild).toMatchSnapshot();
    expect(scrollTo.mock.calls).toHaveLength(1);
    delete window.scrollTo;
  });
});
