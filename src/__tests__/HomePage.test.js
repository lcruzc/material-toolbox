// @flow strict
import React from 'react';
import { MemoryRouter } from 'react-router'; // eslint-disable-line
import { render, cleanup } from '@testing-library/react';

import HomePage from '../HomePage';

afterEach(cleanup);

describe('component::HomePage', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <MemoryRouter>
        <HomePage />
      </MemoryRouter>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
