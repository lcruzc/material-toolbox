// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TopAppBarPage from '../TopAppBarPage';

afterEach(cleanup);

describe('page::TopAppBarPage', () => {
  it('Should match snapshoot', () => {
    const scrollTo = jest.fn();
    window.scrollTo = scrollTo;
    const { container } = render(<TopAppBarPage />);
    expect(container.firstChild).toMatchSnapshot();
    expect(scrollTo.mock.calls).toHaveLength(1);
    delete window.scrollTo;
  });
});
