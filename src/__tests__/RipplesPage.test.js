// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import RipplesPage from '../RipplesPage';

afterEach(cleanup);

describe('page::RipplesPage', () => {
  it('Should match snapshoot', () => {
    const scrollTo = jest.fn();
    window.scrollTo = scrollTo;
    const { container } = render(<RipplesPage />);
    expect(container.firstChild).toMatchSnapshot();
    expect(scrollTo.mock.calls).toHaveLength(1);
    delete window.scrollTo;
  });
});
