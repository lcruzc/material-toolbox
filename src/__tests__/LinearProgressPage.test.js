// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import LinearProgressPage from '../LinearProgressPage';

afterEach(cleanup);

describe('page::LinearProgressPage', () => {
  it('Should match snapshoot', () => {
    const scrollTo = jest.fn();
    window.scrollTo = scrollTo;
    const { container } = render(<LinearProgressPage />);
    expect(container.firstChild).toMatchSnapshot();
    expect(scrollTo.mock.calls).toHaveLength(1);
    delete window.scrollTo;
  });
});
