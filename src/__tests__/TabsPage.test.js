// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TabsPage from '../TabsPage';

afterEach(cleanup);

describe('page::TabsPage', () => {
  it('Should match snapshoot', () => {
    const scrollTo = jest.fn();
    window.scrollTo = scrollTo;
    const { container } = render(<TabsPage />);
    expect(container.firstChild).toMatchSnapshot();
    expect(scrollTo.mock.calls).toHaveLength(1);
    delete window.scrollTo;
  });
});
