// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import FABPage from '../FABPage';

afterEach(cleanup);

describe('page::FABPage', () => {
  it('Should match snapshoot', () => {
    const scrollTo = jest.fn();
    window.scrollTo = scrollTo;
    const { container } = render(<FABPage />);
    expect(container.firstChild).toMatchSnapshot();
    expect(scrollTo.mock.calls).toHaveLength(1);
    delete window.scrollTo;
  });
});
