// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ImageListImage from '../ImageListPage';

afterEach(cleanup);

describe('page::ImageListImage', () => {
  it('Should match snapshoot', () => {
    const scrollTo = jest.fn();
    window.scrollTo = scrollTo;
    const { container } = render(<ImageListImage />);
    expect(container.firstChild).toMatchSnapshot();
    expect(scrollTo.mock.calls).toHaveLength(1);
    delete window.scrollTo;
  });
});
