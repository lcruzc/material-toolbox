// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ComponentTitle from '../ComponentTitle';

afterEach(cleanup);

describe('component::ComponentTitle', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<ComponentTitle>Title</ComponentTitle>);
    expect(container.firstChild).toMatchSnapshot();
  });
});
