// @flow strict
import React from 'react';
// $FlowFixMe
import { MemoryRouter } from 'react-router'; // eslint-disable-line
import { render, cleanup } from '@testing-library/react';

import App from '../App';

afterEach(cleanup);

describe('app::App', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <MemoryRouter>
        <App />
      </MemoryRouter>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
