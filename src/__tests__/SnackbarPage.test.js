// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import SnackbarPage from '../SnackbarPage';

afterEach(cleanup);

describe('page::SnackbarPage', () => {
  it('Should match snapshoot', () => {
    const scrollTo = jest.fn();
    window.scrollTo = scrollTo;
    const { container } = render(<SnackbarPage />);
    expect(container.firstChild).toMatchSnapshot();
    expect(scrollTo.mock.calls).toHaveLength(1);
    delete window.scrollTo;
  });
});
