// @flow strict
import React from 'react';
// $FlowFixMe
import { MemoryRouter } from 'react-router'; // eslint-disable-line
import { render, cleanup } from '@testing-library/react';

import MainNav from '../MainNav';

afterEach(cleanup);

describe('component::MainNav', () => {
  it('Should match snapshoot', () => {
    const onClick = jest.fn();
    const { container } = render(
      <MemoryRouter>
        <MainNav onClick={onClick} />
      </MemoryRouter>,
    );
    expect(container.firstChild).toMatchSnapshot();
    // expect(onClick.mock.calls).toHaveLength(1);
  });
});
