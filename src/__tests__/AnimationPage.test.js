// @flow strict
import React from 'react';
// $FlowFixMe
import { MemoryRouter } from 'react-router'; // eslint-disable-line
import { render, cleanup } from '@testing-library/react';

import AnimationPage from '../AnimationPage';

afterEach(cleanup);

describe('page::AnimationPage', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <MemoryRouter>
        <AnimationPage />
      </MemoryRouter>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
