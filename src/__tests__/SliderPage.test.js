// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import SliderPage from '../SliderPage';

afterEach(cleanup);

describe('page::SliderPage', () => {
  it('Should match snapshoot', () => {
    const scrollTo = jest.fn();
    window.scrollTo = scrollTo;
    const { container } = render(<SliderPage />);
    expect(container.firstChild).toMatchSnapshot();
    expect(scrollTo.mock.calls).toHaveLength(1);
    delete window.scrollTo;
  });
});
