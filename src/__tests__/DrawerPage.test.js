// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import DrawerPage from '../DrawerPage';

afterEach(cleanup);

describe('page::DrawerPage', () => {
  it('Should match snapshoot', () => {
    const scrollTo = jest.fn();
    window.scrollTo = scrollTo;
    const { container } = render(<DrawerPage />);
    expect(container.firstChild).toMatchSnapshot();
    expect(scrollTo.mock.calls).toHaveLength(1);
    delete window.scrollTo;
  });
});
