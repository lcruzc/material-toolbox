// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Example from '../Example';

afterEach(cleanup);

describe('component::Example', () => {
  it('Should match snapshoot', () => {
    const { container } = render(
      <Example title="Example title" source="source code">
        source code
      </Example>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should match snapshoot scss', () => {
    const { container } = render(
      <Example title="Example title" source="source code" scss="scss code">
        source code
      </Example>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
