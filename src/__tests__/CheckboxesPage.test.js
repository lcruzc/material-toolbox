// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import CheckboxesPage from '../CheckboxesPage';

afterEach(cleanup);

describe('page::CheckboxesPage', () => {
  it('Should match snapshoot', () => {
    const scrollTo = jest.fn();
    window.scrollTo = scrollTo;
    const { container } = render(<CheckboxesPage />);
    expect(container.firstChild).toMatchSnapshot();
    expect(scrollTo.mock.calls).toHaveLength(1);
    delete window.scrollTo;
  });
});
