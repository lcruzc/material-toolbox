// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import DialogPage from '../DialogPage';

afterEach(cleanup);

describe('page::DialogPage', () => {
  it('Should match snapshoot', () => {
    const scrollTo = jest.fn();
    window.scrollTo = scrollTo;
    const { container } = render(<DialogPage />);
    expect(container.firstChild).toMatchSnapshot();
    expect(scrollTo.mock.calls).toHaveLength(1);
    delete window.scrollTo;
  });
});
