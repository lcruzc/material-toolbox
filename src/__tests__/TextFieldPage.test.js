// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TextFieldPage from '../TextFieldPage';

afterEach(cleanup);

describe('page::TextFieldPage', () => {
  it('Should match snapshoot', () => {
    global.MutationObserver = class {
      constructor(callback) {} // eslint-disable-line

      disconnect() {} // eslint-disable-line

      observe(element, initObject) {} // eslint-disable-line
    };
    const scrollTo = jest.fn();
    window.scrollTo = scrollTo;
    const { container } = render(<TextFieldPage />);
    expect(container.firstChild).toMatchSnapshot();
    expect(scrollTo.mock.calls).toHaveLength(1);
    delete window.scrollTo;
    delete global.MutationObserver;
  });
});
