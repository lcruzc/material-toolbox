// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ButtonPage from '../ButtonPage';

afterEach(cleanup);

describe('page::ButtonPage', () => {
  it('Should match snapshoot', () => {
    const scrollTo = jest.fn();
    window.scrollTo = scrollTo;
    const { container } = render(<ButtonPage />);
    expect(container.firstChild).toMatchSnapshot();
    expect(scrollTo.mock.calls).toHaveLength(1);
    delete window.scrollTo;
  });
});
