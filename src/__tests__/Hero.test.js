// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Hero from '../Hero';

afterEach(cleanup);

describe('component::Hero', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<Hero />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
