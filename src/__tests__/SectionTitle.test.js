// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import SectionTitle from '../SectionTitle';

afterEach(cleanup);

describe('component::SectionTitle', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<SectionTitle>Title</SectionTitle>);
    expect(container.firstChild).toMatchSnapshot();
  });
});
