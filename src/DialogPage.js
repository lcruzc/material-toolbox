// @flow strict
import React, { useEffect } from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';
import Text from './material-toolbox/typography/Text';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';

import DialogAlert from './dialog-examples/Alert';
import DialogSimple from './dialog-examples/Simple';
import ConfirmationDialog from './dialog-examples/Confirmation';
import ScrollableDialog from './dialog-examples/Scrollable';

const resources = [
  {
    href: 'https://material.io/go/design-dialogs',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href: 'https://material.io/components/web/catalog/dialogs/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const DialogPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Dialog</h1>

      <Text as="p" text="overline">
        Dialog components are React wrappers of mdc-dialog components.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>Dialog</ComponentTitle>

      <p>
        This component inject <strong>name</strong> and
        <strong>reverted</strong> props to its children. Used by DialogActions
        and DialogContent
      </p>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>name</td>
            <td>string</td>
            <td />
            <td>✔</td>
            <td>used to enable Accessibility</td>
          </tr>
          <tr>
            <td>title</td>
            <td>string</td>
            <td />
            <td />
            <td>Set the title for dialog</td>
          </tr>
          <tr>
            <td>open</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>defines if dialog is opened</td>
          </tr>
          <tr>
            <td>autoStackButtons</td>
            <td>boolean</td>
            <td />
            <td />
            <td>
              Sets whether stacked/unstacked action button layout is
              automatically handled during layout logic.
            </td>
          </tr>
          <tr>
            <td>onOpening</td>
            <td>{'() => void'}</td>
            <td />
            <td />
            <td>Fired when opening</td>
          </tr>
          <tr>
            <td>onOpened</td>
            <td>{'() => void'}</td>
            <td />
            <td />
            <td>Fired when opened</td>
          </tr>
          <tr>
            <td>onClosing</td>
            <td>{'() => void'}</td>
            <td />
            <td />
            <td>Fired when closing</td>
          </tr>
          <tr>
            <td>onClosed</td>
            <td>{'() => void'}</td>
            <td />
            <td />
            <td>Fired when closed</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>DialogContent</ComponentTitle>

      <ComponentTitle>DialogButton</ComponentTitle>

      <p>Wrapper of Button adding extra props.</p>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>default</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set the button as default for dialog.</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>DialogActions</ComponentTitle>

      <SectionTitle>Demos</SectionTitle>

      <DialogAlert />

      <DialogSimple />

      <ConfirmationDialog />

      <ScrollableDialog />
    </LayoutCell>
  );
};

export default DialogPage;
