// @flow strict
import React, { useEffect } from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';
import Text from './material-toolbox/typography/Text';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';

import RadioDemo from './radio-examples/Demo';

const resources = [
  {
    href: 'https://material.io/go/design-radio-buttons',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href:
      'https://material.io/components/web/catalog/input-controls/radio-buttons/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const RadioButtonPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Radio Button</h1>

      <Text as="p" text="overline">
        Radio Button components is a React wrapper of mdc-radio-button
        component.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>Radio</ComponentTitle>

      <SectionTitle>Demos</SectionTitle>

      <RadioDemo />
    </LayoutCell>
  );
};

export default RadioButtonPage;
