// @flow strict
import React, { useState, useCallback } from 'react';

import DrawerContent from './material-toolbox/drawer/Content';
import DrawerMainContent from './material-toolbox/drawer/MainContent';
import Drawer from './material-toolbox/drawer';

import TopAppBar from './material-toolbox/top-app-bar';
import TopAppBarAdjuster from './material-toolbox/top-app-bar/Adjuster';
import TopAppBarIcon from './material-toolbox/top-app-bar/Icon';
import TopAppBarRow from './material-toolbox/top-app-bar/Row';
import TopAppBarSection from './material-toolbox/top-app-bar/Section';
import TopAppBarTitle from './material-toolbox/top-app-bar/Title';

import MainNav from './MainNav';
import Content from './Content';

const App = () => {
  const [open, setOpen] = useState<boolean>(false);
  const [scrollTarget, setScrollTarget] = useState();

  const toogleDrawer = useCallback(() => {
    setOpen(isOpen => !isOpen);
  }, []);

  const onRefUpdate = useCallback((element: ?HTMLElement) => {
    setScrollTarget(element === null ? undefined : element);
  }, []);

  return (
    <div className="layout">
      <TopAppBar fixed className="mt-app-bar" scrollTarget={scrollTarget}>
        <TopAppBarRow>
          <TopAppBarSection align="start">
            <TopAppBarIcon className="material-icons" onClick={toogleDrawer}>
              menu
            </TopAppBarIcon>
            <TopAppBarTitle>
              Material Component Web for React - Material Toolbox
            </TopAppBarTitle>
          </TopAppBarSection>
        </TopAppBarRow>
      </TopAppBar>

      <TopAppBarAdjuster fixed>
        <Drawer className="mt-drawer" dismissible open={open}>
          <DrawerContent>
            <MainNav />
          </DrawerContent>
        </Drawer>
      </TopAppBarAdjuster>

      <TopAppBarAdjuster fixed>
        <DrawerMainContent dismissible className="mt-main">
          <main className="mt-content" ref={onRefUpdate}>
            <Content />
          </main>
        </DrawerMainContent>
      </TopAppBarAdjuster>
    </div>
  );
};

export default App;
