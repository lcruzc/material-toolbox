// @flow strict
import React from 'react';

import Example from '../Example';
import Text from '../material-toolbox/typography/Text';
import TextField from '../material-toolbox/textfield';
import TextFieldHelperText from '../material-toolbox/textfield/HelperText';

const source = `import React from 'react';

import Text from 'material-toolbox/typography/Text';
import TextField from 'material-toolbox/textfield';
import TextFieldHelperText from 'material-toolbox/textfield/HelperText';

const MoreExamples = ({ title, label, withCharacterCounter }) => {
  return (
    <React.Fragment>
      <Text text="subtitle1" as="h1">
        {title}
      </Text>

      <section className="text-field-row">
        <div className="text-field-container">
          <TextField
            label={label}
            withCharacterCounter={withCharacterCounter}
            maxLength={18}
            helperText={<TextFieldHelperText text="Helper Text" persistent />}
          />
        </div>

        <div className="text-field-container">
          <TextField
            outlined
            label={label}
            withCharacterCounter={withCharacterCounter}
            maxLength={18}
            helperText={<TextFieldHelperText text="Helper Text" persistent />}
          />
        </div>

        <div className="text-field-container">
          <TextField
            outlined
            className="textfield-outlined-shaped"
            label={label}
            withCharacterCounter={withCharacterCounter}
            maxLength={18}
            helperText={<TextFieldHelperText text="Helper Text" persistent />}
          />
        </div>
      </section>
    </React.Fragment>
  );
};

export default MoreExamples;`;

const scssSource = `.text-field-row {
  display: flex;
  justify-content: space-between;
  margin-bottom: 30px;

  .textfield-filled-shaped {
    @include mdc-text-field-shape-radius(16px, false);
  }

  .textfield-outlined-shaped {
    @include mdc-notched-outline-shape-radius(28px, true);
  }
}`;

type Props = {
  title: string,
  label?: string,
  withCharacterCounter?: boolean,
};

const MoreExamples = ({ title, label, withCharacterCounter }: Props) => {
  return (
    <>
      <Text text="subtitle1" as="h1">
        {title}
      </Text>

      <section className="text-field-row">
        <div className="text-field-container">
          <TextField
            label={label}
            withCharacterCounter={withCharacterCounter}
            maxLength={18}
            helperText={<TextFieldHelperText text="Helper Text" persistent />}
          />
        </div>

        <div className="text-field-container">
          <TextField
            outlined
            label={label}
            withCharacterCounter={withCharacterCounter}
            maxLength={18}
            helperText={<TextFieldHelperText text="Helper Text" persistent />}
          />
        </div>

        <div className="text-field-container">
          <TextField
            outlined
            className="textfield-outlined-shaped"
            label={label}
            withCharacterCounter={withCharacterCounter}
            maxLength={18}
            helperText={<TextFieldHelperText text="Helper Text" persistent />}
          />
        </div>
      </section>
    </>
  );
};

const TextfieldMoreExamples = () => {
  return (
    <Example title="" source={source} scss={scssSource}>
      <MoreExamples title="Text Field without label" />
      <MoreExamples
        title="Text Field with Character Counter"
        label="Standard"
        withCharacterCounter
      />
    </Example>
  );
};

export default TextfieldMoreExamples;
