// @flow strict
import React from 'react';

import Example from '../Example';
import Text from '../material-toolbox/typography/Text';
import TextField from '../material-toolbox/textfield';
import TextFieldIcon from '../material-toolbox/textfield/Icon';
import TextFieldHelperText from '../material-toolbox/textfield/HelperText';

const source = `import React from 'react';

import Text from 'material-toolbox/typography/Text';
import TextField from 'material-toolbox/textfield';
import TextFieldIcon from 'material-toolbox/textfield/Icon';
import TextFieldHelperText from 'material-toolbox/textfield/HelperText';

const BasicExample = ({ title, className, outlined }) => {
  return (
    <React.Fragment>
      <Text text="subtitle1" as="h1">
        {title}
      </Text>

      <section className="text-field-row">
        <div className="text-field-container">
          <TextField
            outlined={outlined}
            className={className}
            label="Standard"
            helperText={<TextFieldHelperText text="Helper Text" persistent />}
          />
        </div>

        <div className="text-field-container">
          <TextField
            outlined={outlined}
            className={className}
            label="Standard"
            leadingIcon={
              <TextFieldIcon className="material-icons">event</TextFieldIcon>
            }
            helperText={<TextFieldHelperText text="Helper Text" persistent />}
          />
        </div>

        <div className="text-field-container">
          <TextField
            outlined={outlined}
            className={className}
            label="Standard"
            trailingIcon={
              <TextFieldIcon className="material-icons">delete</TextFieldIcon>
            }
            helperText={<TextFieldHelperText text="Helper Text" persistent />}
          />
        </div>
      </section>
    </React.Fragment>
  );
};

export default BasicExample;`;

const scssSource = `.text-field-row {
  display: flex;
  justify-content: space-between;
  margin-bottom: 30px;

  .textfield-filled-shaped {
    @include mdc-text-field-shape-radius(16px, false);
  }

  .textfield-outlined-shaped {
    @include mdc-notched-outline-shape-radius(28px, true);
  }
}`;

type Props = {
  title: string,
  outlined?: boolean,
  className?: string,
};

const BasicExample = ({ title, className, outlined }: Props) => {
  return (
    <>
      <Text text="subtitle1" as="h1">
        {title}
      </Text>

      <section className="text-field-row">
        <div className="text-field-container">
          <TextField
            outlined={outlined}
            className={className}
            label="Standard"
            helperText={<TextFieldHelperText text="Helper Text" persistent />}
          />
        </div>

        <div className="text-field-container">
          <TextField
            outlined={outlined}
            className={className}
            label="Standard"
            leadingIcon={
              <TextFieldIcon className="material-icons">event</TextFieldIcon>
            }
            helperText={<TextFieldHelperText text="Helper Text" persistent />}
          />
        </div>

        <div className="text-field-container">
          <TextField
            outlined={outlined}
            className={className}
            label="Standard"
            trailingIcon={
              <TextFieldIcon className="material-icons">delete</TextFieldIcon>
            }
            helperText={<TextFieldHelperText text="Helper Text" persistent />}
          />
        </div>
      </section>
    </>
  );
};

const BasicOptionsExample = () => {
  return (
    <Example title="" source={source} scss={scssSource}>
      <BasicExample title="Filled" />
      <BasicExample title="Shaped filled" className="textfield-filled-shaped" />
      <BasicExample title="Outlined" outlined />
      <BasicExample
        title="Shaped Outlined"
        outlined
        className="textfield-outlined-shaped"
      />
    </Example>
  );
};

export default BasicOptionsExample;
