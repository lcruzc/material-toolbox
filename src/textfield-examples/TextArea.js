// @flow strict
import React from 'react';

import Text from '../material-toolbox/typography/Text';
import Example from '../Example';
import TextField from '../material-toolbox/textfield';
import TextFieldHelperText from '../material-toolbox/textfield/HelperText';

const source = `import React from 'react';

import Text from 'material-toolbox/typography/Text';
import TextField from 'material-toolbox/textfield';
import TextFieldHelperText from 'material-toolbox/textfield/HelperText';

const TextArea = () => {
  return (
    <React.Fragment>
      <Text text="subtitle1" as="h1">
        Textarea
      </Text>
      <TextField multiline label="Standard" />

      <Text text="subtitle1" as="h1">
        Textarea with Character Counter
      </Text>
      <TextField
        multiline
        label="Standard"
        helperText={<TextFieldHelperText text="Helper Text" persistent />}
        withCharacterCounter
        maxLength={18}
      />
    </React.Fragment>
  );
};

export default TextArea;`;

const TextArea = () => {
  return (
    <Example source={source} title="">
      <Text text="subtitle1" as="h1">
        Textarea
      </Text>
      <TextField multiline label="Standard" />

      <Text text="subtitle1" as="h1">
        Textarea with Character Counter
      </Text>
      <TextField
        multiline
        label="Standard"
        withCharacterCounter
        maxLength={18}
        helperText={<TextFieldHelperText text="Helper Text" persistent />}
      />
    </Example>
  );
};

export default TextArea;
