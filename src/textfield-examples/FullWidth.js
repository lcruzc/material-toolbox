// @flow strict
import React from 'react';

import Example from '../Example';
import Text from '../material-toolbox/typography/Text';
import TextField from '../material-toolbox/textfield';
import TextFieldHelperText from '../material-toolbox/textfield/HelperText';

const source = `import React from 'react';

import Text from 'material-toolbox/typography/Text';
import TextField from 'material-toolbox/textfield';
import TextFieldHelperText from 'material-toolbox/textfield/HelperText';

const FullWidth = () => {
  return (
    <React.Fragment>
      <Text text="subtitle1" as="h1">
        Full Width
      </Text>
      <TextField
        fullWidth
        label="Standard"
        helperText={<TextFieldHelperText text="Helper Text" persistent />}
      />

      <Text text="subtitle1" as="h1">
        Full Width Textarea
      </Text>
      <TextField
        fullWidth
        multiline
        label="Standard"
        helperText={<TextFieldHelperText text="Helper Text" persistent />}
      />
    </React.Fragment>
  );
};

export default FullWidth;`;

const FullWidth = () => {
  return (
    <Example source={source} title="">
      <Text text="subtitle1" as="h1">
        Full Width
      </Text>
      <TextField
        fullWidth
        label="Standard"
        helperText={<TextFieldHelperText text="Helper Text" persistent />}
      />

      <Text text="subtitle1" as="h1">
        Full Width Textarea
      </Text>
      <TextField
        fullWidth
        multiline
        label="Standard"
        helperText={<TextFieldHelperText text="Helper Text" persistent />}
      />
    </Example>
  );
};

export default FullWidth;
