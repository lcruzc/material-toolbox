// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import FullWidth from '../FullWidth';

afterEach(cleanup);

describe('example::FullWidth', () => {
  it('Should match snapshoot', () => {
    global.MutationObserver = class {
      constructor(callback) {} // eslint-disable-line

      disconnect() {} // eslint-disable-line

      observe(element, initObject) {} // eslint-disable-line
    };
    const { container } = render(<FullWidth />);
    expect(container.firstChild).toMatchSnapshot();
    delete global.MutationObserver;
  });
});
