// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TextArea from '../TextArea';

afterEach(cleanup);

describe('example::TextArea', () => {
  it('Should match snapshoot', () => {
    global.MutationObserver = class {
      constructor(callback) {} // eslint-disable-line

      disconnect() {} // eslint-disable-line

      observe(element, initObject) {} // eslint-disable-line
    };
    const { container } = render(<TextArea />);
    expect(container.firstChild).toMatchSnapshot();
    delete global.MutationObserver;
  });
});
