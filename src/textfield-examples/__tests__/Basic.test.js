// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import BasicOptionsExample from '../Basic';

afterEach(cleanup);

describe('example::BasicOptionsExample', () => {
  it('Should match snapshoot', () => {
    global.MutationObserver = class {
      constructor(callback) {} // eslint-disable-line

      disconnect() {} // eslint-disable-line

      observe(element, initObject) {} // eslint-disable-line
    };
    const { container } = render(<BasicOptionsExample />);
    expect(container.firstChild).toMatchSnapshot();
    delete global.MutationObserver;
  });
});
