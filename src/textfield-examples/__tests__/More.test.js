// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import TextFieldMoreExamples from '../More';

afterEach(cleanup);

describe('example::TextFieldMoreExamples', () => {
  it('Should match snapshoot', () => {
    global.MutationObserver = class {
      constructor(callback) {} // eslint-disable-line

      disconnect() {} // eslint-disable-line

      observe(element, initObject) {} // eslint-disable-line
    };
    const { container } = render(<TextFieldMoreExamples />);
    expect(container.firstChild).toMatchSnapshot();
    delete global.MutationObserver;
  });
});
