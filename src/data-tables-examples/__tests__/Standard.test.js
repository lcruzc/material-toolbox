import React from 'react';
import { render, cleanup } from '@testing-library/react';

import DataTableStandard from '../Standard';

afterEach(cleanup);

describe('example::DataTableStandard', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<DataTableStandard />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
