import React from 'react';
import { render, cleanup } from '@testing-library/react';

import DataTableWithSelection from '../WithSelection';

afterEach(cleanup);

describe('example::DataTableWithSelection', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<DataTableWithSelection />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
