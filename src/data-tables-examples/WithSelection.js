// @flow strict
import React, { useState, useCallback } from 'react';

import Example from '../Example';

import DataTable from '../material-toolbox/data-table';
import DataTableHead from '../material-toolbox/data-table/Head';
import DataTableRow from '../material-toolbox/data-table/Row';
import DataTableCell from '../material-toolbox/data-table/Cell';
import DataTableContent from '../material-toolbox/data-table/Content';

const source = `import React, { useState, useCallback } from 'react';

import DataTable from 'material-toolbox/data-table';
import DataTableHead from 'material-toolbox/data-table/Head';
import DataTableRow from 'material-toolbox/data-table/Row';
import DataTableCell from 'material-toolbox/data-table/Cell';
import DataTableContent from 'material-toolbox/data-table/Content';

const DataTableWithSelection = () => {
  const [selected, setSelected] = useState(['ice-cream']);

  const handleRowSelectionChanged = useCallback(
    ({ rowId, selected: isSelected }) => {
      setSelected(selected_ => {
        if (isSelected) {
          return [...selected_, rowId];
        }
        return selected_.filter(s => s !== rowId);
      });
    },
    [],
  );

  return (
    <Example title="Data Table with Row Selection" source={source}>
      <DataTable
        label="Dessert calories"
        onRowSelectionChanged={handleRowSelectionChanged}
        selected={selected}
      >
        <DataTableHead>
          <DataTableRow header withCheckbox>
            <DataTableCell header>Dessert</DataTableCell>
            <DataTableCell header>Calories</DataTableCell>
            <DataTableCell header>Fat</DataTableCell>
            <DataTableCell header>Carbs</DataTableCell>
            <DataTableCell header>Protein (g)</DataTableCell>
          </DataTableRow>
        </DataTableHead>
        <DataTableContent>
          <DataTableRow withCheckbox rowId="yogurt">
            <DataTableCell>Frozen yogurt</DataTableCell>
            <DataTableCell numeric>159</DataTableCell>
            <DataTableCell numeric>6</DataTableCell>
            <DataTableCell numeric>24</DataTableCell>
            <DataTableCell numeric>4</DataTableCell>
          </DataTableRow>
          <DataTableRow withCheckbox rowId="ice-cream">
            <DataTableCell>Ice cream sandwich</DataTableCell>
            <DataTableCell numeric>237</DataTableCell>
            <DataTableCell numeric>9</DataTableCell>
            <DataTableCell numeric>37</DataTableCell>
            <DataTableCell numeric>4.3</DataTableCell>
          </DataTableRow>
          <DataTableRow withCheckbox rowId="eclair">
            <DataTableCell>Eclair</DataTableCell>
            <DataTableCell numeric>262</DataTableCell>
            <DataTableCell numeric>16</DataTableCell>
            <DataTableCell numeric>24</DataTableCell>
            <DataTableCell numeric>6</DataTableCell>
          </DataTableRow>
        </DataTableContent>
      </DataTable>
    </Example>
  );
};

export default DataTableWithSelection;`;

const DataTableWithSelection = () => {
  const [selected, setSelected] = useState(['ice-cream']);

  const handleRowSelectionChanged = useCallback(
    ({ rowId, selected: isSelected }) => {
      setSelected(selected_ => {
        if (isSelected) {
          return [...selected_, rowId];
        }
        return selected_.filter(s => s !== rowId);
      });
    },
    [],
  );

  return (
    <Example title="Data Table with Row Selection" source={source}>
      <DataTable
        label="Dessert calories"
        onRowSelectionChanged={handleRowSelectionChanged}
        selected={selected}
      >
        <DataTableHead>
          <DataTableRow header withCheckbox>
            <DataTableCell header>Dessert</DataTableCell>
            <DataTableCell header>Calories</DataTableCell>
            <DataTableCell header>Fat</DataTableCell>
            <DataTableCell header>Carbs</DataTableCell>
            <DataTableCell header>Protein (g)</DataTableCell>
          </DataTableRow>
        </DataTableHead>
        <DataTableContent>
          <DataTableRow withCheckbox rowId="yogurt">
            <DataTableCell>Frozen yogurt</DataTableCell>
            <DataTableCell numeric>159</DataTableCell>
            <DataTableCell numeric>6</DataTableCell>
            <DataTableCell numeric>24</DataTableCell>
            <DataTableCell numeric>4</DataTableCell>
          </DataTableRow>
          <DataTableRow withCheckbox rowId="ice-cream">
            <DataTableCell>Ice cream sandwich</DataTableCell>
            <DataTableCell numeric>237</DataTableCell>
            <DataTableCell numeric>9</DataTableCell>
            <DataTableCell numeric>37</DataTableCell>
            <DataTableCell numeric>4.3</DataTableCell>
          </DataTableRow>
          <DataTableRow withCheckbox rowId="eclair">
            <DataTableCell>Eclair</DataTableCell>
            <DataTableCell numeric>262</DataTableCell>
            <DataTableCell numeric>16</DataTableCell>
            <DataTableCell numeric>24</DataTableCell>
            <DataTableCell numeric>6</DataTableCell>
          </DataTableRow>
        </DataTableContent>
      </DataTable>
    </Example>
  );
};

export default DataTableWithSelection;
