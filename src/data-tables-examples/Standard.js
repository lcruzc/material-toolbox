// @flow strict
import React from 'react';

import Example from '../Example';

import DataTable from '../material-toolbox/data-table';
import DataTableHead from '../material-toolbox/data-table/Head';
import DataTableRow from '../material-toolbox/data-table/Row';
import DataTableCell from '../material-toolbox/data-table/Cell';
import DataTableContent from '../material-toolbox/data-table/Content';

const source = `import React from 'react';

import DataTable from 'material-toolbox/data-table';
import DataTableHead from 'material-toolbox/data-table/Head';
import DataTableRow from 'material-toolbox/data-table/Row';
import DataTableCell from 'material-toolbox/data-table/Cell';
import DataTableContent from 'material-toolbox/data-table/Content';

const DataTableStandard = () => {
  return (
    <DataTable label="Dessert calories">
      <DataTableHead>
        <DataTableRow header>
          <DataTableCell header>Dessert</DataTableCell>
          <DataTableCell header>Calories</DataTableCell>
          <DataTableCell header>Fat</DataTableCell>
          <DataTableCell header>Carbs</DataTableCell>
          <DataTableCell header>Protein (g)</DataTableCell>
        </DataTableRow>
      </DataTableHead>
      <DataTableContent>
        <DataTableRow>
          <DataTableCell>Frozen yogurt</DataTableCell>
          <DataTableCell numeric>159</DataTableCell>
          <DataTableCell numeric>6</DataTableCell>
          <DataTableCell numeric>24</DataTableCell>
          <DataTableCell numeric>4</DataTableCell>
        </DataTableRow>
        <DataTableRow>
          <DataTableCell>Ice cream sandwich</DataTableCell>
          <DataTableCell numeric>237</DataTableCell>
          <DataTableCell numeric>9</DataTableCell>
          <DataTableCell numeric>37</DataTableCell>
          <DataTableCell numeric>4.3</DataTableCell>
        </DataTableRow>
        <DataTableRow>
          <DataTableCell>Eclair</DataTableCell>
          <DataTableCell numeric>262</DataTableCell>
          <DataTableCell numeric>16</DataTableCell>
          <DataTableCell numeric>24</DataTableCell>
          <DataTableCell numeric>6</DataTableCell>
        </DataTableRow>
      </DataTableContent>
    </DataTable>
  );
};

export default DataTableStandard;`;

const DataTableStandard = () => {
  return (
    <Example title="Data Table Standard" source={source}>
      <DataTable label="Dessert calories">
        <DataTableHead>
          <DataTableRow header>
            <DataTableCell header>Dessert</DataTableCell>
            <DataTableCell header>Calories</DataTableCell>
            <DataTableCell header>Fat</DataTableCell>
            <DataTableCell header>Carbs</DataTableCell>
            <DataTableCell header>Protein (g)</DataTableCell>
          </DataTableRow>
        </DataTableHead>
        <DataTableContent>
          <DataTableRow>
            <DataTableCell>Frozen yogurt</DataTableCell>
            <DataTableCell numeric>159</DataTableCell>
            <DataTableCell numeric>6</DataTableCell>
            <DataTableCell numeric>24</DataTableCell>
            <DataTableCell numeric>4</DataTableCell>
          </DataTableRow>
          <DataTableRow>
            <DataTableCell>Ice cream sandwich</DataTableCell>
            <DataTableCell numeric>237</DataTableCell>
            <DataTableCell numeric>9</DataTableCell>
            <DataTableCell numeric>37</DataTableCell>
            <DataTableCell numeric>4.3</DataTableCell>
          </DataTableRow>
          <DataTableRow>
            <DataTableCell>Eclair</DataTableCell>
            <DataTableCell numeric>262</DataTableCell>
            <DataTableCell numeric>16</DataTableCell>
            <DataTableCell numeric>24</DataTableCell>
            <DataTableCell numeric>6</DataTableCell>
          </DataTableRow>
        </DataTableContent>
      </DataTable>
    </Example>
  );
};

export default DataTableStandard;
