// @flow strict
import React, { useEffect } from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import Text from './material-toolbox/typography/Text';

import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';

import BasicTextField from './textfield-examples/Basic';
import MoreTextFieldExamples from './textfield-examples/More';
import TextArea from './textfield-examples/TextArea';
import FullWidth from './textfield-examples/FullWidth';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import ThemedLink from './ThemedLink';

const resources = [
  {
    href: 'https://material.io/go/design-text-fields',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href:
      'https://material.io/components/web/catalog/input-controls/text-field/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
  {
    href:
      'https://material.io/components/web/catalog/input-controls/text-field/icon/',
    target: '_blank',
    children: 'Material Components Web Documentation: Icon',
  },
  {
    href:
      'https://material.io/components/web/catalog/input-controls/text-field/helper-text/',
    target: '_blank',
    children: 'Material Components Web Documentation: Helper Text',
  },
  {
    href:
      'https://material.io/components/web/catalog/input-controls/text-field/character-counter/',
    target: '_blank',
    children: 'Material Components Web Documentation: Character Counter',
  },
  {
    href:
      'https://material.io/components/web/catalog/input-controls/floating-label/',
    target: '_blank',
    children: 'Material Components Web Documentation: Floating Label',
  },
  {
    href:
      'https://material.io/components/web/catalog/input-controls/line-ripple/',
    target: '_blank',
    children: 'Material Components Web Documentation: Line Ripple',
  },
  {
    href:
      'https://material.io/components/web/catalog/input-controls/notched-outline/',
    target: '_blank',
    children: 'Material Components Web Documentation: Notched Outline',
  },
];

const TextFieldPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Text Field</h1>

      <Text as="p" text="overline">
        Text field component is a React wrapper of mdc-text-field component.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>TextFieldIcon</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>clickable</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Enable click feature into icon</td>
          </tr>
          <tr>
            <td>onActived</td>
            <td>{'() => void'}</td>
            <td />
            <td />
            <td>
              Callback called when component icon is clicked or Enter key.
            </td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>TextFieldHelperText</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>text</td>
            <td>string</td>
            <td />
            <td>✔</td>
            <td>Text to display</td>
          </tr>
          <tr>
            <td>persistent</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set persistent state</td>
          </tr>
          <tr>
            <td>validation</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set validation state</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>TextField</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>label</td>
            <td>string</td>
            <td />
            <td>✔</td>
            <td>Label text for element</td>
          </tr>
          <tr>
            <td>fullWidth</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>If true set full width element and do not use outlined.</td>
          </tr>
          <tr>
            <td>multiline</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>If true render a textarea element.</td>
          </tr>
          <tr>
            <td>disabled</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set disabled state</td>
          </tr>
          <tr>
            <td>outlined</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set the outlined UI.</td>
          </tr>
          <tr>
            <td>helperText</td>
            <td>React.Node</td>
            <td />
            <td />
            <td>TextFieldHelperText element used to render the helper text</td>
          </tr>
          <tr>
            <td>leadingIcon</td>
            <td>React.Node</td>
            <td />
            <td />
            <td>TextFieldIcon element used to render the leading icon</td>
          </tr>
          <tr>
            <td>trailingIcon</td>
            <td>React.Node</td>
            <td />
            <td />
            <td>TextFieldIcon element used to render the trailing icon</td>
          </tr>
          <tr>
            <td>withCharacterCounter</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Enable the character count element</td>
          </tr>
          <tr>
            <td>invalid</td>
            <td>boolean</td>
            <td />
            <td />
            <td>If true set invalid/error UI to component.</td>
          </tr>
        </tbody>
      </table>

      <BasicTextField />
      <MoreTextFieldExamples />
      <TextArea />
      <FullWidth />
    </LayoutCell>
  );
};

export default TextFieldPage;
