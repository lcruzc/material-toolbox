import React from 'react';
import { render, cleanup } from '@testing-library/react';

import CommonContent from '../CommonContent';

afterEach(cleanup);

describe('component::CommonContent', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<CommonContent />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
