// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import WithActions from '../WithActions';

afterEach(cleanup);

describe('example::WithActions', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<WithActions />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
