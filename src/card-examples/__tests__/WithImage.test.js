// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import WithImage from '../WithImage';

afterEach(cleanup);

describe('example::WithImage', () => {
  it('Should match snapshoot', () => {
    const { container } = render(<WithImage />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
