// @flow strict
import React from 'react';
import cx from 'classnames';
import Example from '../Example';

import Button from '../material-toolbox/button';
import IconButton from '../material-toolbox/icon-button';
import IconButtonState from '../material-toolbox/icon-button/State';
import Card from '../material-toolbox/card';
import CardActions from '../material-toolbox/card/Actions';
import CardActionButtons from '../material-toolbox/card/ActionButtons';
import CardAction from '../material-toolbox/card/Action';
import CardActionIcons from '../material-toolbox/card/ActionIcons';
import CardPrimaryAction from '../material-toolbox/card/PrimaryAction';

import CommonContent from './CommonContent';

const source = `import Text from 'material-toolbox/typography/Text';
import Button from 'material-toolbox/button/Button';
import ButtonIcon from 'material-toolbox/button/Icon';
import IconToggle from 'material-toolbox/icon-toggle';
import Card from 'material-toolbox/card/Card';
import CardActions from 'material-toolbox/card/Actions';
import CardActionButtons from 'material-toolbox/card/ActionButtons';
import CardAction from 'material-toolbox/card/Action';
import CardActionIcons from 'material-toolbox/card/ActionIcons';
import CardPrimaryAction from 'material-toolbox/card/PrimaryAction';

const BasicWithAction = ({ shaped = false }: { shaped?: boolean }) => (
  <Card className={cx('demo-card', { 'demo-card-shaped': shaped })}>
    <CardPrimaryAction>
      <div className="demo-card__primary">
        <Text text="headline6" as="h2" className="demo-card__title">
          Our Changing Planet
        </Text>
        <Text text="subtitle2" as="h3" className="demo-card__subtitle">
          by Kurt Wagner
        </Text>
      </div>
      <Text className="demo-card__secondary" text="body2" as="div">
        Visit ten places on our planet that are undergoing the biggest changes
        today.
      </Text>
    </CardPrimaryAction>
    <CardActions>
      <CardActionButtons>
        <CardAction>
          <Button>Read</Button>
          <Button>Bookmark</Button>
        </CardAction>
      </CardActionButtons>
      <CardActionIcons>
        <CardAction icon>
          <IconButton
            onLabel="Remove from favorites"
            offLabel="Add to favorites"
            toggle>
            <IconButtonState className="material-icons">
              favorite_border
            </IconButtonState>
            <IconButtonState className="material-icons" isOn>
              favorite
            </IconButtonState>
          </IconButton>
          <IconButton className="material-icons">share</IconButton>
          <IconButton className="material-icons">more_vert</IconButton>
        </CardAction>
      </CardActionIcons>
    </CardActions>
  </Card>
);

const WithActions = () => (
  <React.Fragment>
    <BasicWithAction />
    <BasicWithAction shaped />
  </React.Fragment>
);

export default WithActions;`;

const scssSource = `.demo-card {
  width: 350px;
  margin: 48px 0;

  &__primary {
    padding: 1rem;
  }

  &__title,
  &__subtitle {
    margin: 0;
  }

  &__secondary {
    padding: 0 1rem 8px;
  }

  &__secondary,
  &__subtitle {
    color: rgba(0, 0, 0, 0.54);
    color: var(--mdc-theme-text-secondary-on-background, rgba(0, 0, 0, 0.54));
  }

  &-shaped {
    border-radius: 24px 8px;
  }
}`;

const BasicWithAction = ({ shaped = false }: { shaped?: boolean }) => (
  <Card className={cx('demo-card', { 'demo-card-shaped': shaped })}>
    <CardPrimaryAction>
      <CommonContent />
    </CardPrimaryAction>
    <CardActions>
      <CardActionButtons>
        <CardAction>
          <Button>Read</Button>
          <Button>Bookmark</Button>
        </CardAction>
      </CardActionButtons>
      <CardActionIcons>
        <CardAction icon>
          <IconButton
            onLabel="Remove from favorites"
            offLabel="Add to favorites"
            toggle
          >
            <IconButtonState className="material-icons">
              favorite_border
            </IconButtonState>
            <IconButtonState className="material-icons" isOn>
              favorite
            </IconButtonState>
          </IconButton>
          <IconButton className="material-icons">share</IconButton>
          <IconButton className="material-icons">more_vert</IconButton>
        </CardAction>
      </CardActionIcons>
    </CardActions>
  </Card>
);

const WithActions = () => (
  <Example title="With actions" source={source} scss={scssSource}>
    <BasicWithAction />
    <BasicWithAction shaped />
  </Example>
);

export default WithActions;
