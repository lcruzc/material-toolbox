// @flow strict
import React from 'react';

import Text from '../material-toolbox/typography/Text';

const CommonContent = () => (
  <>
    <div className="demo-card__primary">
      <Text text="headline6" as="h2" className="demo-card__title">
        Our Changing Planet
      </Text>
      <Text text="subtitle2" as="h3" className="demo-card__subtitle">
        by Kurt Wagner
      </Text>
    </div>
    <Text className="demo-card__secondary" text="body2" as="div">
      Visit ten places on our planet that are undergoing the biggest changes
      today.
    </Text>
  </>
);

export default CommonContent;
