// @flow strict
import React from 'react';
import Example from '../Example';

import Card from '../material-toolbox/card';
import CardMedia from '../material-toolbox/card/Media';
import CardPrimaryAction from '../material-toolbox/card/PrimaryAction';

import CommonContent from './CommonContent';

const source = `import Text from 'material-toolbox/typography/Text';
import Card from 'material-toolbox/card/Card';
import CardMedia from 'material-toolbox/card/Media';
import CardPrimaryAction from 'material-toolbox/card/PrimaryAction';

const WithImage = () => (
  <Card className="demo-card">
    <CardPrimaryAction>
      <CardMedia
        ratio169
        style={{
          'background-image':
            'url("https://material-components.github.io/material-components-web-catalog/static/media/photos/3x2/2.jpg")',
        }}
      />
      <div className="demo-card__primary">
        <Text text="headline6" as="h2" className="demo-card__title">
          Our Changing Planet
        </Text>
        <Text text="subtitle2" as="h3" className="demo-card__subtitle">
          by Kurt Wagner
        </Text>
      </div>
      <Text className="demo-card__secondary" text="body2" as="div">
        Visit ten places on our planet that are undergoing the biggest changes
        today.
      </Text>
    </CardPrimaryAction>
  </Card>
);
export default WithImage;`;

const scssSource = `.demo-card {
  width: 350px;
  margin: 48px 0;

  &__primary {
    padding: 1rem;
  }

  &__title,
  &__subtitle {
    margin: 0;
  }

  &__secondary {
    padding: 0 1rem 8px;
  }

  &__secondary,
  &__subtitle {
    color: rgba(0, 0, 0, 0.54);
    color: var(--mdc-theme-text-secondary-on-background, rgba(0, 0, 0, 0.54));
  }

  &-shaped {
    border-radius: 24px 8px;
  }
}`;

const WithImage = () => (
  <Example title="With image" source={source} scss={scssSource}>
    <Card className="demo-card">
      <CardPrimaryAction>
        <CardMedia
          ratio169
          style={{
            backgroundImage:
              'url("https://material-components.github.io/material-components-web-catalog/static/media/photos/3x2/2.jpg")',
          }}
        />
        <CommonContent />
      </CardPrimaryAction>
    </Card>
  </Example>
);

export default WithImage;
