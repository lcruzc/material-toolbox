// @flow strict
import React, { useEffect } from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import Text from './material-toolbox/typography/Text';
import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';

import Example from './Example';
import ThemedLink from './ThemedLink';
import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import Highlighter from './Highlighter';

const source = `import React from 'react';

import TopAppBar from 'material-toolbox/top-app-bar';
import TopAppBarRow from 'material-toolbox/top-app-bar/Row';
import TopAppBarSection from 'material-toolbox/top-app-bar/Section';
import TopAppBarTitle from 'material-toolbox/top-app-bar/Title';
import TopAppBarIcon from 'material-toolbox/top-app-bar/Icon';
import TopAppBarAction from 'material-toolbox/top-app-bar/Action';
import TopAppBarAdjuster from 'material-toolbox/top-app-bar/Adjuster';

const TopAppBarExample = ({ type }) => {
  let titleApp;
  const props = {
    short: false,
    fixed: false,
    dense: false,
    prominent: false,
    collapsed: false,
  };

  switch (type) {
    case 'standard':
      titleApp = 'Standard';
      break;
    case 'fixed':
      titleApp = 'Fixed';
      props.fixed = true;
      break;
    case 'dense':
      titleApp = 'Dense';
      props.dense = true;
      break;
    case 'prominent':
      titleApp = 'Prominent';
      props.prominent = true;
      break;
    case 'short':
      titleApp = 'Short';
      props.short = true;
      break;
    case 'alwaysShort':
      props.short = true;
      props.collapsed = true;
      break;
    default:
      break;
  }

  return (
    <React.Fragment>
      <TopAppBar {...props}>
        <TopAppBarRow>
          <TopAppBarSection>
            <TopAppBarIcon href="#" className="material-icons">
              menu
            </TopAppBarIcon>
            <TopAppBarTitle>{titleApp}</TopAppBarTitle>
          </TopAppBarSection>

          <TopAppBarSection end>
            <TopAppBarAction
              label="Download"
              href="#"
              className="material-icons">
              file_download
            </TopAppBarAction>
            {!props.short && (
              <React.Fragment>
                <TopAppBarAction
                  label="Print"
                  href="#"
                  className="material-icons">
                  print
                </TopAppBarAction>
                <TopAppBarAction
                  label="Bookmark this page"
                  href="#"
                  className="material-icons">
                  bookmark
                </TopAppBarAction>
              </React.Fragment>
            )}
          </TopAppBarSection>
        </TopAppBarRow>
      </TopAppBar>
      <TopAppBarAdjuster {...props}>
        <main className="demo-main">
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec odio
            lorem, efficitur ac faucibus eget, tempor ac tellus. Sed a neque
            leo. Donec hendrerit ac purus in rutrum. Aliquam in velit eu massa
            porttitor dapibus. Sed posuere volutpat orci, non egestas lectus
            rhoncus id. Cras a odio molestie, semper nibh id, pellentesque
            neque. Aliquam imperdiet risus non nisl ultricies, sit amet rhoncus
            purus congue. Donec consectetur leo quis leo dapibus consequat.
            Aliquam pretium nulla posuere tortor aliquam suscipit. Duis ornare
            orci vel nisl lobortis lacinia.
          </p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec odio
            lorem, efficitur ac faucibus eget, tempor ac tellus. Sed a neque
            leo. Donec hendrerit ac purus in rutrum. Aliquam in velit eu massa
            porttitor dapibus. Sed posuere volutpat orci, non egestas lectus
            rhoncus id. Cras a odio molestie, semper nibh id, pellentesque
            neque. Aliquam imperdiet risus non nisl ultricies, sit amet rhoncus
            purus congue. Donec consectetur leo quis leo dapibus consequat.
            Aliquam pretium nulla posuere tortor aliquam suscipit. Duis ornare
            orci vel nisl lobortis lacinia.
          </p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec odio
            lorem, efficitur ac faucibus eget, tempor ac tellus. Sed a neque
            leo. Donec hendrerit ac purus in rutrum. Aliquam in velit eu massa
            porttitor dapibus. Sed posuere volutpat orci, non egestas lectus
            rhoncus id. Cras a odio molestie, semper nibh id, pellentesque
            neque. Aliquam imperdiet risus non nisl ultricies, sit amet rhoncus
            purus congue. Donec consectetur leo quis leo dapibus consequat.
            Aliquam pretium nulla posuere tortor aliquam suscipit. Duis ornare
            orci vel nisl lobortis lacinia.
          </p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec odio
            lorem, efficitur ac faucibus eget, tempor ac tellus. Sed a neque
            leo. Donec hendrerit ac purus in rutrum. Aliquam in velit eu massa
            porttitor dapibus. Sed posuere volutpat orci, non egestas lectus
            rhoncus id. Cras a odio molestie, semper nibh id, pellentesque
            neque. Aliquam imperdiet risus non nisl ultricies, sit amet rhoncus
            purus congue. Donec consectetur leo quis leo dapibus consequat.
            Aliquam pretium nulla posuere tortor aliquam suscipit. Duis ornare
            orci vel nisl lobortis lacinia.
          </p>
        </main>
      </TopAppBarAdjuster>
    </React.Fragment>
  );
};

export default TopAppBarExample;`;

const scssSource = `@import url("https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css");

$mdc-theme-primary: #00838f;
$mdc-theme-secondary: #80cbc4;

@import "@material/theme/mdc-theme";
@import "@material/ripple/mdc-ripple";
@import "@material/top-app-bar/mdc-top-app-bar";
@import "@material/icon-button/mdc-icon-button";
@import "@material/form-field/mdc-form-field";`;

const resources = [
  {
    href: 'https://material.io/go/design-app-bar-top',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href: 'https://material.io/components/web/catalog/top-app-bar/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const TopAppBarPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <LayoutCell align="bottom" span={12}>
      <h1>Top App Bar</h1>

      <Text as="p" text="overline">
        Top App Bar components are a React wrappers of mdc-top-app-bar
        component.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>TopAppBar</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>fixed</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set a fixed TopAppBar</td>
          </tr>
          <tr>
            <td>short</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set a short style TopAppBar</td>
          </tr>
          <tr>
            <td>collapsed</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Sets collapsed version of short style TopAppBar</td>
          </tr>
          <tr>
            <td>prominent</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set prominent style TopAppBar</td>
          </tr>
          <tr>
            <td>scrollTarget</td>
            <td>HTMLElement</td>
            <td />
            <td />
            <td>Used to control scroll, if not set use window object</td>
          </tr>
          <tr>
            <td>dense</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>
              Set dense version on standard, fixed, prominent syle TopAppBar
            </td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>TopAppBarRow</ComponentTitle>

      <ComponentTitle>TopAppBarSection</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>end</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>The second section into the row</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>TopAppBarTitle</ComponentTitle>

      <ComponentTitle>TopAppBarAdjuster</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>short</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set a short style TopAppBarAdjuster</td>
          </tr>
          <tr>
            <td>prominent</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set prominent style TopAppBarAdjuster</td>
          </tr>
          <tr>
            <td>dense</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>
              Set dense version on standard, fixed, prominent syle
              TopAppBarAdjuster
            </td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>useTopAppBarAdjuster</ComponentTitle>

      <Highlighter language="jsx">{`const useTopAppBarAdjuster = ({
  short = false,
  dense = false,
  prominent = false,
}: {
  short: boolean,
  dense: boolean,
  prominent: boolean,
}) => string`}</Highlighter>

      <ComponentTitle>TopAppBarAction</ComponentTitle>

      <ComponentTitle>TopAppBarIcon</ComponentTitle>

      <SectionTitle>Demos</SectionTitle>

      <Example title="" source={source} scss={scssSource}>
        <div className="top-app-bar-demo-grid">
          <div className="top-app-bar-demo">
            <div>
              <a href="./top-app-bar/standard.html">Standard</a>
            </div>
            <div>
              <iframe
                className="top-app-bar-frame"
                title="Standard"
                src="./top-app-bar/standard.html"
                frameBorder="0"
              />
            </div>
          </div>
          <div className="top-app-bar-demo">
            <div>
              <a href="./top-app-bar/fixed.html">Fixed</a>
            </div>
            <div>
              <iframe
                className="top-app-bar-frame"
                title="Dense"
                src="./top-app-bar/fixed.html"
                frameBorder="0"
              />
            </div>
          </div>
          <div className="top-app-bar-demo">
            <div>
              <a href="./top-app-bar/dense.html">Dense</a>
            </div>
            <div>
              <iframe
                className="top-app-bar-frame"
                title="Dense"
                src="./top-app-bar/dense.html"
                frameBorder="0"
              />
            </div>
          </div>
          <div className="top-app-bar-demo">
            <div>
              <a href="./top-app-bar/prominent.html">Prominent</a>
            </div>
            <div>
              <iframe
                className="top-app-bar-frame"
                title="Prominent"
                src="./top-app-bar/prominent.html"
                frameBorder="0"
              />
            </div>
          </div>
          <div className="top-app-bar-demo">
            <div>
              <a href="./top-app-bar/short.html">Short</a>
            </div>
            <div>
              <iframe
                className="top-app-bar-frame"
                title="Short"
                src="./top-app-bar/short.html"
                frameBorder="0"
              />
            </div>
          </div>
          <div className="top-app-bar-demo">
            <div>
              <a href="./top-app-bar/always-short.html">
                Short - Always Collapsed
              </a>
            </div>
            <div>
              <iframe
                className="top-app-bar-frame"
                title="Always Short"
                src="./top-app-bar/always-short.html"
                frameBorder="0"
              />
            </div>
          </div>
        </div>
      </Example>
    </LayoutCell>
  );
};

export default TopAppBarPage;
