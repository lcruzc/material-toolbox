// @flow strict
import React, { useEffect } from 'react';

import LayoutCell from './material-toolbox/layout-grid/Cell';
import List from './material-toolbox/list';
import ListItem from './material-toolbox/list/Item';
import Text from './material-toolbox/typography/Text';

import ThemedLink from './ThemedLink';

import ComponentTitle from './ComponentTitle';
import SectionTitle from './SectionTitle';
import TabBars from './tabs-examples/TabBars';

const resources = [
  {
    href: 'https://material.io/go/design-tabs',
    target: '_blank',
    children: 'Material Design Guidelines',
  },
  {
    href: 'https://material.io/components/web/catalog/tabs/',
    target: '_blank',
    children: 'Material Components Web Documentation',
  },
];

const TabsPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <LayoutCell span={12}>
      <h1>Tab Bars</h1>

      <Text as="p" text="overline">
        Tab Bars components is a React wrapper of mdc-tabs component.
      </Text>

      <SectionTitle>Resources</SectionTitle>

      <List>
        {resources.map(item => (
          <ListItem key={item.href} as={ThemedLink} {...item} />
        ))}
      </List>

      <SectionTitle>Components</SectionTitle>

      <ComponentTitle>TabIcon</ComponentTitle>

      <ComponentTitle>TabText</ComponentTitle>

      <ComponentTitle>TabScroller</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>align</td>
            <td>String: center start end</td>
            <td />
            <td />
            <td>Add align to tabs, tabs needs to be use minWidth prop</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>Tab</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>as</td>
            <td>Any component</td>
            <td>button</td>
            <td />
            <td>Element type that will generate the button</td>
          </tr>
          <tr>
            <td>contentIndicator</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Sets the indicator only to content</td>
          </tr>
          <tr>
            <td>fade</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Sets fade effect instead of slicing.</td>
          </tr>
          <tr>
            <td>minWidth</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Sets the width to minimun width possible</td>
          </tr>
          <tr>
            <td>stacked</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Set as stacked the content for icon into tab</td>
          </tr>
        </tbody>
      </table>

      <ComponentTitle>TabBar</ComponentTitle>

      <table className="table-props">
        <thead>
          <tr>
            <th>Property</th>
            <th>Type</th>
            <th>Default</th>
            <th>Required</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>initialActive</td>
            <td>number</td>
            <td />
            <td />
            <td>Sets the initial active tab</td>
          </tr>
          <tr>
            <td>focusOnActivate</td>
            <td>boolean</td>
            <td>true</td>
            <td />
            <td>Enable focus when item is active</td>
          </tr>
          <tr>
            <td>useAutomaticActivation</td>
            <td>boolean</td>
            <td>false</td>
            <td />
            <td>Use automatic activation</td>
          </tr>
          <tr>
            <td>onChange</td>
            <td>{'(index: number) => void'}</td>
            <td />
            <td />
            <td>Event call when new tab is selected.</td>
          </tr>
        </tbody>
      </table>

      <SectionTitle>Demos</SectionTitle>

      <TabBars />
    </LayoutCell>
  );
};

export default TabsPage;
