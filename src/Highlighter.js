// @flow strict
import React from 'react';

import Highlight from 'react-syntax-highlighter/dist/cjs/prism'; // eslint-disable-line
import xonokai from 'react-syntax-highlighter/dist/cjs/styles/prism/vs'; // eslint-disable-line

import type { Node } from 'react';

type Props = {
  children?: Node,
  language?: string,
};

const Highlighter = ({ children, language }: Props) => (
  <Highlight showLineNumbers language={language} style={xonokai}>
    {children}
  </Highlight>
);

Highlighter.defaultProps = {
  language: 'bash',
};

export default Highlighter;
