// @flow strict
import React, { useState, useCallback } from 'react';

import IconButton from '../material-toolbox/icon-button';
import IconButtonState from '../material-toolbox/icon-button/State';

import Example from '../Example';

const source = `import React, { useState, useCallback } from 'react';

import IconButton from '../material-toolbox/icon-button';
import IconButtonState from '../material-toolbox/icon-button/State';

const Demo = () => {
  const [isOn, setIsOn] = useState(false);

  const handleChange = useCallback(() => {
    setIsOn(prevIsOn => !prevIsOn);
  }, []);

  return (
    <div>
      <p>Icon Button</p>
      <IconButton className="material-icons">wifi</IconButton>

      <p>Icon Toggle Button</p>
      <IconButton toggle on={isOn} onChange={handleChange}>
        <IconButtonState className="material-icons" isOn>
          favorite
        </IconButtonState>
        <IconButtonState className="material-icons">
          favorite_border
        </IconButtonState>
      </IconButton>
    </div>
  );
};

export default Demo;`;

const Demo = () => {
  const [isOn, setIsOn] = useState<boolean>(false);
  const handleChange = useCallback(() => {
    setIsOn(prevIsOn => !prevIsOn);
  }, []);

  return (
    <Example title="" source={source}>
      <p>Icon Button</p>
      <IconButton className="material-icons">wifi</IconButton>

      <p>Icon Toggle Button</p>
      <IconButton toggle on={isOn} onChange={handleChange}>
        <IconButtonState className="material-icons" isOn>
          favorite
        </IconButtonState>
        <IconButtonState className="material-icons">
          favorite_border
        </IconButtonState>
      </IconButton>
    </Example>
  );
};

export default Demo;
