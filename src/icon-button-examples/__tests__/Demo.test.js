// @flow strict
import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Demo from '../Demo';

afterEach(cleanup);

describe('example::Demo', () => {
  it('Should match snapshoot standard', () => {
    const { container } = render(<Demo />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
