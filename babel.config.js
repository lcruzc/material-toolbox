/*
 * This is babel configuration for server, for frontend webpack uses own config
 */

module.exports = {
  only: ['./node_modules/@material', './src'],

  presets: [
    [
      '@babel/env',
      {
        targets: {
          browsers: '> 0.25%, not dead',
        },
      },
    ],
    '@babel/react',
    '@babel/flow',
  ],

  plugins: [
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-syntax-dynamic-import',
    '@babel/plugin-proposal-optional-chaining',
    // '@babel/plugin-proposal-export-default-from',
    // ['@babel/plugin-proposal-decorators', { legacy: true }],
  ],

  env: {
    development: {
      plugins: ['react-hot-loader/babel'],
    },
  },
};
