const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const strip = require('strip-loader');
const FlowtypePlugin = require('flowtype-loader/plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const { NODE_ENV } = process.env;
const PROD = NODE_ENV === 'production';

const JS_LOADERS = PROD
  ? [strip.loader('debug'), 'babel-loader']
  : [
      {
        loader: 'babel-loader',
      },
      {
        loader: 'eslint-loader',
        options: {
          // eslint-disable-next-line
          formatter: require('eslint').CLIEngine.getFormatter('stylish'),
        },
      },
      { loader: 'flowtype-loader' },
    ];

const appEntry = PROD
  ? ['./src/index.js']
  : ['react-hot-loader/patch', './src/index.js'];

const appTopAppBar = PROD
  ? [path.resolve('./src/top-app-bar-examples/index.js')]
  : [
      'react-hot-loader/patch',
      path.resolve('./src/top-app-bar-examples/index.js'),
    ];

const appDrawerPermanent = PROD
  ? [path.resolve('./src/drawer-examples/permanent/index.js')]
  : [
      'react-hot-loader/patch',
      path.resolve('./src/drawer-examples/permanent/index.js'),
    ];

const appDrawerPersistent = PROD
  ? [path.resolve('./src/drawer-examples/dismissible/index.js')]
  : [
      'react-hot-loader/patch',
      path.resolve('./src/drawer-examples/dismissible/index.js'),
    ];

const appDrawerTemporary = PROD
  ? [path.resolve('./src/drawer-examples/modal/index.js')]
  : [
      'react-hot-loader/patch',
      path.resolve('./src/drawer-examples/modal/index.js'),
    ];

const CSS_LOADER_CONFIG = [
  {
    loader: 'css-loader',
    options: {
      modules: {
        localIdentName: '[local]',
      },
      sourceMap: true,
      importLoaders: 2,
    },
  },
  {
    loader: 'postcss-loader',
    options: {
      sourceMap: true,
      // eslint-disable-next-line
      plugins: () => [require('autoprefixer')({ grid: false })],
    },
  },
  {
    loader: 'sass-loader',
    options: {
      sourceMap: true,
      sassOptions: {
        includePaths: ['./node_modules'],
      },
    },
  },
];

const createCssLoaderConfig = () =>
  PROD
    ? ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: CSS_LOADER_CONFIG,
      })
    : [{ loader: 'style-loader' }].concat(CSS_LOADER_CONFIG);

const createCssExtractTextPlugin = () =>
  new ExtractTextPlugin(
    {
      filename: 'mt.[name].css',
      disable: false,
      allChunks: true,
      } // eslint-disable-line
  );

const plugins = PROD
  ? [
      createCssExtractTextPlugin(),
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify('production'),
        },
      }),
      new UglifyJSPlugin({
        sourceMap: true,
      }),
    ]
  : [
      createCssExtractTextPlugin(),
      new webpack.NamedModulesPlugin(),
      new FlowtypePlugin(),
    ];

module.exports = {
  mode: PROD ? 'production' : 'development',
  entry: {
    app: appEntry,
    topAppBar: appTopAppBar,
    drawerPermanent: appDrawerPermanent,
    drawerPersistent: appDrawerPersistent,
    drawerTemporary: appDrawerTemporary,
  },
  output: {
    path: path.join(path.resolve('./'), 'static'),
    publicPath: PROD ? '/material-toolbox/' : '/',
    filename: `mt.[name].js`,
  },
  devtool: PROD ? 'source-map' : 'inline-source-map',
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: createCssLoaderConfig(),
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: JS_LOADERS,
      },
      {
        test: /\.(gif|jpe?g|png)$/,
        loader: 'url-loader?limit=250',
        query: {
          limit: 100,
          name: '[name].[ext]',
        },
      },
    ],
  },
  optimization: {
    splitChunks: {
      name: 'common',
      chunks: 'all',
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all',
        },
      },
    },
  },
  plugins,
};
