module.exports = {
  // verbose: true,
  transformIgnorePatterns: ['node_modules/(?!(@material)/)'],
  collectCoverageFrom: ['src/**/*.js'],
};
